import { CustomLogger } from './CustomLogger';

export class CustomMisc {

    static getArrayFromStringSplit(str: string, splitDelimiter: string) {
        return str.split(splitDelimiter);
    }

    static getCurrentTimeInMilli() {
        return Date.now();
    }
    static getCurrentTimeInString() {
        let d = new Date(Date.now());
        let delim = "/";
        let delim2 = ":";
        return d.getDate() + delim + d.getMonth() + delim + d.getFullYear() + delim2 + d.getHours() + delim2 + d.getMinutes() + delim2 + d.getSeconds();
    }

    static getCurrentBrowser() {
        CustomLogger.logStringWithObject("navigator:: ", window.navigator);
        return window.navigator.appCodeName + " - " + window.navigator.appVersion;
    }

    static showAlert(message: string, isError: boolean = false) {
        if (isError)
            alert("!!!! " + message + " !!!!");
        else
            alert(message);
    }

    static showErrorObject(errorObj) {
        var str = "ERROR. Please Check Internal Logs.";
        try {
            str = errorObj.error.data.details;
        } catch (error) {
            CustomLogger.logError(error);
        }

        alert("!!!!! " + str + " !!!!!");
    }


    static searchDataArr(tableDataArr, term: string, fieldName) {
        let filteredTableDataArr = tableDataArr;
        if (!term) {
            filteredTableDataArr = tableDataArr;
        } else {
            filteredTableDataArr = tableDataArr.filter(x =>
                x[fieldName].trim().toLowerCase().includes(term.trim().toLowerCase())
            );
        }
        return filteredTableDataArr;
    }

    static convertRuleStringArrToMap(tmpStr) {
        let map = new Map();
        try {
            let tmpStrArr = tmpStr.split("#");
            for (let k = 0; k < tmpStrArr.length; k++) {
                let tmpEntityPermissionStrArr = tmpStrArr[k].split(":");
                let key = tmpEntityPermissionStrArr[0];
                let value: string[] = tmpEntityPermissionStrArr[1].split(",")
                CustomLogger.logString("key:" + key + " value:" + value);
                map.set(key, value);
            }
        } catch (error) {
            CustomLogger.logStringWithObject("Error in converting string arr to map", error);
        }
        return map;
    }

    static convertRuleMapToString(map) {
        let tmpStrArr = [];
        map.forEach((value, key) => {
            tmpStrArr.push(key + ":" + value);
        });
        return tmpStrArr.join("#");
    }

    static calculateDaysBetweenTimes(oldTime, newTime) {
        CustomLogger.logString("calculateDaysBetweenTimes:oldTime:" + oldTime + " newTime:" + newTime);
        let different_time = newTime - oldTime;
        return Math.round(different_time / (60 * 60 * 24 * 1000));
    }

    /**
     * 
     */
    static getNewUUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    static deleteItemFromArray(arr: string[], item: string) {
        // delete arr[item];
        const index = arr.indexOf(item, 0);
        if (index > -1) {
            arr.splice(index, 1);
        }
        return arr;
    }

    static removeDuplicatesFromArray(arr: string[]) {
        let newArr = [];
        if (arr != null && arr.length > 0) {
            for (let k = 0; k < arr.length; k++) {
                if (newArr.indexOf(arr[k]) < 0)
                    newArr.push(arr[k]);
            }
        }
        return newArr;
    }
}