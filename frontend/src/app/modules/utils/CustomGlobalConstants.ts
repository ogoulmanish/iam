/**
 * All constants used in the application will go in this class file
 */
import { environment } from "../../../environments/environment";

export class CustomGlobalConstants {

    static readonly DEFAULT_PARENT_USER_ID = {
        ADMIN: "1",
        NON_ADMIN: "-1"
    }

    static readonly DEFAULT_ROLES = {
        ADMIN_ROLE: "1",
        MANAGER_ROLE: "2",
        STANDARD_ROLE: "3",
        HR_ROLE: "4"
    }


    static readonly FORM_ELEMENT_TYPE = {
        COMOBO_BOX: "Combo Box",
        TEXT_BOX: "Text Box"
    }

    static readonly ADVANCED_USERS_SEARCH_BY = {
        ROLE_NAME: "Role Name",
        ACCESS_POLICY_NAME: "Access Policy",
        ENTITY_NAME: "Entity Name"
    }


    static readonly SPECIFIC_RULE_ATTRIBUTE_NAMES = {
        crudUser_department: "crudUser_department",
        crudUser_job_title: "crudUser_job_title",
        crudUser_user_type: "crudUser_user_type"
    }

    static readonly COMBO_BOX_NAMES = {
        crudUser_salutation: "crudUser_salutation",
        editProfile_salutation: "editProfile_salutation",
        crudEntity_risk_level: "crudEntity_risk_level",
        crudUser_department: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department,
        crudUser_job_title: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_job_title
    }


    ///////////////////USER SPECIFIC - START
    //user type
    static readonly USER_TYPES = {
        ADMIN: "admin",
        MANAGER: "manager",
        STANDARD: "standard",
        HR: "hr"
    }

    static readonly ARRAY_ADMIN_USER_TYPES = [
        {
            id: CustomGlobalConstants.USER_TYPES.ADMIN,
            name: "Admin"
        },
        {
            id: CustomGlobalConstants.USER_TYPES.HR,
            name: "HRMS"
        },
        {
            id: CustomGlobalConstants.USER_TYPES.MANAGER,
            name: "Manager"
        },
        {
            id: CustomGlobalConstants.USER_TYPES.STANDARD,
            name: "Standard"
        },
    ]
    static readonly ARRAY_MANAGER_USER_TYPES = [
        {
            id: CustomGlobalConstants.USER_TYPES.STANDARD,
            name: "Standard"
        },
    ]
    static readonly ARRAY_HR_USER_TYPES = [
        {
            id: CustomGlobalConstants.USER_TYPES.MANAGER,
            name: "Manager"
        },
        {
            id: CustomGlobalConstants.USER_TYPES.STANDARD,
            name: "Standard"
        },
    ]
    static readonly ARRAY_STANDARD_USER_TYPES = [
        {
            id: CustomGlobalConstants.USER_TYPES.STANDARD,
            name: "Standard"
        },
    ]

    static readonly SEARCH_USER_BOOLEAN_OPTION = {
        ANY: "Any",
        TRUE: "True",
        FALSE: "False"
    }
    static readonly ARRAY_SEARCH_USER_BOOLEAN_OPTION = [
        CustomGlobalConstants.SEARCH_USER_BOOLEAN_OPTION.ANY,
        CustomGlobalConstants.SEARCH_USER_BOOLEAN_OPTION.TRUE,
        CustomGlobalConstants.SEARCH_USER_BOOLEAN_OPTION.FALSE
    ]

    // static readonly ARRAY_USER_DEPARTMENTS = [
    //     {
    //         id: CustomGlobalConstants.USER_DEPARTMENTS.HR,
    //         value: "HR"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_DEPARTMENTS.ENGINEERING,
    //         value: "Engineering"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_DEPARTMENTS.MARKETING,
    //         value: "Marketing"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_DEPARTMENTS.SALES,
    //         value: "Sales"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_DEPARTMENTS.IT,
    //         value: "IT"
    //     }
    // ]

    // static readonly USER_JOB_TITLES = {
    //     CEO: "CEO",
    //     CTO: "CTO",
    //     TECHNICAL_HEAD: "Technical Head",
    //     PROGRAM_MANAGER: "Program Manager",
    //     FRESHER: "Fresher",
    //     INTERN: "Intern",
    //     MARKETING_MANAGER: "Marketing Manager",
    //     PROJECT_MANAGER: "Project Manager",
    //     WEB_DEVELOPER: "Web Developer",
    //     WEB_DESIGNER: "Web Designer"
    // }

    // static readonly ARRAY_USER_JOB_TITLES = [
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.CEO,
    //         value: "CEO"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.CTO,
    //         value: "CTO"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.TECHNICAL_HEAD,
    //         value: "Technical Head"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.PROGRAM_MANAGER,
    //         value: "Program Manager"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.FRESHER,
    //         value: "Fresher"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.INTERN,
    //         value: "Intern"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.MARKETING_MANAGER,
    //         value: "Marketing Manager"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.PROJECT_MANAGER,
    //         value: "Project Manager"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.WEB_DEVELOPER,
    //         value: "Web Developer"
    //     },
    //     {
    //         id: CustomGlobalConstants.USER_JOB_TITLES.WEB_DESIGNER,
    //         value: "Web Designer"
    //     }
    // ]


    static readonly USER_DEPARTMENTS = {
        ENGINEERING: "Engineering",
        HR: "HR",
        MARKETING: "Marketing",
        SALES: "Sales",
        IT: "IT"
    }

    static readonly ARRAY_ACTIVE_DIRECTORY_DEPARTMENT_DN = [
        {
            id: "ou=engineering,ou=developer,dc=ogoul1,dc=com",
            name: CustomGlobalConstants.USER_DEPARTMENTS.ENGINEERING
        },
        {
            id: "ou=hr,ou=developer,dc=ogoul1,dc=com",
            name: CustomGlobalConstants.USER_DEPARTMENTS.HR
        },
        {
            id: "ou=marketing,ou=developer,dc=ogoul1,dc=com",
            name: CustomGlobalConstants.USER_DEPARTMENTS.MARKETING
        },

    ]

    // static readonly RULES_USER_SPECIFIC = {
    //     USER_DEPARTMENTS: "1",
    //     USER_JOB_TITLES: "2"
    // }

    static readonly ARRAY_RULES_USER_SPECIFIC = [
        {
            id: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department,
            value: "Department",
            data: []
        },
        {
            id: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_job_title,
            value: "Job Title",
            data: []
        }
    ]

    static readonly ARITHMETIC_OPERATIONS = {
        EQUAL: "=",
        NOT_EQUAL: "!="
    }

    static readonly ARRAY_ARITHMETIC_OPERATIONS = [
        {
            id: "1",
            value: CustomGlobalConstants.ARITHMETIC_OPERATIONS.EQUAL
        },
        {
            id: "2",
            value: CustomGlobalConstants.ARITHMETIC_OPERATIONS.NOT_EQUAL
        },

    ]

    ///////////////////USER SPECIFIC - END


    static PROJECT_CONFIGURATION_CONSTANTS = {
        SESSION_TIMEOUT_IN_SECONDS: 300,
    }

    static readonly DEFAULT_SELECTED_STRING_VALUE = "-1";

    static readonly ACCESS_ENTITY_STATUS = {
        DECLINE: 0, //NONE PERMISSION
        APPROVE: 1, //READ PERMISSION
        PENDING: -1
    }

    static readonly MAKER_CHECKER_REQUEST_STATUS = {
        PENDING: 1,
        APPROVE: 2,
        DECLINE: 3,
        CANCEL: 4
    }

    static readonly FRONTEND_MODULE_NAME_ID = {
        USER: 1,
        ENTITY: 2,
        SECTOR: 3,
        ROLE: 4,
        PERMISSION: 5,
        RULE: 6,
        MAPPING_USER_ENTITY: 7,
        ACCESS_POLICY: 8
    }

    static readonly DEFAULT_PERMISSIONS = {
        NONE: 1
    }

    static readonly PERMISSION_TYPES = {
        NONE_PERMISSION: "1",
        CREATE_PERMISSION: "2",
        READ_PERMISSION: "3",
        UPDATE_PERMISSION: "4",
        DELETE_PERMISSION: "5"
    }

    static readonly ARRAY_PERMISSION_TYPES = [
        {
            permission_id: CustomGlobalConstants.PERMISSION_TYPES.NONE_PERMISSION,
            name: "None"
        },
        {
            permission_id: CustomGlobalConstants.PERMISSION_TYPES.CREATE_PERMISSION,
            name: "Create"
        },
        {
            permission_id: CustomGlobalConstants.PERMISSION_TYPES.READ_PERMISSION,
            name: "Read"
        },
        {
            permission_id: CustomGlobalConstants.PERMISSION_TYPES.UPDATE_PERMISSION,
            name: "Update"
        },
        {
            permission_id: CustomGlobalConstants.PERMISSION_TYPES.DELETE_PERMISSION,
            name: "Delete"
        }
    ]

    static readonly RULE_TYPES = {
        ROLE_SPECIFIC: 1,
        USER_SPECIFIC: 2
    }


    static readonly DATABASE_DEFAULTS = {
        QUERY_LIMIT: 0
    }

    static readonly PREFERRED_LANGUAGE = {
        ENGLISH: "en",
        ARABIC: "ar"
    }

    static readonly ARRAY_PREFERRED_LANGUAGE = [
        {
            id: CustomGlobalConstants.PREFERRED_LANGUAGE.ENGLISH,
            name: "English"
        }, {
            id: CustomGlobalConstants.PREFERRED_LANGUAGE.ARABIC,
            name: "عربى"
        }
    ]



    static readonly ACTIVE_DIRECTORY_SERVER = {
        LDAP_HOST_NAME: "localhost",
        LDAP_PORT: "10389",
        BASE_DOMAIN: "ou=developer,dc=ogoul1,dc=com",
        LDAP_LOGIN: "",
        LDAP_PASSWORD: "",
        USERNAME_START_STRING: "cn="
    }

    static readonly EVENT_TYPES = {
        CREATE: "Create",
        READ: "Read",
        UPDATE: "Update",
        DELETE: "Delete",
        ADDING_USER: "Adding User",
        ADDING_FIELD: "Adding Field"

    }

    static readonly ALERT_LEVEL = {
        FATAL: 0,
        CRITICAL: 1,
        HIGH: 2,
        MEDIUM: 3,
        LOW: 4
    }

    //local storage variable key names
    static readonly LOCAL_STORAGE_KEY_AUTH_TOKEN = "LOCAL_STORAGE_KEY_AUTH_TOKEN";

    //read from environment file
    static readonly BACKEND_SERVER_URL = environment.BACKEND_SERVER_URL;
    static readonly SERVER_URL = environment.SERVER_URL;

    //default file locations
    static readonly DEFAULTS_FILE_LOCATIONS = {
        // ENTITY_ICONS: "assets/images/sectors/"
        // ENTITY_ICONS: "c://fakepath//"
        ENTITY_ICONS: "../../backend/images/"
    }

    //reporting types
    static readonly REPORTING_TYPES = {
        DEFAULT: 1,
        EXISTING_USERS: 2,
        USERS_RECENTLY_CREATED: 3,
        LOGGING_REPORTS: 4,
        USER_ACTIVITY_REPORTS: 5,
        SYSTEM_EVENTS: 6,
        ENTITY_EVENTS: 7

    }

    //authentication applications
    static readonly EXTERNAL_AUTH_TYPE = {
        DEFAULT: 1,
        ACTIVE_DIRECTORY: 2,
        MSSQL: 3,
        ORACLE: 4
    }

    static readonly EXTERNAL_AUTH_TYPE_OBJECT_ARRAY = [
        {
            id: CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT,
            name: "Default"
        },
        {
            id: CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY,
            name: "Active Directory"
        },
        {
            id: CustomGlobalConstants.EXTERNAL_AUTH_TYPE.MSSQL,
            name: "MS SQL"
        },
        {
            id: CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ORACLE,
            name: "ORACLE"
        },
    ]

    //
    static readonly APP_MENU_PATH_NAMES = {
        DEFAULT_COMPONENT: "default",
        DASHBOARD: "dashboard",
        SECTORS: "sectors",
        USERS: "users",
        CONFIGURATION: "configuration",
        ACCESS_ENTITY: "accessEntity",
        REPORTING: "reporting",
        SETTINGS: "settings",
        STANDARD_DASHBOARD: "standardDashboard",
        HR_DASHBOARD: "hrDashboard",
        DB_CONNECTOR: "dbConnector"
    }

    //model names
    static readonly MODEL_NAME = {
        ACCESS_ENTITY: 1,
        ALERT_NOTIFICATION: 2,
        APPROVAL_MATRIX: 3,
        DASHBOARD: 4,
        DEFAULT_OPTIONS: 5,
        DEFAULT_SETTING: 6,
        EMAIL_ALERT: 7,
        ENTITY_EVENT: 8,
        FIELD_DETAIL: 9,
        FORM_ELEMENT: 10,
        MAKER_CHECKER_REQUEST: 11,
        MANAGER_USER_DETAIL: 12,
        PAGE_MATRIX: 13,
        SYSTEM_EVENT: 14,
        USER_DETAIL: 15,
        USER_SESSION_TRACKING: 16,
        LOGGING_TRACKING: 17,
        MAP_GROUP_ENTITY: 18,
        MAP_ROLE_ENTITY: 19,
        MAP_USER_ROLE: 20,
        BASE_SECTOR_DETAIL: 21,
        ENTITY_DETAIL: 22,
        PERMISSION_DETAIL: 23,
        ROLE_DETAIL: 24,
        RULE_DETAIL: 25,
        SECTOR_DETAIL: 26,
        BASE_MODEL: 27,
        COMMON_MODEL: 28,
        COMMON_EMAIL: 29,
        DB_CONNECTOR: 30,
        ACCESS_POLICY: 31,
        SPECIFIC_RULE: 32,
        ACTIVE_DIRECTORY_CONNECTOR: 33,
        RESTORATION_DETAIL: 34,
        LINUX_CONNECTOR: 35
    }

    //component names
    static readonly COMPONENT_NAME = {
        CRUD_USER: "crudUser",
        LIST_USER: "listUser",
        CRUD_SECTOR: "crudSector",
        CRUD_ENTITY: "crudEntity",
        CRUD_PERMISSION: "crudPermission",
        CRUD_ROLE: "crudRole",
        CRUD_REPORTING: "crudReporting",
        CRUD_RULE: "crudRule",
        EDIT_USER_PROFILE: "editUserProfile",
        DASHBOARD: "dashboard",
        LIST_ACCESS_ENTITY: "listAccessEntity",
        LIST_ENTITY: "listEntity",
        LIST_SECTOR: "listSector",
        LIST_ROLE: "listRole",
        LIST_PERMISSION: "listPermission",
        LIST_RULE: "listRule",
        EDIT_FIELDS: "editFields",
        EMAIL_ALERT: "emailAlert",
        ALERT_NOTIFICATION: "alertNotification",
        LIST_PAGE_MATRIX: "listPageMatrix",
        CRUD_PAGE_MATRIX: "crudPageMatrix",
        LIST_FORM_ELEMENT: "listFormElement",
        CRUD_FORM_ELEMENT: "crudFormElement",
        LIST_MANAGER_USER: "listManagerUser",
        CRUD_MANAGER_USER: "crudManagerUser",
        LIST_MAKER_CHECKER: "listMakerChecker",
        LIST_CONNECTOR: "listConnector",
        CRUD_CONNECTOR: "crudConnector",
        LIST_ACCESS_POLICY: "listAccessPolicy",
        CRUD_ACCESS_POLICY: "crudAccessPolicy",
        COMMON: "common"
    }

    static readonly ALL = {
        ARRAY_COMPONENT_NAMES: [
            CustomGlobalConstants.COMPONENT_NAME.CRUD_USER,
            CustomGlobalConstants.COMPONENT_NAME.LIST_USER,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_PERMISSION,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_REPORTING,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_RULE,
            CustomGlobalConstants.COMPONENT_NAME.EDIT_USER_PROFILE,
            CustomGlobalConstants.COMPONENT_NAME.DASHBOARD,
            CustomGlobalConstants.COMPONENT_NAME.LIST_ACCESS_ENTITY,
            CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY,
            CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR,
            CustomGlobalConstants.COMPONENT_NAME.LIST_ROLE,
            CustomGlobalConstants.COMPONENT_NAME.LIST_PERMISSION,
            CustomGlobalConstants.COMPONENT_NAME.LIST_RULE,
            CustomGlobalConstants.COMPONENT_NAME.EDIT_FIELDS,
            CustomGlobalConstants.COMPONENT_NAME.EMAIL_ALERT,
            CustomGlobalConstants.COMPONENT_NAME.ALERT_NOTIFICATION,
            CustomGlobalConstants.COMPONENT_NAME.LIST_PAGE_MATRIX,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_PAGE_MATRIX,
            CustomGlobalConstants.COMPONENT_NAME.LIST_FORM_ELEMENT,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_FORM_ELEMENT,
            CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER,
            CustomGlobalConstants.COMPONENT_NAME.CRUD_MANAGER_USER,
            CustomGlobalConstants.COMPONENT_NAME.LIST_MAKER_CHECKER,
            CustomGlobalConstants.COMPONENT_NAME.COMMON
        ]
    }


    //SECTOR CONSTANTS
    static readonly DEFAULT_SECTOR_APPLICATIONS_ID = "1";
    static readonly DEFAULT_SECTOR_NETWORKS_ID = "2";
    static readonly DEFAULT_SECTOR_SYSTEMS_ID = "3";
    static readonly DEFAULT_SECTOR_FILES_ID = "4";
    static readonly SECTORS_ARRAY = ["Applications", "Networks", "Systems", "Files"];
    //params constants
    static readonly SECRET_KEY = "PAM_SECRET";

    //Roles - Defaults
    static readonly ROLE_ADMIN = "Admin";
    static readonly ROLE_DATABASE_ENGINEER = "Database Engineer";
    static readonly ROLE_NETWORK_ENGINEER = "Network Engineer"
    static readonly ROLES_ARRAY = [CustomGlobalConstants.ROLE_ADMIN, CustomGlobalConstants.ROLE_DATABASE_ENGINEER, CustomGlobalConstants.ROLE_NETWORK_ENGINEER];


    //Entities - Defaults
    static readonly ENTITY_DB_MANAGER = "DB Manager";
    static readonly ENTITY_NETWORK_ENGINEER = "Network Engineer";
    static readonly DEFAULT_ENTITY_ARRAY = [CustomGlobalConstants.ENTITY_DB_MANAGER, CustomGlobalConstants.ENTITY_NETWORK_ENGINEER];

    //CRUD OPERATION CONSTANTS
    static readonly CRUD_CREATE = "c";
    static readonly CRUD_READ = "r";
    static readonly CRUD_UPDATE = "u";
    static readonly CRUD_DELETE = "d";

    static readonly PARAM_CRUD_TYPE = "crudType";

    //alertNotification constants
    static readonly ALERT_NOFICATION_CONSTANTS = {
        COMMON: {
            session_logout: "session_logout",
        },
        LIST_USER: {
            listUser_user_deleted_success: "listUser_user_deleted_success",
            listUser_user_cannot_be_deleted: "listUser_user_cannot_be_deleted",
        },
        CRUD_USER: {
            crudUser_user_add_success: "crudUser_user_add_success",
            crudUser_user_update_success: "crudUser_user_update_success",
            crudUser_user_add_error: "crudUser_user_add_error",
            crudUser_user_update_error: "crudUser_user_update_error",
            crudUser_user_alrady_exists: "crudUser_user_alrady_exists",
            crudUser_user_does_not_exist: "crudUser_user_does_not_exist"
        },
        LIST_ENTITY: {
            listEntity_Entity_deleted_success: "listEntity_Entity_deleted_success",
            listEntity_Entity_cannot_be_deleted: "listEntity_Entity_cannot_be_deleted",
        },
        CRUD_ENTITY: {
            crudEntity_entity_add_success: "crudEntity_entity_add_success",
            crudEntity_entity_update_success: "crudEntity_entity_update_success",
            crudEntity_entity_add_error: "crudEntity_entity_add_error",
            crudEntity_entity_update_error: "crudEntity_entity_update_error",

        },
        LIST_SECTOR: {
            listSector_Sector_deleted_success: "listSector_Sector_deleted_success",
            listSector_Sector_cannot_be_deleted: "listSector_Sector_cannot_be_deleted",
        },
        CRUD_SECTOR: {
            crudSector_sector_add_success: "crudSector_sector_add_success",
            crudSector_sector_update_success: "crudSector_sector_update_success",
            crudSector_sector_add_error: "crudSector_sector_add_error",
            crudSector_sector_update_error: "crudSector_sector_update_error",
        },
        LIST_ROLE: {
            listRole_role_deleted_success: "listRole_role_deleted_success",
            listRole_role_cannot_be_deleted: "listRole_role_cannot_be_deleted",
        },

        CRUD_ROLE: {
            crudRole_role_add_success: "crudRole_role_add_success",
            crudRole_role_update_success: "crudRole_role_update_success",
            crudRole_role_add_error: "crudRole_role_add_error",
            crudRole_role_update_error: "crudRole_role_update_error",
        },
        LIST_PERMISSION: {
            listPermission_permission_deleted_success: "listPermission_permission_deleted_success",
            listPermission_permission_cannot_be_deleted: "listPermission_permission_cannot_be_deleted",
        },
        CRUD_PERMISSION: {
            crudPermission_permission_add_success: "crudPermission_permission_add_success",
            crudPermission_permission_update_success: "crudPermission_permission_update_success",
            crudPermission_permission_add_error: "crudPermission_permission_add_error",
            crudPermission_permission_update_error: "crudPermission_permission_update_error",
        },
        LIST_RULE: {
            listRule_rule_deleted_success: "listRule_rule_deleted_success",
            listRule_rule_cannot_be_deleted: "listRule_rule_cannot_be_deleted",
        },
        CRUD_RULE: {
            crudRule_rule_add_success: "crudRule_rule_add_success",
            crudRule_rule_update_success: "crudRule_rule_update_success",
            crudRule_rule_add_error: "crudRule_rule_add_error",
            crudRule_rule_update_error: "crudRule_rule_update_error",
        },
        EDIT_FIELDS: {
            editFields_update_success: "editFields_update_success",
            editFields_update_error: "editFields_update_error"

        },
        EMAIL_ALERT: {
            emailAlert_update_success: "emailAlert_update_success",
            emailAlert_update_error: "emailAlert_update_error"

        },
        EDIT_USER_PROFILE: {
            editUserProfile_update_success: "editUserProfile_update_success",
            editUserProfile_update_error: "editUserProfile_update_error"

        },
        ALERT_NOTIFICATION: {
            alertNotification_no_records_found: "alertNotification_no_records_found",
            alertNotification_update_success: "alertNotification_update_success",
            alertNotification_update_error: "alertNotification_update_error"
        },
        CRUD_PAGE_MATRIX: {
            crudPageMatrix_add_success: "crudPageMatrix_add_success",
            crudPageMatrix_update_success: "crudPageMatrix_update_success",
            crudPageMatrix_add_error: "crudPageMatrix_add_error",
            crudPageMatrix_update_error: "crudPageMatrix_update_error"
        },
        LIST_PAGE_MATRIX: {
            listPageMatrix_pageMatrix_deleted_success: "listPageMatrix_pageMatrix_deleted_success",
            listPageMatrix_pageMatrix_cannot_be_deleted: "listPageMatrix_pageMatrix_cannot_be_deleted",
            listPageMatrix_pageMatrix_sure_to_delete: "listPageMatrix_pageMatrix_sure_to_delete",
        }
    }

    static readonly NOTIFICATION_TYPE = {
        EMAIL: 1,
        SMA: 2,
        WHATSAPP: 3
    }


    //page matrix group
    static readonly DEFAULT_PAGE_MATRIX_ID = {
        EVERYTHING: "1",
        SPECIFIC_MANAGER: "2",
        STANDARD: "3",
        HR: "4"
    }

    static readonly ARRAY_DEFAULT_PAGE_MATRIX_ID = [
        {
            id: CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.EVERYTHING,
            name: "Everything"
        },
        {
            id: CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.SPECIFIC_MANAGER,
            name: "Manager Specific"
        },
        {
            id: CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.STANDARD,
            name: "Standard"
        },
        {
            id: CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.HR,
            name: "HR Specific"
        },
        
    ]

    static COMMON_TABLE_OPTIONS = "table table-sm";
    // static COMMON_TABLE_OPTIONS = "table table-striped";
    /////////////////////////connector specific - will be read from database later
    static readonly CONNECTOR_CATEGORIES = {
        DATABASE_CONNECTOR: "1",
        DIRECTORY_CONNECTOR: "2",
        OS_CONNECTOR: "3"
    }

    static readonly CONNECTOR_TYPE = {
        MONGODB: "1",
        ACTIVE_DIRECTORY: "2",
        LINUX: "3",
        MSSQL: "4"
    }

    static readonly ARRAY_ACTUAL_CONNECTORS = [
        {
            actual_connector_id: "A1",
            connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.DATABASE_CONNECTOR,
            connector_type: CustomGlobalConstants.CONNECTOR_TYPE.MONGODB,
            name: "MongoDB",
            details: "Mongodb Database Connector"
        },
        {
            actual_connector_id: "B1",
            connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.DIRECTORY_CONNECTOR,
            connector_type: CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY,
            name: "Active Directory",
            details: "LDAP - Active Directory Connector"
        },
        {
            actual_connector_id: "C1",
            connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.OS_CONNECTOR,
            connector_type: CustomGlobalConstants.CONNECTOR_TYPE.LINUX,
            name: "Linux",
            details: "Linux Connector"
        },
    ]


    // static readonly ACTUAL_CONNECTORS_IDS = {
    //     MONGODB: "1",
    //     ACTIVE_DIRECTORY: "2",
    //     MSSQL: "3"
    // }
    // static readonly OLD_CONNECTOR_CATEGORIES = {
    //     DATABASE_CONNECTOR: {
    //         MONGODB: {
    //             id: "1",
    //             name: "MongoDB",
    //             details: "Mongodb Database Connector"
    //         },
    //         MYSQL: {
    //             id: "2",
    //             name: "MSSQL",
    //             details: "MSSQL Database Connector"
    //         },
    //     }
    // }


    // static readonly ARRAY_CONNECTOR_CATEGORIES = [
    //     {
    //         DATABASE_CONNECTOR: [
    //             CustomGlobalConstants.OLD_CONNECTOR_CATEGORIES.DATABASE_CONNECTOR.MONGODB,
    //             CustomGlobalConstants.OLD_CONNECTOR_CATEGORIES.DATABASE_CONNECTOR.MYSQL
    //         ]
    //     }
    // ];

    // static readonly newCategories = [
    //     {
    //         category_id: "1",
    //         name: "Database Connector",
    //         connector_types: [
    //             {
    //                 connector_type_id: "1",
    //                 name: "MongoDB",
    //                 details: "Mongodb Database Connector"
    //             },
    //             {
    //                 connector_type_id: "2",
    //                 name: "MSSQL",
    //                 details: "MSSQL Database Connector"
    //             },
    //         ]
    //     },
    //     {
    //         category_id: "2",
    //         name: "Directory Connector",
    //         connector_types: [
    //             {
    //                 connector_type_id: "1",
    //                 name: "Active Directory",
    //                 details: "Active Directory Connector"
    //             }
    //         ]
    //     }
    // ]




    constructor() {
    }
}