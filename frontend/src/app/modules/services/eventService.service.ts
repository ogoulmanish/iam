import { Injectable } from "@angular/core";
import { BaseServiceConfig } from "./baseServiceConfig";
import { LoggingTracking } from '../models/events/LoggingTracking.model';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { CustomLogger } from '../utils/CustomLogger';
import { EmailAlert } from '../models/common/EmailAlert.model';
import { EntityEvent } from '../models/common/EntityEvent.model';
import { CustomGlobalConstants } from '../utils/CustomGlobalConstants';
import { AccessEntity } from '../models/common/AccessEntity.model';



@Injectable()
export class EventService extends BaseServiceConfig {

    constructor(private _http: HttpClient, private _globalService: GlobalService) {
        super();
    }

    addLoggingTracking(loggingTracking: LoggingTracking, improperUsername = "1") {
        if (this._globalService.getCurrentUserDetail())
            loggingTracking.source_username = this._globalService.getCurrentUserDetail().source_username;
        else
            loggingTracking.source_username = "";
        if (improperUsername != "1") loggingTracking.source_username = improperUsername;
        return this._http.post(this.EVENT_HOST_URL + "/addLoggingTracking", loggingTracking);
    }


    updateLoggingTracking(loggingTracking: LoggingTracking) {
        loggingTracking.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this._http.post(this.EVENT_HOST_URL + "/updateLoggingTracking", loggingTracking);
    }

    getLastLoginTrackingOfUsername(usernameToFind) {
        let params = this.getDefaultParams();
        params["usernameToFind"] = usernameToFind;
        return this._http.get(this.EVENT_HOST_URL + "/getLastLoginTrackingOfUsername", { params });
    }

    getAllSystemEvents(limit = -1) {
        let params = this.getDefaultParams();
        params["limit"] = limit;
        return this._http.get(this.EVENT_HOST_URL + "/getAllSystemEvents", { params });
    }


    getEmailAlert() {
        let params = this.getDefaultParams();
        return this._http.get(this.EVENT_HOST_URL + "/getEmailAlert", { params });
    }

    updateEmailAlert(emailAlert: EmailAlert) {
        emailAlert.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this._http.post(this.EVENT_HOST_URL + "/updateEmailAlert", emailAlert);
    }

    addEntityEvent(entityEvent: EntityEvent) {
        return this._http.post(this.EVENT_HOST_URL + '/addEntityEvent', entityEvent);
    }

    getSpecificEntityEvents(accessed_username: string, entity_id: string, risk_level_id: string) {
        let params = this.getDefaultParams();
        params["limit"] = CustomGlobalConstants.DATABASE_DEFAULTS.QUERY_LIMIT;
        params["accessed_username"] = accessed_username;
        params["entity_id"] = entity_id;
        params["risk_level_id"] = risk_level_id;
        return this._http.get(this.EVENT_HOST_URL + "/getSpecificEntityEvents", { params });
    }

    //accessEntity
    addAccessEntity(accessEntity: AccessEntity) {
        accessEntity.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this._http.post(this.EVENT_HOST_URL + '/addAccessEntity', accessEntity);
    }

    updateAccessEntity(accessEntity: AccessEntity) {
        accessEntity.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this._http.post(this.EVENT_HOST_URL + '/updateAccessEntity', accessEntity);
    }

    getAllAccessEntity(limit = -1) {
        let params = this.getDefaultParams();
        params["limit"] = limit;
        return this._http.get(this.EVENT_HOST_URL + "/getAllAccessEntity", { params });
    }

    getSpecificAccessEntityByCurrentStatus(current_status) {
        let params = this.getDefaultParams();
        params["current_status"] = current_status;
        return this._http.get(this.EVENT_HOST_URL + "/getSpecificAccessEntityByCurrentStatus", { params });
    }


    ////////////// util methods
    getSourceIP() {

        let url = "https://jsonip.com";//this needs to be changed to a better site or way to find IP Address
        try {
            let ip = this._http.get(url);
            return ip;
        } catch (error) {
            CustomLogger.logStringWithObject("Cannot find the source IP...", error);
        }
        return null;
    }

    getDefaultParams() {
        let params =
            { "source_username": this._globalService.getCurrentUserDetail().source_username }
        return params;
    }

}