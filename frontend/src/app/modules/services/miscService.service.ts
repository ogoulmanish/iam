import { Injectable } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ConfirmationDialogComponent } from "../../views/misc/confirmDialog.component";
import { ApprovalMatrix } from '../models/common/ApprovalMatrix.model';
import { MakerCheckerRequest } from '../models/common/MakerCheckerRequest.model';
import { PageMatrix } from '../models/common/PageMatrix.model';
import { CommonModel } from '../models/CommonModel';
import { CustomGlobalConstants } from '../utils/CustomGlobalConstants';
import { CustomLogger } from '../utils/CustomLogger';
import { CustomMisc } from '../utils/CustomMisc';
import { BaseServiceConfig } from "./baseServiceConfig";
import { DBService } from './dbService.service';
import { GlobalService } from './global.service';
import { EmailDetail } from '../models/common/CommonEmail.model';
import { HttpClient } from '@angular/common/http';
import { UserDetail } from '../models/common/UserDetail.model';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class MiscService extends BaseServiceConfig {
    constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _http: HttpClient, private _globalService: GlobalService, private _dbService: DBService, private _modalService: NgbModal) {
        super();
    }

    public confirmDialogBox(
        title: string,
        message: string,
        btnOkText: string = 'OK',
        btnCancelText: string = 'Cancel',
        dialogSize: 'sm' | 'lg' = 'sm'): Promise<boolean> {
        const modalRef = this._modalService.open(ConfirmationDialogComponent, { size: dialogSize });
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.message = message;
        modalRef.componentInstance.btnOkText = btnOkText;
        modalRef.componentInstance.btnCancelText = btnCancelText;
        return modalRef.result;
    }


    isUserAdmin(roleIdArray): boolean {
        return roleIdArray.indexOf(CustomGlobalConstants.ROLE_ADMIN) >= 0 ? true : false;
    }

    getDisplayRoleName(roleIdArray: string[]): string {
        let displayRoleName = "";
        if (roleIdArray.indexOf(CustomGlobalConstants.ROLE_ADMIN)) displayRoleName = "Admin";
        else if (roleIdArray.indexOf(CustomGlobalConstants.ROLE_NETWORK_ENGINEER)) displayRoleName = "Network";
        else if (roleIdArray.indexOf(CustomGlobalConstants.ROLE_DATABASE_ENGINEER)) displayRoleName = "DB";
        return displayRoleName;
    }

    isPageVisible(menuPathName) {
        let currentPageMatrix: PageMatrix = this._globalService.getPageMatrix();
        CustomLogger.logStringWithObject("AuthGuard:menuPathName:", menuPathName);
        CustomLogger.logStringWithObject("AuthGuard:Current Page Matrix:", currentPageMatrix);
        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.DEFAULT_COMPONENT)
            return true;
        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.DASHBOARD) {
            if (currentPageMatrix.show_field_menu_heading_navigation) return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.DB_CONNECTOR) {
            if (currentPageMatrix.show_field_menu_link_listPredefinedConnectors)
                return true;
            return false;
        }
        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.USERS) {
            if (currentPageMatrix.show_field_menu_link_listUser ||
                // currentPageMatrix.show_field_menu_link_crudUser ||
                currentPageMatrix.show_field_menu_link_listAccessEntity ||
                currentPageMatrix.show_field_menu_link_listManagerUser ||
                currentPageMatrix.show_field_menu_link_listMakerCheckerRequest)
                return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.SECTORS) {
            if (currentPageMatrix.show_field_menu_link_listEntity ||
                currentPageMatrix.show_field_menu_link_listSector ||
                currentPageMatrix.show_field_menu_link_listSectorElements)
                return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.CONFIGURATION) {
            if (currentPageMatrix.show_field_menu_link_listRole ||
                currentPageMatrix.show_field_menu_link_listPermission ||
                currentPageMatrix.show_field_menu_link_listRule ||
                currentPageMatrix.show_field_menu_link_listAccessPolicy)
                return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.SETTINGS) {
            if (currentPageMatrix.show_field_menu_link_editFields ||
                currentPageMatrix.show_field_menu_link_emailAlerts ||
                currentPageMatrix.show_field_menu_link_editUserProfile ||
                currentPageMatrix.show_field_menu_link_listAlertNotification ||
                currentPageMatrix.show_field_menu_link_defaultSetting ||
                currentPageMatrix.show_field_menu_link_listPageMatrix ||
                currentPageMatrix.show_field_menu_link_listFormElement ||
                currentPageMatrix.show_field_standard_menu_link_editUserProfile ||
                currentPageMatrix.show_field_menu_link_listApprovalMatrix
            )
                return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.REPORTING) {
            if (currentPageMatrix.show_field_menu_link_report_default ||
                currentPageMatrix.show_field_menu_link_report_entityEvents ||
                currentPageMatrix.show_field_menu_link_report_systemEvents ||
                currentPageMatrix.show_field_menu_link_report_existingUsers ||
                currentPageMatrix.show_field_menu_link_report_userLogging ||
                currentPageMatrix.show_field_menu_link_report_userActivityReports
            )
                return true;
            return false;
        }
        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.STANDARD_DASHBOARD) {
            if (currentPageMatrix.show_field_standard_menu_heading_navigation) return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.HR_DASHBOARD) {
            if (currentPageMatrix.show_field_hr_menu_heading_navigation) return true;
            return false;
        }

        if (menuPathName == CustomGlobalConstants.APP_MENU_PATH_NAMES.ACCESS_ENTITY) {
            if (currentPageMatrix.show_field_standard_menu_link_showEntities) return true;
            return false;
        }
        return false;
    }

    ///////////////////////////////

    getApprovalMatrixOfComponentFromArray(approvalMatrixArray: ApprovalMatrix[], componentName) {
        for (let k = 0; k < approvalMatrixArray.length; k++) {
            let approvalMatrix = approvalMatrixArray[k];
            if (approvalMatrix.component_name == componentName) {
                return approvalMatrix
            }
        }
        return null;
    }


    async sendMakerCheckerRequest(obj, maker_component_name, isComponentNew) {
        let result = null;
        let makerCheckerRequest = new MakerCheckerRequest();
        makerCheckerRequest.maker_component_name = maker_component_name;
        makerCheckerRequest.current_status = CustomGlobalConstants.MAKER_CHECKER_REQUEST_STATUS.PENDING;
        makerCheckerRequest.maker_user_id = this._globalService.getCurrentUserDetail().user_id;
        makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
        if (isComponentNew) {
            makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_CREATE;
        } else {
            makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_UPDATE;
        }
        makerCheckerRequest.maker_object = obj;
        result = await this._dbService.addMakerCheckerRequest(makerCheckerRequest).toPromise();
        CustomLogger.logStringWithObject("addMakerCheckerRequest:result:", result);
        CustomMisc.showAlert("Request for adding/updating sent and It will be done only if approved.");
    }

    async commonApprovalForAddUpdate(obj: CommonModel, componentName, isComponentNew) {
        CustomLogger.logString("Enter commonApprovalForAddUpdate for component:" + componentName + " and is new ?" + isComponentNew);
        let approvalMatrix: ApprovalMatrix = this.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), componentName);
        CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
        if (approvalMatrix != null && approvalMatrix.requires_approval) {
            CustomLogger.logString("APPROVAL REQUIRED");
            await this.sendMakerCheckerRequest(obj, componentName, isComponentNew);
        } else {
            CustomLogger.logString("APPROVAL NOT REQUIRED");
            let result = null;
            if (isComponentNew) {
                result = await this._dbService.commonCreate(obj).toPromise();
                CustomMisc.showAlert("Success in Adding");
            }
            else {
                result = await this._dbService.commonUpdate(obj).toPromise();
                if (componentName == CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY) {
                    CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ENTITY.crudEntity_entity_update_success));
                } else {
                    CustomMisc.showAlert("Success in Updating");
                }
            }
            CustomLogger.logStringWithObject("commonApprovalForAddUpdate:result:", result);
        }
    }


    async commonApprovalForDelete(obj: CommonModel, componentName, heading) {
        let optionChosen = await this.confirmDialogBox("Delete", "Are you sure you want to delete this " + heading + " ?");
        CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
        if (optionChosen) {
            let approvalMatrix: ApprovalMatrix = this.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), componentName);
            CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
            let result = null;
            if (approvalMatrix != null && approvalMatrix.delete_permission) {
                CustomLogger.logString("APPROVAL REQUIRED");
                let result = null;
                let makerCheckerRequest = new MakerCheckerRequest();
                makerCheckerRequest.maker_component_name = componentName;
                makerCheckerRequest.current_status = CustomGlobalConstants.MAKER_CHECKER_REQUEST_STATUS.PENDING;
                makerCheckerRequest.maker_user_id = this._globalService.getCurrentUserDetail().user_id;
                makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
                makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_DELETE;
                makerCheckerRequest.maker_object = obj;
                result = await this._dbService.addMakerCheckerRequest(makerCheckerRequest).toPromise();
                CustomLogger.logStringWithObject("addMakerCheckerRequest:result:", result);
                CustomMisc.showAlert("Request for Deletion sent and It will be done only if approved.");
            } else {
                CustomLogger.logString("APPROVAL NOT REQUIRED");

                //check for specific component delete
                if (componentName == CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY) {
                    result = await this._dbService.deleteEntityDetail(obj).toPromise();
                } else {
                    //all others
                    result = await this._dbService.commonDelete(obj).toPromise();
                }
                CustomMisc.showAlert("Successfully Deleted", true);
            }
            CustomLogger.logStringWithObject("delete:result:", result);
        }

    }


    ////////////////////////////// EMAILS and OTHER NOTIFICATIONS
    //send email
    sendEmail(commonEmail: EmailDetail) {
        commonEmail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this._http.post(this.COMMON_HOST_URL + '/sendEmail', commonEmail);
    }

    sendSMS() {

    }

    sendWhatsApp() {

    }

    async sendNotificationsForNewUserCreation(newUserDetail: UserDetail, nonHashedPassword: string, notificationType: number = CustomGlobalConstants.NOTIFICATION_TYPE.EMAIL) {
        // 2 -
        let notificationSent = true;
        try {
            switch (notificationType) {
                case CustomGlobalConstants.NOTIFICATION_TYPE.EMAIL:
                    let commonEmail = new EmailDetail();
                    commonEmail.subject = "New user successfully created.";
                    commonEmail.text = "A new user was successfully created. <br> Username: '" + newUserDetail.username + "' and password: '" + nonHashedPassword + "'";
                    commonEmail.to_email = this._globalService.getCurrentUserDetail().email;
                    commonEmail.source_username = this._globalService.getCurrentUserDetail().source_username;
                    CustomLogger.logString("About to send email for new User Creation.");
                    await this.sendEmail(commonEmail).toPromise();
                    break;

                default:
                    CustomLogger.logString("No valid Notification type received. So not sending any notifications.");
                    notificationSent = false;
                    break;
            }
        } catch (error) {
            notificationSent = false;
        }
        return notificationSent;

    }

    goToURL(urlToGo, activatedRoute) {
        // this._router.navigate([backUrl]);
        this._router.navigate([urlToGo], { relativeTo: activatedRoute.parent, skipLocationChange: true });
    }


}