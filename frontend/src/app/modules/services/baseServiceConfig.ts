import { CustomGlobalConstants } from "../utils/CustomGlobalConstants";

export class BaseServiceConfig {
    HOST_URL = CustomGlobalConstants.SERVER_URL;
    CONFIG_HOST_URL = this.HOST_URL + "/config";
    USERS_HOST_URL = this.HOST_URL + "/user";
    OTP_LOGIN_HOST_URL = this.HOST_URL + "/otpLogin";
    COMMON_HOST_URL = this.HOST_URL + "/common";
    EVENT_HOST_URL = this.HOST_URL + "/event";
    CONNECTOR_HOST_URL = this.HOST_URL + "/connector";
    LDAP_HOST_URL = this.HOST_URL + "/ldap";
    MSSQL_HOST_URL = this.HOST_URL + "/mssql";
    
    OLD_LOGIN_HOST_URL = this.HOST_URL + "/login";
    OLD_LOGIN_TFA_HOST_URL = this.HOST_URL + "/tfa";
    OLD_LOGIN_REGISTER_HOST_URL = this.HOST_URL + "/register";

    PASSWORD_HOST_URL = this.HOST_URL;
    
}