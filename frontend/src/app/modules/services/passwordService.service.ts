import { Injectable } from "@angular/core";
import { PasswordInfo } from "../models/passwordModule/PasswordInfo.model";
import { HttpClient } from "@angular/common/http";
import { BaseServiceConfig } from "./baseServiceConfig";
import { PasswordScheduler } from '../models/passwordModule/PasswordScheduler.model';

@Injectable()
export class PasswordService extends BaseServiceConfig {

    constructor(private http: HttpClient) {
        super();
    }

    getAllPasswordInfo() {
        return this.http.get(this.PASSWORD_HOST_URL + "/getAllPasswordInfo");
    }

    getCurrentPasswordScheduler() {
        return this.http.get(this.PASSWORD_HOST_URL + "/getCurrentPasswordScheduler");
    } 
    updatePasswordScheduler(passwordScheduler: PasswordScheduler) {
        return this.http.post(this.PASSWORD_HOST_URL + "/updatePasswordScheduler", passwordScheduler);
    }
}
