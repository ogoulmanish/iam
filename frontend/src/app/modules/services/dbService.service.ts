import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AlertNotification } from '../models/common/AlertNotification.model';
import { ApprovalMatrix } from '../models/common/ApprovalMatrix.model';
import { DefaultSetting } from '../models/common/DefaultSetting.model';
import { FieldDetail } from '../models/common/FieldDetail.model';
import { FormElement } from '../models/common/FormElement.model';
import { MakerCheckerRequest } from '../models/common/MakerCheckerRequest.model';
import { ManagerUserDetail } from '../models/common/ManagerUserDetail.model';
import { PageMatrix } from '../models/common/PageMatrix.model';
import { UserDetail } from '../models/common/UserDetail.model';
import { CommonModel } from '../models/CommonModel';
import { MappingUserEntity } from '../models/mapping/MappingUserEntity.model';
import { EntityDetail } from '../models/sectors/EntityDetail.model';
import { PermissionDetail } from '../models/sectors/PermissionDetail.model';
import { RoleDetail } from '../models/sectors/RoleDetail.model';
import { RuleDetail } from '../models/sectors/RuleDetail.model';
import { SectorDetail } from '../models/sectors/SectorDetail.model';
import { CustomGlobalConstants } from '../utils/CustomGlobalConstants';
import { CustomLogger } from "../utils/CustomLogger";
import { BaseServiceConfig } from "./baseServiceConfig";
import { GlobalService } from "./global.service";
import { SearchUserDetail } from '../models/common/SearchUserDetail.model';

@Injectable()
export class DBService extends BaseServiceConfig {

    constructor(private http: HttpClient, private _globalService: GlobalService) {
        super();
    }

    //common - will be done for most of other models
    commonCreate(obj: CommonModel) {
        this.setSourceUsername(obj);
        return this.http.post(this.CONFIG_HOST_URL + '/commonCreate', obj);
    }
    commonUpdate(obj: CommonModel) {
        this.setSourceUsername(obj);
        return this.http.post(this.CONFIG_HOST_URL + '/commonUpdate', obj);
    }
    commonDelete(obj: CommonModel) {
        this.setSourceUsername(obj);
        return this.http.post(this.CONFIG_HOST_URL + '/commonDelete', obj);
    }
    commonGetAll(model_name) {
        let params = this.getDefaultParams();
        params["model_name"] = model_name;
        return this.http.get(this.CONFIG_HOST_URL + '/commonGetAll', { params });
    }
    commonGetSpecific(model_name, id) {
        let params = this.getDefaultParams();
        params["model_name"] = model_name;
        params["id"] = id;
        return this.http.get(this.CONFIG_HOST_URL + '/commonGetSpecific', { params });
    }

    //sectors
    addSectorDetail(sectorDetail: SectorDetail) {
        this.setSourceUsername(sectorDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/addSectorDetail', sectorDetail);
    }
    updateSectorDetail(sectorDetail: SectorDetail) {
        this.setSourceUsername(sectorDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/updateSectorDetail', sectorDetail);
    }
    deleteSectorDetail(sectorDetail: SectorDetail) {
        this.setSourceUsername(sectorDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/deleteSectorDetail', sectorDetail);
    }


    getAllSectorDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllSectorDetail', { params });
    }

    getSpecificSectorDetail(sector_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "sector_id": sector_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificSectorDetail', { params });
    }

    getAllUserDetailsOfSpecificRole(role_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "role_id": role_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAllUserDetailsOfSpecificRole', { params });
    }


    getAllUserDetailsOfSpecificEntities(entity_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "entity_id": entity_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAllUserDetailsOfSpecificEntities', { params });
    }

    getAllUserDetailsOfSpecificAccessPolicies(access_policy_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "access_policy_id": access_policy_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAllUserDetailsOfSpecificAccessPolicies', { params });
    }


    //users
    getAllUserDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.USERS_HOST_URL + "/getAllUserDetail", { params });
    }

    addUserDetail(userDetail: UserDetail) {
        let url = this.USERS_HOST_URL + '/addUserDetail/';
        userDetail.source_username = this._globalService.getCurrentUserDetail().username;
        return this.http.post(url, userDetail);
    }

    updateUserDetail(userDetail: UserDetail) {
        let url = this.USERS_HOST_URL + '/updateUserDetail/';
        userDetail.source_username = this._globalService.getCurrentUserDetail().username;
        return this.http.post(url, userDetail);
    }

    deleteUserDetail(userDetail: UserDetail) {
        let url = this.USERS_HOST_URL + '/deleteUserDetail/';
        return this.http.post(url, userDetail);
    }

    getSpecificUserDetailFromUsername(username: string) {
        let url = this.USERS_HOST_URL + "/getSpecificUserDetailFromUsername";
        let params = {
            username: username
        }
        return this.http.get(url, { params });
    }

    authenticateUser(userDetail: UserDetail) {
        let url = this.USERS_HOST_URL + '/authenticateUser';
        return this.http.post(this.USERS_HOST_URL + '/authenticateUser', userDetail);
    }

    getSpecificUserDetail(user_id: string) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "user_id": user_id
        }
        return this.http.get(this.USERS_HOST_URL + "/getSpecificUserDetail", { params });
    }
    getUsersOfManagerUser(parent_user_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "parent_user_id": parent_user_id
        }
        return this.http.get(this.USERS_HOST_URL + "/getUsersOfManagerUser", { params });
    }
    getUsersForHRUser() {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username
        }
        return this.http.get(this.USERS_HOST_URL + "/getUsersForHRUser", { params });
    }
    getUnmanagedUsers(calling_user_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "calling_user_id": calling_user_id
        }
        return this.http.get(this.USERS_HOST_URL + "/getUnmanagedUsers", { params });
    }
    updateManagersForUsers(managerUserDetail: ManagerUserDetail) {
        return this.http.post(this.USERS_HOST_URL + '/updateManagersForUsers/', managerUserDetail);
    }

    getAdvancedUsersSearchValues(search_by) {
        let params = this.getDefaultParams();
        params["search_by"] = search_by;
        return this.http.get(this.USERS_HOST_URL + "/getAdvancedUsersSearchValues", { params });
    }

    getAllUsersOfType(user_type) {
        let params = this.getDefaultParams();
        params["user_type"] = user_type;
        return this.http.get(this.USERS_HOST_URL + "/getAllUsersOfType", { params });
    }

    searchUsers(searchUserDetail: SearchUserDetail) {
        return this.http.post(this.USERS_HOST_URL + '/searchUsers/', searchUserDetail);
    }

    getRoleDetailsOfSpecificUserId(user_id) {
        let params = this.getDefaultParams();
        params["user_id"] = user_id;
        return this.http.get(this.USERS_HOST_URL + '/getRoleDetailsOfSpecificUserId', { params });
    }

    //roles
    addRoleDetail(roleDetail: RoleDetail) {
        this.setSourceUsername(roleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/addRoleDetail/', roleDetail);
    }

    updateRoleDetail(roleDetail: RoleDetail) {
        this.setSourceUsername(roleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/updateRoleDetail/', roleDetail);
    }

    deleteRoleDetail(roleDetail: RoleDetail) {
        this.setSourceUsername(roleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/deleteRoleDetail/', roleDetail);
    }

    getAllRoleDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllRoleDetail', { params });
    }

    getRoleDetailOfUserType(user_type) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "user_type": user_type
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getRoleDetailOfUserType', { params });
    }

    getSpecificRoleDetail(role_id) {
        let params = this.getDefaultParams();
        params["role_id"] = role_id;
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificRoleDetail', { params });
    }

    //specific rules
    getSpecificRuleForRole(role_id) {
        let params = this.getDefaultParams();
        params["role_id"] = role_id;
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificRuleForRole', { params });
    }


    //rules
    addRuleDetail(ruleDetail: RuleDetail) {
        this.setSourceUsername(ruleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/addRuleDetail', ruleDetail);
    }

    updateRuleDetail(ruleDetail: RuleDetail) {
        this.setSourceUsername(ruleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/updateRuleDetail', ruleDetail);
    }

    deleteRuleDetail(ruleDetail: RuleDetail) {
        this.setSourceUsername(ruleDetail);
        return this.http.post(this.CONFIG_HOST_URL + '/deleteRuleDetail/', ruleDetail);
    }

    getAllRuleDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllRuleDetail', { params });
    }

    getParticularRuleDetail(rule_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "rule_id": rule_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getParticularRuleDetail', { params });
    }

    getParticularRuleDetailFromRoleId(role_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "role_id": role_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getParticularRuleDetailFromRoleId', { params });
    }

    // getParticularRuleDetailFromUsername(associated_username) {
    //     let params = {
    //         "username": this._globalService.getCurrentUserDetail().source_username,
    //         "associated_username": associated_username
    //     }
    //     return this.http.get(this.CONFIG_HOST_URL + '/getParticularRuleDetailFromUsername', { params });
    // }

    getPermissionDetailsForRule(role_id: string, entity_id: string) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "role_id": role_id,
            "entity_id": entity_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getPermissionDetailsForRule', { params });
    }

    //permission
    addPermissionDetail(permissionDetail: PermissionDetail) {
        permissionDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/addPermissionDetail/', permissionDetail);
    }

    updatePermissionDetail(permissionDetail: PermissionDetail) {
        permissionDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updatePermissionDetail/', permissionDetail);
    }


    deletePermissionDetail(permissionDetail: PermissionDetail) {
        permissionDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/deletePermissionDetail/', permissionDetail);
    }

    getAllPermissionDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllPermissionDetail', { params });
    }

    getSpecificPermissionDetail(permission_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "permission_id": permission_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificPermissionDetail', { params });
    }

    getAllEntityDetail() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllEntityDetail', { params });
    }

    getMultipleEntityDetail(multiple_entity_id_arr: string[]) {
        let body = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "multiple_entity_id_arr": multiple_entity_id_arr
        }
        return this.http.post(this.CONFIG_HOST_URL + '/getMultipleEntityDetail', body);
    }

    updateEntityDetail(entityDetail: EntityDetail) {
        entityDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updateEntityDetail/', entityDetail);
    }

    deleteEntityDetail(entityDetail: CommonModel) {
        entityDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/deleteEntityDetail/', entityDetail);
    }

    getSpecificEntityDetail(entity_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "entity_id": entity_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificEntityDetail', { params });
    }

    getAllEntityDetailsOfSpecificRole(role_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "role_id": role_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAllEntityDetailsOfSpecificRole', { params });
    }


    //fields
    getFieldDetailsForComponent(component_name) {
        let params = this.getDefaultParams();
        params["component_name"] = component_name;
        params["field_language"] = this._globalService.getPreferredLanguage();
        return this.http.get(this.COMMON_HOST_URL + '/getFieldDetailsForComponent', { params });
    }

    updateFieldDetail(fieldDetail: FieldDetail) {
        fieldDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.COMMON_HOST_URL + '/updateFieldDetail/', fieldDetail);
    }

    getModuleIdNameArray(module_name_id) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "module_name_id": module_name_id
        }
        return this.http.get(this.COMMON_HOST_URL + '/getModuleIdNameArray', { params });
    }


    //alertnotification
    getAllAlertNotification() {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "preferred_language": this._globalService.getPreferredLanguage()
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAllAlertNotification', { params });
    }

    getAlertNotificationOfComponent(component_name) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "component_name": component_name,
            "preferred_language": this._globalService.getPreferredLanguage()
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getAlertNotificationOfComponent', { params });
    }

    updateAlertNotification(alertNotification: AlertNotification) {
        alertNotification.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updateAlertNotification/', alertNotification);
    }

    alertNameToAlertValueMap: Map<string, string> = new Map();
    //alert notifications
    loadAllAlertNotification(alertNotificationArr: AlertNotification[]) {
        // let alertNotificationArr: AlertNotification[] = result["data"];
        alertNotificationArr.forEach(alertNotification => {
            this.alertNameToAlertValueMap.set(alertNotification.alert_name, alertNotification.alert_value);
        });


        //fill default things until stored in the database
        this.alertNameToAlertValueMap.set(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.COMMON.session_logout, "Your session is expired.");
    }

    async reloadAllAlertNotification() {
        let result = await this.getAllAlertNotification().toPromise();
        CustomLogger.logStringWithObject("reloadAllAlertNotification: result", result);
        let alertNotificationArr: AlertNotification[] = result["data"];
        this.loadAllAlertNotification(alertNotificationArr);
        // alertNotificationArr.forEach(alertNotification => {
        //     this.alertNameToAlertValueMap.set(alertNotification.alert_name, alertNotification.alert_value);
        // });
    }

    getAlertValueFromAlertName(alert_name): string {
        CustomLogger.logString("ABout to get alert value for name:" + alert_name);
        return this.alertNameToAlertValueMap.get(alert_name);
    }


    //defaultSetting
    updateDefaultSetting(defaultSetting: DefaultSetting) {
        defaultSetting.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updateDefaultSetting/', defaultSetting);
    }

    getDefaultSetting() {
        let params =
            { "source_username": "" } //if this is set to some value then defaultsetting may fail
        return this.http.get(this.CONFIG_HOST_URL + "/getDefaultSetting", { params });
    }

    //page matrix
    getAllPageMatrix() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getAllPageMatrix', { params });
    }
    getSpecificPageMatrix(page_matrix_id) {
        let params = this.getDefaultParams();
        params["page_matrix_id"] = page_matrix_id;
        return this.http.get(this.CONFIG_HOST_URL + '/getSpecificPageMatrix', { params });
    }
    getPageMatrixForRoleId(role_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "role_id": role_id
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getPageMatrixForRoleId', { params });
    }
    addPageMatrix(pageMatrix: PageMatrix) {
        pageMatrix.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/addPageMatrix/', pageMatrix);
    }
    deletePageMatrix(pageMatrix: PageMatrix) {
        pageMatrix.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/deletePageMatrix', pageMatrix);
    }
    getRespectivePageMatrixForUsername(associated_username) {
        let params = this.getDefaultParams();
        params["associated_username"] = associated_username;
        // let params = {
        //     "username": this._globalService.getCurrentUserDetail().source_username,
        //     "associated_username": associated_username
        // }
        return this.http.get(this.CONFIG_HOST_URL + '/getRespectivePageMatrixForUsername', { params });
    }
    // loadPageMatrixForUserId(user_id) {
    //     let params = this.getDefaultParams();
    //     params["user_id"] = user_id;
    //     return this.http.get(this.CONFIG_HOST_URL + '/loadPageMatrixForUserId', { params });
    // }
    getUnassociatedUsernames() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getUnassociatedUsernames', { params });
    }
    updatePageMatrix(pageMatrix: PageMatrix) {
        pageMatrix.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updatePageMatrix/', pageMatrix);
    }


    //formElement    
    getUniqueFormElement() {
        let params = this.getDefaultParams();
        return this.http.get(this.CONFIG_HOST_URL + '/getUniqueFormElement', { params });
    }
    getDataOfFormElement(form_element_name) {
        let params = {
            "source_username": this._globalService.getCurrentUserDetail().source_username,
            "form_element_name": form_element_name
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getDataOfFormElement', { params });
    }
    addFormElement(formElement: FormElement) {
        formElement.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/addFormElement', formElement);
    }
    updateFormElement(formElement: FormElement) {
        formElement.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updateFormElement', formElement);
    }
    deleteFormElement(formElement: FormElement) {
        formElement.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/deleteFormElement', formElement);
    }

    //maker-checker
    addMakerCheckerRequest(makerCheckerRequest: MakerCheckerRequest) {
        makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/addMakerCheckerRequest', makerCheckerRequest);
    }
    approveMakerCheckerRequest(makerCheckerRequest: MakerCheckerRequest) {
        makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/approveMakerCheckerRequest', makerCheckerRequest);
    }
    declineMakerCheckerRequest(makerCheckerRequest: MakerCheckerRequest) {
        makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/declineMakerCheckerRequest', makerCheckerRequest);
    }
    cancelMakerCheckerRequest(makerCheckerRequest: MakerCheckerRequest) {
        makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/cancelMakerCheckerRequest', makerCheckerRequest);
    }

    getMakerCheckerRequestFromStatus(current_status) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "current_status": current_status
        }
        return this.http.get(this.CONFIG_HOST_URL + '/getMakerCheckerRequestFromStatus', { params });
    }

    //approval matrix
    getApprovalMatrixFromUserType(user_type) {
        let params = this.getDefaultParams();
        params["user_type"] = user_type;
        return this.http.get(this.CONFIG_HOST_URL + '/getApprovalMatrixFromUserType', { params });
    }
    updateSpecificApprovalMatrix(approvalMatrix: ApprovalMatrix) {
        approvalMatrix.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONFIG_HOST_URL + '/updateSpecificApprovalMatrix', approvalMatrix);
    }

    //file
    uploadFile(formData: FormData) {
        return this.http.post(this.COMMON_HOST_URL + '/uploadFile', formData);
    }


    refreshMappingUserEntity(user_id) {
        let params = {
            "souce_username": this._globalService.getCurrentUserDetail().source_username,
            "user_id": user_id
        }
        return this.http.get(this.USERS_HOST_URL + '/refreshMappingUserEntity', { params });
    }
    getMappingUserEntityOfUserId(user_id) {
        let params = this.getDefaultParams();
        params["user_id"] = user_id;
        return this.http.get(this.USERS_HOST_URL + '/getMappingUserEntityOfUserId', { params });
    }
    getSpecificMappingUserEntity(mapping_user_entity_id) {
        let params = {
            "username": this._globalService.getCurrentUserDetail().source_username,
            "mapping_user_entity_id": mapping_user_entity_id
        }
        return this.http.get(this.USERS_HOST_URL + '/getSpecificMappingUserEntity', { params });
    }
    getAllEntityDetailsOfMappingUser(user_id) {
        let params = this.getDefaultParams();
        params["user_id"] = user_id;
        return this.http.get(this.USERS_HOST_URL + '/getAllEntityDetailsOfMappingUser', { params });
    }
    updateMappingUserEntity(mappingUserEntity: MappingUserEntity) {
        mappingUserEntity.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.USERS_HOST_URL + '/updateMappingUserEntity', mappingUserEntity);
    }

    /////////////////
    runRestoration() {
        return this.http.post(this.CONFIG_HOST_URL + '/runRestoration', null);
    }

    runHRRestoration() {
        return this.http.post(this.LDAP_HOST_URL + '/createLDAPUsersFromHR', null);
    }

    getDefaultParams() {
        let params =
            { "source_username": this._globalService.getCurrentUserDetail().source_username }
        return params;
    }

    setSourceUsername(commonModel: CommonModel) {
        commonModel.source_username = this._globalService.getCurrentUserDetail().source_username;
    }

}