import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseServiceConfig } from "./baseServiceConfig";
import { GlobalService } from "./global.service";
import { MongoDBConnectorModel } from '../models/connectors/database/MongoDBConnectorModel.model';
import { UserDetail } from '../models/common/UserDetail.model';
import { DBConnectorModel } from '../models/connectors/database/DBConnectorModel.model';
import { ActiveDirectoryConnectorModel } from '../models/connectors/ActiveDirectoryConnectorModel.model';
import { LinuxConnectorModel } from '../models/connectors/LinuxConnectorModel.model';

@Injectable()
export class ConnectorService extends BaseServiceConfig {

    constructor(private http: HttpClient, private _globalService: GlobalService) {
        super();
    }

    createMongoDBUser(mongoDbconnectorModel: MongoDBConnectorModel) {
        return this.http.post(this.CONNECTOR_HOST_URL + '/MongoDBCreateUser', mongoDbconnectorModel);
    }
    createActiveDirectoryUser(activeDirectoryConnectorModel:ActiveDirectoryConnectorModel){
        return this.http.post(this.CONNECTOR_HOST_URL + '/ActiveDirectoryCreateUser', activeDirectoryConnectorModel);
    }
    createLinuxUser(linuxConnectorModel:LinuxConnectorModel){
        return this.http.post(this.CONNECTOR_HOST_URL + '/LinuxCreateUser', linuxConnectorModel);
    }


    deprovisionUser(userDetail: UserDetail) {
        userDetail.source_username = this._globalService.getCurrentUserDetail().source_username;
        return this.http.post(this.CONNECTOR_HOST_URL + '/deprovisionUser', userDetail);
    }



    getDefaultParams() {
        let params =
            { "source_username": this._globalService.getCurrentUserDetail().source_username }
        return params;
    }
    
}