import { Injectable } from '@angular/core';
import { UserDetail } from '../models/common/UserDetail.model';
import { CustomGlobalConstants } from '../utils/CustomGlobalConstants';
import { DefaultSetting } from '../models/common/DefaultSetting.model';
import { PageMatrix } from '../models/common/PageMatrix.model';
import { ApprovalMatrix } from '../models/common/ApprovalMatrix.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }
  private current_user_detail: UserDetail;
  private last_login_time = Date.now();
  private is_otp_required = false;
  private auth_token = null;
  private current_logging_tracking = null;
  // private roleName = "";
  private auth_type = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT;
  private preferred_language = CustomGlobalConstants.PREFERRED_LANGUAGE.ENGLISH;
  private default_setting: DefaultSetting;
  private page_matrix: PageMatrix;
  private approval_matrix: ApprovalMatrix[];


  getApprovalMatrix() {
    return this.approval_matrix;
  }

  setApprovalMatrix(approvalMatrix) {
    this.approval_matrix = approvalMatrix;
  }

  getPageMatrix() {
    return this.page_matrix;
  }

  setPageMatrix(pageMatrix) {
    this.page_matrix = pageMatrix;
  }

  getDefaultSetting() {
    return this.default_setting;
  }

  setDefaultSetting(defaultSetting) {
    this.default_setting = defaultSetting;
  }

  getPreferredLanguage() {
    console.log("Getting Preferred Lanauge: " + this.preferred_language);
    return this.preferred_language;
  }

  setPreferredLanguage(preferredLanguage) {
    console.log("Setting Preferred Lanauge to : " + preferredLanguage);
    this.preferred_language = preferredLanguage;
  }
  getAuthType() {
    return this.auth_type;
  }

  setAuthType(authType) {
    this.auth_type = authType;
  }

  // getRoleName() {
  //   return this.roleName;
  // }

  // setRoleName(roleName) {
  //   this.roleName = roleName;
  // }

  getCurrentLoggingTracking() {
    return this.current_logging_tracking;
  }

  setCurrentLoggingTracking(loggingTracking) {
    this.current_logging_tracking = loggingTracking;
  }

  getAuthToken() {
    return this.auth_token;
  }

  setAuthToken(authToken: string) {
    this.auth_token = authToken;
  }

  getIsOTPRequired() {
    return this.is_otp_required;
  }

  setIsOTPRequired(boo: boolean) {
    this.is_otp_required = boo;
  }

  getCurrentUserDetail() {
    return this.current_user_detail;
  }

  setCurrentUserDetail(userDetail) {
    this.current_user_detail = userDetail;
  }


  getLastLoginTime() {
    return this.last_login_time;
  }

  setLastLoginTime(lastLoginTime) {
    this.last_login_time = lastLoginTime;
  }

  clearAll() {
    this.current_user_detail = null;
    this.last_login_time = Date.now();
    this.is_otp_required = false;
    this.auth_token = null;
    this.current_logging_tracking = null;
    this.auth_type = 1;
  }

  showAll() {

  }
}
