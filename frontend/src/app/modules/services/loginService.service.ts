import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserDetail } from '../models/common/UserDetail.model';
import { LoggingTracking } from '../models/events/LoggingTracking.model';
import { CustomLogger } from '../utils/CustomLogger';
import { CustomMisc } from '../utils/CustomMisc';
import { BaseServiceConfig } from './baseServiceConfig';
import { EventService } from './eventService.service';
import { GlobalService } from './global.service';
import { SystemEvent } from '../models/common/SystemEvent.model';
import { CommonModel } from '../models/CommonModel';

@Injectable({
    providedIn: 'root'
})
export class LoginService extends BaseServiceConfig {
    headerOptions: any = null;
    _isLoggedIn: boolean = false;
    authSub = new Subject<any>();

    constructor(private _http: HttpClient, private _globalService: GlobalService, private _eventService: EventService) {
        super();
    }


    loginAuth(userObj: any) {
        CustomLogger.logStringWithObject("loginAuth: userObj:", userObj);
        let userDetail = new UserDetail();
        userDetail.username = userObj.uname;
        userDetail.password = userObj.password;
        let headerOptions = null;
        if (userObj.authcode) {
            headerOptions = new HttpHeaders({
                'x-tfa': userObj.authcode
            });
        }

        let reply = this._http.post(this.OTP_LOGIN_HOST_URL + "/login", { uname: userObj.uname, upass: userObj.upass }, { observe: 'response', headers: headerOptions });
        CustomLogger.logStringWithObject("Reply from login:", reply);
        return reply;
    }

    setupAuth() {
        return this._http.post(this.OLD_LOGIN_TFA_HOST_URL + "/setup", {}, { observe: 'response' })
    }

    registerUser(userObj: any) {
        return this._http.post(this.OLD_LOGIN_REGISTER_HOST_URL + "/register", { uname: userObj.uname, upass: userObj.upass }, { observe: "response" });
    }

    updateAuthStatus(value: boolean) {
        this._isLoggedIn = value
        this.authSub.next(this._isLoggedIn);
        localStorage.setItem('isLoggedIn', value ? "true" : "false");
    }

    getAuthStatus() {
        this._isLoggedIn = localStorage.getItem('isLoggedIn') == "true" ? true : false;
        return this._isLoggedIn
    }

    logoutUser() {
        try {
            let loggingTracking: LoggingTracking = this._globalService.getCurrentLoggingTracking();
            if (loggingTracking != null) {
                loggingTracking.logout_time = CustomMisc.getCurrentTimeInMilli();
                loggingTracking.clean_logged_out_flag = true;
                this._eventService.updateLoggingTracking(loggingTracking).subscribe(
                    data => { CustomLogger.logStringWithObject("updateLoggingTracking Success:", data); },
                    err => { CustomLogger.logStringWithObject("updateLoggingTracking Error:", err); }
                );
                this._isLoggedIn = false;
                this.authSub.next(this._isLoggedIn);
                localStorage.setItem('isLoggedIn', "false");
            }
        } catch (error) {
            CustomLogger.logStringWithObject("Error:logoutUser:", error);
        }

    }

    getAuth(username) {
        return this._http.get(this.OTP_LOGIN_HOST_URL + "/otpSetup/" + username, { observe: 'response' });
    }

    deleteAuth() {
        return this._http.delete(this.OLD_LOGIN_TFA_HOST_URL + "/setup", { observe: 'response' });
    }

    verifyAuthAndGetToken(user_id: string, authCode: any) {
        let tempSecret = this._globalService.getCurrentUserDetail().tempSecret;
        return this._http.post(this.OTP_LOGIN_HOST_URL + "/verify", { "user_id": user_id, "tempSecret": tempSecret, "token": authCode }, { observe: 'response' });
    }

    getAuthTokenForLoggedInUser(user_id: string) {
        return this._http.post(this.OTP_LOGIN_HOST_URL + "/getAuthTokenForLoggedInUser", { "user_id": user_id });
    }


    //ldap related
    checkIfLDAPUserExists(username) {
        let url = this.LDAP_HOST_URL + "/getSpecificLdapUserDetail/" + username;
        CustomLogger.logString("url:" + url);
        return this._http.get(url);
    }

    authenticateLDAPUser(userDetail: UserDetail) {
        // this.setSourceUsername(userDetail); // here the source_username will never be set as it is called from login page
        let url = this.LDAP_HOST_URL + "/authenticateLDAPUser";
        return this._http.post(url, userDetail);
    }


    addLdapUser(userDetail: UserDetail) {
        this.setSourceUsername(userDetail);
        let url = this.LDAP_HOST_URL + "/addLdapUser";
        return this._http.post(url, userDetail);
    }

    updateLdapUser(userDetail: UserDetail) {
        this.setSourceUsername(userDetail);
        let url = this.LDAP_HOST_URL + "/updateLdapUser";
        CustomLogger.logString("url:" + url);
        return this._http.post(url, userDetail);
    }

    deleteLdapUser(userDetail: UserDetail) {
        this.setSourceUsername(userDetail);
        let url = this.LDAP_HOST_URL + "/deleteLdapUser";
        CustomLogger.logString("url:" + url);
        return this._http.post(url, userDetail);
    }

    getAllLoggingTracking() {
        let params = this.getDefaultParams();
        return this._http.get(this.EVENT_HOST_URL + '/getAllLoggingTracking', { params });
    }

    addSystemEvent(systemEvent: SystemEvent) {
        this.setSourceUsername(systemEvent);
        return this._http.post(this.EVENT_HOST_URL + '/addSystemEvent', systemEvent);
    }

    //mssql related
    authenticateMSSQLUser(username, password) {
        let url = this.MSSQL_HOST_URL + "/authenticateUser";
        CustomLogger.logString("url:" + url);
        return this._http.post(url, { username: username, password: password });
    }


    getDefaultParams() {
        let params =
            { "source_username": this._globalService.getCurrentUserDetail().source_username }
        return params;
    }

    setSourceUsername(commonModel:CommonModel){
        commonModel.source_username = this._globalService.getCurrentUserDetail().source_username;
    }
}
