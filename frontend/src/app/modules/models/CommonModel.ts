export class CommonModel {
    source_user_id: string;
    source_username: string;
    details: string;
    remarks: string;
    model_name: number;
    created_time: number = Date.now();
    last_modified_time: number = Date.now();
}