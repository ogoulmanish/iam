import { CustomMisc } from '../../utils/CustomMisc';
import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class LoggingTracking extends BaseModel {
    logging_tracking_id: string = CustomMisc.getNewUUID();
    username: string;
    password: string;
    access_browser: string;
    login_time: number = CustomMisc.getCurrentTimeInMilli();
    logout_time: number;
    source_ip_address: string;
    clean_logged_out_flag: boolean = false;
    // status_code: string = "200";
    is_login_successful: boolean = false;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.LOGGING_TRACKING;
    }
}
