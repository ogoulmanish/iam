import { CommonModel } from '../CommonModel';

export class BaseConnectorModel extends CommonModel {
    connector_id: string;
    connector_name: string;
    connector_type: string;//e.g. MSSQL Connector
    connector_category: string;//e.g. Database connector
}