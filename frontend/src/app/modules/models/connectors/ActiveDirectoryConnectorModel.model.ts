import { BaseConnectorModel } from './BaseConnector.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class ActiveDirectoryConnectorModel extends BaseConnectorModel {
    host_name: string = "localhost";
    host_username: string;
    host_password: string;
    host_port: string = "10389";
    container_dn: string; //specific department
    source_user_id: string;
    new_login_name: string;
    new_password: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ACTIVE_DIRECTORY_CONNECTOR;
        this.connector_category = CustomGlobalConstants.CONNECTOR_CATEGORIES.DIRECTORY_CONNECTOR;
        this.connector_type = CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY
    }
}