import { BaseConnectorModel } from '../BaseConnector.model';
import { CustomGlobalConstants } from '../../../utils/CustomGlobalConstants';

/**
 * MongoDB Specific Connector
 */
export class DBConnectorModel extends BaseConnectorModel {
    host_name: string;
    host_username: string;
    host_password: string;
    host_port: string;
    database_name: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.DB_CONNECTOR;
        this.connector_category = CustomGlobalConstants.CONNECTOR_CATEGORIES.DATABASE_CONNECTOR;
    }
}