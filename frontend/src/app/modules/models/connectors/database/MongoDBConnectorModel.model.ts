import { CustomGlobalConstants } from '../../../utils/CustomGlobalConstants';
import { DBConnectorModel } from './DBConnectorModel.model';


/**
 * MongoDB Specific Connector
 */
export class MongoDBConnectorModel extends DBConnectorModel {
    new_login_name: string;
    new_password: string;
    constructor() {
        super();
        this.connector_type = CustomGlobalConstants.CONNECTOR_TYPE.MONGODB
    }
}