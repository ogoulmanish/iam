import { BaseConnectorModel } from './BaseConnector.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class LinuxConnectorModel extends BaseConnectorModel {
    host_name: string = "localhost";
    host_username: string;
    host_password: string;
    host_port: string = "10389";
    source_user_id: string;
    new_login_name: string;
    new_password: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.LINUX_CONNECTOR;
        this.connector_category = CustomGlobalConstants.CONNECTOR_CATEGORIES.OS_CONNECTOR;
        this.connector_type = CustomGlobalConstants.CONNECTOR_TYPE.LINUX
    }
}