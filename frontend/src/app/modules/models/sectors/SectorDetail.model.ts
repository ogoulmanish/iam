import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

/**
 * 
 */
export class SectorDetail extends BaseSectorDetail {
    sector_id: string;
    sector_name: string;
    sector_type: number;//1-Systems, 2-Network etc.
    sector_icon_location: string;
    entity_id_arr: string[];//which entities belong to this sector

    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.SECTOR_DETAIL;
    }

}