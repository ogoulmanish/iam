import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class AccessPolicy extends BaseSectorDetail {
    access_policy_id: string;
    access_policy_name: string;
    entity_id_arr: []; //[{'entity_id':<id1>, 'permission_id':<id2>}, {'entity_id':<id3>, 'permission_id':<id4>}]
    
    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ACCESS_POLICY;
    }
}