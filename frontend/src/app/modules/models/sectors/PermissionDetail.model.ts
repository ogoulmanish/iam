import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class PermissionDetail extends BaseSectorDetail {
    permission_id: string;
    permission_name: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.PERMISSION_DETAIL;
    }
}