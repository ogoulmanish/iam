import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

/**
 * Every rule will decide which type of permission can be given based on the role and entity
 */
export class RuleDetail extends BaseModel {
    rule_id: string;
    rule_name: string;
    // sector_id_arr: string[] = [];
    role_id: string;
    // entity_id: string;
    // permission_id_arr: string[] = [];
    mapEntityToPermission: Map<string, string[]> = new Map();
    tmpStr: string;
    associated_username: string = CustomGlobalConstants.DEFAULT_SELECTED_STRING_VALUE;
    rule_type: number = CustomGlobalConstants.RULE_TYPES.ROLE_SPECIFIC;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.RULE_DETAIL;
    }
}