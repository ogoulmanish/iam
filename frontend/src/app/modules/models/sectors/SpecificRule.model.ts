import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class SpecificRule extends BaseSectorDetail {
    specific_rule_id: string = "";
    role_id: string = "";
    attribute_name: string = "";
    attribute_value: string = "";
    arithmetic_operation: string = "";

    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.SPECIFIC_RULE;
    }
}