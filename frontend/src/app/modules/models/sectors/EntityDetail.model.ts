import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

/**
 * Actual entity to which the user can connect to 
 */
export class EntityDetail extends BaseSectorDetail {
    entity_id: string; //
    entity_name: string;
    access_location: string;//place where this application be connected to  
    icon_location: string;
    is_default: boolean = false;
    risk_level_id: string;
    icon_filename: string;

    //connector specific
    connector_type: string;
    connector_object: Object;
    // connector_id: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ENTITY_DETAIL;
    }
}