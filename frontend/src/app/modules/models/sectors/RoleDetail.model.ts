import { BaseSectorDetail } from './BaseSectorDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

/**
 * Every role has entities it can connect to
 */
export class RoleDetail extends BaseSectorDetail {
    role_id: string;
    role_name: string;//engineer, representative, electrician etc.
    // allowed_entity_id_arr: string[];//which entities can this role access - WILL BE DELETED
    allowed_access_policy_id_arr: string[] = [];//which access policies is this role assigned to
    allowed_permission_id_arr: string[] = [];
    allowed_user_id_arr: string[] = [];//which users are to be part of  entities can this role access - WILL BE DELETED
    rule_associated : boolean = false;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ROLE_DETAIL;
    }
} 