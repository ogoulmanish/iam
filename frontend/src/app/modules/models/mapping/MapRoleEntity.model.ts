import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class MapRoleEntity extends BaseModel {
    role_id: string;
    entity_id: string;
    access_status: number; //1 = ACCESS, 2 = PENDING, 3 = UNACCESSIBLE etc.
    permission_status: number;//1-read, 2-write etc.
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.MAP_ROLE_ENTITY;
    }
} 