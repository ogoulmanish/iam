import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class MapUserRole extends BaseModel {
    user_id: string;
    role_id: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.MAP_USER_ROLE;
    }
}