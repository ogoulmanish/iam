import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class MappingUserEntity extends BaseModel {
    mapping_user_entity_id: string;
    user_id: string;
    username: string;
    entity_id: string;
    role_id: string;
    current_status: number = CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING;
    login_name: string;
    password: string;
    permissions_arr: string[]; //permissions allowed
}