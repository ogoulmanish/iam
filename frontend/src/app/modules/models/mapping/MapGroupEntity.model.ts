import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class MapGroupEntity extends BaseModel {
    group_id: string;
    entity_id: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.MAP_GROUP_ENTITY;
    }
}