import { CommonModel } from './CommonModel';

export class BaseModel extends CommonModel {
    can_be_deleted: boolean = false;
    is_default: boolean = false;
}