import { BaseModel } from "../BaseModel.model";

/**
 * This class will tell when to update the passwords
 */
export class PasswordScheduler extends BaseModel {
    device_uid: string;
    password_change_days: string;
    password_verification_days: string;
}