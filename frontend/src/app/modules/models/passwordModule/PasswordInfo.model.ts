import { BaseModel } from "../BaseModel.model";

/**
 * This class keeps holding the current password scenario
 */
export class PasswordInfo extends BaseModel {
    user_uid: string; // user id for which this password obj is generated
    device_uid: string;//device id for which password needs to be updated
    original_password: string;
    last_password: string;//last changed password
    new_password: string;//new changed password
    new_password_update_time: string;
    remarks: string;    
}
