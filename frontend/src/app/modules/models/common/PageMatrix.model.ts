import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';
import { BaseModel } from '../BaseModel.model';
import { SimplePermissions } from '../sectors/SimplePermissions.model';

export class PageMatrix extends BaseModel {
    page_matrix_id: string;
    role_id: string;
    page_matrix_name: string;
    associated_user_id: string = CustomGlobalConstants.DEFAULT_SELECTED_STRING_VALUE;//for user centric page matrix
    show_field_menu_heading_navigation = false;
    show_field_menu_link_listUser = false;
    permission_listUser = new SimplePermissions();
    show_field_menu_link_listAccessEntity = false;
    permission_listAccessEntity = new SimplePermissions();
    show_field_menu_link_listEntity = false;
    permission_listEntity = new SimplePermissions();
    show_field_menu_link_listSector = false;
    permission_listSector = new SimplePermissions();
    show_field_menu_link_listSectorElements = false;
    permission_listSectorElements = new SimplePermissions();
    show_field_menu_link_listRole = false;
    permission_listRole = new SimplePermissions();
    show_field_menu_link_listPermission = false;
    permission_listPermission = new SimplePermissions();
    show_field_menu_link_listRule = false;
    permission_listRule = new SimplePermissions();
    show_field_menu_link_listPageMatrix = false;
    permission_listPageMatrix = new SimplePermissions();
    show_field_menu_link_listFormElement = false;
    permission_listFormElement = new SimplePermissions();
    show_field_menu_link_editFields = false;
    permission_editFields = new SimplePermissions();
    show_field_menu_link_emailAlerts = false;
    permission_emailAlerts = new SimplePermissions();
    show_field_menu_link_editUserProfile = false;
    permission_editUserProfile = new SimplePermissions();
    show_field_menu_link_listAlertNotification = false;
    permission_listAlertNotification = new SimplePermissions();
    show_field_menu_link_defaultSetting = false;
    permission_defaultSetting = new SimplePermissions();
    show_field_menu_link_report_default = false;
    permission_report_default = new SimplePermissions();
    show_field_menu_link_report_entityEvents = false;
    permission_report_entityEvents = new SimplePermissions();
    show_field_menu_link_report_systemEvents = false;
    permission_report_systemEvents = new SimplePermissions();
    show_field_menu_link_report_existingUsers = false;
    permission_report_existingUsers = new SimplePermissions();
    show_field_menu_link_report_userLogging = false;
    permission_report_userLogging = new SimplePermissions();
    show_field_menu_link_report_userActivityReports = false;
    permission_report_userActivityReports = new SimplePermissions();
    show_field_menu_link_listManagerUser = false;
    permission_listManagerUser = new SimplePermissions();
    show_field_menu_link_listMakerCheckerRequest = false;
    permission_listMakerCheckerRequest = new SimplePermissions();
    show_field_menu_link_listApprovalMatrix = false;
    permission_listApprovalMatrix = new SimplePermissions();
    show_field_menu_link_listPredefinedConnectors = false;
    permission_listPredefinedConnectors = new SimplePermissions();
    show_field_menu_link_listAccessPolicy = false;
    permission_listAccessPolicy = new SimplePermissions();

    //standard
    show_field_standard_menu_heading_navigation = false;
    show_field_standard_menu_link_showEntities = false;
    permission_standard_showEntities = new SimplePermissions();
    show_field_standard_menu_link_editUserProfile = false;
    permission_standard_editUserProfile = new SimplePermissions();

    //hr
    show_field_hr_menu_heading_navigation = false;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.PAGE_MATRIX;
    }
}