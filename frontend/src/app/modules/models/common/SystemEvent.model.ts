import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';
import { CustomMisc } from '../../utils/CustomMisc';
import { BaseModel } from '../BaseModel.model';

export class SystemEvent extends BaseModel {
    event_id: string;
    event_type: string = CustomGlobalConstants.EVENT_TYPES.READ;
    event_time: number = CustomMisc.getCurrentTimeInMilli();
    event_source: string = "Browser";
    error_code: string = null;
    error_details: string;
    alert_type: number = CustomGlobalConstants.ALERT_LEVEL.LOW;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.SYSTEM_EVENT;
    }
}