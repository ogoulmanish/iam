import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';
import { CustomMisc } from '../../utils/CustomMisc';

export class MakerCheckerRequest extends CommonModel {
    maker_checker_request_id: string;
    maker_user_id: string;
    maker_request_time: number = CustomMisc.getCurrentTimeInMilli();
    maker_component_name: string;
    maker_CRUD_operation: string;
    maker_object: Object;
    current_status: number = CustomGlobalConstants.MAKER_CHECKER_REQUEST_STATUS.PENDING;
    maker_query: string;//specific query (database) to carry out if any

    checker_user_id: string = CustomGlobalConstants.DEFAULT_PARENT_USER_ID.ADMIN;//the checker who change the status
    checker_status_time: string;//time the checkr change the status

    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.MAKER_CHECKER_REQUEST;
    }
}