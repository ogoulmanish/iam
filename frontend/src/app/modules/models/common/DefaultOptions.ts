import { BaseModel } from '../BaseModel.model'
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class DefaultOptions extends BaseModel {
    option_id: string;
    option_name: string;
    option_type: number;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.DEFAULT_OPTIONS;
    }
}