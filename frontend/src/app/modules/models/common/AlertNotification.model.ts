import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class AlertNotification extends CommonModel {
    alert_notification_id: string;
    component_name: string;
    alert_name: string;
    alert_value: string;
    preferred_language: string = CustomGlobalConstants.PREFERRED_LANGUAGE.ENGLISH;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ALERT_NOTIFICATION;
    }
}