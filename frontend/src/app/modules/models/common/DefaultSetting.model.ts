import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class DefaultSetting extends CommonModel {
    session_timeout: number;

    //login expiry
    account_expiry_days: number = 1000;//how many days should the user login is expired since last login

    //email 
    email_service: string;
    email_default_from_name: string;
    email_address: string;
    email_username: string;
    email_password: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.DEFAULT_SETTING;
    }
}