import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class UserSessionTracking extends BaseModel {
    session_id: string;
    browser: string;
    login_name: string;
    password: string;
    login_time: string = "0";
    logout_time: string = "0";
    page_accessed_name: string;
    page_accessed_time: string;
    source_ip_address: string;
    clean_logged_out_flag: boolean = false;
    status_code: string = "200";
    remarks: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.USER_SESSION_TRACKING;
    }
}