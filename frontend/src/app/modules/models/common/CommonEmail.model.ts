import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class EmailDetail extends CommonModel {
    to_email: string;
    subject: string;
    text: string;
    cc: string[];
    bcc: string[];

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.COMMON_EMAIL;
    }
}