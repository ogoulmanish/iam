import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class EmailAlert extends BaseModel {
    should_send_email: boolean = true;
    alert_level_all: boolean = true;
    alert_level_critical: boolean = false;
    alert_level_high: boolean = false;
    alert_level_medium: boolean = false;
    alert_level_low: boolean = false;
    alert_level: number = CustomGlobalConstants.ALERT_LEVEL.HIGH;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.EMAIL_ALERT;
    }
}