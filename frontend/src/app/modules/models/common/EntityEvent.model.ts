import { BaseModel } from '../BaseModel.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class EntityEvent extends BaseModel {
    entity_event_id: string;
    accessed_username: string;
    entity_id: string;
    entity_name: string;
    time_of_access: number;
    risk_level_id: string;
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ENTITY_EVENT;
    }
}