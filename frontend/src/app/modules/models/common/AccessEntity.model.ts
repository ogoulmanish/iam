import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';
import { CustomMisc } from '../../utils/CustomMisc';
import { CommonModel } from '../CommonModel';

export class AccessEntity extends CommonModel {
    access_entity_id: string;
    mapping_user_entity_id: string;
    requested_user_id: string;
    requested_username: string;
    requested_entity_id: string;
    is_request_new: boolean = true;
    requested_time: number = CustomMisc.getCurrentTimeInMilli();
    current_status: number = CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING;
    last_update_time: number = CustomMisc.getCurrentTimeInMilli();
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.ACCESS_ENTITY;
    }
}