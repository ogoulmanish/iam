import { UserDetail } from './UserDetail.model';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class ManagerUserDetail extends UserDetail {
    parent_user_id: string;
    managerUserIdArr: string[];
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.MANAGER_USER_DETAIL;
    }
}