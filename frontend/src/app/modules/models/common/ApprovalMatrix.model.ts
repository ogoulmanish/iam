import { CommonModel } from '../CommonModel'
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

/**
 * Model denotes the approval of each of the operations on the specific component
 */
export class ApprovalMatrix extends CommonModel {
    approval_matrix_id: string;
    // role_id: string = CustomGlobalConstants.DEFAULT_ROLES.MANAGER_ROLE;
    user_type: string = CustomGlobalConstants.DEFAULT_ROLES.MANAGER_ROLE; 
    component_name: string; //as given in CustomGlobalConstants.COMPONENT_NAME
    requires_approval: boolean = false;
    create_permission: boolean = true;//approval required for create operation
    update_permission: boolean = true;//approval required for update operation
    delete_permission: boolean = true;//approval required for delete operation
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.APPROVAL_MATRIX;
    }
}