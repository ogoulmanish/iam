import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';
import { BaseModel } from '../BaseModel.model';

export class UserDetail extends BaseModel {
    user_id: string;
    username: string;
    user_type: string = CustomGlobalConstants.USER_TYPES.STANDARD;
    external_authentication_application: number;//CustomGlobalConstants.EXTERNAL_AUTHENTICATION_APPLICATIONS
    password: string;
    email: string;
    salutation: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    full_name: string;
    facebook_id: string;
    google_id: string;
    phone: number;
    date_of_birth: string;
    profile_link: string;
    created_on: number;
    last_modified: number;
    last_login_time: number;
    // role_id: string;
    DN: string;
    creator_user_id: string = CustomGlobalConstants.DEFAULT_PARENT_USER_ID.ADMIN;//default to admin - specifically for manager user
    parent_user_id: string = CustomGlobalConstants.DEFAULT_PARENT_USER_ID.ADMIN;//default to admin - who created this user

    is_deleted: boolean = false;// CURRENTLY DELETE IT but in future if this field is true then it is marked as deleted but the user will not be deleted from database
    is_active: boolean = true;// if false then it will not be able to do anything
    is_added_to_main: boolean = false;// if true then it is added to the the main AD from HRMS
    is_locked: boolean = false;// if true will not be able to log in 

    department: string;
    job_title: string;
    page_matrix_id: string;

    is_otp_required: boolean = true;
    secret_key: string;
    tempSecret: string;
    dataURL: string;
    tfaURL: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.USER_DETAIL;
    }
}
