import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class RestorationDetail extends CommonModel {

    restoration_id: string;
    restoration_time: number;
    was_successful: boolean = true;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.RESTORATION_DETAIL;
    }
}