import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class FieldDetail extends CommonModel {
    field_id: string;
    component_id: string;
    field_language: string;
    field_name: string;
    field_value: string;
    field_details: string;
    is_required: boolean = false;
    error_message: string = "";
    
    constructor(){
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.FIELD_DETAIL;
    }
}
