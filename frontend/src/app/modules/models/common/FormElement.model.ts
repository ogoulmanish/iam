import { CommonModel } from '../CommonModel';
import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class FormElement extends CommonModel {
    form_element_id: string;
    form_element_type: string; //"Combo box",
    form_element_name: string;
    data_name: string;
    data_value: string;

    constructor() {
        super();
        this.model_name = CustomGlobalConstants.MODEL_NAME.FORM_ELEMENT;
    }
}
