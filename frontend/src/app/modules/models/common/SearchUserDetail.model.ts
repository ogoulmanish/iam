import { CustomGlobalConstants } from '../../utils/CustomGlobalConstants';

export class SearchUserDetail {
    source_user_type = CustomGlobalConstants.USER_TYPES.STANDARD;
    username: string = "";
    is_added_to_main: string = "";
    user_id: string = "";
    email: string = "";
    first_name: string = "";
    last_name: string = "";
    active: string = "";
    locked: string = "";
    department: string = "";
    job_title: string = "";

}