import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { GlobalService } from '../services/global.service';
import { MiscService } from '../services/miscService.service';
import { CustomLogger } from '../utils/CustomLogger';
import { UserDetail } from '../models/common/UserDetail.model';

@Injectable()
export class AppAuthGuard extends KeycloakAuthGuard {
  constructor(protected router: Router, protected keycloakAngular: KeycloakService, protected globalService: GlobalService, protected miscService: MiscService) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login();
        return;
      }
      CustomLogger.logStringWithObject('role restriction given at app-routing.module for this route', route.data.roles);
      CustomLogger.logStringWithObject('User roles coming after login from keycloak :', this.roles);
      CustomLogger.logStringWithObject('THIS::::: :', this);
      // CustomLogger.logStringWithObject('ROUTE::::: :', route);
      // CustomLogger.logStringWithObject('STATE::::: :', state);
      this.initialize();
      const requiredRoles = route.data.roles;
      let granted: boolean = false;
      if (!requiredRoles || requiredRoles.length === 0) {
        granted = true;
      } else {
        for (const requiredRole of requiredRoles) {
          if (this.roles.indexOf(requiredRole) > -1) {
            granted = true;
            break;
          }
        }
      }

      if (granted === false) {
        this.router.navigate(['/']);
      }
      resolve(granted);

    });
  }

  async initialize() {
    CustomLogger.logStringWithObject('User roles userProfile :', this.keycloakAngular.getUsername());
    CustomLogger.logStringWithObject('User roles userRoles :', this.keycloakAngular.getUserRoles());
    let keycloakUserProfile = await this.keycloakAngular.loadUserProfile();
    CustomLogger.logStringWithObject('this.keycloakAngular.loadUserProfile() :', keycloakUserProfile);

    //fill up the user profile
    let userProfile = new UserDetail();
    userProfile.source_username = keycloakUserProfile.username;
    userProfile.username = keycloakUserProfile.username;
    // userProfile.role_array = this.keycloakAngular.getUserRoles();
    userProfile.first_name = keycloakUserProfile.firstName;
    userProfile.last_name = keycloakUserProfile.lastName;
    userProfile.full_name = keycloakUserProfile.firstName + " " + keycloakUserProfile.lastName;
    userProfile.user_id = keycloakUserProfile.id;
    // userProfile.is_admin = this.miscService.isUserAdmin(userProfile.role_array);
    // userProfile.display_role_name = this.miscService.getDisplayRoleName(userProfile.role_array);
    CustomLogger.logStringWithObject("User Profile::: ", userProfile);
    this.globalService.setCurrentUserDetail(userProfile);
  }

  /**
   * 
   */
  logout() {
    CustomLogger.logString("LOGGING OUT ....");
    this.keycloakAngular.getKeycloakInstance().logout();
  }
}