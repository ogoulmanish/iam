import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/loginService.service';
import { GlobalService } from '../services/global.service';
import { CustomMisc } from '../utils/CustomMisc';
import { MiscService } from '../services/miscService.service';
import { CustomLogger } from '../utils/CustomLogger';


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private _loginService: LoginService, private _router: Router, private _globalService: GlobalService, private _miscService: MiscService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        CustomLogger.logStringWithObject("AUTH GUARD ENTERED...::", this._loginService.getAuthStatus());
        CustomLogger.logStringWithObject("AUTH GUARD ENTERED...:next:", next);
        CustomLogger.logStringWithObject("AUTH GUARD ENTERED...:state:", state);
        CustomLogger.logStringWithObject("this._globalService.getCurrentUserDetail()::", this._globalService.getCurrentUserDetail());
        if (this._globalService.getCurrentUserDetail() != null) {
            // let currentUserType = this._globalService.getCurrentUserDetail().user_type;
            if (this._loginService.getAuthStatus()) {
                CustomLogger.logStringWithObject("next.data::", next.data);
                let menuPathName = next.data.menuPathName;
                let isAuthorized = this._miscService.isPageVisible(menuPathName);
                if (!isAuthorized) {
                    console.log("This user is not authorized to view this page.....");
                    CustomMisc.showAlert("You are not authorized to view this page.", true);
                    return false;
                }
                // CustomMisc.showAlert("isAuthorized:::" + isAuthorized);
                //check for user types
                // if (next.data.userTypeArray && next.data.userTypeArray.indexOf(currentUserType) === -1) {
                //     //user type not authorized
                //     console.log("This user is not authorized to view this page.....");
                //     CustomMisc.showAlert("You are not authorized to view this page.", true);
                //     return false;
                // }

                return true;
            }
        }
        this._router.navigate(['/login']);
        return false;
    }

}
