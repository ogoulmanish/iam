import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserIdleModule } from 'angular-user-idle';
import { KeycloakService } from 'keycloak-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { AppAuthGuard } from './modules/guard/AppAuthGuard';
import { ConnectorService } from './modules/services/connectorService.service';
import { DBService } from './modules/services/dbService.service';
import { EventService } from './modules/services/eventService.service';
import { LoginService } from './modules/services/loginService.service';
import { MiscService } from './modules/services/miscService.service';
import { PasswordService } from './modules/services/passwordService.service';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './views/login/home.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/login/register.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    TitleComponent,
    BreadcrumbsComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    FormsModule,
    UserIdleModule.forRoot({ idle: 600, timeout: 300, ping: 120 })
  ],
  providers: [
    DBService,
    MiscService,
    AppAuthGuard,
    KeycloakService,
    PasswordService,
    LoginService,
    LoginService,
    EventService,
    ConnectorService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
