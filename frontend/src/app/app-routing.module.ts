import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './layouts/admin/admin.component';
import { AuthGuard } from './modules/guard/AuthGuard';
import { CustomGlobalConstants } from './modules/utils/CustomGlobalConstants';
import { HomeComponent } from './views/login/home.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/login/register.component';


const routes: Routes = [

  { path: "", redirectTo: '/login', pathMatch: 'full' },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "register", component: RegisterComponent },

  {
    path: '',
    component: AdminComponent,
    data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.DEFAULT_COMPONENT },
    canActivate: [AuthGuard],
    children: [
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.DASHBOARD,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.DASHBOARD },
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.SECTORS,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.SECTORS },
        loadChildren: () => import('./views/sectors/sector.module').then(m => m.SectorModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.USERS,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.USERS },
        loadChildren: () => import('./views/users/user.module').then(m => m.UserModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.CONFIGURATION,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.CONFIGURATION },
        loadChildren: () => import('./views/configuration/configuration.module').then(m => m.ConfigurationModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.ACCESS_ENTITY,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN, CustomGlobalConstants.USER_TYPES.STANDARD], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.ACCESS_ENTITY },
        loadChildren: () => import('./views/accessEntity/accessEntity.module').then(m => m.AccessEntityModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.REPORTING,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.REPORTING },
        loadChildren: () => import('./views/reporting/reporting.module').then(m => m.ReportingModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.REPORTING + '/:reporting_id',
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.REPORTING },
        loadChildren: () => import('./views/reporting/reporting.module').then(m => m.ReportingModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.SETTINGS,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN, CustomGlobalConstants.USER_TYPES.STANDARD], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.SETTINGS },
        loadChildren: () => import('./views/settings/settings.module').then(m => m.SettingsModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.STANDARD_DASHBOARD,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN, CustomGlobalConstants.USER_TYPES.STANDARD], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.STANDARD_DASHBOARD },
        loadChildren: () => import('./views/allDashboard/standardDashboard/standardDashboard.module').then(m => m.StandardDashboardModule)
      },
      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.HR_DASHBOARD,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN, CustomGlobalConstants.USER_TYPES.HR], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.HR_DASHBOARD },
        loadChildren: () => import('./views/allDashboard/hrDashboard/hrDashboard.module').then(m => m.HRDashboardModule)
      },

      {
        path: CustomGlobalConstants.APP_MENU_PATH_NAMES.DB_CONNECTOR,
        canActivate: [AuthGuard],
        data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN], menuPathName: CustomGlobalConstants.APP_MENU_PATH_NAMES.DB_CONNECTOR },
        loadChildren: () => import('./views/connector/connector.module').then(m => m.ConnectorModule)
      },

      // {
      //   path: 'passwordManagement',
      //   data: { userTypeArray: [CustomGlobalConstants.USER_TYPES.ADMIN] },
      //   loadChildren: () => import('./views/passwordManagement/passwordManagement.module').then(m => m.PasswordManagementModule)
      // },


      // {
      //   path: 'systemEntity',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./views/systemEntity/systemEntity.module').then(m => m.SystementityModule)
      // },

      // {
      //   path: 'networkEntity',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./views/networkEntity/networkEntity.module').then(m => m.NetworkEntityModule)
      // },



      // {
      //   path: 'filesEntity',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./views/filesEntity/filesEntity.module').then(m => m.FilesEntityModule)
      // },


      // {
      //   path: 'applicationsEntity',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./views/applicationsEntity/applicationsEntity.module').then(m => m.ApplicationsEntityModule)
      // },

      // {
      //   path: 'accessPrivileges',
      //   loadChildren: () => import('./views/accessPrivileges/accessPrivileges.module').then(m => m.AccessPrivilegesModule)
      // },
      // {
      //   path: 'disasterRecovery',
      //   loadChildren: () => import('./views/disasterRecovery/disasterRecovery.module').then(m => m.DisasterRecoveryModule)
      // },
      // {
      //   path: 'notifications',
      //   loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      // },
      // {
      //   path: 'reporting',
      //   loadChildren: () => import('./views/reporting/reporting.module').then(m => m.ReportingModule)
      // },
      // {
      //   path: 'settings',
      //   loadChildren: () => import('./views/settings/settings.module').then(m => m.SettingsModule)
      // },



      // {
      //   path: 'basic',
      //   loadChildren: () => import('./components/basic/basic.module').then(m => m.BasicModule)
      // }, {
      //   path: 'notifications',
      //   loadChildren: () => import('./components/advance/notifications/notifications.module').then(m => m.NotificationsModule)
      // }, {
      //   path: 'forms',
      //   loadChildren: () => import('./components/forms/basic-elements/basic-elements.module').then(m => m.BasicElementsModule)
      // }, {
      //   path: 'bootstrap-table',
      //   loadChildren: () => import('./components/tables/bootstrap-table/basic-bootstrap/basic-bootstrap.module').then(m => m.BasicBootstrapModule),
      // }, {
      //   path: 'map',
      //   loadChildren: () => import('./map/google-map/google-map.module').then(m => m.GoogleMapModule),
      // }, {
      //   path: 'simple-page',
      //   loadChildren: () => import('./simple-page/simple-page.module').then(m => m.SimplePageModule)
      // }
    ]
  },
  // {
  //   path: '',
  //   component: AuthComponent,
  //   children: [
  //     {
  //       path: 'auth',
  //       loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  //     }
  //   ]
  // },
  // {
  //   path: '**',
  //   redirectTo: 'dashboard'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



