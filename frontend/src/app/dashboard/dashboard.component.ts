import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { Chart } from 'chart.js';
import { EntityDetail } from '../modules/models/sectors/EntityDetail.model';
import { SectorDetail } from '../modules/models/sectors/SectorDetail.model';
import { EventService } from '../modules/services/eventService.service';
import { CustomGlobalConstants } from '../modules/utils/CustomGlobalConstants';
import { CustomLogger } from '../modules/utils/CustomLogger';
import { BaseComponent } from '../shared/BaseComponent.component';
import { CustomMisc } from '../modules/utils/CustomMisc';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent extends BaseComponent implements OnInit {

  constructor(private _eventService: EventService, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.DASHBOARD, injector);
  }

  flagShowShortcuts = true;

  title = 'dashboard';
  chart;
  chart2 = [];
  pie: any;
  doughnut: any;
  data1 = [];

  systemEventsArr = [];
  totalApplicationEntities: any;
  totalNetworkEntities: any;
  totalSystemEntities: any;
  totalFileEntities: any;

  async ngOnInit() {
    await this.populateFields();

    if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.ADMIN) {
      this.flagShowShortcuts = true;
    } else {
      this.flagShowShortcuts = false;
    }


    let sectorDetailResult = await this._dbService.getAllSectorDetail().toPromise();
    CustomLogger.logStringWithObject("sectorDetailResult:", sectorDetailResult);
    let sectorDetailArr: SectorDetail[] = sectorDetailResult["data"];
    sectorDetailArr.forEach(sectorDetail => {
      if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_APPLICATIONS_ID) this.totalApplicationEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_NETWORKS_ID) this.totalNetworkEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_SYSTEMS_ID) this.totalSystemEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_FILES_ID) this.totalFileEntities = sectorDetail.entity_id_arr.length;
    });


    this.chart = new Chart('line', {
      type: 'line',
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Usage Status'
        },
      },
      data: {
        labels: ['Day 1', 'Day 2', 'Day 3', 'Day 4'],
        datasets: [
          {
            type: 'line',
            label: 'Active User',
            data: [243, 156, 365, 400],
            backgroundColor: '#229186',
            borderColor: '#229186',
            fill: false,
          },

          {
            type: 'line',
            label: 'Inactive User',
            data: [273, 156, 365, 470].reverse(),
            backgroundColor: '#224f91',
            borderColor: '#224f91',
            fill: false,
          },
          {
            type: 'line',
            label: 'New User',
            data: [243, 156, 36, 510].reverse(),
            backgroundColor: '#912254',
            borderColor: '#912254',
            fill: false,
          },
          {
            type: 'line',
            label: 'Other User',
            data: [243, 156, 536, 310].reverse(),
            backgroundColor: '#8d9122',
            borderColor: '#8d9122',
            fill: false,
          }
        ]
      }
    });
    this.chart = new Chart('bar', {
      type: 'bar',
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Usage Status'
        },
      },
      data: {
        labels: ['Day 1', 'Day 2', 'Day 3', 'Day 4'],
        datasets: [
          {
            type: 'bar',
            label: 'Active User',
            data: [243, 156, 365, 400],
            backgroundColor: '#229186',
            borderColor: '#229186',
            fill: false,
          },

          {
            type: 'bar',
            label: 'Inactive User',
            data: [273, 156, 365, 470].reverse(),
            backgroundColor: '#224f91',
            borderColor: '#224f91',
            fill: false,
          },
          {
            type: 'bar',
            label: 'New User',
            data: [243, 156, 36, 510].reverse(),
            backgroundColor: '#912254',
            borderColor: '#912254',
            fill: false,
          },
          {
            type: 'bar',
            label: 'Other User',
            data: [243, 156, 536, 310].reverse(),
            backgroundColor: '#8d9122',
            borderColor: '#8d9122',
            fill: false,
          }
        ]
      }
    });



    ////////////////////// PIE CHARTS
    let allBackgroundColorArr = ["#94d1df", "#df94c6", "#dcdf94", "#9498df", "#5096b5", "#b5a750", "#7fb550", "#b150b5", "#C2272D", "#F8931F", "#009245", "#0193D9", "#0C04ED", "#612F90"];
    let allSampleDataArr = [32, 11, 58, 32, 98, 43, 32, 66, 99, 55, 41, 30, 40, 20, 25, 15, 33, 13, 98, 90, 25];

    //1 - Applications
    let applicationEntityNameArr = [];
    let applicationEntityBackgroundColorArr = [];
    let applicationEntityDataArr = [];
    let applicationEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_APPLICATIONS_ID).toPromise();
    let applicationSector: SectorDetail = applicationEntityResult["data"];
    CustomLogger.logStringWithObject("applicationSector:", applicationSector);
    CustomLogger.logStringWithObject("applicationSector.entity_id_arr:", applicationSector.entity_id_arr);
    let multipleEntityIdArrResult = await this._dbService.getMultipleEntityDetail(applicationSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleEntityIdArrResult:", multipleEntityIdArrResult);
    let multipleEntityDetailArr: EntityDetail[] = multipleEntityIdArrResult["data"];
    for (let k = 0; k < multipleEntityDetailArr.length; k++) {
      applicationEntityNameArr.push(multipleEntityDetailArr[k].entity_name);
      applicationEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      applicationEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie3', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: applicationEntityDataArr,
          backgroundColor: applicationEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: applicationEntityNameArr
      }
    });


    //2 - Networks
    let networkEntityNameArr = [];
    let networkEntityBackgroundColorArr = [];
    let networkEntityDataArr = [];
    let networkEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_NETWORKS_ID).toPromise();
    let networkSector: SectorDetail = networkEntityResult["data"];
    CustomLogger.logStringWithObject("networkSector:", networkSector);
    CustomLogger.logStringWithObject("networkSector.entity_id_arr:", networkSector.entity_id_arr);
    let multipleNetworkEntityIdArrResult = await this._dbService.getMultipleEntityDetail(networkSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleNetworkEntityIdArrResult:", multipleNetworkEntityIdArrResult);
    let multipleNetworkEntityDetailArr: EntityDetail[] = multipleNetworkEntityIdArrResult["data"];
    for (let k = 0; k < multipleNetworkEntityDetailArr.length; k++) {
      networkEntityNameArr.push(multipleNetworkEntityDetailArr[k].entity_name);
      networkEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      networkEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    CustomLogger.logStringWithObject("networkEntityNameArr:::", networkEntityNameArr);
    this.pie = new Chart('pie1', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: networkEntityDataArr,
          backgroundColor: networkEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: networkEntityNameArr
      }
    })


    //3 - Systems
    let systemEntityNameArr = [];
    let systemEntityBackgroundColorArr = [];
    let systemEntityDataArr = [];
    let systemEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_SYSTEMS_ID).toPromise();
    let systemSector: SectorDetail = systemEntityResult["data"];
    CustomLogger.logStringWithObject("systemSector:", systemSector);
    CustomLogger.logStringWithObject("systemSector.entity_id_arr:", systemSector.entity_id_arr);
    let multipleSystemEntityIdArrResult = await this._dbService.getMultipleEntityDetail(systemSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleSystemEntityIdArrResult:", multipleSystemEntityIdArrResult);
    let multipleSystemEntityDetailArr: EntityDetail[] = multipleSystemEntityIdArrResult["data"];
    for (let k = 0; k < multipleSystemEntityDetailArr.length; k++) {
      systemEntityNameArr.push(multipleSystemEntityDetailArr[k].entity_name);
      systemEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      systemEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        }
      },
      data: {
        datasets: [{
          data: systemEntityDataArr,
          backgroundColor: systemEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: systemEntityNameArr
      }
    })

    //4 - Files
    let fileEntityNameArr = [];
    let fileEntityBackgroundColorArr = [];
    let fileEntityDataArr = [];
    let fileEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_FILES_ID).toPromise();
    let fileSector: SectorDetail = fileEntityResult["data"];
    CustomLogger.logStringWithObject("fileSector:", fileSector);
    CustomLogger.logStringWithObject("fileSector.entity_id_arr:", fileSector.entity_id_arr);
    let multipleFileEntityIdArrResult = await this._dbService.getMultipleEntityDetail(fileSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleFileEntityIdArrResult:", multipleFileEntityIdArrResult);
    let multipleFileEntityDetailArr: EntityDetail[] = multipleFileEntityIdArrResult["data"];
    for (let k = 0; k < multipleFileEntityDetailArr.length; k++) {
      fileEntityNameArr.push(multipleFileEntityDetailArr[k].entity_name);
      fileEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      fileEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie2', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: fileEntityDataArr,
          backgroundColor: fileEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: fileEntityNameArr
      }
    })


    //get all system events
    let queryLimit = 50;
    let systemEventResult = await this._eventService.getAllSystemEvents(queryLimit).toPromise();
    CustomLogger.logStringWithObject("systemEventResult:::", systemEventResult);
    this.systemEventsArr = systemEventResult["data"];

  }


  addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    chart.update();
  }

  removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
      dataset.data.pop();
    });
    chart.update();
  }

  updateChartData(chart, data, dataSetIndex) {
    chart.data.datasets[dataSetIndex].data = data;
    chart.update();
  }

  SHORTCUT_RESTORATION = 1;
  SHORTCUT_HR_RESTORATION = 2;

  async onShortcutClick(shortcut) {
    try {
      let optionChosen: any;
      switch (shortcut) {
        case this.SHORTCUT_RESTORATION:
          optionChosen = await this._miscService.confirmDialogBox("Run", "Are you sure you want to run the restoration ?");
          CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
          if (optionChosen) {
            let startTime = CustomMisc.getCurrentTimeInMilli();
            CustomLogger.logString("Will run restoration");
            let result = await this._dbService.runRestoration().toPromise();
            let totalTime = CustomMisc.getCurrentTimeInMilli() - startTime;
            CustomMisc.showAlert("Restoration was successful and it took : " + totalTime + " ms.");
            CustomLogger.logStringWithObject("runRestoration:result:", result);
          }
          break;

        case this.SHORTCUT_HR_RESTORATION:
          optionChosen = await this._miscService.confirmDialogBox("Run", "Are you sure you want to Collaborate Users from HR  ?");
          CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
          if (optionChosen) {
            let startTime = CustomMisc.getCurrentTimeInMilli();
            CustomLogger.logString("Will run HR restoration");
            let result = await this._dbService.runHRRestoration().toPromise();
            let totalTime = CustomMisc.getCurrentTimeInMilli() - startTime;
            CustomMisc.showAlert("HR Restoration was successful and it took : " + totalTime + " ms.");
            CustomLogger.logStringWithObject("runHRRestoration:result:", result);
          }
          break;

        default:
          CustomLogger.logString("Invalid Shortcut");
          break;
      }
    } catch (error) {
      CustomLogger.logStringWithObject("Error:", error);
    }

  }
}
