import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTableModule } from 'angular-6-datatable';
import { NotificationsRoutes } from './notifications.routing';
import { ListNotificationsComponent } from './listNotifications.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forChild(NotificationsRoutes),
    FormsModule,
    NgxDatatableModule,
    DataTableModule
  ],
  declarations: [
    ListNotificationsComponent
  ]
})
export class NotificationsModule { }
