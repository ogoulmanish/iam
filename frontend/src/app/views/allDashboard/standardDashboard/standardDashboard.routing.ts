import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StandardDashboardComponent } from './standardDashboard.component';

const routes: Routes = [
    {
        path: '',
        component: StandardDashboardComponent,
        data: {
            breadcrumb: 'Dashboard'
        }
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StandardDashboardRoutingModule { }