import { Component, ViewEncapsulation } from '@angular/core';
import { Chart } from 'chart.js';
import { DBService } from 'src/app/modules/services/dbService.service';
import { GlobalService } from 'src/app/modules/services/global.service';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';

declare var $: any;

@Component({
    selector: 'app-standardDashboard',
    templateUrl: './standardDashboard.component.html',
    encapsulation: ViewEncapsulation.None
})
export class StandardDashboardComponent {
    constructor(private _dbService: DBService, private _globalService:GlobalService) {
    }
    title = 'dashboard';
    chart;
    chart2 = [];
    pie: any;
    doughnut: any;
    data1 = [];
    
    async ngOnInit() {
        ////////////////////// PIE CHARTS
        let allBackgroundColorArr = ["#94d1df", "#df94c6", "#dcdf94", "#9498df", "#5096b5", "#b5a750", "#7fb550", "#b150b5", "#C2272D", "#F8931F", "#009245", "#0193D9", "#0C04ED", "#612F90"];
        let allSampleDataArr = [32, 11, 58, 32, 98, 43, 32, 66, 99, 55, 41, 30, 40, 20, 25, 15, 33, 13, 98, 90, 25];

        //1 - Entities
        //from role id get the entities it can access
        let userDetail = this._globalService.getCurrentUserDetail();
        let applicationEntityNameArr = [];
        let applicationEntityBackgroundColorArr = [];
        let applicationEntityDataArr = [];
        
        let result = await this._dbService.getAllEntityDetailsOfMappingUser(userDetail.user_id).toPromise();
        CustomLogger.logStringWithObject("getAllEntityDetailsOfMappingUser:::", result);
        let multipleEntityDataArr = result["data"];
        for (let k = 0; k < multipleEntityDataArr.length; k++) {
            applicationEntityNameArr.push(multipleEntityDataArr[k].entity_name);
            applicationEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
            applicationEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
        }
        this.pie = new Chart('pie', {
            type: 'pie',
            options: {
                responsive: true,
                title: {
                    display: true,
                }, legend: {
                    position: 'right',
                }, animation: {
                    animateScale: true,
                    animateRotate: true
                },
            },
            data: {
                datasets: [{
                    data: applicationEntityDataArr,
                    backgroundColor: applicationEntityBackgroundColorArr,
                    label: 'Dataset 1'
                }],
                labels: applicationEntityNameArr
            }
        });
    }
}

