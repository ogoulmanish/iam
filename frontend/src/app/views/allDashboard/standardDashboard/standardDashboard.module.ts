import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular-6-datatable';
import { StandardDashboardComponent } from './standardDashboard.component';
import { StandardDashboardRoutingModule } from './standardDashboard.routing';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DataTableModule,
        StandardDashboardRoutingModule
    ],
    declarations: [
        StandardDashboardComponent
    ]
})
export class StandardDashboardModule { } 