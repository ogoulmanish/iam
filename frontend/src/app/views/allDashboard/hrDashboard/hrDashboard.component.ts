import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { Chart } from 'chart.js';
import { ActivatedRoute, Router } from '@angular/router';
import { DBService } from 'src/app/modules/services/dbService.service';
import { GlobalService } from 'src/app/modules/services/global.service';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';

declare var $: any;


@Component({
    selector: 'app-hrDashboard',
    templateUrl: './hrDashboard.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HRDashboardComponent extends BaseComponent implements OnInit {


    allBackgroundColorArr = ["#94d1df", "#df94c6", "#dcdf94", "#9498df", "#5096b5", "#b5a750", "#7fb550", "#b150b5", "#C2272D", "#F8931F", "#009245", "#0193D9", "#0C04ED", "#612F90"];
    allSampleDataArr = [32, 11, 58, 32, 98, 43, 32, 66, 99, 55, 41, 30, 40, 20, 25, 15, 33, 13, 98, 90, 25];

    pie: any;


    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.DASHBOARD, injector)
    }

    async ngOnInit() {

        let result = await this._dbService.getAllUserDetail().toPromise();
        let userDetailArr: UserDetail[] = result["data"];
        

        let totalActiveUsers = 0;
        let totalInactiveUsers = 0;
        let totalUsersAddedToMain = 0;
        let totalUsersNotAddedToMain = 0;
        for (let k = 0; k < userDetailArr.length; k++) {
            let userDetail = userDetailArr[k];
            if (userDetail.is_active) totalActiveUsers += 1;
            else totalInactiveUsers += 1;
            if (userDetail.is_added_to_main) totalUsersAddedToMain += 1;
            else totalUsersNotAddedToMain += 1;
        }

        //pie1
        let label1Arr = [];
        let data1Arr = [];
        let backgroundColor1Arr = [];
        label1Arr.push("Active Users");
        label1Arr.push("Inactive Users");
        backgroundColor1Arr.push(this.allBackgroundColorArr[0]);
        backgroundColor1Arr.push(this.allBackgroundColorArr[1]);
        data1Arr.push(totalActiveUsers);
        data1Arr.push(totalInactiveUsers);
        this.pie = new Chart('pie1', {
            type: 'pie',
            options: {
                responsive: true,
                title: {
                    display: true,
                }, legend: {
                    position: 'right',
                }, animation: {
                    animateScale: true,
                    animateRotate: true
                },
            },
            data: {
                datasets: [{
                    data: data1Arr,
                    backgroundColor: backgroundColor1Arr,
                    label: 'Dataset 1'
                }],
                labels: label1Arr
            }
        });

        
        //pie1
        let label2Arr = [];
        let data2Arr = [];
        let backgroundColor2Arr = [];
        label2Arr.push("Total Users In Main");
        label2Arr.push("Total Users Not In Main");
        backgroundColor2Arr.push(this.allBackgroundColorArr[2]);
        backgroundColor2Arr.push(this.allBackgroundColorArr[3]);
        data2Arr.push(totalUsersAddedToMain);
        data2Arr.push(totalUsersNotAddedToMain);
        this.pie = new Chart('pie2', {
            type: 'pie',
            options: {
                responsive: true,
                title: {
                    display: true,
                }, legend: {
                    position: 'right',
                }, animation: {
                    animateScale: true,
                    animateRotate: true
                },
            },
            data: {
                datasets: [{
                    data: data2Arr,
                    backgroundColor: backgroundColor2Arr,
                    label: 'Dataset 1'
                }],
                labels: label2Arr
            }
        });

    }
}
