import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular-6-datatable';
import { HRDashboardRoutingModule } from './hrDashboard.routing';
import { HRDashboardComponent } from './hrDashboard.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DataTableModule,
        HRDashboardRoutingModule
    ],
    declarations: [
        HRDashboardComponent
    ]
})
export class HRDashboardModule { } 