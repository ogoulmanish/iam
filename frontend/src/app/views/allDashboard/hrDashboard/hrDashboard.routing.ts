import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HRDashboardComponent } from './hrDashboard.component';

const routes: Routes = [
    {
        path: '',
        component: HRDashboardComponent,
        data: {
            breadcrumb: 'Dashboard'
        }
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HRDashboardRoutingModule { }