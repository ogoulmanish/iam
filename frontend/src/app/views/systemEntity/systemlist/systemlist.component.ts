import { Component, OnInit } from '@angular/core';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';

@Component({
  selector: 'app-systemlist',
  templateUrl: './systemlist.component.html',
  styleUrls: ['./systemlist.component.css']
})
export class SystemlistComponent implements OnInit {


  entitydetailsArr = [];
  entityDetail = new EntityDetail();
  entityDetail2 = new EntityDetail();
  entityDetail3 = new EntityDetail();
  entityDetail4 = new EntityDetail();
  entityDetail5 = new EntityDetail();
  entityDetail6 = new EntityDetail();

  constructor() { }
  ngOnInit() {
    this.entityDetail.entity_id = '1';
    this.entityDetail.entity_name = 'Linux';
    this.entityDetail.access_location = '';
    this.entityDetail.icon_location = "assets/images/sectors/systems/linux.png";


    this.entityDetail2.entity_id = '10';
    this.entityDetail2.entity_name = 'Windows';
    this.entityDetail2.access_location = '';
    this.entityDetail2.icon_location = "assets/images/sectors/systems/windows.png";


    this.entityDetail3.entity_id = '10';
    this.entityDetail3.entity_name = 'Unix';
    this.entityDetail3.access_location = '';
    this.entityDetail3.icon_location = "assets/images/sectors/systems/unix.png";



    this.entityDetail4.entity_id = '4';
    this.entityDetail4.entity_name = 'Apple';
    this.entityDetail4.access_location = '';
    this.entityDetail4.icon_location = "assets/images/sectors/systems/apple.png";




    this.entitydetailsArr.push(this.entityDetail)
    this.entitydetailsArr.push(this.entityDetail2)
    this.entitydetailsArr.push(this.entityDetail3)
    this.entitydetailsArr.push(this.entityDetail4)
  }

}