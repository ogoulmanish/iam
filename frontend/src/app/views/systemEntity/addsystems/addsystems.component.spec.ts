import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsystemsComponent } from './addsystems.component';

describe('AddsystemsComponent', () => {
  let component: AddsystemsComponent;
  let fixture: ComponentFixture<AddsystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
