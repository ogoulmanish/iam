import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemlistComponent } from './systemlist/systemlist.component';
import { AddsystemsComponent } from './addsystems/addsystems.component';


const routes: Routes = [
    {
      path: '',
      data: {
        breadcrumb: 'Systems',
        status: false
      },
      children: [
        
        {
          path: 'systemlist',
          component: SystemlistComponent
        }, 
        {
          path: 'addsystems',
          component: AddsystemsComponent
        }, 
       
      ]
    }
  ];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class SystementityRoutingModule { }
  