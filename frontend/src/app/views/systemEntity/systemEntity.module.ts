import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SystementityRoutingModule } from './systemEntity.routing';
import { SystemlistComponent } from './systemlist/systemlist.component';
import { AddsystemsComponent } from './addsystems/addsystems.component';



@NgModule({
    imports: [
      CommonModule,

      SystementityRoutingModule
    ],
    declarations: [
       
    SystemlistComponent,
       
    AddsystemsComponent],
    // entryComponents: [ButtonRenderComponent],
  })
  export class SystementityModule { }