import { Component, OnInit } from '@angular/core';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';


@Component({
  selector: 'app-fileslist',
  templateUrl: './fileslist.component.html',
  styleUrls: ['./fileslist.component.css']
})
export class FileslistComponent implements OnInit {


  entitydetailsArr = [];
  entityDetail = new EntityDetail();
  entityDetail2 = new EntityDetail();
  entityDetail3 = new EntityDetail();
  entityDetail4 = new EntityDetail();
  entityDetail5 = new EntityDetail();
  entityDetail6 = new EntityDetail();

  constructor() { }
  ngOnInit() {
    this.entityDetail.entity_id = '1';
    this.entityDetail.entity_name = 'Encrypted File';
    this.entityDetail.access_location = '';
    this.entityDetail.icon_location = "assets/images/sectors/files/file1.png";

    this.entityDetail2.entity_id = '10';
    this.entityDetail2.entity_name = 'Configuration File';
    this.entityDetail2.access_location = '';
    this.entityDetail2.icon_location = "assets/images/sectors/files/file2.png";

    this.entitydetailsArr.push(this.entityDetail)
    this.entitydetailsArr.push(this.entityDetail2)
  }

}