import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilesEntityRoutingModule } from './filesEntity.routing';
import { FileslistComponent } from './fileslist/fileslist.component';



@NgModule({
    imports: [
      CommonModule,

      FilesEntityRoutingModule
    ],
    declarations: [
       
       
    FileslistComponent],
    // entryComponents: [ButtonRenderComponent],
  })
  export class FilesEntityModule { }