import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FileslistComponent} from './fileslist/fileslist.component';





const routes: Routes = [
    {
      path: '',
      data: {
        breadcrumb: 'Files',
        status: false
      },
      children: [
        // {
        //   path: 'listDevices',
        //   component: ListDevicesComponent
        // }, 
        {
          path: 'filesList',
          component: FileslistComponent
        }, 
       
      ]
    }
  ];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class FilesEntityRoutingModule { }
  