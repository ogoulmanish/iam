import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
  selector: 'app-listEntity',
  templateUrl: './listEntity.component.html',
  styleUrls:['./listEntity.component.css']
})
export class ListEntityComponent extends BaseComponent implements OnInit {
  field_listEntityComponent_info = "List of all Entities (Applications/Resources etc.).";
  tableDataArr = [];
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY, injector);
  }

  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  async init() {
    let result = await this._dbService.getAllEntityDetail().toPromise();
    CustomLogger.logStringWithObject("getAllEntityDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this._router.navigate(['crudEntity'], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
  }

  onClickEdit(entityDetail: EntityDetail) {
    CustomLogger.logStringWithObject("entity:", entityDetail);
    this._router.navigate(['crudEntity', CustomGlobalConstants.CRUD_UPDATE, entityDetail.entity_id], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
  }

  async onClickDelete(obj: EntityDetail) {
    try {
      let tmpStr = "entity";
      if (!obj.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_ENTITY.listEntity_Entity_cannot_be_deleted), true);
      } else {
        await this._miscService.commonApprovalForDelete(obj, CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY, tmpStr);
      }

    } catch (error) {
      CustomLogger.logError(error);
    }
    await this.init();
  }

  filteredTableDataArr: any;
  search(term: string) {
    let fieldName = "entity_name";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }
};

