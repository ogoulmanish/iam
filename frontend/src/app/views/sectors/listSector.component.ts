import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SectorDetail } from 'src/app/modules/models/sectors/SectorDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { ApprovalMatrix } from 'src/app/modules/models/common/ApprovalMatrix.model';


@Component({
  selector: 'app-listSector',
  templateUrl: './listSector.component.html'
})
export class ListSectorComponent extends BaseComponent implements OnInit {
  field_listSectorComponent_info = "List of all sectors (groups) in the system.";
  tableDataArr = [];
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR, injector);
  }


  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  async init() {
    let result = await this._dbService.getAllSectorDetail().toPromise();
    CustomLogger.logStringWithObject("getAllSectorDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this.router.navigate(['crudSector'], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  onClickEdit(sector: SectorDetail) {
    CustomLogger.logStringWithObject("sector:", sector);
    this.router.navigate(['crudSector', CustomGlobalConstants.CRUD_UPDATE, sector.sector_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  async onClickDelete(sectorDetail: SectorDetail) {
    try {
      let tmpStr = "sector";
      if (!sectorDetail.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_SECTOR.listSector_Sector_cannot_be_deleted), true);
      } else {
        await this._miscService.commonApprovalForDelete(sectorDetail, CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR, tmpStr);
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }
  }

  // async onClickDelete(sectorDetail: SectorDetail) {
  //   try {
  //     let tmpStr = "sector";
  //     if (!sectorDetail.can_be_deleted) {
  //       CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_SECTOR.listSector_Sector_cannot_be_deleted), true);
  //     } else {
  //       let optionChosen = await this._miscService.confirmDialogBox("Delete", "Are you sure you want to delete this " + tmpStr + " ?");
  //       CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
  //       if (optionChosen) {
  //         let approvalMatrix: ApprovalMatrix = this._miscService.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR);
  //         CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
  //         if (approvalMatrix != null && approvalMatrix.delete_permission) {
  //           CustomLogger.logString("APPROVAL REQUIRED");
  //           await this._miscService.sendMakerCheckerRequestForDelete(sectorDetail, CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR);
  //         } else {
  //           CustomLogger.logString("APPROVAL NOT REQUIRED");
  //           let result = await this._dbService.commonDelete(sectorDetail).toPromise();
  //           CustomLogger.logStringWithObject("delete:result:", result);
  //           CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_SECTOR.listSector_Sector_deleted_success));
  //         }
  //         await this.init();
  //       }
  //     }
  //   } catch (error) {
  //     CustomLogger.logError(error);
  //   }
  // }


  filteredTableDataArr: any;
  search(term: string) {
    let fieldName = "sector_name";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }

};
