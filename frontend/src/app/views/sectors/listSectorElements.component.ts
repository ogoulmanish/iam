import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { SectorDetail } from 'src/app/modules/models/sectors/SectorDetail.model';
import { DBService } from 'src/app/modules/services/dbService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';

@Component({
  selector: 'app-listSectorElements',
  templateUrl: './listSectorElements.component.html',
  styleUrls: ['./listSectorElements.component.css']
})
export class ListSectorElementsComponent implements OnInit {
  field_listSectorElementsComponent_info = "List of all Entities that belong to this particular Sector";
  currentSectorDetail: SectorDetail;
  currentSectorDetailID: string;
  currentSectorDetailName: string = "Default";
  sectorDetailArr: SectorDetail[] = [];
  entityDetailArr: EntityDetail[] = [];

  PICKLIST_TYPE_OF_SECTORS = [];
  sectorDetail: SectorDetail;

  entitydetailsArr = [];
  entityDetail = new EntityDetail();
  entityDetail2 = new EntityDetail();
  entityDetail3 = new EntityDetail();
  entityDetail4 = new EntityDetail();
  entityDetail5 = new EntityDetail();
  entityDetail6 = new EntityDetail();
  IMAGES_BASE_LOCATION: any;

  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute) {
    this.sectorDetail = new SectorDetail();
    this.PICKLIST_TYPE_OF_SECTORS = CustomGlobalConstants.SECTORS_ARRAY;
    this.IMAGES_BASE_LOCATION = CustomGlobalConstants.BACKEND_SERVER_URL;
  }

  async fillEntityDetailArr() {
    CustomLogger.logStringWithObject("Sector will change..", this.currentSectorDetailID);
    let result = await this._dbService.getSpecificSectorDetail(this.currentSectorDetailID).toPromise();
    this.currentSectorDetail = result["data"];
    this.currentSectorDetailName = this.currentSectorDetail.sector_name;
    result = await this._dbService.getMultipleEntityDetail(this.currentSectorDetail.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("entities:::", result);
    this.entityDetailArr = result["data"];
  }

  async ngOnInit() {

    let result = await this._dbService.getAllSectorDetail().toPromise();
    this.sectorDetailArr = result["data"];
    this.currentSectorDetail = this.sectorDetailArr[0];
    this.currentSectorDetailID = this.currentSectorDetail.sector_id;
    this.currentSectorDetailName = this.currentSectorDetail.sector_name;
    this.fillEntityDetailArr();

  }

  async onSectorChange() {
    this.fillEntityDetailArr();
  }

  onClick(entityDetail: EntityDetail) {
    CustomLogger.logStringWithObject("onClick:obj:", entityDetail);
    this._router.navigate(['crudEntity', CustomGlobalConstants.CRUD_UPDATE, entityDetail.entity_id], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
  }

}