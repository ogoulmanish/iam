import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSectorComponent } from './listSector.component';
import { CrudSectorComponent } from './crudSector.component';
import { ListSectorElementsComponent } from './listSectorElements.component';
import { ListEntityComponent } from './listEntity.component';
import { CrudEntityComponent } from './crudEntity.component';


const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Sectors',
      status: false
    },
    children: [
      {
        path: 'listSector',
        component: ListSectorComponent
      },
      {
        path: 'listSectorElements',
        component: ListSectorElementsComponent
      },
      {
        path: 'crudSector',
        component: CrudSectorComponent
      },
      {
        path: 'crudSector/:crudType/:id',
        component: CrudSectorComponent
      },
      {
        path: 'listEntity',
        component: ListEntityComponent
      },
      {
        path: 'crudEntity',
        component: CrudEntityComponent
      },
      {
        path: 'crudEntity/:crudType/:id',
        component: CrudEntityComponent
      },
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SectorRoutingModule { }
