import { HttpClient } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { MongoDBConnectorModel } from 'src/app/modules/models/connectors/database/MongoDBConnectorModel.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../shared/BaseComponent.component';
import { ActiveDirectoryConnectorModel } from 'src/app/modules/models/connectors/ActiveDirectoryConnectorModel.model';
import { LinuxConnectorModel } from 'src/app/modules/models/connectors/LinuxConnectorModel.model';
import Stepper from 'bs-stepper';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-crudEntity',
  templateUrl: './crudEntity.component.html',
  styleUrls:['./crudEntity.component.css']
})
export class CrudEntityComponent extends BaseComponent implements OnInit {
  field_crudEntity_info = "All Entity details.";
  field_crudEntity_risk_level = "Risk Level";
  field_crudEntity_connector_type = "Connector Type";

  field_connector_connector_name = "Connector Name";
  field_connector_connector_type = "Connector Type";
  field_connector_host_name = "Hostname";
  field_connector_host_username = "Host Username";
  field_connector_host_password = "Host Password";
  field_connector_host_port = "Host Port";
  field_connector_database_name = "Database Name";

  field_crudEntity_activeDirectoryConnector_container_dn = "Container DN";
  field_crudEntity_activeDirectoryConnector_container_dn_required = true;
  field_crudEntity_activeDirectoryConnector_container_error_message = "Error";
  field_crudEntity_activeDirectoryConnector_host_name = "AD Hostname";
  field_crudEntity_activeDirectoryConnector_host_name_required = true;
  field_crudEntity_activeDirectoryConnector_host_name_error_message = "Error";
  field_crudEntity_activeDirectoryConnector_host_username = "AD Username";
  field_crudEntity_activeDirectoryConnector_host_username_required = true;
  field_crudEntity_activeDirectoryConnector_host_username_error_message = "Error";
  field_crudEntity_activeDirectoryConnector_host_password = "AD Password";
  field_crudEntity_activeDirectoryConnector_host_password_required = true;
  field_crudEntity_activeDirectoryConnector_host_password_error_message = "Error";
  field_crudEntity_activeDirectoryConnector_host_port = "AD Port";
  field_crudEntity_activeDirectoryConnector_host_port_required = true;
  field_crudEntity_activeDirectoryConnector_host_port_error_message = "Error";

  field_crudEntity_linuxConnector_host_name = "AD Hostname";
  field_crudEntity_linuxConnector_host_name_required = true;
  field_crudEntity_linuxConnector_host_name_error_message = "Error";
  field_crudEntity_linuxConnector_host_username = "AD Username";
  field_crudEntity_linuxConnector_host_username_required = true;
  field_crudEntity_linuxConnector_host_username_error_message = "Error";
  field_crudEntity_linuxConnector_host_password = "AD Password";
  field_crudEntity_linuxConnector_host_password_required = true;
  field_crudEntity_linuxConnector_host_password_error_message = "Error";
  field_crudEntity_linuxConnector_host_port = "AD Port";
  field_crudEntity_linuxConnector_host_port_required = true;
  field_crudEntity_linuxConnector_host_port_error_message = "Error";

  flagShowDefaultIcon = true;

  images;
  tmpIconLocation: any;
  entityDetail: EntityDetail;
  riskLevelFormElementArr: FormElement[];

  PICKLIST_ACTUAL_CONNECTOR_TYPES = [];
  PICKLIST_CONTAINER_DN = [];
  IMAGES_BASE_LOCATION: any;


  mongodbConnector: MongoDBConnectorModel;
  activeDirectoryConnector: ActiveDirectoryConnectorModel;
  linuxConnector: LinuxConnectorModel;
  stepper1: Stepper;
  


  constructor(private _router: Router, private activatedRoute: ActivatedRoute, private http: HttpClient, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY, injector);
    this.mongodbConnector = new MongoDBConnectorModel();
    this.activeDirectoryConnector = new ActiveDirectoryConnectorModel();
    this.linuxConnector = new LinuxConnectorModel();
    this.IMAGES_BASE_LOCATION = CustomGlobalConstants.BACKEND_SERVER_URL;
  }

 
  async ngOnInit() {
    this.entityDetail = new EntityDetail();
    await this.populateFields();
    this.PICKLIST_ACTUAL_CONNECTOR_TYPES = CustomGlobalConstants.ARRAY_ACTUAL_CONNECTORS;
    this.PICKLIST_CONTAINER_DN = CustomGlobalConstants.ARRAY_ACTIVE_DIRECTORY_DEPARTMENT_DN;
    this.activeDirectoryConnector.container_dn = CustomGlobalConstants.ARRAY_ACTIVE_DIRECTORY_DEPARTMENT_DN[0].id;

    //fill up combo boxes
    let result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudEntity_risk_level).toPromise();
    this.riskLevelFormElementArr = result["data"];
    this.entityDetail.risk_level_id = this.riskLevelFormElementArr[0].data_value;

    this.activatedRoute.params.subscribe(
      async params => {
        CustomLogger.logStringWithObject("Params Values ::: ", params);
        let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
        if (tmpCrudType) {
          this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
          if (!this.isComponentNew) {
            let entity_id = params["id"];
            let result = await this._dbService.getSpecificEntityDetail(entity_id).toPromise();
            CustomLogger.logStringWithObject("getSpecificEntityDetail:result:", result);
            this.entityDetail = result["data"];

            //check connector type
            this.loadConnectorObject();
            this.flagShowDefaultIcon = false;
          }
        }
      }
    );
    this.stepper1 = new Stepper(document.querySelector('#stepper1'), {
      linear: false,
      animation: true
    })
  }

  loadConnectorObject() {
    if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.MONGODB) {
      this.mongodbConnector = new MongoDBConnectorModel();
      let tmp1: MongoDBConnectorModel = <MongoDBConnectorModel>this.entityDetail.connector_object;
      this.mongodbConnector.connector_category = tmp1.connector_category;
      this.mongodbConnector.connector_name = tmp1.connector_name;
      this.mongodbConnector.connector_type = tmp1.connector_type;
      this.mongodbConnector.host_username = tmp1.host_username;
      this.mongodbConnector.host_name = tmp1.host_name;
      this.mongodbConnector.host_password = tmp1.host_password;
      this.mongodbConnector.host_port = tmp1.host_port;
      this.mongodbConnector.database_name = tmp1.database_name;
    } else if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY) {
      this.activeDirectoryConnector = new ActiveDirectoryConnectorModel();
      let tmp1: ActiveDirectoryConnectorModel = <ActiveDirectoryConnectorModel>this.entityDetail.connector_object;
      this.activeDirectoryConnector.connector_category = tmp1.connector_category;
      this.activeDirectoryConnector.connector_name = tmp1.connector_name;
      this.activeDirectoryConnector.connector_type = tmp1.connector_type;
      this.activeDirectoryConnector.container_dn = tmp1.container_dn;
      this.activeDirectoryConnector.host_username = tmp1.host_username;
      this.activeDirectoryConnector.host_name = tmp1.host_name;
      this.activeDirectoryConnector.host_password = tmp1.host_password;
      this.activeDirectoryConnector.host_port = tmp1.host_port;
      this.activeDirectoryConnector.details = tmp1.details;
      this.activeDirectoryConnector.model_name = tmp1.model_name;
    } else if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.LINUX) {
      this.linuxConnector = new LinuxConnectorModel();
      let tmp1: ActiveDirectoryConnectorModel = <ActiveDirectoryConnectorModel>this.entityDetail.connector_object;
      this.linuxConnector.connector_category = tmp1.connector_category;
      this.linuxConnector.connector_name = tmp1.connector_name;
      this.linuxConnector.connector_type = tmp1.connector_type;
      this.linuxConnector.host_username = tmp1.host_username;
      this.linuxConnector.host_name = tmp1.host_name;
      this.linuxConnector.host_password = tmp1.host_password;
      this.linuxConnector.host_port = tmp1.host_port;
      this.linuxConnector.details = tmp1.details;
      this.linuxConnector.model_name = tmp1.model_name;
    }
  }

  async onSubmit() {
   
   

    CustomLogger.logStringWithObject("Will save entity:", this.entityDetail);
    CustomLogger.logStringWithObject("MongoDB connector:", this.mongodbConnector);
    CustomLogger.logStringWithObject("ActiveDirectory connector:", this.activeDirectoryConnector);
    CustomLogger.logStringWithObject("Linux connector:", this.linuxConnector);

    if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.MONGODB) {
      this.entityDetail.access_location = "mongodb://" + this.mongodbConnector.host_name + ":"
        + this.mongodbConnector.host_port;
      this.mongodbConnector.connector_name = this.entityDetail.entity_name;
      this.entityDetail.connector_object = this.mongodbConnector;
    } else if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY) {
      this.entityDetail.access_location = "ldap://" + this.activeDirectoryConnector.host_name + ":"
        + this.activeDirectoryConnector.host_port + "/" + this.activeDirectoryConnector.container_dn;
      this.activeDirectoryConnector.connector_name = this.entityDetail.entity_name;
      this.entityDetail.connector_object = this.activeDirectoryConnector;
    } else if (this.entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.LINUX) {
      this.entityDetail.access_location = this.linuxConnector.host_name + ":" + this.linuxConnector.host_port;
      this.linuxConnector.connector_name = this.entityDetail.entity_name;
      this.entityDetail.connector_object = this.linuxConnector;
    }

        
    try {
      let result = null;
      if (this.images != undefined || this.images != null) {
        const formData = new FormData();
        formData.append('newFileToSave', this.images);
        result = await this._dbService.uploadFile(formData).toPromise();
        CustomLogger.logStringWithObject("uploadFile:result:", result);

        let fileName = CustomGlobalConstants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS + result["data"].filename;//received after file added
        CustomLogger.logStringWithObject("fileNameeeeeeeeeee:", fileName);
        this.entityDetail.icon_location = fileName;
      }
      CustomLogger.logStringWithObject("Images:", this.images);
      if (this.images != undefined && this.images != null) {
        this.entityDetail.icon_filename = this.images.name;

      }

      CustomLogger.logStringWithObject("this.entityDetail", this.entityDetail);

      await this._miscService.commonApprovalForAddUpdate(this.entityDetail, CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY, this.isComponentNew);

      // // check if approval is required or not
      // let approvalMatrix: ApprovalMatrix = this._miscService.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR);
      // CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
      // if (approvalMatrix != null && approvalMatrix.requires_approval) {
      //   CustomLogger.logString("APPROVAL REQUIRED");
      //   await this._miscService.sendMakerCheckerRequest(this.entityDetail, CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY, this.isComponentNew);
      // } else {
      //   CustomLogger.logString("APPROVAL NOT REQUIRED");
      //   if (this.isComponentNew) {
      //     result = await this._dbService.commonCreate(this.entityDetail).toPromise();
      //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ENTITY.crudEntity_entity_add_success));
      //   }
      //   else {
      //     result = await this._dbService.commonUpdate(this.entityDetail).toPromise();
      //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ENTITY.crudEntity_entity_update_success));
      //   }
      //   CustomLogger.logStringWithObject("addEntityDetail:result:", result);
      // }
      this._router.navigate(["sectors/listEntity"]);     
       
     

    } catch (error) {
      CustomLogger.logError(error);
      CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ENTITY.crudEntity_entity_add_error), true);
    }
  }
  next(){
    this.stepper1.next();
   
}
previous(){
  this.stepper1.previous();
}

   
  selectImage(event) {
    CustomLogger.logString("Select image....");
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
      CustomLogger.logStringWithObject("IMAGES:", this.images);
      CustomLogger.logStringWithObject("this.images.name:", this.images.name);
    }
  }

  async onUpload() {

  }

  onCancel() {
    this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY, this.activatedRoute);
  }
  
}