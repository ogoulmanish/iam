import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { SectorDetail } from 'src/app/modules/models/sectors/SectorDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../shared/BaseComponent.component';

@Component({
    selector: 'app-crudSector',
    templateUrl: './crudSector.component.html'
})
export class CrudSectorComponent extends BaseComponent implements OnInit {
    field_crudSectorComponent_info = "All Sector details.";
    currentCrudType = CustomGlobalConstants.PARAM_CRUD_TYPE;
    entityDetailArr: EntityDetail[] = [];
    sectorDetail: SectorDetail;

    constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR, injector);
    }

    async htmlInit() {
        try {
            let result = await this._dbService.getAllEntityDetail().toPromise();
            this.entityDetailArr = result["data"];
        } catch (error) {
            CustomLogger.logError(error);
        }
    }

    async ngOnInit() {
        this.sectorDetail = new SectorDetail();
        await this.populateFields();
        this.htmlInit();
        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[this.currentCrudType];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let sector_id = params["id"];
                        let result = await this._dbService.getSpecificSectorDetail(sector_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificSectorDetail:result:", result);
                        this.sectorDetail = result["data"];
                    }
                }
            }
        );
    }


    async onSubmit() {
        CustomLogger.logStringWithObject("New Sector will now be added/updated ", this.sectorDetail);
        try {            
            await this._miscService.commonApprovalForAddUpdate(this.sectorDetail, CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR, this.isComponentNew);
            this._router.navigate(["sectors/listSector"]);
        } catch (error) {
            CustomLogger.logError(error);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_SECTOR.crudSector_sector_add_error), true);
        }
    }

    onClickReset() {

    }
    
    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR, this.activatedRoute);
    }
}