import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular-6-datatable';
import { CrudEntityComponent } from './crudEntity.component';
import { CrudSectorComponent } from './crudSector.component';
import { ListEntityComponent } from './listEntity.component';
import { ListSectorComponent } from './listSector.component';
import { ListSectorElementsComponent } from './listSectorElements.component';
import { SectorRoutingModule } from './sector.routing';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    SectorRoutingModule,
    DataTableModule
  ],
  declarations: [
    ListSectorComponent,
    CrudSectorComponent,
    ListSectorElementsComponent,
    CrudEntityComponent,
    ListEntityComponent
  ]
})
export class SectorModule { }