import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/modules/services/global.service';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BnNgIdleService } from 'bn-ng-idle';
import { DBService } from 'src/app/modules/services/dbService.service';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tfa: any = {};
  authcode: string = "";
  errorMessage: string = null;

  constructor(private _loginService: LoginService, private _router: Router, private _globalService: GlobalService, private _bnIdle: BnNgIdleService, private _dbService: DBService) {
    this.getAuthDetails();
  }

  ngOnInit() {
    CustomLogger.logString("Inside Home Component");
    // this._bnIdle.startWatching(CustomGlobalConstants.PROJECT_CONFIGURATION_CONSTANTS.SESSION_TIMEOUT_IN_SECONDS).subscribe((isTimedOut: boolean) => {
    //   if (isTimedOut) {
    //     CustomLogger.logString("Session Expired. Logging out");
    //     try {
    //       this._bnIdle.stopTimer();
    //       this._loginService.logoutUser();
    //     } catch (error) {
    //       CustomLogger.logStringWithObject("Error:", error);
    //     }

    //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.COMMON.session_logout));
    //     this._router.navigate(["/login"]);
    //   }
    // });
  }

  async getAuthDetails() {
    CustomLogger.logString("Entered:getAuthDetails():");
    let userDetail = this._globalService.getCurrentUserDetail();
    CustomLogger.logStringWithObject("userDetail::", userDetail);
    if (userDetail == undefined || !userDetail || userDetail == null) {
      this._router.navigate(["/login"]);
    }

    if (userDetail.is_otp_required) {
      let result = await this._loginService.getAuth(userDetail.user_id).toPromise();
      CustomLogger.logStringWithObject("getAuthDetails:getAuth:result:", result);
      userDetail = result.body["data"];
      this._globalService.setCurrentUserDetail(userDetail);
      this.tfa.dataURL = userDetail.dataURL;
      this.tfa.tempSecret = userDetail.tempSecret;
      CustomLogger.logStringWithObject("tfa::", this.tfa);
      if (this.tfa.secret) {
        this.navigateToDashboard();
      }
    } else {
      CustomLogger.logString("Current user doesn't require OTP Verification.");
      let result = await this._loginService.getAuthTokenForLoggedInUser(this._globalService.getCurrentUserDetail().user_id).toPromise();
      CustomLogger.logStringWithObject("getAuthDetails:getAuthTokenForLoggedInUser:result:", result);
      this._globalService.setAuthToken(result["data"]);
      localStorage.setItem(CustomGlobalConstants.LOCAL_STORAGE_KEY_AUTH_TOKEN, result["data"]);
      this.navigateToDashboard();
    }

  }

  setup() {
    this._loginService.setupAuth().subscribe((data) => {
      CustomLogger.logStringWithObject("setup:setupAuth:data:", data);
      const result = data.body;
      if (data['status'] === 200) {
        CustomLogger.logStringWithObject("setup:Result status is 200", result);
        // this.tfa = result;
        this.navigateToDashboard();
      }
    });
  }

  async confirm() {
    CustomLogger.logString("About to confirm against authcode:" + this.authcode);
    try {
      let data = await this._loginService.verifyAuthAndGetToken(this._globalService.getCurrentUserDetail().user_id, this.authcode).toPromise();
      CustomLogger.logStringWithObject("home:confirm():data", data);
      const result = data.body;
      if (result['status'] === 200) {
        CustomLogger.logStringWithObject("confirm:Result status is 200", result);
        this.errorMessage = null;
        // this.tfa.secret = this.tfa.tempSecret;
        this.tfa.tempSecret = "";
        this.navigateToDashboard();
      } else {
        this.errorMessage = result['message'];
      }
    } catch (error) {
      CustomLogger.logError(error);
      this.errorMessage = error.error.message;
    }

  }

  disabledTfa() {
    this._loginService.deleteAuth().subscribe((data) => {
      const result = data.body
      if (data['status'] === 200) {
        console.log(result);
        this.authcode = "";
        this.getAuthDetails();
      }
    });
  }

  navigateToDashboard() {
    if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.STANDARD)
      this._router.navigate(["/standardDashboard"]);
    else if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.HR)
      this._router.navigate(["/hrDashboard"]);
    else
      this._router.navigate(["/dashboard"]);
  }

}
