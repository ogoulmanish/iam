import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertNotification } from 'src/app/modules/models/common/AlertNotification.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { LoggingTracking } from 'src/app/modules/models/events/LoggingTracking.model';
import { DBService } from 'src/app/modules/services/dbService.service';
import { EventService } from 'src/app/modules/services/eventService.service';
import { GlobalService } from 'src/app/modules/services/global.service';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { ConnectionService } from 'ng-connection-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  clicked = false;
  // auth_type = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT;
  // auth_type_text = "Default";
  auth_type = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
  auth_type_text = "Active Directory";
  DN = "";
  PICKLIST_AUTH_TYPE_OBJECT_ARRAY = CustomGlobalConstants.EXTERNAL_AUTH_TYPE_OBJECT_ARRAY;
  PICKLIST_PREFERRED_LANGUAGE_ARRAY = CustomGlobalConstants.ARRAY_PREFERRED_LANGUAGE;
  preferred_language = CustomGlobalConstants.PREFERRED_LANGUAGE.ENGLISH;

  selected_user_type: any;
  PICKLIST_USER_TYPES_ARRAY: any;

  show = false;
  tfaFlag: boolean = false

  password = "";

  userObject = {
    uname: "",
    upass: ""
  }
  errorMessage: string = null
  constructor(private _dbService: DBService, private _eventService: EventService, private _connectionService: ConnectionService,
    private _loginService: LoginService, private _router: Router, private _globalService: GlobalService) {
  }

  ngOnInit() {
    this.password = 'password';
    this.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.BASE_DOMAIN;
    this.PICKLIST_USER_TYPES_ARRAY = CustomGlobalConstants.ARRAY_ADMIN_USER_TYPES;
    this.selected_user_type = CustomGlobalConstants.USER_TYPES.ADMIN;
  }

  async loginUser() {
    this.clicked = true;
    CustomLogger.logString("About to check for login");
    this._globalService.setPreferredLanguage(this.preferred_language);
    let result = null;
    let userDetail: UserDetail = new UserDetail();
    userDetail.username = this.userObject.uname;
    userDetail.password = this.userObject.upass;
    CustomLogger.logStringWithObject("userDetail::::", userDetail);
    let loggingTracking = new LoggingTracking();

    //check if internet connection is available
    // this._connectionService.monitor().subscribe(
    //   isConnected => {
    //     if(isConnected) console.log("YYYYYYYYYYYYYYYY");
    //     else console.log("NNNNNNNNNNNNN");
    //   }
    // );
    
    
    //logging tracking
    let ipAddress = await this._eventService.getSourceIP().toPromise();
    CustomLogger.logStringWithObject("ipaddress:", ipAddress);  
    if (ipAddress == null)
      loggingTracking.source_ip_address = "IP Not Found";
    else
      loggingTracking.source_ip_address = ipAddress["ip"];
    loggingTracking.access_browser = CustomMisc.getCurrentBrowser();
    loggingTracking.source_username = userDetail.username;
    loggingTracking.username = userDetail.username;
    loggingTracking.password = userDetail.password;

    //check to use LDAP or not
    CustomLogger.logString("authType:::" + this.auth_type);
    this._globalService.setAuthType(this.auth_type);

    let customErrorMessage = null;
    try {
      switch (Number(this.auth_type)) {
        case CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT:
          let tmpUserDetail = new UserDetail();
          tmpUserDetail.username = this.userObject.uname;
          tmpUserDetail.password = this.userObject.upass;
          result = await this._dbService.authenticateUser(tmpUserDetail).toPromise();
          CustomLogger.logStringWithObject("Success in authenticateUser", result);
          if (result["data"] == null) {
            customErrorMessage = "User not found.....";
            CustomLogger.logString(customErrorMessage);
            throw Error(customErrorMessage);
          }
          break;

        case CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY:
          //store DN
          userDetail.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.USERNAME_START_STRING + userDetail.username + "," + this.DN;
          result = await this._loginService.authenticateLDAPUser(userDetail).toPromise();
          CustomLogger.logStringWithObject("Success in authenticateLDAPUser", result);

          if (result["data"] == null) {
            customErrorMessage = "User with given credentials not found.";
            CustomLogger.logString(customErrorMessage);
            this.errorMessage = result['message'];
            this.clicked = false;
            return;
          }
          break;

        case CustomGlobalConstants.EXTERNAL_AUTH_TYPE.MSSQL:
          result = await this._loginService.authenticateMSSQLUser(this.userObject.uname, this.userObject.upass).toPromise();
          CustomLogger.logStringWithObject("Success in authenticateMSSQLUser", result);
          if (result["data"] == null) {
            CustomLogger.logString("SQL User with given credentials not found.");
            this.errorMessage = result['message'];
            this.clicked = false;
            return;
          }
          break;

        default:
          CustomMisc.showAlert("This authentication type is not yet implemented");
          return;
      }

      //success in logging
      userDetail = result["data"];

      ///////////////////////////
      // check for valid user type as Active Directory may not store the user type
      // unless there is a seperate ou for it. 
      // At this point we store the user_type in the mongodb so test it against it

      if (userDetail.user_type != this.selected_user_type) {
        customErrorMessage = "User is authenticated but the user type doesn't match.";
        CustomLogger.logString(customErrorMessage);
        throw Error(customErrorMessage);
      }

      //IMPORTANT
      userDetail.source_username = userDetail.username;
      this._globalService.setCurrentUserDetail(userDetail);

      CustomLogger.logStringWithObject("User Detail::: ", userDetail);

      //check if user is active or not
      if (userDetail.is_active != undefined && !userDetail.is_active) {
        CustomMisc.showAlert("This user is inactive. Please contact administrator.");
        return;
      }


      //load all defaults
      await this.loadAllDefaults();

      //check if the user is expired or not
      let loggingTrackingResult = await this._eventService.getLastLoginTrackingOfUsername(userDetail.username).toPromise();
      let tmpLoggingTracking: LoggingTracking = loggingTrackingResult["data"];
      CustomLogger.logStringWithObject("tmpLoggingTracking:", tmpLoggingTracking);
      if (tmpLoggingTracking != null) {
        let current_time = (new Date()).getTime();
        let days = CustomMisc.calculateDaysBetweenTimes(tmpLoggingTracking.login_time, current_time);
        let differenceDays = this._globalService.getDefaultSetting().account_expiry_days - days;
        CustomLogger.logString("days:" + days + " differenceDays:" + differenceDays);

        if (differenceDays >= 0) {
          loggingTracking.is_login_successful = true;
          loggingTracking.remarks = "Successfully logged in";
          CustomLogger.logStringWithObject("Logging Tracking:", loggingTracking);
          this._globalService.setCurrentLoggingTracking(loggingTracking);
          this._eventService.addLoggingTracking(loggingTracking).subscribe(
            data => { CustomLogger.logStringWithObject("addLoggingTracking Success:", data); },
            err => { CustomLogger.logStringWithObject("addLoggingTracking Error:", err); }
          );
        } else {
          //account is expired
          userDetail.is_active = false;
          await this._dbService.updateUserDetail(userDetail).toPromise();
          CustomMisc.showAlert("Sorry, your account is expired. Kindly contact administrator.");

          loggingTracking.is_login_successful = false;
          loggingTracking.remarks = "Account Expired.";
          CustomLogger.logStringWithObject("Logging Tracking:", loggingTracking);
          this._eventService.addLoggingTracking(loggingTracking).subscribe(
            data => { CustomLogger.logStringWithObject("addLoggingTracking Success:", data); },
            err => { CustomLogger.logStringWithObject("addLoggingTracking Error:", err); }
          );
          return;
        }
      }

      this.errorMessage = null;
      let status = result['status'];

      if (status === 200 || status === 203) {
        this._loginService.updateAuthStatus(true);

        // //get role id
        // try {
        //   let roleResult = await this._dbService.getSpecificRoleDetail(userDetail.role_id).toPromise();
        //   CustomLogger.logStringWithObject("getSpecificRoleDetail", roleResult);
        //   this._globalService.setRoleName(roleResult["data"].role_name);
        // } catch (error) {
        //   CustomLogger.logStringWithObject("Error:getSpecificRoleDetail", error);
        // }

        this._globalService.setIsOTPRequired(userDetail.is_otp_required);
        this._loginService.updateAuthStatus(true);

        //MFA
        this._router.navigateByUrl('/home');
      }
    } catch (err) {
      this.clicked = false;
      CustomLogger.logStringWithObject("Error in Authenticating User", err);
      if (err && err.error && err.error.message) {
        this.errorMessage = err.error.message;
      } else {
        if (customErrorMessage != null)
          this.errorMessage = customErrorMessage;
        else if (this.auth_type == CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY)
          this.errorMessage = "User with given credentials not found.";
        else
          this.errorMessage = err.message;
      }

      loggingTracking.remarks = this.errorMessage;
      this._globalService.setCurrentLoggingTracking(loggingTracking);
      this._eventService.addLoggingTracking(loggingTracking, userDetail.username).subscribe(
        data => { CustomLogger.logStringWithObject("addLoggingTracking Success:", data); },
        err => { CustomLogger.logStringWithObject("addLoggingTracking Error:", err); }
      );
      return;
    }
  }

  async loadAllDefaults() {
    //load alert notification
    try {
      let result = await this._dbService.getAllAlertNotification().toPromise();
      CustomLogger.logStringWithObject("getAllAlertNotification: result", result);
      let alertNotificationArr: AlertNotification[] = result["data"];
      this._dbService.loadAllAlertNotification(alertNotificationArr);
    } catch (error) {
      CustomLogger.logStringWithObject("Error:getAllAlertNotification", error);
    }

    //load default setting
    try {
      let result = await this._dbService.getDefaultSetting().toPromise();
      CustomLogger.logStringWithObject("getDefaultSetting: result", result);
      this._globalService.setDefaultSetting(result["data"]);
    } catch (error) {
      CustomLogger.logStringWithObject("Error:getDefaultSetting", error);
    }

    //load page matrix
    try {
      // let result = await this._dbService.getRespectivePageMatrixForUsername(this._globalService.getCurrentUserDetail().username).toPromise();
      let result = await this._dbService.getSpecificPageMatrix(this._globalService.getCurrentUserDetail().page_matrix_id).toPromise();
      CustomLogger.logStringWithObject("loadPageMatrixForUserId: result", result);
      this._globalService.setPageMatrix(result["data"]);
    } catch (error) {
      CustomLogger.logStringWithObject("loadPageMatrixForUserId:error:", error);
    }

    //load approval matrix
    try {
      CustomLogger.logString("Will Load Approval Matrix");
      let result = await this._dbService.getApprovalMatrixFromUserType(this._globalService.getCurrentUserDetail().user_type).toPromise();
      CustomLogger.logStringWithObject("getApprovalMatrixFromRoleId:result:", result);
      this._globalService.setApprovalMatrix(result["data"]);
    } catch (error) {
      CustomLogger.logStringWithObject("getApprovalMatrixFromRoleId:error:", error);
    }
  }

  onClick() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }


  onClickActiveDirectory() {
    CustomLogger.logString("Clicked AD");
    this.auth_type_text = "Active Directory";
    this.auth_type = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
  }
  onClickDefault() {
    CustomLogger.logString("Clicked Default");
    this.auth_type_text = "Default";
    this.auth_type = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT;
  }

  onClickWindowsServer() {
    alert("Windows Server Integration WIll be implemented soon.");
  }
  onClickHRMS() {
    alert("HRMS Integration WIll be implemented soon.");
  }
}
