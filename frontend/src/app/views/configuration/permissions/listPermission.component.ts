import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionDetail } from 'src/app/modules/models/sectors/PermissionDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';


@Component({
  selector: 'app-listPermission',
  templateUrl: './listPermission.component.html'
})
export class ListPermissionComponent extends BaseComponent implements OnInit {
  field_listPermission_info = "List of Permissions required during Rule assignment";
  tableDataArr = [];
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_PERMISSION, injector);
  }

  async ngOnInit() {
    await this.populateFields();
    this.init();
  }

  async init() {
    let result = await this._dbService.getAllPermissionDetail().toPromise();
    CustomLogger.logStringWithObject("getAllPermissionDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this.router.navigate(['crudPermission'], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  onClickEdit(permission: PermissionDetail) {
    CustomLogger.logStringWithObject("permission:", permission);
    this.router.navigate(['crudPermission', CustomGlobalConstants.CRUD_UPDATE, permission.permission_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  filteredTableDataArr: any;
  search(term: string) {
    let fieldName = "permission_name";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }


  async onClickDelete(obj: PermissionDetail) {
    try {
      let tmpStr = "permission";
      if (!obj.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PERMISSION.listPermission_permission_cannot_be_deleted), true);
      } else {
        await this._miscService.commonApprovalForDelete(obj, CustomGlobalConstants.COMPONENT_NAME.LIST_PERMISSION, tmpStr);
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }


    // try {
    //   if (!obj.can_be_deleted) {
    //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PERMISSION.listPermission_permission_cannot_be_deleted), true);
    //     return;
    //   }
    //   let optionChosen = await this._miscService.confirmDialogBox("Delete", "Are you sure you want to delete this user ?");
    //   CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
    //   if (optionChosen) {
    //     let result = this._dbService.deletePermissionDetail(obj).toPromise();
    //     CustomLogger.logStringWithObject("deletePermissionDetail:result:", result);
    //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PERMISSION.listPermission_permission_deleted_success));
    //     await this.init();
    //   }
    // } catch (error) {
    //   CustomLogger.logError(error);
    // }
  }
};
