import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionDetail } from 'src/app/modules/models/sectors/PermissionDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../../shared/BaseComponent.component';

@Component({
    selector: 'app-crudPermission',
    templateUrl: './crudPermission.component.html'
})
export class CrudPermissionComponent extends BaseComponent implements OnInit {
    field_crudPermission_info = "Details of permissions that the entities can have.";
    permissionDetail: PermissionDetail;
    constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_PERMISSION, injector);
    }

    async ngOnInit() {
        this.permissionDetail = new PermissionDetail();
        await this.populateFields();

        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let permission_id = params["id"];
                        let result = await this._dbService.getSpecificPermissionDetail(permission_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificPermissionDetail:result:", result);
                        this.permissionDetail = result["data"];
                    }
                }
            }
        );
    }

    async onSubmit() {
        CustomLogger.logStringWithObject("Will add/update permission:", this.permissionDetail);
        try {
            await this._miscService.commonApprovalForAddUpdate(this.permissionDetail, CustomGlobalConstants.COMPONENT_NAME.CRUD_PERMISSION, this.isComponentNew);
            this._router.navigate(["configuration/listPermission"]);
        } catch (error) {
            CustomLogger.logError(error);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_PERMISSION.crudPermission_permission_add_error), true);
        }
    }

    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_PERMISSION, this.activatedRoute);
    }
}
