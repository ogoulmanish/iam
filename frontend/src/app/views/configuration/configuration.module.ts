import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular-6-datatable';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrudAccessPolicyComponent } from './accessPolicies/crudAccessPolicy.component';
import { ListAccessPolicyComponent } from './accessPolicies/listAccessPolicy.component';
import { ConfigurationRoutingModule } from './configuration.routing';
import { CrudPermissionComponent } from './permissions/crudPermission.component';
import { ListPermissionComponent } from './permissions/listPermission.component';
import { CrudRoleComponent } from './roles/crudRole.component';
import { ListRoleComponent } from './roles/listRole.component';
import { CrudRuleComponent } from './rules/crudRule.component';
import { ListRuleComponent } from './rules/listRule.component';


@NgModule({
  imports: [
    SharedModule,
    DataTableModule,
    ConfigurationRoutingModule
  ],
  declarations: [
    ListPermissionComponent,
    ListRoleComponent,
    ListRuleComponent,
    CrudRuleComponent,
    CrudRoleComponent,
    CrudPermissionComponent,
    ListAccessPolicyComponent,
    CrudAccessPolicyComponent
  ],
})
export class ConfigurationModule { }
