import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { PermissionDetail } from 'src/app/modules/models/sectors/PermissionDetail.model';
import { RoleDetail } from 'src/app/modules/models/sectors/RoleDetail.model';
import { RuleDetail } from 'src/app/modules/models/sectors/RuleDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
  selector: 'app-crudRule',
  templateUrl: './crudRule.component.html',
  styleUrls: ['./crudRule.component.css']
})
export class CrudRuleComponent extends BaseComponent implements OnInit {
  field_crudRule_info = "Rule decides what permissions can be given to individual entities of specific role. The rule can be devised for Entire Role or for Specific User";
  ruleDetail: RuleDetail;
  sectorDetailArr = [];
  roleDetailArr: RoleDetail[] = [];
  entityDetailArr: EntityDetail[] = [];
  userDetailArr: UserDetail[] = [];
  permissionDetailArr: PermissionDetail[] = [];
  currentEntityId: string = "";
  currentPermissionIdArr: string[] = [];

  field_associated_username = "Users";
  constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.CRUD_RULE, injector);
  }

  async ngOnInit() {
    this.ruleDetail = new RuleDetail();
    await this.populateFields();

    //get all roles
    let result = await this._dbService.getAllRoleDetail().toPromise();
    this.roleDetailArr = result["data"];
    this.activatedRoute.params.subscribe(
      async params => {
        CustomLogger.logStringWithObject("Params Values ::: ", params);
        let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
        if (tmpCrudType) {
          this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
          if (!this.isComponentNew) {
            let rule_id = params["id"];
            let result = await this._dbService.getParticularRuleDetail(rule_id).toPromise();
            CustomLogger.logStringWithObject("getParticularRuleDetail:result:", result);
            this.ruleDetail = result["data"];
            if (this.ruleDetail.rule_type == CustomGlobalConstants.RULE_TYPES.ROLE_SPECIFIC) {
              this.fillUpEntitiesForRole(this.ruleDetail.role_id);
            }
            else {
              this.fillUpEntitiesForUser(this.ruleDetail.associated_username);
            }
            this.ruleDetail.mapEntityToPermission = CustomMisc.convertRuleStringArrToMap(this.ruleDetail.tmpStr);
            CustomLogger.logStringWithObject("this.ruleDetail.mapEntityToPermission::", this.ruleDetail.mapEntityToPermission);
          }
        }
      }
    );

    //fill up all users
    await this.fillUpUsers();

    if (this.isComponentNew) {
      this.ruleDetail.role_id = this.roleDetailArr[0].role_id;
      let result = await this._dbService.getAllEntityDetailsOfSpecificRole(this.ruleDetail.role_id).toPromise();
      this.entityDetailArr = result["data"];

      this.ruleDetail.associated_username = this.userDetailArr[0].source_username;
      this.currentEntityId = this.entityDetailArr[0].entity_id;
    }
  }

  async onClickSave() {
    CustomLogger.logString("currentUsername::" + this.ruleDetail.associated_username);

    if (this.ruleDetail.rule_type != CustomGlobalConstants.RULE_TYPES.USER_SPECIFIC) {
      this.ruleDetail.associated_username = CustomGlobalConstants.DEFAULT_SELECTED_STRING_VALUE;
    }

    this.ruleDetail.tmpStr = CustomMisc.convertRuleMapToString(this.ruleDetail.mapEntityToPermission);

    CustomLogger.logString("TMPSTR::" + this.ruleDetail.tmpStr)
    CustomLogger.logStringWithObject("Rule Detail:", this.ruleDetail);
    try {
      let result = null;
      if (this.isComponentNew)
        result = await this._dbService.addRuleDetail(this.ruleDetail).toPromise();
      else
        result = await this._dbService.updateRuleDetail(this.ruleDetail).toPromise();
      CustomLogger.logStringWithObject("addRuleDetail:result:", result);
      if (this.isComponentNew)
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_RULE.crudRule_rule_add_success));
      else
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_RULE.crudRule_rule_update_success));
      this._router.navigate(["configuration/listRule"]);

    } catch (error) {
      CustomLogger.logError(error);
      CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_RULE.crudRule_rule_add_error), true);
    }
  }


  async fillUpUsers(role_id = -1) {
    //fill up users
    CustomLogger.logString("Fill up all users ");
    let result = null;
    if (role_id == -1)
      result = await this._dbService.getAllUserDetail().toPromise();
    else
      result = await this._dbService.getAllUserDetailsOfSpecificRole(role_id).toPromise();
    CustomLogger.logStringWithObject("getAllUserDetail:result", result);
    this.userDetailArr = result["data"];
  }

  async fillUpEntitiesForUser(username) {
    // CustomLogger.logStringWithObject("Fill up entities for username::", username);
    // //get role of current user
    // let userDetailResult = await this._dbService.getSpecificUserDetailFromUsername(username).toPromise();
    // CustomLogger.logStringWithObject("getSpecificUserDetailFromUsername:userDetailResult", userDetailResult);
    // let userDetail: UserDetail = userDetailResult["data"];
    // let result = await this._dbService.getAllEntityDetailsOfSpecificRole(userDetail.role_id).toPromise();
    // CustomLogger.logStringWithObject("getAllEntityDetailsOfSpecificRole:result", result);
    // this.entityDetailArr = result["data"];
  }

  async fillUpEntitiesForRole(role_id) {
    CustomLogger.logStringWithObject("Fill up entities for role_id::", role_id);
    let result = await this._dbService.getAllEntityDetailsOfSpecificRole(role_id).toPromise();
    CustomLogger.logStringWithObject("getAllEntityDetailsOfSpecificRole:result", result);
    this.entityDetailArr = result["data"];
  }

  async fillUpPermissions() {
    CustomLogger.logStringWithObject("Fill up permissions for currentEntityId::", this.currentEntityId);
    let result1 = await this._dbService.getAllPermissionDetail().toPromise();
    this.permissionDetailArr = result1["data"];
    if (!this.ruleDetail.mapEntityToPermission.get(this.currentEntityId)) {
      let permissionIdsArr: string[] = ["1"];
      this.ruleDetail.mapEntityToPermission.set(this.currentEntityId, permissionIdsArr);
    }
    this.currentPermissionIdArr = this.ruleDetail.mapEntityToPermission.get(this.currentEntityId);
    CustomLogger.logStringWithObject("entityToPermissionMap::::", this.ruleDetail.mapEntityToPermission);
  }

  async onSelectedPermission() {
    CustomLogger.logStringWithObject("Clicked... permission_id_arr::", this.currentPermissionIdArr);
    this.ruleDetail.mapEntityToPermission.set(this.currentEntityId, this.currentPermissionIdArr);
  }
    
  onCancel() {
      this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_RULE, this.activatedRoute);
  }
}
