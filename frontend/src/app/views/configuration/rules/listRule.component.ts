import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RuleDetail } from 'src/app/modules/models/sectors/RuleDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';



@Component({
  selector: 'app-listRule',
  templateUrl: './listRule.component.html',
  styleUrls: ['./listRule.component.css']
})
export class ListRuleComponent extends BaseComponent implements OnInit {
  field_listRule_info = "List of rules that are assigned for roles/users";
  tableDataArr = [];
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_RULE, injector);
  }

  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  async init() {
    let result = await this._dbService.getAllRuleDetail().toPromise();
    CustomLogger.logStringWithObject("getAllRuleDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this.router.navigate(['crudRule'], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  onClickEdit(rule: RuleDetail) {
    CustomLogger.logStringWithObject("rule:", rule);
    this.router.navigate(['crudRule', CustomGlobalConstants.CRUD_UPDATE, rule.rule_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  filteredTableDataArr: any;
  search(term: string) {
    let fieldName = "rule_name";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }

  async onClickDelete(rule: RuleDetail) {
    try {
      if (!rule.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_RULE.listRule_rule_cannot_be_deleted), true);
        return;
      }
      let optionChosen = await this._miscService.confirmDialogBox("Delete", "Are you sure you want to delete this user ?");
      CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
      if (optionChosen) {
        let result = this._dbService.deleteRuleDetail(rule).toPromise();
        CustomLogger.logStringWithObject("deleteRuleDetail:result:", result);
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_RULE.listRule_rule_deleted_success));
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }
  }

};
