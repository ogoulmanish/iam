import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../../shared/BaseComponent.component';
import { AccessPolicy } from 'src/app/modules/models/sectors/AccessPolicy.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { PermissionDetail } from 'src/app/modules/models/sectors/PermissionDetail.model';

@Component({
    selector: 'app-crudAccessPolicy',
    templateUrl: './crudAccessPolicy.component.html'
})
export class CrudAccessPolicyComponent extends BaseComponent implements OnInit {
    field_crudAccessPolicy_info = "Details of access policies that the entities can have.";
    field_crudAccessPolicy_access_policy_name = "Name";
    field_crudAccessPolicy_access_policy_details = "Details";
    field_crudAccessPolicy_access_policy_name_error_message = "Policy Name Required";
    field_crudAccessPolicy_details_error_message = "Details Required";

    field_crudAccessPolicy_can_be_deleted = "Can be deleted ";
    field_crudAccessPolicy_button_save = "Save";
    field_crudAccessPolicy_button_update = "Update";
    field_crudAccessPolicy_button_reset = "Reset";
    field_crudAccessPolicy_button_cancel = "Cancel";
    field_crudAccessPolicy_entity_name = "Entity Names";


    accessPolicy: AccessPolicy;
    entityDetailArr: EntityDetail[] = [];
    // permissionDetailArr: PermissionDetail[] = [];
    // selectedEntityDetailArr = [];
    // selectedPermissionDetailArr = [];

    constructor(private _router: Router, private _activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_ACCESS_POLICY, injector);
        this.accessPolicy = new AccessPolicy();
    }

    async fillUpAllEntities() {
        let result = await this._dbService.getAllEntityDetail().toPromise();
        CustomLogger.logStringWithObject("getAllEntityDetail:result", result);
        this.entityDetailArr = result["data"];
    }

    // async fillUpPermissions() {
    //     let result1 = await this._dbService.getAllPermissionDetail().toPromise();
    //     this.permissionDetailArr = result1["data"];
    // }

    async ngOnInit() {
        await this.populateFields();
        //fill up all users
        await this.fillUpAllEntities();
        this._activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let access_policy_id = params["id"];
                        let result = await this._dbService.commonGetSpecific(CustomGlobalConstants.MODEL_NAME.ACCESS_POLICY, access_policy_id).toPromise();
                        CustomLogger.logStringWithObject("commonGetSpecific:result:", result);
                        this.accessPolicy = result["data"];
                    }
                }
            }
        );
    }

    async onSubmit() {
        try {
            CustomLogger.logStringWithObject("Will add/update access Policy:", this.accessPolicy);
            if (this.isComponentNew) {
                let result = await this._dbService.commonCreate(this.accessPolicy).toPromise();
                CustomLogger.logStringWithObject("commonCreate:result:", result);
                CustomMisc.showAlert("Success in Adding");
            } else {
                CustomMisc.showAlert("The associated users of this access policy will be updated in future. Currently this will reflect only for new role assignment");
                let result = await this._dbService.commonUpdate(this.accessPolicy).toPromise();
                CustomLogger.logStringWithObject("commonUpdate:result:", result);
                // CustomMisc.showAlert("Success in Updating");
            }
        } catch (error) {
            CustomLogger.logStringWithObject("Error:", error);
            CustomMisc.showAlert("Error in transaction: " + error.message, true);
        }
        this._miscService.goToURL('listAccessPolicy', this._activatedRoute);
    }

    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_ACCESS_POLICY, this._activatedRoute);
    }
}
