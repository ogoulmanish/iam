import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessPolicy } from 'src/app/modules/models/sectors/AccessPolicy.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';


@Component({
    selector: 'app-listAccessPolicy',
    templateUrl: './listAccessPolicy.component.html'
})
export class ListAccessPolicyComponent extends BaseComponent implements OnInit {
    field_listAccessPolicy_heading_access_policy_management = "Access Policy Management";
    field_listAccessPolicy_info = "List of Access Policies required during Rule assignment";
    field_listAccessPolicy_button_add_access_policy = "Add";
    field_listAccessPolicy_heading_access_policy_details = "Access Policy Details";
    field_listAccessPolicy_access_policy_name = "Name";
    field_listAccessPolicy_details = "Details";
    field_listAccessPolicy_access_policy_id = "ID";
    field_listAccessPolicy_button_edit = "Edit";
    field_listAccessPolicy_button_delete = "Delete";

    tableDataArr = [];
    constructor(private _router: Router, private _activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_ACCESS_POLICY, injector);
    }

    async ngOnInit() {
        await this.populateFields();
        this.init();
    }

    async init() {
        let result = await this._dbService.commonGetAll(CustomGlobalConstants.MODEL_NAME.ACCESS_POLICY).toPromise();
        CustomLogger.logStringWithObject("commonGetAll:result:", result);
        this.tableDataArr = result["data"];
    }

    onClickAdd() {
        this._router.navigate(['crudAccessPolicy'], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
    }

    onClickEdit(accessPolicy: AccessPolicy) {
        CustomLogger.logStringWithObject("AccessPolicy:", accessPolicy);
        this._router.navigate(['crudAccessPolicy', CustomGlobalConstants.CRUD_UPDATE, accessPolicy.access_policy_id], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
      }
};
