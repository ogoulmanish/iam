import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrudPermissionComponent } from './permissions/crudPermission.component';
import { ListPermissionComponent } from './permissions/listPermission.component';
import { CrudRoleComponent } from './roles/crudRole.component';
import { ListRoleComponent } from './roles/listRole.component';
import { CrudRuleComponent } from './rules/crudRule.component';
import { ListRuleComponent } from './rules/listRule.component';
import { CrudAccessPolicyComponent } from './accessPolicies/crudAccessPolicy.component';
import { ListAccessPolicyComponent } from './accessPolicies/listAccessPolicy.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Configuration',
      status: false
    },
    children: [
      {
        path: 'listPermission',
        component: ListPermissionComponent
      },
      {
        path: 'listRole',
        component: ListRoleComponent
      },
      {
        path: 'listRule',
        component: ListRuleComponent
      },
      {
        path: 'crudRule',
        component: CrudRuleComponent
      },
      {
        path: 'crudRule/:crudType/:id',
        component: CrudRuleComponent
      },
      {
        path: 'crudRole',
        component: CrudRoleComponent
      },
      {
        path: 'crudRole/:crudType/:id',
        component: CrudRoleComponent
      },
      {
        path: 'crudPermission',
        component: CrudPermissionComponent
      },
      {
        path: 'crudPermission/:crudType/:id',
        component: CrudPermissionComponent
      },
      {
        path: 'listAccessPolicy',
        component: ListAccessPolicyComponent
      },
      {
        path: 'crudAccessPolicy',
        component: CrudAccessPolicyComponent
      },
      {
        path: 'crudAccessPolicy/:crudType/:id',
        component: CrudAccessPolicyComponent
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
