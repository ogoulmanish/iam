import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DBService } from 'src/app/modules/services/dbService.service';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';



@Component({
    selector: 'app-selectUsersModal',
    templateUrl: './selectUsersModal.component.html',
})
export class SelectUsersModalComponent implements OnInit {

    // @Input() title: string;
    // @Input() message: string;
    // @Input() btnOkText: string;
    // @Input() btnCancelText: string;
    @Input() selectedUserIdArr: string[];
    // @Input() public user;
    // @Output() passToParent: EventEmitter = new EventEmitter();
    @Output() passEntry = new EventEmitter();


    field_listUser_user_name = "Username";
    field_listUser_first_name = "First Name";
    field_listUser_last_name = "Last Name";
    field_listUser_department = "Department";
    field_listUser_job_title = "Job Title";
    btnOkText = "Ok";
    btnCancelText = "Cancel";

    // selectedUserIdArr = [];
    tableDataArr = [];
    filteredTableDataArr = [];
    constructor(private activeModal: NgbActiveModal, private _dbService: DBService) { }

    async ngOnInit() {
        CustomLogger.logStringWithObject("selectedUserIdArr::", this.selectedUserIdArr);
        let result = await this._dbService.getAllUserDetail().toPromise();
        this.tableDataArr = result["data"];
        this.filteredTableDataArr = this.tableDataArr;
    }

    public decline() {
        this.activeModal.close(false);
    }

    public accept() {
        this.activeModal.close(true);
    }

    public dismiss() {
        this.activeModal.dismiss();
    }

    search(term: string) {
        let fieldName = "username";
        this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
    }

    onSelectUser(user_id) {
        CustomLogger.logString("Selected user:" + user_id);
        if (this.selectedUserIdArr.indexOf(user_id) >= 0) {
            this.selectedUserIdArr = CustomMisc.deleteItemFromArray(this.selectedUserIdArr, user_id);
        } else {
            this.selectedUserIdArr.push(user_id);
        }
    }

    isSelected(user_id) {
        if (this.selectedUserIdArr.indexOf(user_id) < 0) return false;
        return true;
    }

    passBack() {
        //remove duplicates
        CustomLogger.logStringWithObject("Before: this.selectedUserIdArr: ", this.selectedUserIdArr);
        this.selectedUserIdArr = CustomMisc.removeDuplicatesFromArray(this.selectedUserIdArr);
        CustomLogger.logStringWithObject("After: this.selectedUserIdArr: ", this.selectedUserIdArr);
        this.passEntry.emit(this.selectedUserIdArr);
        this.activeModal.close(true);
    }

}
