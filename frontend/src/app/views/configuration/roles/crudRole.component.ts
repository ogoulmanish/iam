import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApprovalMatrix } from 'src/app/modules/models/common/ApprovalMatrix.model';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { RoleDetail } from 'src/app/modules/models/sectors/RoleDetail.model';
import { SpecificRule } from 'src/app/modules/models/sectors/SpecificRule.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../../shared/BaseComponent.component';
import { SelectAccessPolicyComponent } from './selectAccessPolicyModal.component';
import { SelectUsersModalComponent } from './selectUsersModal.component';

@Component({
    selector: 'app-crudRole',
    templateUrl: './crudRole.component.html',
    styleUrls: ['./crudRole.component.css']
})
export class CrudRoleComponent extends BaseComponent implements OnInit {
    field_crudRole_info = "Details of roles including the entities that this role has access to.";
    field_crudRole_allowed_access_policy_id_arr = "Access Policies";
    field_crudRole_allowed_access_policy_id_arr_required = false;
    field_crudRole_allowed_access_policy_id_arr_error_message = "Error";
    field_crudRole_allowed_access_user_id_arr = "Associate Users";
    field_crudRole_allowed_access_user_id_arr_required = false;
    field_crudRole_allowed_access_user_id_arr_error_message = "Error";
    field_crudRole_rule_if = "IF";
    field_crudRole_add_rule = "Add Rule";
    field_crudRole_can_be_deleted = "Can be deleted";

    roleDetail: RoleDetail;
    entityDetailArr: EntityDetail[] = [];
    // accessPolicyArr: AccessPolicy[] = [];
    tmpId1 = "";
    chosen_remaining_user_id = "";
    chosen_selected_user_id = "";
    remaining_user_id_arr: string[] = [];
    selected_user_id_arr: string[] = [];
    selected_access_policy_id_arr: string[] = [];
    mapUserIdToUsername: Map<string, string>;
    mapAccessPolicyIdToName: Map<string, string>;

    PICKLIST_ARRAY_RULES_USER_SPECIFIC = [];
    PICKLIST_ARRAY_ARITHMETIC_OPERATIONS = [];
    PICKLIST_ARRAY_PERMISSIONS = [];

    flagAddSpecificRule = false;
    specificRule: SpecificRule;
    specificRuleShowSelectBox = true;
    mapFormIdToFormElements: Map<string, FormElement[]> = new Map();
    specificRuleFormElementArr: FormElement[] = [];

    flagIsRead = false;
    flagIsCreate = false;
    flagIsUpdate = false;

    constructor(private _activatedRoute: ActivatedRoute, private _modalService: NgbModal, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE, injector);
        this.specificRule = new SpecificRule();
    }

    async ngOnInit() {
        this.roleDetail = new RoleDetail();
        this.mapUserIdToUsername = new Map();
        this.mapAccessPolicyIdToName = new Map();
        // this.PICKLIST_ARRAY_RULES_USER_SPECIFIC = CustomGlobalConstants.ARRAY_RULES_USER_SPECIFIC;

        try {
            //read form elements of the rules
            let result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department).toPromise();
            this.mapFormIdToFormElements.set(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_department, result["data"]);
            result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_job_title).toPromise();
            this.mapFormIdToFormElements.set(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_job_title, result["data"]);

            this.PICKLIST_ARRAY_RULES_USER_SPECIFIC.push({
                id: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department,
                value: "Department"
            });
            this.PICKLIST_ARRAY_RULES_USER_SPECIFIC.push({
                id: CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_job_title,
                value: "Job Title"
            });

            //fill up combo boxes defaults
            this.specificRule.attribute_name = CustomGlobalConstants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department;
            this.specificRule.arithmetic_operation = CustomGlobalConstants.ARRAY_ARITHMETIC_OPERATIONS[0].id;
            this.specificRuleFormElementArr = this.mapFormIdToFormElements.get(this.specificRule.attribute_name)
            this.specificRule.attribute_value = this.mapFormIdToFormElements.get(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_department)[0].data_value;

        } catch (error) {
            CustomLogger.logStringWithObject("Error:", error);
        }

        this.PICKLIST_ARRAY_ARITHMETIC_OPERATIONS = CustomGlobalConstants.ARRAY_ARITHMETIC_OPERATIONS;
        this.PICKLIST_ARRAY_PERMISSIONS = CustomGlobalConstants.ARRAY_PERMISSION_TYPES;

        //default settings for roleDetail
        this.roleDetail.allowed_permission_id_arr.push(CustomGlobalConstants.PERMISSION_TYPES.NONE_PERMISSION);

        await this.populateFields();
        this._activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType == undefined || tmpCrudType == CustomGlobalConstants.CRUD_CREATE) {
                    this.flagIsCreate = true;
                } else if (tmpCrudType == CustomGlobalConstants.CRUD_UPDATE) {
                    this.flagIsUpdate = true;
                } else if (tmpCrudType == CustomGlobalConstants.CRUD_READ) {
                    this.flagIsRead = true;
                }

                if (this.flagIsUpdate || this.flagIsRead) {
                    this.isComponentNew = false;
                    let role_id = params["id"];
                    let result = await this._dbService.getSpecificRoleDetail(role_id).toPromise();
                    CustomLogger.logStringWithObject("getSpecificRoleDetail:result:", result);
                    this.roleDetail = result["data"];

                    //specific rule
                    if (this.roleDetail.rule_associated) {
                        result = await this._dbService.getSpecificRuleForRole(role_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificRuleForRole:result:", result);
                        this.specificRule = result["data"];
                    }

                    //update the rule flag
                    this.flagAddSpecificRule = this.roleDetail.rule_associated;
                }
            }
        );

        try {
            // let result = await this._dbService.commonGetAll(CustomGlobalConstants.MODEL_NAME.ACCESS_POLICY).toPromise();
            // this.accessPolicyArr = result["data"];
            // CustomLogger.logStringWithObject("commonGetAll:result:", result);

            //fill up the arrays
            let resultModuleIdNameArray = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.USER).toPromise();
            CustomLogger.logStringWithObject("getModuleIdNameArray:resultModuleIdNameArray:", resultModuleIdNameArray);

            let allUserDetailArr = resultModuleIdNameArray["data"];
            allUserDetailArr.forEach(element => {
                this.mapUserIdToUsername.set(element.user_id, element.username);
                if (this.roleDetail.allowed_user_id_arr.includes(element.user_id)) {
                    this.selected_user_id_arr.push(element.user_id);
                } 
                // else {
                //     this.remaining_user_id_arr.push(element.user_id);
                // }

            });


            resultModuleIdNameArray = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.ACCESS_POLICY).toPromise();
            CustomLogger.logStringWithObject("getModuleIdNameArray:resultModuleIdNameArray:", resultModuleIdNameArray);
            let allAccessPolicyArr = resultModuleIdNameArray["data"];
            allAccessPolicyArr.forEach(element => {
                this.mapAccessPolicyIdToName.set(element.access_policy_id, element.access_policy_name);
                if (this.roleDetail.allowed_access_policy_id_arr.includes(element.access_policy_id)) {
                    this.selected_access_policy_id_arr.push(element.access_policy_id);
                } 
            });

        } catch (error) {
            CustomLogger.logError(error);
        }

    }

    async onSubmit() {
        CustomLogger.logStringWithObject("Will save role:", this.roleDetail);
        CustomLogger.logStringWithObject("Specific Rule:", this.specificRule);

        //if the rule flag is set then update the role detail
        this.roleDetail.rule_associated = this.flagAddSpecificRule;
        try {
            //add users
            this.roleDetail.allowed_user_id_arr = this.selected_user_id_arr;
            this.roleDetail.allowed_access_policy_id_arr = this.selected_access_policy_id_arr;

            let result = null;
            // check if approval is required or not
            let approvalMatrix: ApprovalMatrix = this._miscService.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR);
            CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
            if (approvalMatrix != null && approvalMatrix.requires_approval) {
                CustomLogger.logString("APPROVAL REQUIRED");
                await this._miscService.sendMakerCheckerRequest(this.roleDetail, CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE, this.isComponentNew);
            } else {
                CustomLogger.logString("APPROVAL NOT REQUIRED");
                if (this.isComponentNew) {
                    result = await this._dbService.addRoleDetail(this.roleDetail).toPromise();
                    CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ROLE.crudRole_role_add_success));

                    //now insert the rule Detail
                    if (this.flagAddSpecificRule) {
                        this.roleDetail = result["data"];
                        this.specificRule.role_id = this.roleDetail.role_id;
                        result = await this._dbService.commonCreate(this.specificRule).toPromise();
                    }
                }
                else {
                    result = await this._dbService.updateRoleDetail(this.roleDetail).toPromise();
                    if (this.flagAddSpecificRule) {
                        this.specificRule.role_id = this.roleDetail.role_id;
                        result = await this._dbService.commonUpdate(this.specificRule).toPromise();
                    } else {
                        //remove all rules of this role detail
                        if (this.specificRule != null && (this.specificRule.role_id == this.roleDetail.role_id))
                            result = await this._dbService.commonDelete(this.specificRule).toPromise();
                        // 
                        //                         
                    }
                    ///////////////////
                    //Update users that are belonging to this role in the backend..... IMPORTANT

                    CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ROLE.crudRole_role_update_success));
                }
                CustomLogger.logStringWithObject("RoleDetail:result:", result);
            }
            this.onCancel();

        } catch (error) {
            CustomLogger.logError(error);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_ROLE.crudRole_role_add_error), true);
        }
    }

    fillupPermissions() {
        // CustomLogger.logStringWithObject("idddd", id);
        CustomLogger.logStringWithObject("ACCESS Policy", this.roleDetail.allowed_access_policy_id_arr);

    }

    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_ROLE, this._activatedRoute);
    }


    getUsernameFromUserId(user_id) {
        return this.mapUserIdToUsername.get(user_id);
    }

    getAccessPolicyNameFromId(access_policy_id) {
        return this.mapAccessPolicyIdToName.get(access_policy_id);
    }

    // onClickAddUser() {
    //     if (this.chosen_remaining_user_id == "") return;
    //     this.remaining_user_id_arr = CustomMisc.deleteItemFromArray(this.remaining_user_id_arr, this.chosen_remaining_user_id);
    //     this.selected_user_id_arr.push(this.chosen_remaining_user_id);
    //     CustomLogger.logStringWithObject("AddUser: chosen_remaining_user_id:", this.chosen_remaining_user_id);
    //     CustomLogger.logStringWithObject("AddUser: selected user id arr:", this.selected_user_id_arr);
    // }

    // onClickRemoveUser() {
    //     if (this.chosen_selected_user_id == "") return;
    //     this.selected_user_id_arr = CustomMisc.deleteItemFromArray(this.selected_user_id_arr, this.chosen_selected_user_id);
    //     this.remaining_user_id_arr.push(this.chosen_selected_user_id);
    //     CustomLogger.logStringWithObject("RemoveUser: chosen_remaining_user_id:", this.chosen_remaining_user_id);
    //     CustomLogger.logStringWithObject("RemoveUser: selected user id arr:", this.selected_user_id_arr);
    // }

    onAttributeNameChange() {
        CustomLogger.logStringWithObject("onAttributeNameChange:specificRule.attribute_name", this.specificRule.attribute_name);
        this.specificRuleFormElementArr = this.mapFormIdToFormElements.get(this.specificRule.attribute_name)
    }

    onPermissionChange() {
        if (this.roleDetail.allowed_permission_id_arr.indexOf(CustomGlobalConstants.PERMISSION_TYPES.NONE_PERMISSION) >= 0) {
            this.roleDetail.allowed_permission_id_arr = [];
            this.roleDetail.allowed_permission_id_arr.push(CustomGlobalConstants.PERMISSION_TYPES.NONE_PERMISSION);
        }
    }

    public user = {
        name: 'Clive Lyod',
        age: 26
    }

    async onClickGetUsers() {
        const modalRef = this._modalService.open(SelectUsersModalComponent, { windowClass: 'my-class' });
        modalRef.componentInstance.selectedUserIdArr = this.selected_user_id_arr;
        modalRef.componentInstance.passEntry.subscribe((res) => {
            CustomLogger.logStringWithObject("res::", res);
            this.selected_user_id_arr = res;

        })
    }

    async onClickGetAccessPolicy() {
        const modalRef = this._modalService.open(SelectAccessPolicyComponent, { windowClass: 'my-class' });
        modalRef.componentInstance.selectedAccessPolicyIdArr = this.selected_access_policy_id_arr;
        modalRef.componentInstance.passEntry.subscribe((res) => {
            CustomLogger.logStringWithObject("res::", res);
            this.selected_access_policy_id_arr = res;
        })
    }

}
