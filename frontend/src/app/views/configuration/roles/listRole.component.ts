import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleDetail } from 'src/app/modules/models/sectors/RoleDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';


@Component({
  selector: 'app-listRole',
  templateUrl: './listRole.component.html',
  styleUrls: ['./listRole.component.css']
})
export class ListRoleComponent extends BaseComponent implements OnInit {
  field_listRole_info = "List of all Roles that the users needs to be assigned to."
  field_listRole_button_view = "View Role";
  tableDataArr = [];
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_ROLE, injector);
  }

  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  async init() {
    let result = await this._dbService.getAllRoleDetail().toPromise();
    CustomLogger.logStringWithObject("getAllRoleDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this.router.navigate([CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  onClickEdit(role: RoleDetail) {
    CustomLogger.logStringWithObject("role:", role);
    this.router.navigate([CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE, CustomGlobalConstants.CRUD_UPDATE, role.role_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  onClickView(role: RoleDetail) {
    CustomLogger.logStringWithObject("onClickView:role:", role);
    this.router.navigate([CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE, CustomGlobalConstants.CRUD_READ, role.role_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  filteredTableDataArr: any;
  search(term: string) {
    let fieldName = "role_name";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }

  async onClickDelete(role: RoleDetail) {
    try {
      if (!role.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_ROLE.listRole_role_cannot_be_deleted), true);
        return;
      }
      let optionChosen = await this._miscService.confirmDialogBox("Delete", "Are you sure you want to delete this user ?");
      CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
      if (optionChosen) {
        let result = this._dbService.deleteRoleDetail(role).toPromise();
        CustomLogger.logStringWithObject("deleteRoleDetail:result:", result);
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_ROLE.listRole_role_deleted_success));
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }
  }
};