import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DBService } from 'src/app/modules/services/dbService.service';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';



@Component({
    selector: 'app-selectAccessPolicy',
    templateUrl: './selectAccessPolicyModal.component.html',
})
export class SelectAccessPolicyComponent implements OnInit {

    @Input() selectedAccessPolicyIdArr: string[];
    @Output() passEntry = new EventEmitter();

    field_listAccessPolicy_access_policy_id = "ID";
    field_listAccessPolicy_access_policy_name = "Access Policy Name";
    field_listAccessPolicy_details = "Details";
    btnOkText = "Ok";
    btnCancelText = "Cancel";

    tableDataArr = [];
    filteredTableDataArr = [];
    constructor(private activeModal: NgbActiveModal, private _dbService: DBService) { }

    async ngOnInit() {
        CustomLogger.logStringWithObject("selectedAccessPolicyIdArr::", this.selectedAccessPolicyIdArr);
        let result = await this._dbService.commonGetAll(CustomGlobalConstants.MODEL_NAME.ACCESS_POLICY).toPromise();
        this.tableDataArr = result["data"];
        this.filteredTableDataArr = this.tableDataArr;
    }

    public decline() {
        this.activeModal.close(false);
    }

    public accept() {
        this.activeModal.close(true);
    }

    public dismiss() {
        this.activeModal.dismiss();
    }

    search(term: string) {
        let fieldName = "access_policy_name";
        this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
    }

    onSelectAccessPolicy(access_policy_id) {
        CustomLogger.logString("Selected AccessPolicy:" + access_policy_id);
        if (this.selectedAccessPolicyIdArr.indexOf(access_policy_id) >= 0) {
            this.selectedAccessPolicyIdArr = CustomMisc.deleteItemFromArray(this.selectedAccessPolicyIdArr, access_policy_id);
        } else {
            this.selectedAccessPolicyIdArr.push(access_policy_id);
        }
    }

    isSelected(access_policy_id) {
        if (this.selectedAccessPolicyIdArr.indexOf(access_policy_id) < 0) return false;
        return true;
    }

    passBack() {
        //remove duplicates
        CustomLogger.logStringWithObject("Before: this.selectedAccessPolicyIdArr: ", this.selectedAccessPolicyIdArr);
        this.selectedAccessPolicyIdArr = CustomMisc.removeDuplicatesFromArray(this.selectedAccessPolicyIdArr);
        CustomLogger.logStringWithObject("After: this.selectedAccessPolicyIdArr: ", this.selectedAccessPolicyIdArr);
        this.passEntry.emit(this.selectedAccessPolicyIdArr);
        this.activeModal.close(true);
    }

}
