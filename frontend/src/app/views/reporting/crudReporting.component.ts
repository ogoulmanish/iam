import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { DBService } from 'src/app/modules/services/dbService.service';
import { BaseComponent } from '../../shared/BaseComponent.component';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';


@Component({
    selector: 'app-crudReporting',
    templateUrl: './crudReporting.component.html'
})
export class CrudReportingComponent extends BaseComponent implements OnInit {

    constructor(private _router:Router, injector: Injector) { 
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_REPORTING, injector);
    }

    async ngOnInit() {
    }

}
