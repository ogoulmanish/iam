import { Component, OnInit } from '@angular/core';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { DBService } from 'src/app/modules/services/dbService.service';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { EventService } from 'src/app/modules/services/eventService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';


@Component({
    selector: 'app-entityEventReport',
    templateUrl: './entityEventReport.component.html'
})
export class EntityEventReportComponent implements OnInit {


    //field names
    field_entityEventReport_info = "Reports of entities that are being accessed by which User which Entity and which Risk Level";
    field_entityEventReport_heading = "Search Entities";
    field_entityEventReport_select_user = "Select User";
    field_entityEventReport_select_entity = "Select Entity";
    field_entityEventReport_button_search = "Search";
    field_entityEventReport_risk_level = "Risk Level";

    tableDataArr = [];
    userDetailArr: UserDetail[] = [];
    entityDetailArr: EntityDetail[] = [];
    riskLevelFormElementArr: FormElement[];
    
    currentUsername = "-1"
    currentEntityId = "-1";
    currentRiskLevelId = "-1";

    constructor(private _dbService: DBService, private _eventService: EventService) { }
    async ngOnInit() {
        let result = await this._dbService.getAllUserDetail().toPromise();
        this.userDetailArr = result["data"];
        result = await this._dbService.getAllEntityDetail().toPromise();
        this.entityDetailArr = result["data"];
        result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudEntity_risk_level).toPromise();
        this.riskLevelFormElementArr = result["data"];
    }

    async onSelectUsername() {
        CustomLogger.logString("username:" + this.currentUsername + " EntityId:" + this.currentEntityId + " Risk Level:" + this.currentRiskLevelId);
    }
    async onSelectEntityId() {
        CustomLogger.logString("username:" + this.currentUsername + " EntityId:" + this.currentEntityId + " Risk Level:" + this.currentRiskLevelId);
    }

    async onClickSearch() {
        CustomLogger.logString("username:" + this.currentUsername + " EntityId:" + this.currentEntityId + " Risk Level:" + this.currentRiskLevelId);
        let result = await this._eventService.getSpecificEntityEvents(this.currentUsername, this.currentEntityId, this.currentRiskLevelId).toPromise();
        CustomLogger.logStringWithObject("getSpecificEntityEvents: result", result);
        this.tableDataArr = result["data"];
    }

}