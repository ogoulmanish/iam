import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { SystemEvent } from 'src/app/modules/models/common/SystemEvent.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { LoggingTracking } from 'src/app/modules/models/events/LoggingTracking.model';
import { EventService } from 'src/app/modules/services/eventService.service';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CommonComponent } from 'src/app/shared/CommonComponent.component';
import { EntityDetail } from '../../modules/models/sectors/EntityDetail.model';
import { SectorDetail } from '../../modules/models/sectors/SectorDetail.model';
import { DBService } from '../../modules/services/dbService.service';
import { CustomGlobalConstants } from '../../modules/utils/CustomGlobalConstants';
import { CustomLogger } from '../../modules/utils/CustomLogger';


declare var $: any;

@Component({
  selector: 'app-listReporting',
  templateUrl: './listReporting.component.html',
  styleUrls: [
    './listReporting.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ListReportingComponent extends CommonComponent implements OnInit {
  field_listReportingComponent_system_events_info = "Report that gives All the system events that were carried out inside the system.";
  field_listReportingComponent_user_login_info = "Report that gives information about all successful/failed login attempts made.";

  constructor(private _logginService: LoginService, private _dbService: DBService, private _eventService: EventService, private activatedRoute: ActivatedRoute) {
    super();
  }

  REPORTING_TYPE: any;
  title = 'Default';
  chart;
  chart2 = [];
  pie: any;
  doughnut: any;
  data1 = [];
  tableDataExistingUsersArr: UserDetail[] = [];
  tableDataSystemEventsArr: SystemEvent[] = [];
  tableDataLoggingTrackingArr: LoggingTracking[] = [];

  commonReportHeading = "Reporting";
  flagShowDefaultReport = false;
  flagShowExistingUsersReport = false;
  flagShowSystemEventsReport = false;
  flagShowLoggingTrackingReport = false;

  totalApplicationEntities = 10;
  totalNetworkEntities = 20;
  totalSystemEntities = 30;
  totalFileEntities = 40;

  hideAllReports() {
    this.flagShowDefaultReport = false;
    this.flagShowExistingUsersReport = false;
    this.flagShowSystemEventsReport = false;
    this.flagShowLoggingTrackingReport = false;
  }

  async ngOnInit() {
    this.hideAllReports();

    this.REPORTING_TYPE = CustomGlobalConstants.REPORTING_TYPES.DEFAULT;
    CustomLogger.logString("Inside List Reporting...");
    this.activatedRoute.params.subscribe(
      async params => {
        CustomLogger.logStringWithObject("Params Values ::: ", params);
        this.REPORTING_TYPE = Number(params.reporting_id);
        CustomLogger.logStringWithObject("REPORTING_TYPE ::: ", this.REPORTING_TYPE);

        switch (this.REPORTING_TYPE) {
          case CustomGlobalConstants.REPORTING_TYPES.EXISTING_USERS:
            CustomLogger.logString("Existing Users");
            this.commonReportHeading = "Existing Users";
            this.hideAllReports();
            this.flagShowExistingUsersReport = true;
            this.initExistingUsersReport();
            break;

          case CustomGlobalConstants.REPORTING_TYPES.LOGGING_REPORTS:
            CustomLogger.logString("Logging Reports");
            this.commonReportHeading = "Logging";
            this.hideAllReports();
            this.flagShowLoggingTrackingReport = true;
            this.initLoggingTrackingReport();
            break;


          case CustomGlobalConstants.REPORTING_TYPES.SYSTEM_EVENTS:
            CustomLogger.logString("System Events Report");
            this.commonReportHeading = "System Events";
            this.hideAllReports();
            this.flagShowSystemEventsReport = true;
            this.initSystemEventsReport();
            break;

          default:
            CustomLogger.logString("DEFAULT");
            this.commonReportHeading = "Default";
            this.hideAllReports();
            this.flagShowDefaultReport = true;
            await this.initDefaultReport();
            break;

        }

      }
    );
  }

  async initSystemEventsReport() {
    let systemEventResult = await this._eventService.getAllSystemEvents().toPromise();
    CustomLogger.logStringWithObject("systemEventResult:::", systemEventResult);
    this.tableDataSystemEventsArr = systemEventResult["data"];
  }

  async initExistingUsersReport() {
    CustomLogger.logString("Initializing Existing Users Report");
    let result = await this._dbService.getAllUserDetail().toPromise();
    CustomLogger.logStringWithObject("getAllUserDetail:result:", result);
    this.tableDataExistingUsersArr = result["data"];
  }


  async initLoggingTrackingReport() {
    CustomLogger.logString("Initializing Logging Tracking Report");
    let result = await this._logginService.getAllLoggingTracking().toPromise();
    CustomLogger.logStringWithObject("getAllLoggingTracking:result:", result);
    this.tableDataLoggingTrackingArr = result["data"];
  }

  async initDefaultReport() {
    CustomLogger.logString("Initializing Default Report");
    let sectorDetailResult = await this._dbService.getAllSectorDetail().toPromise();
    let sectorDetailArr: SectorDetail[] = sectorDetailResult["data"];
    sectorDetailArr.forEach(sectorDetail => {
      if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_APPLICATIONS_ID) this.totalApplicationEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_NETWORKS_ID) this.totalNetworkEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_SYSTEMS_ID) this.totalSystemEntities = sectorDetail.entity_id_arr.length;
      else if (sectorDetail.sector_id == CustomGlobalConstants.DEFAULT_SECTOR_FILES_ID) this.totalFileEntities = sectorDetail.entity_id_arr.length;
    });


    ////////////////////// PIE CHARTS
    let allBackgroundColorArr = ["#94d1df", "#df94c6", "#dcdf94", "#9498df", "#5096b5", "#b5a750", "#7fb550", "#b150b5", "#C2272D", "#F8931F", "#009245", "#0193D9", "#0C04ED", "#612F90"];
    let allSampleDataArr = [32, 11, 58, 32, 98, 43, 32, 66, 99, 55, 41, 30, 40, 20, 25, 15, 33, 13, 98, 90, 25];

    //1 - Applications
    let applicationEntityNameArr = [];
    let applicationEntityBackgroundColorArr = [];
    let applicationEntityDataArr = [];
    let applicationEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_APPLICATIONS_ID).toPromise();
    let applicationSector: SectorDetail = applicationEntityResult["data"];
    CustomLogger.logStringWithObject("applicationSector:", applicationSector);
    CustomLogger.logStringWithObject("applicationSector.entity_id_arr:", applicationSector.entity_id_arr);
    let multipleEntityIdArrResult = await this._dbService.getMultipleEntityDetail(applicationSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleEntityIdArrResult:", multipleEntityIdArrResult);
    let multipleEntityDetailArr: EntityDetail[] = multipleEntityIdArrResult["data"];
    for (let k = 0; k < multipleEntityDetailArr.length; k++) {
      applicationEntityNameArr.push(multipleEntityDetailArr[k].entity_name);
      applicationEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      applicationEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie3', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: applicationEntityDataArr,
          backgroundColor: applicationEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: applicationEntityNameArr
      }
    });


    //2 - Networks
    let networkEntityNameArr = [];
    let networkEntityBackgroundColorArr = [];
    let networkEntityDataArr = [];
    let networkEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_NETWORKS_ID).toPromise();
    let networkSector: SectorDetail = networkEntityResult["data"];
    CustomLogger.logStringWithObject("networkSector:", networkSector);
    CustomLogger.logStringWithObject("networkSector.entity_id_arr:", networkSector.entity_id_arr);
    let multipleNetworkEntityIdArrResult = await this._dbService.getMultipleEntityDetail(networkSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleNetworkEntityIdArrResult:", multipleNetworkEntityIdArrResult);
    let multipleNetworkEntityDetailArr: EntityDetail[] = multipleNetworkEntityIdArrResult["data"];
    for (let k = 0; k < multipleNetworkEntityDetailArr.length; k++) {
      networkEntityNameArr.push(multipleNetworkEntityDetailArr[k].entity_name);
      networkEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      networkEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    CustomLogger.logStringWithObject("networkEntityNameArr:::", networkEntityNameArr);
    this.pie = new Chart('pie1', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: networkEntityDataArr,
          backgroundColor: networkEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: networkEntityNameArr
      }
    })


    //3 - Systems
    let systemEntityNameArr = [];
    let systemEntityBackgroundColorArr = [];
    let systemEntityDataArr = [];
    let systemEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_SYSTEMS_ID).toPromise();
    let systemSector: SectorDetail = systemEntityResult["data"];
    CustomLogger.logStringWithObject("systemSector:", systemSector);
    CustomLogger.logStringWithObject("systemSector.entity_id_arr:", systemSector.entity_id_arr);
    let multipleSystemEntityIdArrResult = await this._dbService.getMultipleEntityDetail(systemSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleSystemEntityIdArrResult:", multipleSystemEntityIdArrResult);
    let multipleSystemEntityDetailArr: EntityDetail[] = multipleSystemEntityIdArrResult["data"];
    for (let k = 0; k < multipleSystemEntityDetailArr.length; k++) {
      systemEntityNameArr.push(multipleSystemEntityDetailArr[k].entity_name);
      systemEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      systemEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        }
      },
      data: {
        datasets: [{
          data: systemEntityDataArr,
          backgroundColor: systemEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: systemEntityNameArr
      }
    })

    //4 - Files
    let fileEntityNameArr = [];
    let fileEntityBackgroundColorArr = [];
    let fileEntityDataArr = [];
    let fileEntityResult = await this._dbService.getSpecificSectorDetail(CustomGlobalConstants.DEFAULT_SECTOR_FILES_ID).toPromise();
    let fileSector: SectorDetail = fileEntityResult["data"];
    CustomLogger.logStringWithObject("fileSector:", fileSector);
    CustomLogger.logStringWithObject("fileSector.entity_id_arr:", fileSector.entity_id_arr);
    let multipleFileEntityIdArrResult = await this._dbService.getMultipleEntityDetail(fileSector.entity_id_arr).toPromise();
    CustomLogger.logStringWithObject("multipleFileEntityIdArrResult:", multipleFileEntityIdArrResult);
    let multipleFileEntityDetailArr: EntityDetail[] = multipleFileEntityIdArrResult["data"];
    for (let k = 0; k < multipleFileEntityDetailArr.length; k++) {
      fileEntityNameArr.push(multipleFileEntityDetailArr[k].entity_name);
      fileEntityBackgroundColorArr.push(allBackgroundColorArr[Math.floor(Math.random() * allBackgroundColorArr.length)]);
      fileEntityDataArr.push(allSampleDataArr[Math.floor(Math.random() * allSampleDataArr.length)]);
    }
    this.pie = new Chart('pie2', {
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
        }, legend: {
          position: 'right',
        }, animation: {
          animateScale: true,
          animateRotate: true
        },
      },
      data: {
        datasets: [{
          data: fileEntityDataArr,
          backgroundColor: fileEntityBackgroundColorArr,
          label: 'Dataset 1'
        }],
        labels: fileEntityNameArr
      }
    })
  }

  addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    chart.update();
  }

  removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
      dataset.data.pop();
    });
    chart.update();
  }

  updateChartData(chart, data, dataSetIndex) {
    chart.data.datasets[dataSetIndex].data = data;
    chart.update();
  }


  generatePdf() {

    let data = null;
    const doc = new jsPDF();
    let fileName = "Report.pdf";
    if (this.flagShowDefaultReport) {
      fileName = "Default_Report.pdf";

      data = document.getElementById('content1');
    }
    else if (this.flagShowExistingUsersReport) {
      fileName = "Recent_Users.pdf";
      const col = ["Username", "Email ID", "First Name", "Last Name", "Phone"];
      const rows = [];

      for (let k = 0; k < this.tableDataExistingUsersArr.length; k++) {
        var temp = [this.tableDataExistingUsersArr[k].source_username, this.tableDataExistingUsersArr[k].email,
        this.tableDataExistingUsersArr[k].first_name, this.tableDataExistingUsersArr[k].last_name,
        this.tableDataExistingUsersArr[k].phone
        ];
        rows.push(temp);
      }

      doc.autoTable(col, rows);
      doc.save(fileName);

    }
    else if (this.flagShowSystemEventsReport) {
      fileName = "System_Events.pdf";
      const col = ["Event ID", "Event Source", "Event Type", "Event Details", "Event Time", "Username"];
      const rows = [];

      for (let k = 0; k < this.tableDataSystemEventsArr.length; k++) {
        var temp = [this.tableDataSystemEventsArr[k].event_id, this.tableDataSystemEventsArr[k].event_source,
        this.tableDataSystemEventsArr[k].event_type, this.tableDataSystemEventsArr[k].details,
        this.tableDataSystemEventsArr[k].event_time, this.tableDataSystemEventsArr[k].source_username
        ];
        rows.push(temp);
      }

      doc.autoTable(col, rows);
      doc.save(fileName);

    }

    else {
      alert("PDF Generation not yet implemented for this report.");
      return;
    }

    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      let imgWidth = 208;
      let pageHeight = 295;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)

      pdf.save(fileName); // Generated PDF   
    });
  }



  ///////////////
  testPDF() {
    const doc = new jsPDF();
    const col = ["Username", "Email ID", "First Name", "Last Name", "Phone"];
    const rows = [];

    for (let k = 0; k < this.tableDataExistingUsersArr.length; k++) {
      var temp = [this.tableDataExistingUsersArr[k].source_username, this.tableDataExistingUsersArr[k].email,
      this.tableDataExistingUsersArr[k].first_name, this.tableDataExistingUsersArr[k].last_name,
      this.tableDataExistingUsersArr[k].phone
      ];
      rows.push(temp);
    }

    doc.autoTable(col, rows);
    doc.save('Test.pdf');
  }

}
