import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular-6-datatable';
import { ReportingRoutingModule } from './reporting.routing';
import { ListReportingComponent } from './listReporting.component';
import { CrudReportingComponent } from './crudReporting.component';
import { EntityEventReportComponent } from './entityEventReport.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ReportingRoutingModule
  ],
  declarations: [
    ListReportingComponent,
    CrudReportingComponent,
    EntityEventReportComponent
  ]
})
export class ReportingModule { }