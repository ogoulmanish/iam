import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListReportingComponent } from './listReporting.component';
import { CrudReportingComponent } from './crudReporting.component';
import { EntityEventReportComponent } from './entityEventReport.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      // {
      //   path: 'crudReporting',
      //   component: CrudReportingComponent
      // },
      {
        path: 'entityEventReport',
        component: EntityEventReportComponent
      },
      {
        path: ':reporting_id',
        component: ListReportingComponent
      },
      
    ]
  },

];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule { }
