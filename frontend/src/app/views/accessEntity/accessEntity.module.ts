import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccessEntityRoutingModule } from './accessEntity.routing';
import { ShowEntitiesComponent } from './showEntities.component';


@NgModule({
  imports: [
    SharedModule,
    AccessEntityRoutingModule
  ],
  declarations: [
    ShowEntitiesComponent
  ],
})
export class AccessEntityModule { }
