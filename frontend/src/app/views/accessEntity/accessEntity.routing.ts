import { Routes, RouterModule } from '@angular/router';
import { ShowEntitiesComponent } from './showEntities.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Access Entity',
      status: false
    },
    children: [
      {
        path: 'showEntities',
        component: ShowEntitiesComponent
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessEntityRoutingModule { }
