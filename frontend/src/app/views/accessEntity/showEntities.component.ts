import { Component, OnInit } from '@angular/core';
import { AccessEntity } from 'src/app/modules/models/common/AccessEntity.model';
import { EntityEvent } from 'src/app/modules/models/common/EntityEvent.model';
import { MappingUserEntity } from 'src/app/modules/models/mapping/MappingUserEntity.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { RoleDetail } from 'src/app/modules/models/sectors/RoleDetail.model';
import { RuleDetail } from 'src/app/modules/models/sectors/RuleDetail.model';
import { DBService } from 'src/app/modules/services/dbService.service';
import { EventService } from 'src/app/modules/services/eventService.service';
import { GlobalService } from 'src/app/modules/services/global.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';

@Component({
  selector: 'app-showEntities',
  templateUrl: "./showEntities.component.html",
  styleUrls: ['./showEntities.component.css']
})
export class ShowEntitiesComponent implements OnInit {

  entityDetailArr: EntityDetail[] = [];
  roleDetail: RoleDetail;
  ruleDetail: RuleDetail;
  flagNoMatchingRules = false;
  IMAGES_BASE_LOCATION: any;

  mapEntityIdToEntityDetail: Map<string, EntityDetail>;

  constructor(private _dbService: DBService, private _eventService: EventService, private _globalService: GlobalService) {
    this.IMAGES_BASE_LOCATION = CustomGlobalConstants.BACKEND_SERVER_URL;
    this.mapEntityIdToEntityDetail = new Map();
  }

  getEntityNameFromEntityId(entity_id) {
    let entityDetail = this.mapEntityIdToEntityDetail.get(entity_id);
    if (entityDetail != null) {
      return entityDetail.entity_name;
    }
    return "NOT FOUND";
  }

  getEntityIconFileNameFromEntityId(entity_id) {
    let entityDetail = this.mapEntityIdToEntityDetail.get(entity_id);
    if (entityDetail != null) {
      return entityDetail.icon_filename;
    }
    return "Icon Not Found";
  }

  userToEntityArr: MappingUserEntity[];

  async init() {
    //getMappingUserEntityOfUserId
    let res1 = await this._dbService.getMappingUserEntityOfUserId(this._globalService.getCurrentUserDetail().user_id).toPromise();
    CustomLogger.logStringWithObject("getMappingUserEntityOfUserId:: result::::", res1);
    this.userToEntityArr = res1["data"];
    let entityIdArr = [];
    for (let k = 0; k < this.userToEntityArr.length; k++) {
      entityIdArr.push(this.userToEntityArr[k].entity_id);
    }
    let entityDetailArrResult = await this._dbService.getMultipleEntityDetail(entityIdArr).toPromise();
    CustomLogger.logStringWithObject("entityDetailArr:::", entityDetailArrResult);
    this.entityDetailArr = entityDetailArrResult["data"];

    if (this.entityDetailArr != null && this.entityDetailArr.length > 0) {
      this.entityDetailArr.forEach(entityDetail => {
        this.mapEntityIdToEntityDetail.set(entityDetail.entity_id, entityDetail);
      });
    }

    // await this.fillEntityDetailArr();
  }

  async ngOnInit() {
    await this.init();

  }


  displayCurrentStatus(status_id) {
    let statusName = "Default";
    switch (status_id) {
      case CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE:
        statusName = "Approved";
        break;
      case CustomGlobalConstants.ACCESS_ENTITY_STATUS.DECLINE:
        statusName = "Declined";
        break;
      case CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING:
        statusName = "Pending";
        break;
    }
    return statusName;
  }

  async onClickAccess(mappingUserEntity: MappingUserEntity) {
    CustomLogger.logStringWithObject("clickAccess:userToEntityArr:", this.userToEntityArr);
    CustomLogger.logStringWithObject("clickAccess:mappingUserEntity :", mappingUserEntity);

    try {
      let entityDetail: EntityDetail = this.mapEntityIdToEntityDetail.get(mappingUserEntity.entity_id);
      if (mappingUserEntity.current_status == CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE) {
        CustomMisc.showAlert("User Credentials are already generated and you can access this entity at:" + entityDetail.access_location);
        return;
      }

      if (mappingUserEntity.current_status == CustomGlobalConstants.ACCESS_ENTITY_STATUS.DECLINE) {
        CustomMisc.showAlert("Sorry! You are declined to access this entity.");
        return;
      }

      let accessEntity = new AccessEntity();
      accessEntity.mapping_user_entity_id = mappingUserEntity.mapping_user_entity_id;
      accessEntity.details = "'" + this._globalService.getCurrentUserDetail().username + "' -> '" + entityDetail.entity_name + "'";
      let result = await this._eventService.addAccessEntity(accessEntity).toPromise();
      CustomLogger.logStringWithObject("addAccessEntity: result:", result);

      //add entity event
      let entityEvent = new EntityEvent();
      entityEvent.source_username = this._globalService.getCurrentUserDetail().source_username;
      entityEvent.accessed_username = this._globalService.getCurrentUserDetail().source_username;
      entityEvent.entity_id = entityDetail.entity_id;
      entityEvent.entity_name = entityDetail.entity_name;
      entityEvent.time_of_access = CustomMisc.getCurrentTimeInMilli();
      entityEvent.details = "Requesting Access to Entity: " + entityDetail.entity_name;
      entityEvent.risk_level_id = entityDetail.risk_level_id;
      await this._eventService.addEntityEvent(entityEvent).toPromise();

      //change the request to Pending
      mappingUserEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING;
      await this._dbService.updateMappingUserEntity(mappingUserEntity).toPromise();

      CustomMisc.showAlert("Your request is succesfully sent for Approval.");


    } catch (error) {
      CustomLogger.logStringWithObject("Error in getting permissions for rule", error);
      CustomMisc.showAlert("Error in getting permissions:" + error.message, true);
    }
  }

  async onClickCancelAccess(mappingUserEntity: MappingUserEntity) {
    CustomLogger.logStringWithObject("mappingUserEntity:", mappingUserEntity);
    mappingUserEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING;
    let result = await this._dbService.updateMappingUserEntity(mappingUserEntity).toPromise();
    CustomLogger.logStringWithObject("updateMappingUserEntity:result:", result);
    CustomMisc.showAlert("Your request for cancellation is approved.");
    await this.init();
  }
}
