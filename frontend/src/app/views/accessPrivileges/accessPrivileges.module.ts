import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTableModule } from 'angular-6-datatable';
import { AccessPrivilegesRoutes } from './accessPrivileges.routing';
import { ListAccessPrivilegesComponent } from './listAccessPrivileges.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forChild(AccessPrivilegesRoutes),
    FormsModule,
    NgxDatatableModule,
    DataTableModule
  ],
  declarations: [
    ListAccessPrivilegesComponent
  ]
})
export class AccessPrivilegesModule { }
