import { Location } from '@angular/common';
import { Directive, HostListener, OnInit } from "@angular/core";

@Directive({
    selector: "[appBackButton]"
})
export class BackButtonDirective implements OnInit {
    ngOnInit() { }

    constructor(private location: Location) {
        console.log("Back Button Directive...");
        // this.location.back();
    }

    @HostListener('click')
    onClick() {
        console.log("Back Button Pressed");
        this.location.back();
    }
}   