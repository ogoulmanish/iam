import { Component, Injector, OnInit } from '@angular/core';
import { AccessEntity } from 'src/app/modules/models/common/AccessEntity.model';
import { MongoDBConnectorModel } from 'src/app/modules/models/connectors/database/MongoDBConnectorModel.model';
import { MappingUserEntity } from 'src/app/modules/models/mapping/MappingUserEntity.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';
import { ConnectorService } from 'src/app/modules/services/connectorService.service';
import { EventService } from 'src/app/modules/services/eventService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { ActiveDirectoryConnectorModel } from 'src/app/modules/models/connectors/ActiveDirectoryConnectorModel.model';
import { LinuxConnectorModel } from 'src/app/modules/models/connectors/LinuxConnectorModel.model';

@Component({
    selector: 'app-listAccessEntity',
    templateUrl: './listAccessEntity.component.html'
})
export class ListAccessEntityComponent extends BaseComponent implements OnInit {
    field_listAccessEntityComponent_info = "Requests of entities made by users who were not authorized to access it.";
    field_listAccessEntity_details = "Access Details";
    tableDataArr = [];
    mapIdToName: Map<string, string> = new Map();
    mapIdToDetails: Map<string, string> = new Map();

    constructor(private _eventService: EventService, private _connectorService: ConnectorService, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_ACCESS_ENTITY, injector);
    }

    async init() {
        CustomLogger.logString("Entered List Access Entity Component");
        let result = await this._eventService.getSpecificAccessEntityByCurrentStatus(CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING).toPromise();
        CustomLogger.logStringWithObject("getAllAccessEntity:result:", result);
        this.tableDataArr = result["data"];
        this.tableDataArr = this.tableDataArr;


    }

    async ngOnInit() {
        await this.populateFields();
        await this.init();

        let mapArr1 = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.ENTITY).toPromise();
        CustomLogger.logStringWithObject("getModuleIdNameArray:resultModuleIdNameArray:", mapArr1);
        let mapData1 = mapArr1["data"];
        mapData1.forEach(element => {
            this.mapIdToName.set(element.entity_id, element.entity_name)
        });

        // let mapArr2 = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.MAPPING_USER_ENTITY).toPromise();
        // CustomLogger.logStringWithObject("getModuleIdNameArray:mapArr2:", mapArr2);
        // let mapData2 = mapArr2["data"];
        // mapData2.forEach(element => {
        //     this.mapIdToDetails.set(element.mapping_user_entity_id, element.details);
        // });

    }

    async createUserCredentials(mappingUserEntity: MappingUserEntity) {
        //create username password of user mappingUserEntity.user_id against the entity and change the status
        //1 - get entity details
        let result = null;
        result = await this._dbService.getSpecificEntityDetail(mappingUserEntity.entity_id).toPromise();
        CustomLogger.logStringWithObject("getSpecificEntityDetail:result:", result);
        let entityDetail: EntityDetail = result["data"];

        //////////// PROVISIONING
        //2 - based on the entity type call appropriate function to create the user
        if (entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.MONGODB) {
            CustomLogger.logString("The connector is for MONGODB. Will create user for it.");
            let mongoDbconnectorModel: MongoDBConnectorModel = <MongoDBConnectorModel>entityDetail.connector_object;
            mongoDbconnectorModel.host_name = mongoDbconnectorModel.host_name;
            mongoDbconnectorModel.source_username = mongoDbconnectorModel.source_username;
            mongoDbconnectorModel.host_password = mongoDbconnectorModel.host_password;
            mongoDbconnectorModel.host_port = mongoDbconnectorModel.host_port;
            mongoDbconnectorModel.new_login_name = "user_" + CustomMisc.getCurrentTimeInMilli();
            mongoDbconnectorModel.new_password = "123456";
            result = await this._connectorService.createMongoDBUser(mongoDbconnectorModel).toPromise();
            CustomLogger.logStringWithObject("createMongoDBUser:result:", result);

            //
            mappingUserEntity.login_name = mongoDbconnectorModel.new_login_name;
            mappingUserEntity.password = mongoDbconnectorModel.new_password;
            mappingUserEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE;
            //update the mappingUserEntity
            result = await this._dbService.updateMappingUserEntity(mappingUserEntity).toPromise();
            CustomLogger.logStringWithObject("updateMappingUserEntity:result:", result);
        } else if (entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY) {
            CustomLogger.logString("The connector is for Active Directory. Will create user for it.");
            let activeDirectoryConnectorModel: ActiveDirectoryConnectorModel = <ActiveDirectoryConnectorModel>entityDetail.connector_object;
            // activeDirectoryConnectorModel.host_name = activeDirectoryConnectorModel.host_name;
            // activeDirectoryConnectorModel.source_username = activeDirectoryConnectorModel.source_username;
            // activeDirectoryConnectorModel.host_password = activeDirectoryConnectorModel.host_password;
            // activeDirectoryConnectorModel.host_port = activeDirectoryConnectorModel.host_port;
            // activeDirectoryConnectorModel.new_login_name = "user_" + CustomMisc.getCurrentTimeInMilli();
            activeDirectoryConnectorModel.new_login_name = "cn=" + mappingUserEntity.username + "," + activeDirectoryConnectorModel.container_dn;
            activeDirectoryConnectorModel.new_password = "123456";
            activeDirectoryConnectorModel.source_user_id = mappingUserEntity.user_id;
            result = await this._connectorService.createActiveDirectoryUser(activeDirectoryConnectorModel).toPromise();
            CustomLogger.logStringWithObject("createActiveDirectoryUser:result:", result);

            //
            mappingUserEntity.login_name = activeDirectoryConnectorModel.new_login_name;
            mappingUserEntity.password = activeDirectoryConnectorModel.new_password;
            mappingUserEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE;
            //update the mappingUserEntity
            result = await this._dbService.updateMappingUserEntity(mappingUserEntity).toPromise();
            CustomLogger.logStringWithObject("updateMappingUserEntity:result:", result);
        } else if (entityDetail.connector_type == CustomGlobalConstants.CONNECTOR_TYPE.LINUX) {
            CustomLogger.logString("The connector is for Linux Machine. Will create user for it.");
            let linuxConnectorModel: LinuxConnectorModel = <LinuxConnectorModel>entityDetail.connector_object;
            linuxConnectorModel.new_password = "123456";
            linuxConnectorModel.source_user_id = mappingUserEntity.user_id;
            result = await this._connectorService.createLinuxUser(linuxConnectorModel).toPromise();
            CustomLogger.logStringWithObject("createLinuxUser:result:", result);

            //
            mappingUserEntity.login_name = linuxConnectorModel.new_login_name;
            mappingUserEntity.password = linuxConnectorModel.new_password;
            mappingUserEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE;
            //update the mappingUserEntity
            result = await this._dbService.updateMappingUserEntity(mappingUserEntity).toPromise();
            CustomLogger.logStringWithObject("updateMappingUserEntity:result:", result);
        } else {
            CustomLogger.logString("NO CONNECTORS FOUND....");
            CustomMisc.showAlert("No matching connectors found....");
        }

    }

    async onClickApprove(accessEntity: AccessEntity) {
        try {
            let optionChosen = await this._miscService.confirmDialogBox("Approve", "Are you sure you want to approve this request ?");
            CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
            if (optionChosen) {
                //now based on the request type create appropriate login credentials of that entity of that user
                let result = await this._dbService.getSpecificMappingUserEntity(accessEntity.mapping_user_entity_id).toPromise();
                CustomLogger.logStringWithObject("getSpecificMappingUserEntity:result:", result);
                let mappingUserEntity: MappingUserEntity = result["data"];
                await this.createUserCredentials(mappingUserEntity);

                //update accessEntity
                accessEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE;
                accessEntity.details = "Access given";
                accessEntity.last_update_time = CustomMisc.getCurrentTimeInMilli();
                accessEntity.is_request_new = false;
                result = await this._eventService.updateAccessEntity(accessEntity).toPromise();
                CustomLogger.logStringWithObject("updateAccessEntity:result:", result);
            }
        } catch (error) {
            CustomLogger.logStringWithObject("Error:", error);
        }
        await this.init();

    }



    async onClickDecline(accessEntity: AccessEntity) {
        accessEntity.current_status = CustomGlobalConstants.ACCESS_ENTITY_STATUS.DECLINE;
        accessEntity.last_update_time = CustomMisc.getCurrentTimeInMilli();
        accessEntity.is_request_new = false;
        accessEntity.details = "Access Denied";
        let result = await this._eventService.updateAccessEntity(accessEntity).toPromise();

        //update the rules tab as well
        await this.init();
    }



    displayAccessEntityStatus(status_id: number) {
        let statusStr = "111";
        switch (status_id) {
            case CustomGlobalConstants.ACCESS_ENTITY_STATUS.APPROVE:
                statusStr = "Allowed";
                break;
            case CustomGlobalConstants.ACCESS_ENTITY_STATUS.DECLINE:
                statusStr = "Cancel";
                break;
            case CustomGlobalConstants.ACCESS_ENTITY_STATUS.PENDING:
                statusStr = "Pending";
                break;
            default:
                statusStr = "DONT KNOW";
                break;
        }
        return statusStr;
    }

    onClickRefresh() {
        this.init();
    }
}