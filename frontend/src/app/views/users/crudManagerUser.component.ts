import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerUserDetail } from 'src/app/modules/models/common/ManagerUserDetail.model';
import { PageMatrix } from 'src/app/modules/models/common/PageMatrix.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../shared/BaseComponent.component';

@Component({
    selector: 'app-crudManagerUser',
    templateUrl: './crudManagerUser.component.html'
})
export class CrudManagerUserComponent extends BaseComponent implements OnInit {
    field_crudManagerUser_info = "Assign which users will be managed by this manager.";
    

    currentCrudType = CustomGlobalConstants.PARAM_CRUD_TYPE;
    allUnmanagedUserDetailArr: UserDetail[] = [];
    pageMatrixArr: PageMatrix[] = [];
    currentUserDetail: UserDetail;
    managerUserDetail: ManagerUserDetail;
    noneManagerUserOptionValue = "None";
    
    constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_MANAGER_USER, injector);
        this.currentUserDetail = new UserDetail();
        this.managerUserDetail = new ManagerUserDetail();
    }
 
    async htmlInit() {
        CustomLogger.logStringWithObject("this.activatedRoute.params::", this.activatedRoute.params);

        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let user_id = params["id"];
                        let result = await this._dbService.getSpecificUserDetail(user_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificUserDetail:result:", result);
                        this.currentUserDetail = result["data"];
                        this.managerUserDetail = Object.assign(this.managerUserDetail, this.currentUserDetail);
                        this.managerUserDetail.parent_user_id = user_id;
                        this.managerUserDetail.managerUserIdArr = [];

                        result = await this._dbService.getUsersOfManagerUser(user_id).toPromise();
                        CustomLogger.logStringWithObject("getUsersOfManagerUser:result:", result);
                        let usersOfManagerUserArr: UserDetail[] = result["data"];
                        usersOfManagerUserArr.forEach(tmpUserDetail => {
                            // CustomLogger.logString("000000000000000000000000");
                            this.managerUserDetail.managerUserIdArr.push(tmpUserDetail.user_id);
                        });

                        result = await this._dbService.getUnmanagedUsers(user_id).toPromise();
                        CustomLogger.logStringWithObject("getUnmanagedUsers:result:", result);
                        this.allUnmanagedUserDetailArr = result["data"];

                        result = await this._dbService.getAllPageMatrix().toPromise();
                        CustomLogger.logStringWithObject("getAllPageMatrix:result:", result);
                        this.pageMatrixArr = result["data"];

                        CustomLogger.logStringWithObject("allUnmanagedUserDetailArr::", this.allUnmanagedUserDetailArr);
                        CustomLogger.logStringWithObject("currentUserDetail.managerUserIdArr::", this.managerUserDetail.managerUserIdArr);
                    }
                }
            }
        );
    }

    async ngOnInit() {
        await this.populateFields();
        await this.htmlInit();
    }

    onSelectUser(val) {
        CustomLogger.logStringWithObject("val::", val);
        CustomLogger.logStringWithObject("User details selected::", this.managerUserDetail.managerUserIdArr);
        if (val === this.noneManagerUserOptionValue) {
            CustomLogger.logString("Ignoring others....");
            this.managerUserDetail.managerUserIdArr = [];
        }
    }
    async onSubmit() {
        try {
            CustomLogger.logStringWithObject("User details selected::", this.managerUserDetail.managerUserIdArr);
            let result = this._dbService.updateManagersForUsers(this.managerUserDetail).toPromise();
            CustomLogger.logStringWithObject("updateManagersForUsers:", result);
            CustomMisc.showAlert("Successfully updated the Managed User List");
            this._router.navigate(["users/listManagerUser"]);
        } catch (error) {
            CustomMisc.showAlert("Error in updating Managed User List: " + error.message, true);
            this._router.navigate(["users/listManagerUser"]);
        }
    }
    
    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER, this.activatedRoute);
    }
}