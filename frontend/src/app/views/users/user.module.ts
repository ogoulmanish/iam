import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular-6-datatable';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrudUserComponent } from './crudUser.component';
import { ListUserComponent } from './listUser.component';
import { UserRoutingModule } from './user.routing';
import { ListAccessEntityComponent } from './listAccessEntity.component';
import { ListManagerUserComponent } from './listManagerUser.component';
import { CrudManagerUserComponent } from './crudManagerUser.component';
import { ListMakerCheckerRequestComponent } from './listMakerCheckerRequest.component';
import { InvalidinputsfocusDirective } from './invalidinputsfocus.directive';

@NgModule({
  imports: [
    DataTableModule,
    UserRoutingModule,
    SharedModule
  ],
  declarations: [
    ListUserComponent,
    CrudUserComponent,
    ListAccessEntityComponent,
    ListManagerUserComponent,
    CrudManagerUserComponent,
    ListMakerCheckerRequestComponent,
    InvalidinputsfocusDirective
  ]
})
export class UserModule { }