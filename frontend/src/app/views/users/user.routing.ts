import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserComponent } from './listUser.component';
import { CrudUserComponent } from './crudUser.component';
import { ListAccessEntityComponent } from './listAccessEntity.component';
import { ListManagerUserComponent } from './listManagerUser.component';
import { CrudManagerUserComponent } from './crudManagerUser.component';
import { ListMakerCheckerRequestComponent } from './listMakerCheckerRequest.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Users',
      status: false
    },
    children: [
      {
        path: 'listUser',
        component: ListUserComponent
      },
      {
        path: 'crudUser',
        component: CrudUserComponent
      },
      {
        path: 'crudUser/:crudType/:id',
        component: CrudUserComponent
      },
      {
        path: 'listAccessEntity',
        component: ListAccessEntityComponent
      },
      {
        path: 'listManagerUser',
        component: ListManagerUserComponent
      },
      {
        path: 'crudManagerUser/:crudType/:id',
        component: CrudManagerUserComponent
      },
      {
        path: 'listMakerCheckerRequest',
        component: ListMakerCheckerRequestComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
