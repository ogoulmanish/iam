import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { ConnectorService } from 'src/app/modules/services/connectorService.service';
import { SearchUserDetail } from 'src/app/modules/models/common/SearchUserDetail.model';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';


@Component({
  selector: 'app-listUser',
  templateUrl: './listUser.component.html',
  styleUrls: ['./listUser.component.css']
})
export class ListUserComponent extends BaseComponent implements OnInit {
  field_listUserComponent_info = "List of all users (currently from Active Directory) inside the system.";
  // field_listUser_role_name = "Role Name";
  field_listUser_is_added_to_main = "Is added to main ?";
  // field_listUser_first_name
  // field_listUser_last_name
  field_listUser_is_active = "Is Active";
  field_listUser_is_locked = "Is Locked";
  field_listUser_department = "Department";
  field_listUser_job_title = "Job Title";
  field_listUser_button_view = "View";

  tableDataArr = [];
  filteredTableDataArr: any;
  mapIdToName: Map<string, string> = new Map();

  searchUserDetail: SearchUserDetail;

  advancedUsersSearchBy = "";
  advancedUsersSearchValue = "";
  WHICH_SEARCH_HEADING = "";

  advancedUsersSearchValueArr = [];
  PICKLIST_ADVANCED_USERS_SEARCH_BY = [];
  PICKLIST_SEARCH_USER_BOOLEAN_OPTION = [];
  departmentFormElementArr: FormElement[];
  jobTitleFormElementArr: FormElement[];

  // flagShowViewButton = true;
  // flagShowEditButton = true;
  // flagShowDeleteButton = true;



  constructor(private _connectorService: ConnectorService, private _ldapService: LoginService, private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_USER, injector);
    this.onClickReset();
  }

  async init() {
    CustomLogger.logString("Entered List User Component");
    CustomLogger.logStringWithObject("this._globalService.getCurrentUserDetail():::", this._globalService.getCurrentUserDetail());
    let result = null;
    if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.ADMIN)
      result = await this._dbService.getAllUserDetail().toPromise();
    else if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.HR)
      result = await this._dbService.getUsersForHRUser().toPromise();
    else
      result = await this._dbService.getUsersOfManagerUser(this._globalService.getCurrentUserDetail().user_id).toPromise();
    CustomLogger.logStringWithObject("getAllUserDetail:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;

    let resultModuleIdNameArray = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.ROLE).toPromise();
    CustomLogger.logStringWithObject("getModuleIdNameArray:resultModuleIdNameArray:", resultModuleIdNameArray);
    let mapData = resultModuleIdNameArray["data"];
    mapData.forEach(element => {
      this.mapIdToName.set(element.role_id, element.role_name);
    });

    this.resetAdvancedSearch();
    this.onClickReset();

    //based on user type - add view button
    let userType = this._globalService.getCurrentUserDetail().user_type;
    // if (userType == CustomGlobalConstants.USER_TYPES.HR) {
    //   this.flagShowDeleteButton = false;
    // } else if (userType == CustomGlobalConstants.USER_TYPES.MANAGER) {
    //   this.flagShowDeleteButton = false;
    // }

    //based on Page Matrix, enable/disable buttons
    let pageMatrix = this._globalService.getPageMatrix();
    if (pageMatrix.permission_listUser.create) this.isCreatable = true;
    if (pageMatrix.permission_listUser.read) this.isReadable = true;
    if (pageMatrix.permission_listUser.update) this.isUpdatable = true;
    if (pageMatrix.permission_listUser.delete) this.isDeletable = true;

    // CustomLogger.logStringWithObject("Page Permission:", pageMatrix);
    // CustomLogger.logStringWithObject("isDeletable:", this.isUpdatable);
  }

  async resetAdvancedSearch() {
    this.PICKLIST_ADVANCED_USERS_SEARCH_BY = [
      CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ROLE_NAME,
      CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ACCESS_POLICY_NAME,
      CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ENTITY_NAME
    ]
    this.advancedUsersSearchBy = "";
    this.advancedUsersSearchValue = "";
    this.WHICH_SEARCH_HEADING = "";
  }

  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  onClickAddUser() {
    CustomLogger.logString("Will route to add User...");
    this.router.navigate(["crudUser"], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  onClickEdit(user: UserDetail) {
    CustomLogger.logStringWithObject("onClickEdit:user:", user);
    this.router.navigate(['crudUser', CustomGlobalConstants.CRUD_UPDATE, user.user_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  onClickView(user: UserDetail) {
    CustomLogger.logStringWithObject("onClickView:user:", user);
    this.router.navigate(['crudUser', CustomGlobalConstants.CRUD_READ, user.user_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  async onClickDelete(userDetail: UserDetail) {
    CustomLogger.logStringWithObject("Will delete user:", userDetail);

    try {
      if (!userDetail.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_USER.listUser_user_cannot_be_deleted), true);
        return;
      }

      if (this._globalService.getAuthType() == CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT) {
        CustomLogger.logString("Cannot delete user during Default Auth Type");
        CustomMisc.showAlert("Cannot delete user during Default Auth Type", true);
        return;
      }

      let optionChosen = await this._miscService.confirmDialogBox("Delete", "Are you sure you want to delete this user ?");
      CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
      if (optionChosen) {
        let result = null;

        if (!userDetail.is_added_to_main) {
          result = await this._dbService.deleteUserDetail(userDetail).toPromise();
        } else {
          //Deprovision user - VERY IMPORTANT
          await this._connectorService.deprovisionUser(userDetail).toPromise();
          CustomLogger.logStringWithObject("After deprovisioning the user is:", userDetail);
          result = await this._ldapService.deleteLdapUser(userDetail).toPromise();
        }
        CustomLogger.logStringWithObject("deleted User:result:", result);
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_USER.listUser_user_deleted_success), true);
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }
  }


  async onAdvancedSearchByChange() {
    CustomLogger.logString("Will search users by: " + this.advancedUsersSearchBy);
    //fill up advancedUsersSearchValueArr
    let result = await this._dbService.getAdvancedUsersSearchValues(this.advancedUsersSearchBy).toPromise();
    CustomLogger.logStringWithObject("getAdvancedUsersSearchValues:result", result);
    this.advancedUsersSearchValueArr = result["data"];
    if (this.advancedUsersSearchValueArr != null && this.advancedUsersSearchValueArr.length > 0) {
      this.advancedUsersSearchValue = this.advancedUsersSearchValueArr[0].id;
    }
  }

  async onClickAdvancedUserSearch() {
    CustomLogger.logString("Will search '" + this.advancedUsersSearchBy + "' for value: '" + this.advancedUsersSearchValue + "'");
    if (this.advancedUsersSearchBy == "") return;

    let result = null;
    if (this.advancedUsersSearchBy == CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ROLE_NAME) {
      result = await this._dbService.getAllUserDetailsOfSpecificRole(this.advancedUsersSearchValue).toPromise();
    } else if (this.advancedUsersSearchBy == CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ENTITY_NAME) {
      result = await this._dbService.getAllUserDetailsOfSpecificEntities(this.advancedUsersSearchValue).toPromise();
    } else if (this.advancedUsersSearchBy == CustomGlobalConstants.ADVANCED_USERS_SEARCH_BY.ACCESS_POLICY_NAME) {
      result = await this._dbService.getAllUserDetailsOfSpecificAccessPolicies(this.advancedUsersSearchValue).toPromise();
    }
    this.WHICH_SEARCH_HEADING = "Results of Users belonging to Category of '" + this.advancedUsersSearchBy + "'";
    CustomLogger.logStringWithObject("Search Results:", result);
    if (result == null) this.filteredTableDataArr = [];
    else this.filteredTableDataArr = result["data"];
  }

  async onGetAllUsers() {
    this.WHICH_SEARCH_HEADING = "";
    await this.init();
  }

  onResetAdvancedSearch() {
    this.resetAdvancedSearch();
  }

  async onClickSearch() {
    CustomLogger.logStringWithObject("Will search for: ", this.searchUserDetail);
    let result = await this._dbService.searchUsers(this.searchUserDetail).toPromise();
    CustomLogger.logStringWithObject("result:", result);
    this.filteredTableDataArr = result["data"];
  }

  async onClickReset() {
    this.searchUserDetail = new SearchUserDetail();
    this.searchUserDetail.source_user_type = this._globalService.getCurrentUserDetail().user_type;
    this.PICKLIST_SEARCH_USER_BOOLEAN_OPTION = CustomGlobalConstants.ARRAY_SEARCH_USER_BOOLEAN_OPTION;

    //fill up combo boxes
    let result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_department).toPromise();
    CustomLogger.logStringWithObject("getDataOfFormElement:result:", result);
    this.departmentFormElementArr = result["data"];

    result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_job_title).toPromise();
    CustomLogger.logStringWithObject("getDataOfFormElement:result:", result);
    this.jobTitleFormElementArr = result["data"];
  }


  search(term: string) {
    let fieldName = "username";
    this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }
};
