import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMatrix } from 'src/app/modules/models/common/PageMatrix.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { RoleDetail } from 'src/app/modules/models/sectors/RoleDetail.model';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../shared/BaseComponent.component';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { MappingUserEntity } from 'src/app/modules/models/mapping/MappingUserEntity.model';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';

@Component({
    selector: 'app-crudUser',
    templateUrl: './crudUser.component.html'
})
export class CrudUserComponent extends BaseComponent implements OnInit {
    field_crudUser_info = "Details of user which currently will be accessed in Main Active Directory";
    field_retypePassword = "Confirm Password";
    field_crudUser_department = "Department";
    field_crudUser_job_title = "Job Title";
    alert_crudUser_blank_username = "Please enter non-blank username";
    field_crudUser_read = "View";

    field_crudUser_page_matrix_id = "Page Matrix";
    field_crudUser_page_matrix_id_required = true;
    field_crudUser_page_matrix_id_error_message = "Error";
    field_crudUser_role_details = "Details";
    field_crudUser_role_id = "Role ID";
    field_crudUser_role_name = "Role Name";
    roleTableDataArr = [];

    currentCrudType = CustomGlobalConstants.PARAM_CRUD_TYPE;
    roleDetailArr: RoleDetail[] = [];
    pageMatrixArr: PageMatrix[] = [];

    flagDisableRoleId = false;
    flagIsADAuthenticationType: boolean = false;
    userDetail: UserDetail;
    baseDN: string = "";
    retypePasswordText: string = "";
    originalRoleId = "";

    departmentFormElementArr: FormElement[];
    jobTitleFormElementArr: FormElement[];

    PICKLIST_TYPE_OF_USER = [];
    flagIsReadonly = false;
    flagShowSaveButton = true;
    flagShowResetButton = true;
    flagShowCancelButton = true;

    userToEntityArr: MappingUserEntity[];
    entityDetailArr: EntityDetail[] = [];
    mapEntityIdToEntityDetail: Map<string, EntityDetail> = new Map();
    IMAGES_BASE_LOCATION = CustomGlobalConstants.BACKEND_SERVER_URL;

    constructor(private _ldapService: LoginService, private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_USER, injector);
    }

    async fillupMatchingUserEntity(userId) {
        //getMappingUserEntityOfUserId
        let res1 = await this._dbService.getMappingUserEntityOfUserId(userId).toPromise();
        CustomLogger.logStringWithObject("getMappingUserEntityOfUserId:: result::::", res1);
        this.userToEntityArr = res1["data"];
        let entityIdArr = [];
        for (let k = 0; k < this.userToEntityArr.length; k++) {
            entityIdArr.push(this.userToEntityArr[k].entity_id);
        }
        let entityDetailArrResult = await this._dbService.getMultipleEntityDetail(entityIdArr).toPromise();
        CustomLogger.logStringWithObject("entityDetailArr:::", entityDetailArrResult);
        this.entityDetailArr = entityDetailArrResult["data"];

        if (this.entityDetailArr != null && this.entityDetailArr.length > 0) {
            this.entityDetailArr.forEach(entityDetail => {
                this.mapEntityIdToEntityDetail.set(entityDetail.entity_id, entityDetail);
            });
        }

        await this.getRoleDetailsOfUser(userId);
    }

    modifyFlags(crudUserType) {
        if (crudUserType == undefined || crudUserType == CustomGlobalConstants.CRUD_CREATE) {
            this.flagIsReadonly = false;
            this.flagShowSaveButton = true;
            this.flagShowResetButton = true;
            this.flagShowCancelButton = true;
            this.isComponentNew = true;
        } else if (crudUserType == CustomGlobalConstants.CRUD_READ) {
            this.flagIsReadonly = true;
            this.flagShowSaveButton = false;
            this.flagShowResetButton = false;
            this.flagShowCancelButton = false;
        } else if (crudUserType == CustomGlobalConstants.CRUD_UPDATE) {
            this.flagIsReadonly = false;
            this.flagShowSaveButton = true;
            this.flagShowResetButton = true;
            this.flagShowCancelButton = true;
            this.isComponentNew = false;
        }
    }

    async htmlInit() {
        try {
            if (this._globalService.getAuthType() == CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY) {
                this.flagIsADAuthenticationType = true;
                this.baseDN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.BASE_DOMAIN;
                this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
            }
            let result = null;

            //for HR we need specific user types only
            if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.HR) {
                this.PICKLIST_TYPE_OF_USER = CustomGlobalConstants.ARRAY_HR_USER_TYPES;
                result = await this._dbService.getRoleDetailOfUserType(CustomGlobalConstants.USER_TYPES.HR).toPromise();
            } else if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.MANAGER) {
                this.PICKLIST_TYPE_OF_USER = CustomGlobalConstants.ARRAY_MANAGER_USER_TYPES;
                result = await this._dbService.getRoleDetailOfUserType(CustomGlobalConstants.USER_TYPES.HR).toPromise();
            } else {
                this.PICKLIST_TYPE_OF_USER = CustomGlobalConstants.ARRAY_ADMIN_USER_TYPES;
                result = await this._dbService.getAllRoleDetail().toPromise();
            }
            CustomLogger.logStringWithObject("PICKLIST_TYPE_OF_USER::", this.PICKLIST_TYPE_OF_USER);
            this.roleDetailArr = result["data"];

            result = await this._dbService.getAllPageMatrix().toPromise();
            CustomLogger.logStringWithObject("getAllPageMatrix:result:", result);
            this.pageMatrixArr = result["data"];


            //fill up combo boxes
            result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_department).toPromise();
            CustomLogger.logStringWithObject("getDataOfFormElement:result:", result);
            this.departmentFormElementArr = result["data"];

            result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.crudUser_job_title).toPromise();
            CustomLogger.logStringWithObject("getDataOfFormElement:result:", result);
            this.jobTitleFormElementArr = result["data"];

        } catch (error) {
            CustomLogger.logStringWithObject("ERROR:", error);
        }

    }

    async ngOnInit() {
        this.userDetail = new UserDetail();
        if (this.isComponentNew) {
            this.userDetail.salutation = "Mr.";
            //IMPORTANT
            if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.HR) {
                this.userDetail.parent_user_id = CustomGlobalConstants.DEFAULT_PARENT_USER_ID.ADMIN;
            } else {
                this.userDetail.parent_user_id = this._globalService.getCurrentUserDetail().user_id;
            }
        }

        await this.populateFields();
        this.htmlInit();
        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[this.currentCrudType];
                this.modifyFlags(tmpCrudType);
                if (tmpCrudType) {
                    CustomLogger.logStringWithObject("tmpCrudType 22 ::: ", tmpCrudType);
                    let user_id = params["id"];
                    //check if it is read-only
                    if (tmpCrudType == CustomGlobalConstants.CRUD_READ || tmpCrudType == CustomGlobalConstants.CRUD_UPDATE) {
                        this.isComponentNew = false;
                        let result = await this._dbService.getSpecificUserDetail(user_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificUserDetail:result:", result);
                        this.userDetail = result["data"];
                        this.retypePasswordText = this.userDetail.password;
                    }
                    if (tmpCrudType == CustomGlobalConstants.CRUD_READ) {
                        //get roles and entities of this user
                        this.fillupMatchingUserEntity(user_id);
                    }
                }
            }
        );
    }



    async onSubmit() {
        CustomLogger.logStringWithObject("New User will be added...", this.userDetail);

        if (this._globalService.getAuthType() == CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT) {
            CustomLogger.logString("Cannot add/edit/delete user during Default Auth Type");
            CustomMisc.showAlert("Cannot add/edit/delete user during Default Auth Type", true);
            return;
        }

        //check if the passwords match
        if (this.userDetail.password != this.retypePasswordText) {
            CustomMisc.showAlert("Passwords do not match. Please input correct password.");
            return;
        }


        // first create the user and keep "is_added_to_main" to false. Only during HRMS Restoration the user will be added to the actual AD
        this.userDetail.creator_user_id = this._globalService.getCurrentUserDetail().user_id;
        this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT;
        let successMessage = CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_add_success;
        try {
            let result = null;
            if (this.isComponentNew) {
                let nonHashedPassword = this.userDetail.password;
                if (this.flagIsADAuthenticationType) {
                    this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
                    this.userDetail.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.USERNAME_START_STRING + this.userDetail.username + "," + this.baseDN;
                    // result = await this._ldapService.addLdapUser(this.userDetail).toPromise();
                    result = await this._dbService.addUserDetail(this.userDetail).toPromise();

                    CustomLogger.logStringWithObject("User Detail got from db:", result);
                    //get the details of the new user
                    this.userDetail = result["data"];
                    // 2 -
                    await this._miscService.sendNotificationsForNewUserCreation(this.userDetail, nonHashedPassword);
                    CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(successMessage));
                }
                else {
                    CustomLogger.logString("Since the authentication type is not AD, so ignoring ADD USER");
                    CustomMisc.showAlert("Since the authentication type is not AD, so ignoring ADD USER", true);
                }
            } else {
                if (this.flagIsADAuthenticationType) {
                    this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
                    this.userDetail.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.USERNAME_START_STRING + this.userDetail.username + "," + this.baseDN;
                    if (this.userDetail.is_added_to_main) {
                        result = await this._ldapService.updateLdapUser(this.userDetail).toPromise();
                    } else {
                        result = await this._dbService.updateUserDetail(this.userDetail).toPromise();
                    }
                    successMessage = CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_update_success;
                    CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(successMessage));
                }
                else {
                    // result = await this._dbService.updateUserDetail(this.userDetail).toPromise();
                    CustomLogger.logString("Since the authentication type is not AD, so ignoring UDPATE");
                    CustomMisc.showAlert("Since the authentication type is not AD, so ignoring UDPATE", true);
                }
            }
        } catch (err) {
            CustomLogger.logStringWithObject("error from db:", err.error.message);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_add_error), true);
        }
        this.router.navigate(["users/listUser"]);
    }


    async onSubmit_old() {
        CustomLogger.logStringWithObject("New User will be added...", this.userDetail);
        if (this._globalService.getAuthType() == CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT) {
            CustomLogger.logString("Cannot add/edit/delete user during Default Auth Type");
            CustomMisc.showAlert("Cannot add/edit/delete user during Default Auth Type", true);
            return;
        }

        //check if the passwords match
        if (this.userDetail.password != this.retypePasswordText) {
            CustomMisc.showAlert("Passwords do not match. Please input correct password.");
            return;
        }


        //1 - create the user
        //2 - send email to hr and to the new user with new username and password created


        //1
        this.userDetail.creator_user_id = this._globalService.getCurrentUserDetail().user_id;
        this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.DEFAULT;
        let successMessage = CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_add_success;
        try {
            let result = null;
            if (this.isComponentNew) {
                let nonHashedPassword = this.userDetail.password;
                if (this.flagIsADAuthenticationType) {
                    this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
                    this.userDetail.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.USERNAME_START_STRING + this.userDetail.username + "," + this.baseDN;
                    result = await this._ldapService.addLdapUser(this.userDetail).toPromise();
                }
                else {
                    result = await this._dbService.addUserDetail(this.userDetail).toPromise();
                    CustomLogger.logString("Since the authentication type is not AD, so ignoring ADD");
                }
                CustomLogger.logStringWithObject("User Detail got from db:", result);
                //get the details of the new user
                this.userDetail = result["data"];

                // 2 -
                await this._miscService.sendNotificationsForNewUserCreation(this.userDetail, nonHashedPassword);
            } else {
                if (this.flagIsADAuthenticationType) {
                    this.userDetail.external_authentication_application = CustomGlobalConstants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
                    this.userDetail.DN = CustomGlobalConstants.ACTIVE_DIRECTORY_SERVER.USERNAME_START_STRING + this.userDetail.username + "," + this.baseDN;
                    result = await this._ldapService.updateLdapUser(this.userDetail).toPromise();
                }
                else {
                    // result = await this._dbService.updateUserDetail(this.userDetail).toPromise();
                    CustomLogger.logString("Since the authentication type is not AD, so ignoring UDPATE");
                }
                successMessage = CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_update_success;
            }

            // //////////////
            // let flagRoleChanged = false;
            // if (this.originalRoleId != this.userDetail.role_id)
            //     flagRoleChanged = true;
            // if (this.isComponentNew || flagRoleChanged) {
            //     let res1 = await this._dbService.refreshMappingUserEntity(this.userDetail.user_id).toPromise();
            //     CustomLogger.logStringWithObject("refreshMappingUserEntity:result", res1);
            // }
            ////////////
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(successMessage));
        } catch (err) {
            CustomLogger.logStringWithObject("error from db:", err.error.message);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_add_error), true);
        }
        this.router.navigate(["users/listUser"]);
    }

    onClickReset() {
        this.userDetail.first_name = "";
        this.userDetail.last_name = "";
        this.userDetail.google_id = "";
        this.userDetail.middle_name = "";
        this.userDetail.full_name = "";
        this.userDetail.profile_link = "";
        this.userDetail.date_of_birth = "";
        this.userDetail.email = "";
        this.userDetail.facebook_id = "";
        if (this.isComponentNew) {
            this.userDetail.username = "";
            this.userDetail.password = "";
            this.retypePasswordText = "";
        }
    }

    async onCheckLDAP(username) {
        CustomLogger.logString("Will check for LDAP uid:" + username);
        if (username.trim() == "") {
            CustomMisc.showAlert(this.alert_crudUser_blank_username, true);
            return;
        }
        try {
            let result = await this._ldapService.checkIfLDAPUserExists(username).toPromise();
            // alert("User '" + username + "' Already Exists.");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_alrady_exists), true);
        } catch (error) {
            CustomLogger.logError(error);
            // alert("You can use '" + username + "'.");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_USER.crudUser_user_does_not_exist), true);
        }
    }

    onChangeUserType() {
        CustomLogger.logString("Selected user type:" + this.userDetail.user_type);
    }

    onCancel() {
        this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_USER, this.activatedRoute);
    }

    getEntityNameFromEntityId(entity_id) {
        let entityDetail = this.mapEntityIdToEntityDetail.get(entity_id);
        if (entityDetail != null) {
            return entityDetail.entity_name;
        }
        return "NOT FOUND";
    }

    getEntityIconFileNameFromEntityId(entity_id) {
        let entityDetail = this.mapEntityIdToEntityDetail.get(entity_id);
        if (entityDetail != null) {
            return entityDetail.icon_filename;
        }
        return "Icon Not Found";
    }

    async getRoleDetailsOfUser(user_id) {
        let result = await this._dbService.getRoleDetailsOfSpecificUserId(user_id).toPromise();
        CustomLogger.logStringWithObject("result:", result);
        this.roleTableDataArr = result["data"];
    }
}