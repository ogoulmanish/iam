import { Component, Injector, OnInit } from '@angular/core';
import { MakerCheckerRequest } from 'src/app/modules/models/common/MakerCheckerRequest.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
    selector: 'app-listMakerCheckerRequest',
    templateUrl: './listMakerCheckerRequest.component.html'
})
export class ListMakerCheckerRequestComponent extends BaseComponent implements OnInit {
    field_listMakerCheckerRequest_info = "List of all the pending requests of modifications done by other users.";
    field_listMakerCheckerRequest_maker_object = "Maker Object";
    field_listMakerCheckerRequest_heading = "Pending Maker Checker Requests";
    field_listMakerCheckerRequest_maker_request_time = "Request Time";
    field_listMakerCheckerRequest_maker_component_name = "Component";
    field_listMakerCheckerRequest_maker_CRUD_operation = "Operation";
    field_listMakerCheckerRequest_current_status = "Status";
    field_listMakerCheckerRequest_maker_user_id = "Maker ID";
    field_listMakerCheckerRequest_button_approve = "Approve";
    field_listMakerCheckerRequest_button_decline = "Decline";

    //for non-admin
    field_listMakerCheckerRequest_button_cancel = "Cancel";

    tableDataArr = [];
    isAdmin = true;
    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER, injector);
    }

    async init() {
        CustomLogger.logString("Entered List Maker Checker Request Component");
        let result = await this._dbService.getMakerCheckerRequestFromStatus(CustomGlobalConstants.MAKER_CHECKER_REQUEST_STATUS.PENDING).toPromise();
        CustomLogger.logStringWithObject("getMakerCheckerRequestFromStatus:result:", result);
        this.tableDataArr = result["data"];

        //check if it is admin
        if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.ADMIN)
            this.isAdmin = true;
        else
            this.isAdmin = false;
    }

    async ngOnInit() {
        await this.populateFields();
        await this.init();
    }

    async onClickApprove(makerCheckerRequest: MakerCheckerRequest) {
        CustomLogger.logStringWithObject("WIll approve request:", makerCheckerRequest);
        try {
            let result = await this._dbService.approveMakerCheckerRequest(makerCheckerRequest).toPromise();
            CustomLogger.logStringWithObject("approveMakerCheckerRequest:result:", result);
            CustomMisc.showAlert("Request was approved successfully");
            await this.init();
        } catch (error) {
            CustomLogger.logStringWithObject("Error in approving:", error);
            CustomMisc.showAlert("Error in approving");
        }

    }

    async onClickDecline(makerCheckerRequest: MakerCheckerRequest) {
        try {
            CustomLogger.logStringWithObject("WIll decline request:", makerCheckerRequest);
            let result = await this._dbService.declineMakerCheckerRequest(makerCheckerRequest).toPromise();
            CustomLogger.logStringWithObject("declineMakerCheckerRequest:result:", result);
            CustomMisc.showAlert("Request was declined successfully");
            await this.init();
        } catch (error) {
            CustomLogger.logStringWithObject("Error in declining:", error);
            CustomMisc.showAlert("Error in declining");
        }
    }

    async onClickCancel(makerCheckerRequest: MakerCheckerRequest) {
        try {
            CustomLogger.logStringWithObject("WIll cancel request:", makerCheckerRequest);
            let result = await this._dbService.cancelMakerCheckerRequest(makerCheckerRequest).toPromise();
            CustomLogger.logStringWithObject("cancelMakerCheckerRequest:result:", result);
            CustomMisc.showAlert("Request was cancelled successfully");
            await this.init();
        } catch (error) {
            CustomLogger.logStringWithObject("Error in cancelling:", error);
            CustomMisc.showAlert("Error in cancelling");
        }
    }

    async onClickRefresh() {
        await this.init();
    }

    displayCRUDOperationName(n) {
        let operationName = "Create";
        if (n == CustomGlobalConstants.CRUD_CREATE) operationName = "Create";
        else if (n == CustomGlobalConstants.CRUD_UPDATE) operationName = "Update";
        else if (n == CustomGlobalConstants.CRUD_DELETE) operationName = "Delete";
        return operationName;
    }
}

