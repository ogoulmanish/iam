import { Directive, HostListener, ElementRef } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgForm } from '@angular/forms';

@Directive({
  selector: '[appInvalidinputsfocus]'
})
export class InvalidinputsfocusDirective {


  constructor(private el: ElementRef) { }

  @HostListener('submit')
  OnSubmit() {
    const invalidControl = this.el.nativeElement.querySelector('.ng-invalid');

    if (invalidControl) {
      invalidControl.focus();  
    }
  }
}