import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';


@Component({
    selector: 'app-listManagerUser',
    templateUrl: './listManagerUser.component.html'
})
export class ListManagerUserComponent extends BaseComponent implements OnInit {
    field_listManagerUserComponent_info = "List of all Managers assigned to monitor specific users.";

    tableDataArr = [];
    constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER, injector);
    }

    async init() {
        CustomLogger.logString("Entered List Managed User Component");
        // let result = await this._dbService.getAllUserDetailsOfSpecificRole(CustomGlobalConstants.DEFAULT_ROLES.MANAGER_ROLE).toPromise();
        let result = await this._dbService.getAllUsersOfType(CustomGlobalConstants.USER_TYPES.MANAGER).toPromise();
        CustomLogger.logStringWithObject("getAllUsersOfType:result:", result);
        this.tableDataArr = result["data"];
    }

    async ngOnInit() {
        await this.populateFields();
        await this.init();
    }

    onClickEdit(user: UserDetail) {
        CustomLogger.logStringWithObject("user:", user);
        this.router.navigate(['crudManagerUser', CustomGlobalConstants.CRUD_UPDATE, user.user_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
    }
}