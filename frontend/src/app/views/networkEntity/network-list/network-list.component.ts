import { Component, OnInit } from '@angular/core';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';


@Component({
  selector: 'app-network-list',
  templateUrl: './network-list.component.html',
  styleUrls: ['./network-list.component.css']
})
export class NetworkListComponent implements OnInit {
  entitydetailsArr = [];
  entityDetail = new EntityDetail();
  entityDetail2 = new EntityDetail();
  entityDetail3 = new EntityDetail();
  entityDetail4 = new EntityDetail();
  entityDetail5 = new EntityDetail();
  entityDetail6 = new EntityDetail();

  constructor() { }
  ngOnInit() {
    this.entityDetail.entity_id = '1';
    this.entityDetail.entity_name = 'Switch';
    this.entityDetail.access_location = '';
    this.entityDetail.icon_location = "assets/images/sectors/networks/switch.png";


    this.entityDetail2.entity_id = '10';
    this.entityDetail2.entity_name = 'Router';
    this.entityDetail2.access_location = '';
    this.entityDetail2.icon_location = "assets/images/sectors/networks/router.png";


    this.entityDetail3.entity_id = '10';
    this.entityDetail3.entity_name = 'Printer';
    this.entityDetail3.access_location = '';
    this.entityDetail3.icon_location = "assets/images/sectors/networks/printer.png";



    this.entityDetail4.entity_id = '4';
    this.entityDetail4.entity_name = 'Modem';
    this.entityDetail4.access_location = '';
    this.entityDetail4.icon_location = "assets/images/sectors/networks/modem.png";




    this.entitydetailsArr.push(this.entityDetail)
    this.entitydetailsArr.push(this.entityDetail2)
    this.entitydetailsArr.push(this.entityDetail3)
    this.entitydetailsArr.push(this.entityDetail4)
  }

}