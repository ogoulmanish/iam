import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NetworkListComponent } from './network-list/network-list.component';
import{CreatenetworkComponent} from './createnetwork/createnetwork.component'




const routes: Routes = [
    {
      path: '',
      data: {
        breadcrumb: 'Networks',
        status: false
      },
      children: [
        // {
        //   path: 'listDevices',
        //   component: ListDevicesComponent
        // }, 
        {
          path: 'network-list',
          component: NetworkListComponent
        }, 
        
        {
            path: 'createnetwork',
            component: CreatenetworkComponent
          },   
      ]
    }
  ];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class NetworkEntityRoutingModule { }
  