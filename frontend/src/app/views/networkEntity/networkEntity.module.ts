import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NetworkEntityRoutingModule } from './networkEntity.routing';
import { NetworkListComponent} from './network-list/network-list.component';
import{CreatenetworkComponent} from './createnetwork/createnetwork.component'



@NgModule({
    imports: [
      CommonModule,

      NetworkEntityRoutingModule
    ],
    declarations: [
        NetworkListComponent,
        CreatenetworkComponent,
       
    ],
    // entryComponents: [ButtonRenderComponent],
  })
  export class NetworkEntityModule { }