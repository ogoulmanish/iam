import { Component, Injector, OnInit } from '@angular/core';
import { AlertNotification } from 'src/app/modules/models/common/AlertNotification.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from '../../../shared/BaseComponent.component';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';

@Component({
    selector: 'app-listAlertNotification',
    templateUrl: './listAlertNotification.component.html'
})
export class ListAlertNotificationComponent extends BaseComponent implements OnInit {
    field_alertNotification_info = "Dynamically configure the text of the alert messages of individual components.";
    currentAlertNotification: AlertNotification;
    alertNotificationArr: AlertNotification[] = [];
    componentNameArr = CustomGlobalConstants.ALL.ARRAY_COMPONENT_NAMES;
    idToAlertNotificationMap: Map<string, AlertNotification> = new Map();
    selected_alert_notification_id: string;
    selected_component_name: string;
    flagShowAlertBoxes = false;

    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.ALERT_NOTIFICATION, injector);
    }

    async ngOnInit() {
        await this.populateFields();
        this.currentAlertNotification = new AlertNotification();
    }

    async onSelectComponentChange() {
        let result = await this._dbService.getAlertNotificationOfComponent(this.selected_component_name).toPromise();
        CustomLogger.logStringWithObject("getAlertNotificationOfComponent:result:", result);
        this.alertNotificationArr = result["data"];
        if (this.alertNotificationArr.length == 0) {
            // CustomMisc.showAlert("No records found");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.ALERT_NOTIFICATION.alertNotification_no_records_found), true);
            this.flagShowAlertBoxes = false;
        } else {
            this.alertNotificationArr.forEach(alertNotification => {
                this.idToAlertNotificationMap.set(alertNotification.alert_notification_id, alertNotification);
            });
            this.currentAlertNotification = this.alertNotificationArr[0];
            CustomLogger.logStringWithObject("Selected Alert:", this.currentAlertNotification);
            CustomLogger.logString("before: this.selected_component_name:" + this.selected_component_name);
            this.selected_component_name = this.currentAlertNotification.component_name;
            this.selected_alert_notification_id = this.currentAlertNotification.alert_notification_id;
            this.flagShowAlertBoxes = true;
            CustomLogger.logString("after: this.selected_component_name:" + this.selected_component_name);
        }
    }

    async onSelectAlertNotification() {
        CustomLogger.logStringWithObject("onSelectAlertNotification:alert_notification_id:", this.selected_alert_notification_id);
        this.currentAlertNotification = this.idToAlertNotificationMap.get(this.selected_alert_notification_id);
        // this.currentAlertNotification = obj;
        // CustomLogger.logStringWithObject("onSelectAlertNotification:currentAlertNotification:", this.currentAlertNotification.alert_value);

    }

    async onClickUpdate() {
        CustomLogger.logStringWithObject("Current Alert Notification Object:", this.currentAlertNotification);
        try {
            let result = await this._dbService.updateAlertNotification(this.currentAlertNotification).toPromise();
            CustomLogger.logStringWithObject("updateAlertNotification:result:", result);
            // CustomMisc.showAlert("Alert Message Updated Succssfully");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.ALERT_NOTIFICATION.alertNotification_update_success));

            //reload the map
            this._dbService.reloadAllAlertNotification();
        } catch (error) {
            // CustomMisc.showAlert("Error in updating:" + error.message, true);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.ALERT_NOTIFICATION.alertNotification_update_error), true);
        }
    }
}