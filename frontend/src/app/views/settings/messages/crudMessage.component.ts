import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { BaseComponent } from '../../../shared/BaseComponent.component';

@Component({
    selector: 'app-crudMessage',
    templateUrl: './crudMessage.component.html'
})
export class CrudMessageComponent extends BaseComponent implements OnInit {

    constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_PERMISSION, injector);
    }

    
    async ngOnInit() {
        await this.populateFields();
    }
}