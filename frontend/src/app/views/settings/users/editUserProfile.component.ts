import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { LoginService } from 'src/app/modules/services/loginService.service';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
    selector: 'app-editUserProfile',
    templateUrl: './editUserProfile.component.html'
})
export class EditUserProfileComponent extends BaseComponent implements OnInit {
    field_editUserProfile_info = "Edit details of the logged in user";
    field_password = "";
    field_retypePassword = "Confirm Password";
    userDetail: UserDetail;
    retypePasswordText: any;
    salutationFormElementArr: FormElement[];
    constructor(private _ldapService: LoginService, private _router: Router, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.EDIT_USER_PROFILE, injector);
    }

    async ngOnInit() {
        this.userDetail = this._globalService.getCurrentUserDetail();
        this.isComponentNew = false;
        await this.populateFields();
        this.field_password = "Password";
        //fill up combo boxes
        let result = await this._dbService.getDataOfFormElement(CustomGlobalConstants.COMBO_BOX_NAMES.editProfile_salutation).toPromise();
        this.salutationFormElementArr = result["data"];
        this.retypePasswordText = this.userDetail.password;
    }

    async onSubmit() {
        CustomLogger.logStringWithObject("User will be updated.", this.userDetail);
        try {
            let result = await this._ldapService.updateLdapUser(this.userDetail).toPromise();
            CustomLogger.logStringWithObject("result got from db:", result);
            // CustomMisc.showAlert("User Profile successfully updated...");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EDIT_USER_PROFILE.editUserProfile_update_success));
        } catch (err) {
            CustomLogger.logStringWithObject("error from db:", err.error.message);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EDIT_USER_PROFILE.editUserProfile_update_error), true);
        }
        if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.STANDARD)
            this._router.navigate(["/standardDashboard"]);
        else if (this._globalService.getCurrentUserDetail().user_type == CustomGlobalConstants.USER_TYPES.HR)
            this._router.navigate(["/hrDashboard"]);
        else
            this._router.navigate(["/dashboard"]);
    }

    onClickReset() {
        this.userDetail.first_name = "";
        this.userDetail.last_name = "";
        this.userDetail.google_id = "";
        this.userDetail.middle_name = "";
        this.userDetail.full_name = "";
        this.userDetail.profile_link = "";
        this.userDetail.date_of_birth = "";
        this.userDetail.email = "";
        this.userDetail.facebook_id = "";
    }
}