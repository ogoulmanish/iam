import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular-6-datatable';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmailAlertComponent } from './alerts/emailAlert.component';
import { EditFieldsComponent } from './fields/editFields.component';
import { SettingsRoutingModule } from './settings.routing';
import { EditUserProfileComponent } from './users/editUserProfile.component';
import { ListAlertNotificationComponent } from './messages/listAlertNotification.component';
import { CrudMessageComponent } from './messages/crudMessage.component';
import { ListDefaultSettingComponent } from './default/listDefaultSetting.component';
import { ListPageMatrixComponent } from './pageMatrix/listPageMatrix.component';
import { CrudPageMatrixComponent } from './pageMatrix/crudPageMatrix.component';
import { ListFormElementComponent } from './configureFormElements/listFormElement.component';
import { CrudFormElementComponent } from './configureFormElements/crudFormElement.component';
import { ListApprovalMatrixComponent } from './approvalMatrix/listApprovalMatrix.component';
import { ChildPermissionComponent } from './pageMatrix/childPermission.component';

@NgModule({
  imports: [
    DataTableModule,
    SharedModule,
    SettingsRoutingModule
  ],
  declarations: [
    EditFieldsComponent,
    EditUserProfileComponent,
    EmailAlertComponent,
    ListAlertNotificationComponent,
    CrudMessageComponent,
    ListDefaultSettingComponent,
    ListPageMatrixComponent,
    CrudPageMatrixComponent,
    ListFormElementComponent,
    CrudFormElementComponent,
    ListApprovalMatrixComponent,
    ChildPermissionComponent
  ],
})
export class SettingsModule { }
