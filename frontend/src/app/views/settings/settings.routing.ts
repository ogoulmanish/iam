import { NgModule, Injector, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailAlertComponent } from './alerts/emailAlert.component';
import { EditFieldsComponent } from './fields/editFields.component';
import { CrudMessageComponent } from './messages/crudMessage.component';
import { ListAlertNotificationComponent } from './messages/listAlertNotification.component';
import { EditUserProfileComponent } from './users/editUserProfile.component';
import { ListDefaultSettingComponent } from './default/listDefaultSetting.component';
import { ListPageMatrixComponent } from './pageMatrix/listPageMatrix.component';
import { CrudPageMatrixComponent } from './pageMatrix/crudPageMatrix.component';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { ListFormElementComponent } from './configureFormElements/listFormElement.component';
import { CrudFormElementComponent } from './configureFormElements/crudFormElement.component';
import { ListApprovalMatrixComponent } from './approvalMatrix/listApprovalMatrix.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Settings',
      status: false
    },
    children: [
      {
        path: 'emailAlerts',
        component: EmailAlertComponent
      },
      {
        path: 'editFields',
        component: EditFieldsComponent
      },
      {
        path: 'editUserProfile',
        component: EditUserProfileComponent
      },
      {
        path: 'listMessage',
        component: ListAlertNotificationComponent
      },
      {
        path: 'crudMessage',
        component: CrudMessageComponent
      },
      {
        path: 'listDefaultSetting',
        component: ListDefaultSettingComponent
      },
      {
        path: 'listPageMatrix',
        component: ListPageMatrixComponent
      },
      {
        path: 'crudPageMatrix',
        component: CrudPageMatrixComponent
      },
      {
        path: 'crudPageMatrix/:crudType/:id',
        component: CrudPageMatrixComponent
      },
      {
        path: 'listFormElement',
        component: ListFormElementComponent
      },
      {
        path: 'crudFormElement',
        component: CrudFormElementComponent
      },
      {
        path: 'crudFormElement/:crudType/:id',
        component: CrudFormElementComponent
      },
      {
        path: 'listApprovalMatrix',
        component: ListApprovalMatrixComponent
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule extends BaseComponent implements OnInit {
  constructor(injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.COMMON, injector);
  }

  async ngOnInit() {
    await this.populateFields();
  }
}
