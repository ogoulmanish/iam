import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { ApprovalMatrix } from 'src/app/modules/models/common/ApprovalMatrix.model';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';


@Component({
    selector: 'app-listApprovalMatrix',
    templateUrl: './listApprovalMatrix.component.html'
})
export class ListApprovalMatrixComponent extends BaseComponent implements OnInit {
    field_listApprovalMatrix_info = "Integral part of Maker-Checker model. For each of the Manager Users (users of 'Manager Role'), decide if they need approval for performing CRUD operation on individual components inside the system.";
    field_listApprovalMatrix_heading = "Appoval Matrix";
    field_listApprovalMatrix_role_id = "Role ID";
    field_listApprovalMatrix_component_name = "Component Name";
    field_listApprovalMatrix_requires_approval = "Requires Approval";
    field_listApprovalMatrix_create_permission = "Create Permission";
    field_listApprovalMatrix_update_permission = "Update Permission";
    field_listApprovalMatrix_delete_permission = "Delete Permission";
    field_listApprovalMatrix_button_update = "Update";
    field_listpprovalMatrix_button_reset = "Reset";

    tableDataArr = [];

    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER, injector);
    }

    async init() {
        //currently load for only Manager type.
        let user_type = CustomGlobalConstants.USER_TYPES.MANAGER;
        CustomLogger.logString("Entered List Approval Matrix Component");
        let result = await this._dbService.getApprovalMatrixFromUserType(user_type).toPromise();
        CustomLogger.logStringWithObject("getApprovalMatrixFromRoleId:result:", result);
        this.tableDataArr = result["data"];
    }

    async ngOnInit() {
        await this.populateFields();
        await this.init();
    }

    async onClickUpdate(approvalMatrix: ApprovalMatrix) {
        try {
            CustomLogger.logStringWithObject("Approval Matrix:", approvalMatrix);
            if (!approvalMatrix.requires_approval) {
                approvalMatrix.create_permission = false;
                approvalMatrix.update_permission = false;
                approvalMatrix.delete_permission = false;
            }
            let result = await this._dbService.updateSpecificApprovalMatrix(approvalMatrix).toPromise();
            CustomLogger.logStringWithObject("updateSpecificApprovalMatrix:result:", result);
            CustomMisc.showAlert("Successfully updated the Permissions.");
        } catch (error) {
            CustomLogger.logStringWithObject("updateSpecificApprovalMatrix:error:", error);
            CustomMisc.showAlert("Error in updating the Permissions." + error.message);
        }

    }

}