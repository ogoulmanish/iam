import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApprovalMatrix } from 'src/app/modules/models/common/ApprovalMatrix.model';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { MakerCheckerRequest } from 'src/app/modules/models/common/MakerCheckerRequest.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../../shared/BaseComponent.component';

@Component({
    selector: 'app-crudFormElement',
    templateUrl: './crudFormElement.component.html'
})
export class CrudFormElementComponent extends BaseComponent implements OnInit {

    //fields
    field_crudFormElementComponent_info = "Dynamically configure the name and value of form element.";
    field_crudFormElement_heading_configure_form_element = "Configure";
    field_crudFormElement_select_combo = "Select Data";
    field_crudFormElement_data_name = "Data Name";
    field_crudFormElement_data_value = "Data Value";
    field_crudMessage_button_update = "Update";
    field_crudFormElement_data_name_error_message = "Error in Data name";
    field_crudFormElement_data_value_error_message = "Error in Data value"


    flagFormElementCombo = true;
    currentFormElement: FormElement;
    current_form_data_name = "";
    current_form_data_value = "";
    currentAction = 0;//0-read, 1-add, 2-edit, 3-delete
    currentActionName = "";

    formElementArr: FormElement[];
    constructor(private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_PAGE_MATRIX, injector);
        this.formElementArr = [];
        this.currentFormElement = new FormElement();
    }

    async init(form_element_name) {
        let result = await this._dbService.getDataOfFormElement(form_element_name).toPromise();
        CustomLogger.logStringWithObject("getDataOfFormElement:result:", result);
        this.formElementArr = result["data"];
        this.currentFormElement = this.formElementArr[0];
        this.current_form_data_name = this.currentFormElement.data_name;
        this.current_form_data_value = this.currentFormElement.data_value;
        this.currentActionName = "";
        this.currentAction = 0;
    }

    async ngOnInit() {
        await this.populateFields();
        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let form_element_name = params["id"];
                        await this.init(form_element_name);
                    }
                }
            }
        );
    }


    async onClickAddNew() {
        this.currentAction = 1;
        this.currentActionName = "Add";
        this.current_form_data_name = "";
        this.current_form_data_value = "";
    }

    async onClickUpdate() {
        this.currentAction = 2;
        this.currentActionName = "Update";
        this.current_form_data_name = this.currentFormElement.data_name;
        this.current_form_data_value = this.currentFormElement.data_value;
    }

    async onClickDelete() {
        if (this.formElementArr.length <= 1) {
            CustomMisc.showAlert("Atleast 1 element needs to be present.", true);
            this.currentAction = 0;
            this.currentActionName = "";
            return;
        }
        this.currentAction = 3;
        this.currentActionName = "Delete";
        this.current_form_data_name = this.currentFormElement.data_name;
        this.current_form_data_value = this.currentFormElement.data_value;
    }

    async onClickCurrentAction() {
        CustomLogger.logString("WIll perform: " + this.currentActionName + " on this form element");
        try {
            let requires_approval = false;
            // check if approval is required or not
            let approvalMatrix: ApprovalMatrix = this._miscService.getApprovalMatrixOfComponentFromArray(this._globalService.getApprovalMatrix(), CustomGlobalConstants.COMPONENT_NAME.CRUD_FORM_ELEMENT);
            CustomLogger.logStringWithObject("getApprovalMatrixOfComponentFromArray: approvalMatrix", approvalMatrix);
            if (approvalMatrix != null && approvalMatrix.requires_approval) {
                // CustomMisc.showAlert("Requires Approval");
                requires_approval = true;
            } else {
                // CustomMisc.showAlert("No Approval required");
            }

            //maker-checker
            let makerCheckerRequest = new MakerCheckerRequest();
            makerCheckerRequest.current_status = CustomGlobalConstants.MAKER_CHECKER_REQUEST_STATUS.PENDING;
            makerCheckerRequest.maker_component_name = CustomGlobalConstants.COMPONENT_NAME.CRUD_FORM_ELEMENT;
            makerCheckerRequest.maker_user_id = this._globalService.getCurrentUserDetail().user_id;
            makerCheckerRequest.source_username = this._globalService.getCurrentUserDetail().source_username;
            CustomLogger.logStringWithObject("makerCheckerRequest::::", makerCheckerRequest);

            let result = null;
            switch (this.currentAction) {
                case 1:
                    let newFormElement = new FormElement();
                    newFormElement.data_name = this.current_form_data_name;
                    newFormElement.data_value = this.current_form_data_value;
                    newFormElement.details = this.currentFormElement.details;
                    newFormElement.form_element_name = this.currentFormElement.form_element_name;
                    newFormElement.form_element_type = this.currentFormElement.form_element_type;
                    newFormElement.remarks = this.currentFormElement.remarks;
                    CustomLogger.logStringWithObject("WIll store new form element:", newFormElement);

                    //create
                    if (requires_approval) {
                        makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_CREATE;
                        makerCheckerRequest.maker_object = newFormElement;
                        CustomLogger.logStringWithObject("Sending MakerCheckerRequest:", this.currentFormElement);
                        result = await this._dbService.addMakerCheckerRequest(makerCheckerRequest).toPromise();
                        CustomLogger.logStringWithObject("addMakerCheckerRequest:result:", result);
                    } else {
                        result = await this._dbService.addFormElement(newFormElement).toPromise();
                        CustomLogger.logStringWithObject("addFormElement:result:", result);
                        await this.init(this.currentFormElement.form_element_name);
                    }
                    break;
                case 2:
                    this.currentFormElement.data_name = this.current_form_data_name;
                    this.currentFormElement.data_value = this.current_form_data_value;
                    if (requires_approval) {
                        //update
                        makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_UPDATE; //for updates
                        makerCheckerRequest.maker_object = this.currentFormElement;
                        CustomLogger.logStringWithObject("Sending MakerCheckerRequest:", this.currentFormElement);
                        result = await this._dbService.addMakerCheckerRequest(makerCheckerRequest).toPromise();
                        CustomLogger.logStringWithObject("addMakerCheckerRequest:result:", result);
                    } else {
                        CustomLogger.logStringWithObject("WIll update form element:", this.currentFormElement);
                        result = await this._dbService.updateFormElement(this.currentFormElement).toPromise();
                        CustomLogger.logStringWithObject("updateFormElement:result:", result);
                        await this.init(this.currentFormElement.form_element_name);
                    }
                    break;
                case 3:
                    CustomLogger.logStringWithObject("WIll delete form element:", this.currentFormElement);
                    if (requires_approval) {
                        //delete
                        makerCheckerRequest.maker_CRUD_operation = CustomGlobalConstants.CRUD_DELETE;
                        makerCheckerRequest.maker_object = this.currentFormElement;
                        CustomLogger.logStringWithObject("Sending MakerCheckerRequest:", this.currentFormElement);
                        result = await this._dbService.addMakerCheckerRequest(makerCheckerRequest).toPromise();
                        CustomLogger.logStringWithObject("addMakerCheckerRequest:result:", result);
                    } else {
                        result = await this._dbService.deleteFormElement(this.currentFormElement).toPromise();
                        CustomLogger.logStringWithObject("deleteFormElement:result:", result);
                        await this.init(this.currentFormElement.form_element_name);
                    }
                    break;
            }

            //refresh corresponding form element
            if (requires_approval)
                CustomMisc.showAlert("Your request is being sent for approval and if approved it will be carried out.");
            else
                CustomMisc.showAlert("Added/Updated Successfully");
            await this.init(this.currentFormElement.form_element_name);
        } catch (error) {
            CustomLogger.logError(error);
            CustomMisc.showAlert("Error in Adding/Updating");
        }

    }

    onClickCancelCurrentAction() {
        this.currentActionName = "";
        this.currentAction = 0;
        this.current_form_data_name = this.currentFormElement.data_name;
        this.current_form_data_value = this.currentFormElement.data_value;
    }

    onSelectComboData() {
        CustomLogger.logStringWithObject("currentFormElement:", this.currentFormElement);
        this.current_form_data_name = this.currentFormElement.data_name;
        this.current_form_data_value = this.currentFormElement.data_value;
    }
}