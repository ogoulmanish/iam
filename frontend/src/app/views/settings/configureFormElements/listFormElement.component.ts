import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormElement } from 'src/app/modules/models/common/FormElement.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
  selector: 'app-listFormElement',
  templateUrl: './listFormElement.component.html'
})
export class ListFormElementComponent extends BaseComponent implements OnInit {

  //fields
  field_listFormElement_info = "List of form elements that can be dynamically configured.";
  field_listFormElement_heading_form_element_management = "Form Element";
  field_listFormElement_heading_form_element = "Form Element";
  field_listFormElement_form_element_id = "ID";
  field_listFormElement_form_element_type = "Element Type";
  field_listFormElement_form_element_name = "Element Name";
  field_listFormElement_button_edit = "Edit";

  tableDataArr = [];
  mapIdToName: Map<string, string> = new Map();
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_FORM_ELEMENT, injector);
  }

  async init() {
    let result = await this._dbService.getUniqueFormElement().toPromise();
    CustomLogger.logStringWithObject("getUniqueFormElement:result:", result);
    this.tableDataArr = result["data"];
  }

  async ngOnInit() {
    await this.populateFields();
    await this.init();
  }

  onClickEdit(formElement: FormElement) {
    CustomLogger.logStringWithObject("formElement:", formElement);
    this.router.navigate(['crudFormElement', CustomGlobalConstants.CRUD_UPDATE, formElement.form_element_name], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }
}