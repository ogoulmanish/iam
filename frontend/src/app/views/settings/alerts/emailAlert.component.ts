import { Component, Injector, OnInit } from '@angular/core';
import { EmailAlert } from 'src/app/modules/models/common/EmailAlert.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { EventService } from 'src/app/modules/services/eventService.service';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';

@Component({
    selector: 'app-emailAlert',
    templateUrl: './emailAlert.component.html'
})
export class EmailAlertComponent extends BaseComponent implements OnInit {
    field_emailAlertComponent_info = "Configure if Email needs to be sent and if yes then of which alert level";
    emailAlert: EmailAlert;

    constructor(private _eventService: EventService, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.EMAIL_ALERT, injector);
        this.emailAlert = new EmailAlert();
    }

    async ngOnInit() {
        await this.populateFields();
        let result = await this._eventService.getEmailAlert().toPromise();
        CustomLogger.logStringWithObject("getEmailAlert():result", result);
        this.emailAlert = result["data"];
        if (this.emailAlert.alert_level_all)
            this.onClickAllAlert();
        CustomLogger.logStringWithObject("emailAlert::", this.emailAlert);
    }

    async onClickUpdate() {
        CustomLogger.logStringWithObject("emailAlert::", this.emailAlert);
        try {
            let result = await this._eventService.updateEmailAlert(this.emailAlert).toPromise();
            CustomLogger.logStringWithObject("updateEmailAlert:result", result);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EMAIL_ALERT.emailAlert_update_success));
        } catch (error) {
            CustomLogger.logError(error);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EMAIL_ALERT.emailAlert_update_error), true);
        }
    }

    onClickAllAlert() {
        this.emailAlert.alert_level_critical = true;
        this.emailAlert.alert_level_high = true;
        this.emailAlert.alert_level_medium = true;
        this.emailAlert.alert_level_low = true;
    }

    onClickNoAllAlert() {
        this.emailAlert.alert_level_all = false;
    }

}