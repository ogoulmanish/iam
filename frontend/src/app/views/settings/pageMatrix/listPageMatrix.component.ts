import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMatrix } from 'src/app/modules/models/common/PageMatrix.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';


@Component({
  selector: 'app-listPageMatrix',
  templateUrl: './listPageMatrix.component.html'
})
export class ListPageMatrixComponent extends BaseComponent implements OnInit {
  field_listPageMatrix_info = "List of Access matrixes configured for each of the specific role and user.";
  field_listPageMatrix_page_matrix_name = "Name";

  tableDataArr = [];
  mapIdToName: Map<string, string> = new Map();
  constructor(private router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.LIST_PAGE_MATRIX, injector);
  }

  async ngOnInit() {
    await this.populateFields();
    this.init();


    let resultModuleIdNameArray = await this._dbService.getModuleIdNameArray(CustomGlobalConstants.FRONTEND_MODULE_NAME_ID.ROLE).toPromise();
    CustomLogger.logStringWithObject("getModuleIdNameArray:resultModuleIdNameArray:", resultModuleIdNameArray);
    let mapData = resultModuleIdNameArray["data"];
    mapData.forEach(element => {
      this.mapIdToName.set(element.role_id, element.role_name);
    });

  }

  async init() {
    let result = await this._dbService.getAllPageMatrix().toPromise();
    CustomLogger.logStringWithObject("getAllPageMatrix:result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }

  onClickAdd() {
    CustomLogger.logString("Will route to add ...");
    this.router.navigate(['crudPageMatrix'], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }


  onClickEdit(pageMatrix: PageMatrix) {
    CustomLogger.logStringWithObject("pageMatrix:", pageMatrix);
    this.router.navigate(['crudPageMatrix', CustomGlobalConstants.CRUD_UPDATE, pageMatrix.page_matrix_id], { relativeTo: this.activatedRoute.parent, skipLocationChange: true });
  }

  async onClickDelete(pageMatrix: PageMatrix) {
    CustomLogger.logStringWithObject("Will delete pageMatrix:", pageMatrix);

    try {
      if (!pageMatrix.can_be_deleted) {
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PAGE_MATRIX.listPageMatrix_pageMatrix_cannot_be_deleted), true);
        return;
      }
      let optionChosen = await this._miscService.confirmDialogBox("Delete", this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PAGE_MATRIX.listPageMatrix_pageMatrix_sure_to_delete));
      CustomLogger.logStringWithObject("Option Chosen:", optionChosen);
      if (optionChosen) {
        let result = this._dbService.deletePageMatrix(pageMatrix).toPromise();
        CustomLogger.logStringWithObject("deletePageMatrix:result:", result);
        CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.LIST_PAGE_MATRIX.listPageMatrix_pageMatrix_deleted_success), true);
        await this.init();
      }
    } catch (error) {
      CustomLogger.logError(error);
    }
  }

  filteredTableDataArr: any;
  search(term: string) {
    //   let fieldName = "permission_name";
    //   this.filteredTableDataArr = CustomMisc.searchDataArr(this.tableDataArr, term, fieldName);
  }

  displayAssociatedUsername(associated_user_id) {
    if (associated_user_id == CustomGlobalConstants.DEFAULT_SELECTED_STRING_VALUE)
      return "ROLE SPECIFIC";
    return associated_user_id;
  }

} 