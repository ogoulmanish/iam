import { Component, Injector, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMatrix } from 'src/app/modules/models/common/PageMatrix.model';
import { UserDetail } from 'src/app/modules/models/common/UserDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from '../../../shared/BaseComponent.component';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { SimplePermissions } from 'src/app/modules/models/sectors/SimplePermissions.model';
import { ChildPermissionComponent } from './childPermission.component';

@Component({
    selector: 'app-crudPageMatrix',
    templateUrl: './crudPageMatrix.component.html'
})
export class CrudPageMatrixComponent extends BaseComponent implements OnInit {
    // permissionEditUserProfile: SimplePermissions = new SimplePermissions();

    // exampleParent: SimplePermissions;

    field_crudPageMatrix_info = "Configure Page access to specfic user/role.";
    field_crudPageMatrix_listPredefinedConnector = "Current Connector";
    field_crudPageMatrix_page_matrix_name = "Name";
    field_crudPageMatrix_page_matrix_name_required = true;
    field_crudPageMatrix_page_matrix_name_error_message = "Error";
    field_crudPageMatrix_defaults = "Default Page Matrix Configuration";

    userOrRoleName = "";
    selected_role_id = "";
    flagShowPageMatrix = true;
    pageMatrix: PageMatrix;
    flagIsUserSpecific = true;
    usernameArr: string[];
    defaultPageMatrixArr = [];

    constructor(private _router: Router, private activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_PAGE_MATRIX, injector);
        this.pageMatrix = new PageMatrix();
        //new pagematrix is only for users and they can be deleted as well
        this.pageMatrix.can_be_deleted = true;
    }

    async init() {
        // this.usernameArr = [];
        // let result = await this._dbService.getUnassociatedUsernames().toPromise();
        // CustomLogger.logStringWithObject("getUnassociatedUsernames:result:", result);
        // let resultData = result["data"];
        // for (let k = 0; k < resultData.length; k++) {
        //     this.usernameArr.push(resultData[k].username);
        // }
        // this.pageMatrix.associated_user_id = this.usernameArr[0];

        //populate permissions
        CustomLogger.logStringWithObject("this.pageMatrix.permission_editUserProfile::", this.pageMatrix.permission_editUserProfile);
        this.defaultPageMatrixArr = CustomGlobalConstants.ARRAY_DEFAULT_PAGE_MATRIX_ID;


    }
    // permissionEditUserProfile = new SimplePermissions();
    async ngOnInit() {
        await this.populateFields();
        await this.init();
        this.activatedRoute.params.subscribe(
            async params => {
                CustomLogger.logStringWithObject("Params Values ::: ", params);
                let tmpCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
                if (tmpCrudType) {
                    this.isComponentNew = tmpCrudType == CustomGlobalConstants.CRUD_CREATE ? true : false;
                    if (!this.isComponentNew) {
                        let param_matrix_id = params["id"];
                        let result = await this._dbService.getSpecificPageMatrix(param_matrix_id).toPromise();
                        CustomLogger.logStringWithObject("getSpecificPageMatrix:result:", result);
                        this.pageMatrix = result["data"];
                        // this.permissionEditUserProfile = this.pageMatrix.permission_editUserProfile;

                        //check if it is user-specific ?
                        if (this.pageMatrix.associated_user_id == CustomGlobalConstants.DEFAULT_SELECTED_STRING_VALUE)
                            this.flagIsUserSpecific = false;
                    }
                } else {
                    this.flagIsUserSpecific = true;
                }
            }
        );


        //Delete this
        if (!this.pageMatrix.permission_editUserProfile || this.pageMatrix.permission_editUserProfile == null)
            this.pageMatrix.permission_editUserProfile = new SimplePermissions();
        if (!this.pageMatrix.permission_listUser || this.pageMatrix.permission_listUser == null)
            this.pageMatrix.permission_listUser = new SimplePermissions();

        // if (this.isComponentNew)
        //     this.defaultAdminPageMatrix();
    }

    async onSubmit() {
        // this.pageMatrix.permission_editUserProfile = this.permissionEditUserProfile;
        CustomLogger.logStringWithObject("PageMatrix to Save/Update:::", this.pageMatrix);
        try {
            let result = null;
            if (this.isComponentNew) {
                // //get the role details of the user
                // let userDetailResult = await this._dbService.getSpecificUserDetailFromUsername(this.pageMatrix.associated_user_id).toPromise();
                // let userDetail: UserDetail = userDetailResult["data"];
                // this.pageMatrix.role_id = userDetail.role_id;
                result = await this._dbService.addPageMatrix(this.pageMatrix).toPromise();
                // CustomMisc.showAlert("Page Matrix added Successfully");
                CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_PAGE_MATRIX.crudPageMatrix_add_success));
            } else {
                result = await this._dbService.updatePageMatrix(this.pageMatrix).toPromise();
                // CustomMisc.showAlert("Page Matrix updated Successfully");
                CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_PAGE_MATRIX.crudPageMatrix_update_success));
            }
            CustomLogger.logStringWithObject("onSubmit:result:", result);
        } catch (error) {
            CustomLogger.logStringWithObject("Error:", error);
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.CRUD_PAGE_MATRIX.crudPageMatrix_add_error));
        }

        this._router.navigate(["settings/listPageMatrix"]);
    }

    getAllSimplePermissions() {
        let simplePermission = new SimplePermissions();
        simplePermission.create = true;
        simplePermission.read = true;
        simplePermission.update = true;
        simplePermission.delete = true;
        return simplePermission;
    }

    getNoneSimplePermissions() {
        let simplePermission = new SimplePermissions();
        simplePermission.create = false;
        simplePermission.read = false;
        simplePermission.update = false;
        simplePermission.delete = false;
        return simplePermission;
    }

    selectedDefault = "";
    onChangeDefault() {
        if (this.selectedDefault == CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.EVERYTHING) {
            this.populateDefaultAdminPageMatrix();
        } else if (this.selectedDefault == CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.SPECIFIC_MANAGER) {
            this.populateDefaultSpecificManagerPageMatrix();
        } else if (this.selectedDefault == CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.STANDARD) {
            this.populateDefaultStandardPageMatrix();
        } else if (this.selectedDefault == CustomGlobalConstants.DEFAULT_PAGE_MATRIX_ID.HR) {
            this.populateDefaultHRSpecificPageMatrix();
        } else {
            this.populateNonePageMatrix();
        }
    }

    populateNonePageMatrix(){
        this.pageMatrix.show_field_menu_heading_navigation = false;
        this.pageMatrix.show_field_menu_link_listUser = false;
        this.pageMatrix.permission_listUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessEntity = false;
        this.pageMatrix.permission_listAccessEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listEntity = false;
        this.pageMatrix.permission_listEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSector = false;
        this.pageMatrix.permission_listSector = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSectorElements = false;
        this.pageMatrix.permission_listSectorElements = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRole = false;
        this.pageMatrix.permission_listRole = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPermission = false;
        this.pageMatrix.permission_listPermission = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRule = false;
        this.pageMatrix.permission_listRule = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPageMatrix = false;
        this.pageMatrix.permission_listPageMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listFormElement = false;
        this.pageMatrix.permission_listFormElement = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editFields = false;
        this.pageMatrix.permission_editFields = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_emailAlerts = false;
        this.pageMatrix.permission_emailAlerts = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editUserProfile = false;
        this.pageMatrix.permission_editUserProfile = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAlertNotification = false;
        this.pageMatrix.permission_listAlertNotification = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_defaultSetting = false;
        this.pageMatrix.permission_defaultSetting = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_default = false;
        this.pageMatrix.permission_report_default = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_entityEvents = false;
        this.pageMatrix.permission_report_entityEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_systemEvents = false;
        this.pageMatrix.permission_report_systemEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_existingUsers = false;
        this.pageMatrix.permission_report_existingUsers = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userLogging = false;
        this.pageMatrix.permission_report_userLogging = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userActivityReports = false;
        this.pageMatrix.permission_report_userActivityReports = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listManagerUser = false;
        this.pageMatrix.permission_listManagerUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listMakerCheckerRequest = false;
        this.pageMatrix.permission_listMakerCheckerRequest = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listApprovalMatrix = false;
        this.pageMatrix.permission_listApprovalMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPredefinedConnectors = false;
        this.pageMatrix.permission_listPredefinedConnectors = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessPolicy = false;
        this.pageMatrix.permission_listAccessPolicy = this.getAllSimplePermissions();

        //standard
        this.pageMatrix.show_field_standard_menu_heading_navigation = false;
        this.pageMatrix.show_field_standard_menu_link_showEntities = false;
        this.pageMatrix.permission_standard_showEntities = this.getNoneSimplePermissions();
        this.pageMatrix.show_field_standard_menu_link_editUserProfile = false;
        this.pageMatrix.permission_standard_editUserProfile = this.getNoneSimplePermissions();

        //hr
        this.pageMatrix.show_field_hr_menu_heading_navigation = false;
    }

    populateDefaultSpecificManagerPageMatrix() {
        this.pageMatrix.show_field_menu_heading_navigation = true;
        this.pageMatrix.show_field_menu_link_listUser = true;
        this.pageMatrix.permission_listUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessEntity = false;
        this.pageMatrix.permission_listAccessEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listEntity = true;
        this.pageMatrix.permission_listEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSector = true;
        this.pageMatrix.permission_listSector = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSectorElements = true;
        this.pageMatrix.permission_listSectorElements = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRole = true;
        this.pageMatrix.permission_listRole = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPermission = true;
        this.pageMatrix.permission_listPermission = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRule = true;
        this.pageMatrix.permission_listRule = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPageMatrix = false;
        this.pageMatrix.permission_listPageMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listFormElement = false;
        this.pageMatrix.permission_listFormElement = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editFields = false;
        this.pageMatrix.permission_editFields = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_emailAlerts = false;
        this.pageMatrix.permission_emailAlerts = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editUserProfile = true;
        this.pageMatrix.permission_editUserProfile = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAlertNotification = false;
        this.pageMatrix.permission_listAlertNotification = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_defaultSetting = false;
        this.pageMatrix.permission_defaultSetting = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_default = true;
        this.pageMatrix.permission_report_default = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_entityEvents = true;
        this.pageMatrix.permission_report_entityEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_systemEvents = true;
        this.pageMatrix.permission_report_systemEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_existingUsers = true;
        this.pageMatrix.permission_report_existingUsers = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userLogging = true;
        this.pageMatrix.permission_report_userLogging = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userActivityReports = true;
        this.pageMatrix.permission_report_userActivityReports = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listManagerUser = true;
        this.pageMatrix.permission_listManagerUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listMakerCheckerRequest = false;
        this.pageMatrix.permission_listMakerCheckerRequest = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listApprovalMatrix = false;
        this.pageMatrix.permission_listApprovalMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPredefinedConnectors = false;
        this.pageMatrix.permission_listPredefinedConnectors = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessPolicy = false;
        this.pageMatrix.permission_listAccessPolicy = this.getAllSimplePermissions();

        //standard
        this.pageMatrix.show_field_standard_menu_heading_navigation = false;
        this.pageMatrix.show_field_standard_menu_link_showEntities = false;
        this.pageMatrix.permission_standard_showEntities = this.getNoneSimplePermissions();
        this.pageMatrix.show_field_standard_menu_link_editUserProfile = false;
        this.pageMatrix.permission_standard_editUserProfile = this.getNoneSimplePermissions();

        //hr
        this.pageMatrix.show_field_hr_menu_heading_navigation = false;
    }
    populateDefaultStandardPageMatrix() {

        this.pageMatrix.show_field_menu_heading_navigation = false;
        this.pageMatrix.show_field_menu_link_listUser = false;
        this.pageMatrix.permission_listUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessEntity = false;
        this.pageMatrix.permission_listAccessEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listEntity = false;
        this.pageMatrix.permission_listEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSector = false;
        this.pageMatrix.permission_listSector = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSectorElements = false;
        this.pageMatrix.permission_listSectorElements = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRole = false;
        this.pageMatrix.permission_listRole = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPermission = false;
        this.pageMatrix.permission_listPermission = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRule = false;
        this.pageMatrix.permission_listRule = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPageMatrix = false;
        this.pageMatrix.permission_listPageMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listFormElement = false;
        this.pageMatrix.permission_listFormElement = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editFields = false;
        this.pageMatrix.permission_editFields = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_emailAlerts = false;
        this.pageMatrix.permission_emailAlerts = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editUserProfile = false;
        this.pageMatrix.permission_editUserProfile = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAlertNotification = false;
        this.pageMatrix.permission_listAlertNotification = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_defaultSetting = false;
        this.pageMatrix.permission_defaultSetting = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_default = false;
        this.pageMatrix.permission_report_default = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_entityEvents = false;
        this.pageMatrix.permission_report_entityEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_systemEvents = false;
        this.pageMatrix.permission_report_systemEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_existingUsers = false;
        this.pageMatrix.permission_report_existingUsers = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userLogging = false;
        this.pageMatrix.permission_report_userLogging = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userActivityReports = false;
        this.pageMatrix.permission_report_userActivityReports = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listManagerUser = false;
        this.pageMatrix.permission_listManagerUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listMakerCheckerRequest = false;
        this.pageMatrix.permission_listMakerCheckerRequest = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listApprovalMatrix = false;
        this.pageMatrix.permission_listApprovalMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPredefinedConnectors = false;
        this.pageMatrix.permission_listPredefinedConnectors = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessPolicy = false;
        this.pageMatrix.permission_listAccessPolicy = this.getAllSimplePermissions();

        //standard
        this.pageMatrix.show_field_standard_menu_heading_navigation = true;
        this.pageMatrix.show_field_standard_menu_link_showEntities = true;
        this.pageMatrix.permission_standard_showEntities = this.getNoneSimplePermissions();
        this.pageMatrix.show_field_standard_menu_link_editUserProfile = true;
        this.pageMatrix.permission_standard_editUserProfile = this.getNoneSimplePermissions();

        //hr
        this.pageMatrix.show_field_hr_menu_heading_navigation = false;
    }
    populateDefaultHRSpecificPageMatrix() {

        this.pageMatrix.show_field_menu_heading_navigation = false;
        this.pageMatrix.show_field_menu_link_listUser = true;
        this.pageMatrix.permission_listUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessEntity = false;
        this.pageMatrix.permission_listAccessEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listEntity = false;
        this.pageMatrix.permission_listEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSector = false;
        this.pageMatrix.permission_listSector = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSectorElements = false;
        this.pageMatrix.permission_listSectorElements = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRole = false;
        this.pageMatrix.permission_listRole = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPermission = false;
        this.pageMatrix.permission_listPermission = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRule = false;
        this.pageMatrix.permission_listRule = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPageMatrix = false;
        this.pageMatrix.permission_listPageMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listFormElement = false;
        this.pageMatrix.permission_listFormElement = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editFields = false;
        this.pageMatrix.permission_editFields = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_emailAlerts = false;
        this.pageMatrix.permission_emailAlerts = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editUserProfile = false;
        this.pageMatrix.permission_editUserProfile = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAlertNotification = false;
        this.pageMatrix.permission_listAlertNotification = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_defaultSetting = false;
        this.pageMatrix.permission_defaultSetting = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_default = false;
        this.pageMatrix.permission_report_default = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_entityEvents = false;
        this.pageMatrix.permission_report_entityEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_systemEvents = false;
        this.pageMatrix.permission_report_systemEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_existingUsers = false;
        this.pageMatrix.permission_report_existingUsers = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userLogging = false;
        this.pageMatrix.permission_report_userLogging = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userActivityReports = false;
        this.pageMatrix.permission_report_userActivityReports = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listManagerUser = false;
        this.pageMatrix.permission_listManagerUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listMakerCheckerRequest = false;
        this.pageMatrix.permission_listMakerCheckerRequest = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listApprovalMatrix = false;
        this.pageMatrix.permission_listApprovalMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPredefinedConnectors = false;
        this.pageMatrix.permission_listPredefinedConnectors = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessPolicy = false;
        this.pageMatrix.permission_listAccessPolicy = this.getAllSimplePermissions();

        //standard
        this.pageMatrix.show_field_standard_menu_heading_navigation = false;
        this.pageMatrix.show_field_standard_menu_link_showEntities = false;
        this.pageMatrix.permission_standard_showEntities = this.getNoneSimplePermissions();
        this.pageMatrix.show_field_standard_menu_link_editUserProfile = false;
        this.pageMatrix.permission_standard_editUserProfile = this.getNoneSimplePermissions();

        //hr
        this.pageMatrix.show_field_hr_menu_heading_navigation = true;
    }

    populateDefaultAdminPageMatrix() {
        this.pageMatrix.show_field_menu_heading_navigation = true;
        this.pageMatrix.show_field_menu_link_listUser = true;
        this.pageMatrix.permission_listUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessEntity = true;
        this.pageMatrix.permission_listAccessEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listEntity = true;
        this.pageMatrix.permission_listEntity = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSector = true;
        this.pageMatrix.permission_listSector = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listSectorElements = true;
        this.pageMatrix.permission_listSectorElements = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRole = true;
        this.pageMatrix.permission_listRole = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPermission = true;
        this.pageMatrix.permission_listPermission = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listRule = true;
        this.pageMatrix.permission_listRule = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPageMatrix = true;
        this.pageMatrix.permission_listPageMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listFormElement = true;
        this.pageMatrix.permission_listFormElement = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editFields = true;
        this.pageMatrix.permission_editFields = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_emailAlerts = true;
        this.pageMatrix.permission_emailAlerts = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_editUserProfile = true;
        this.pageMatrix.permission_editUserProfile = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAlertNotification = true;
        this.pageMatrix.permission_listAlertNotification = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_defaultSetting = true;
        this.pageMatrix.permission_defaultSetting = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_default = true;
        this.pageMatrix.permission_report_default = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_entityEvents = true;
        this.pageMatrix.permission_report_entityEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_systemEvents = true;
        this.pageMatrix.permission_report_systemEvents = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_existingUsers = true;
        this.pageMatrix.permission_report_existingUsers = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userLogging = true;
        this.pageMatrix.permission_report_userLogging = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_report_userActivityReports = true;
        this.pageMatrix.permission_report_userActivityReports = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listManagerUser = true;
        this.pageMatrix.permission_listManagerUser = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listMakerCheckerRequest = true;
        this.pageMatrix.permission_listMakerCheckerRequest = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listApprovalMatrix = true;
        this.pageMatrix.permission_listApprovalMatrix = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listPredefinedConnectors = true;
        this.pageMatrix.permission_listPredefinedConnectors = this.getAllSimplePermissions();
        this.pageMatrix.show_field_menu_link_listAccessPolicy = true;
        this.pageMatrix.permission_listAccessPolicy = this.getAllSimplePermissions();

        //standard
        this.pageMatrix.show_field_standard_menu_heading_navigation = false;
        this.pageMatrix.show_field_standard_menu_link_showEntities = false;
        this.pageMatrix.permission_standard_showEntities = this.getNoneSimplePermissions();
        this.pageMatrix.show_field_standard_menu_link_editUserProfile = false;
        this.pageMatrix.permission_standard_editUserProfile = this.getNoneSimplePermissions();

        //hr
        this.pageMatrix.show_field_hr_menu_heading_navigation = false;
    }

    permissionStandardEditUserProfile($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_standard_editUserProfile = $event;
    }
    permissionEditUserProfile($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_editUserProfile = $event;
    }
    permissionListUser($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listUser = $event;
    }
    permissionListApprovalMatrix($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listApprovalMatrix = $event;
    }
    permissionListMakerCheckerRequest($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listMakerCheckerRequest = $event;
    }
    permissionListManagerUser($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listManagerUser = $event;
    }
    permissionEditFields($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_editFields = $event;
    }
    permissionDefaultSetting($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_defaultSetting = $event;
    }
    permissionReport_default($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_default = $event;
    }
    permissionReport_entityEvents($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_entityEvents = $event;
    }
    permissionReport_systemEvents($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_systemEvents = $event;
    }
    permissionReport_existingUsers($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_existingUsers = $event;
    }
    permissionReport_userLogging($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_userLogging = $event;
    }
    permissionReport_userActivityReports($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_report_userActivityReports = $event;
    }
    permissionListPageMatrix($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listPageMatrix = $event;
    }
    permissionListPredefinedConnectors($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listPredefinedConnectors = $event;
    }
    permissionEmailAlerts($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_emailAlerts = $event;
    }
    permissionListAlertNotification($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listAlertNotification = $event;
    }
    permissionListEntity($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listEntity = $event;
    }
    permissionListSector($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listSector = $event;
    }
    permissionStandard_showEntities($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_standard_showEntities = $event;
    }
    permissionListSectorElements($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listSectorElements = $event;
    }
    permissionListRole($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listRole = $event;
    }
    permissionListAccessPolicy($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listAccessPolicy = $event;
    }
    permissionListRule($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listRule = $event;
    }
    permissionListPermission($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listPermission = $event;
    }
    permissionListAccessEntity($event) {
        CustomLogger.logStringWithObject("$event::", $event);
        this.pageMatrix.permission_listAccessEntity = $event;
    }

}