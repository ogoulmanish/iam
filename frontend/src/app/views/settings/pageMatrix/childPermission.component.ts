import { Component, Input, Output, EventEmitter } from "@angular/core";
import { SimplePermissions } from 'src/app/modules/models/sectors/SimplePermissions.model';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';


@Component({
    selector: 'app-childPermission',
    templateUrl: './childPermission.component.html'
})
export class ChildPermissionComponent {
    // simplePermission: SimplePermissions;
    @Input() permissionFromParent: SimplePermissions;
    @Output() permissionFromChild = new EventEmitter<SimplePermissions>();

    // simplPermission: SimplePermissions;
    constructor() {
        // this.simplPermission = this.childExample;
        // this.childExample = new SimplePermissions();
        CustomLogger.logStringWithObject("child:permissionFromParent:", this.permissionFromParent);
    }

    childOutputMethod() {
        CustomLogger.logString("SENDING............")
        this.permissionFromChild.emit(this.permissionFromParent);
    }

    onSelectAll() {
        this.permissionFromParent.create = true;
        this.permissionFromParent.read = true;
        this.permissionFromParent.update = true;
        this.permissionFromParent.delete = true;
    }
}