import { Component, Injector, OnInit } from '@angular/core';
import { FieldDetail } from 'src/app/modules/models/common/FieldDetail.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';

@Component({
    selector: 'app-editFields',
    templateUrl: './editFields.component.html'
})
export class EditFieldsComponent extends BaseComponent implements OnInit {
    field_editFields_info = "All the names of the labels can be dynamically configured. Also, the fields can be made mandatory/optional and also configure the Error Messages that should be displayed during field validation";
    componentNameArr: string[] = [];
    currentFieldDetailArr: FieldDetail[] = [];
    currentFieldDetail: FieldDetail;
    componentName: string;
    // currentFieldValue: string;

    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.EDIT_FIELDS, injector);
        this.currentFieldDetail = new FieldDetail();
    }

    async ngOnInit() {
        await this.populateFields();
        this.componentNameArr = CustomGlobalConstants.ALL.ARRAY_COMPONENT_NAMES;
        this.componentName = this.componentNameArr[0];
        this.fillFieldDetailsForComponent(this.componentName);
    }

    async fillFieldDetailsForComponent(component_name) {
        let result = await this._dbService.getFieldDetailsForComponent(component_name).toPromise();
        CustomLogger.logStringWithObject("getFieldDetailsForComponent:result::", result);
        this.currentFieldDetailArr = result["data"];
        //initialize current field detail
        if (this.currentFieldDetailArr != null && this.currentFieldDetailArr.length > 0)
            this.currentFieldDetail = this.currentFieldDetailArr[0];
        else
            this.currentFieldDetail = new FieldDetail();
    }

    onSelectComponentChange() {
        CustomLogger.logStringWithObject("Will fetch fields for component:", this.componentName);
        this.fillFieldDetailsForComponent(this.componentName);
    }

    onSelectFieldChange() {
        CustomLogger.logStringWithObject("currentFieldDetail::", this.currentFieldDetail);
        // this.currentFieldDetail= fieldDetail;
    }

    async onClickUpdate() {
        try {
            CustomLogger.logStringWithObject("this.currentFieldDetail::", this.currentFieldDetail);
            let result = await this._dbService.updateFieldDetail(this.currentFieldDetail).toPromise();
            CustomLogger.logStringWithObject("updateFieldDetail:result:", result);
            // CustomMisc.showAlert("Successfully updated the field");
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EDIT_FIELDS.editFields_update_success));
        } catch (error) {
            CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.EDIT_FIELDS.editFields_update_error), true);
        }

    }
}