import { Component, Injector, OnInit } from '@angular/core';
import { DefaultSetting } from 'src/app/modules/models/common/DefaultSetting.model';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { BaseComponent } from '../../../shared/BaseComponent.component';

@Component({
    selector: 'app-listDefaultSetting',
    templateUrl: './listDefaultSetting.component.html'
})
export class ListDefaultSettingComponent extends BaseComponent implements OnInit {
    field_listDefaultSetting_info = "The default settigs like session timeout, account expiry etc.";
    field_listDefaultSetting_session_timeout_info = "Configure after how many seconds will the session be timed out.";
    field_listDefaultSetting_account_expiry_info = "Configure after how many days, since last login, should the user be marked Locked.";
    field_listDefaultSetting_email_setting_info = "The settings of email credentials for sending out emails.";
    field_listDefaultSetting_heading_configure_default_setting = "Configure Default Setting";
    field_listDefaultSetting_heading_session_timeout = "Session Timeout";
    field_listDefaultSetting_session_timeout = "Session Timeout (in seconds)";

    field_listDefaultSetting_heading_account_expiry = "Account Expiry";
    field_listDefaultSetting_account_expiry_days = "Expiry Days";

    field_listDefaultSetting_heading_email_setting = "Email Settings";
    field_listDefaultSetting_email_service = "Service Name";
    field_listDefaultSetting_email_default_from_name = "Default from name";
    field_listDefaultSetting_email_address = "Email Address";
    field_listDefaultSetting_email_username = "Email Username";
    field_listDefaultSetting_email_password = "Email Password";
    field_listDefaultSetting_button_update = "Update";
    field_listDefaultSetting_button_reset = "Reset";
    field_listDefaultSetting_button_cancel = "Cancel";

    defaultSetting: DefaultSetting;

    constructor(injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.ALERT_NOTIFICATION, injector);
        this.defaultSetting = new DefaultSetting();
    }

    async ngOnInit() {
        await this.populateFields();
        let result = await this._dbService.getDefaultSetting().toPromise();
        CustomLogger.logStringWithObject("getDefaultSetting:result:", result);
        this.defaultSetting = result["data"];
    }

    async onClickUpdate() {
        CustomLogger.logStringWithObject("Default Serttings Component:", this.defaultSetting);
        try {
            let result = await this._dbService.updateDefaultSetting(this.defaultSetting).toPromise();
            CustomLogger.logStringWithObject("updateDefaultSetting:result:", result);
            this._globalService.setDefaultSetting(this.defaultSetting);
            CustomMisc.showAlert("Updated Successfully");
        } catch (error) {
            CustomLogger.logStringWithObject("updateDefaultSetting:error:", error);
            CustomMisc.showAlert("Error in update");
        }
    }

    onClickReset() {

    }


} 