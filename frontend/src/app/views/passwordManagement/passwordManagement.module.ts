import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTableModule } from 'angular-6-datatable';
import { PasswordManagementRoutingModule } from './passwordManagement.routing';
import { SchedulePasswordsComponent } from './schedulePasswords/schedulePasswords.component';
import { ShowPasswordsComponent } from './showPasswords/showPasswords.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    PasswordManagementRoutingModule,
    FormsModule,
    NgxDatatableModule,
    DataTableModule
  ],
  declarations: [
    ShowPasswordsComponent,
    SchedulePasswordsComponent
  ]
})
export class PasswordManagementModule { }
