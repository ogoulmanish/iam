import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordService } from 'src/app/modules/services/passwordService.service';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { PasswordScheduler } from 'src/app/modules/models/passwordModule/PasswordScheduler.model';


@Component({
  selector: 'app-schedulePasswords',
  templateUrl: './schedulePasswords.component.html'
})
export class SchedulePasswordsComponent implements OnInit {
  passwordScheduler: PasswordScheduler;
  constructor(private router: Router, private passwordService: PasswordService, private activatedRoute: ActivatedRoute) {
  }

  async ngOnInit() {
    this.passwordScheduler = new PasswordScheduler();
    let result = await this.passwordService.getCurrentPasswordScheduler().toPromise();
    CustomLogger.logStringWithObject("result:::::", result);
    this.passwordScheduler = result["data"];
  }

  async onClickUpdate() {
    CustomLogger.logStringWithObject("New passwrod scheduler will be updated...", this.passwordScheduler);
    try {
      let result = await this.passwordService.updatePasswordScheduler(this.passwordScheduler).toPromise();
      CustomLogger.logStringWithObject("result got from db:", result);
      alert("Password Scheduler updated successfully...");
    } catch (err) {
      CustomLogger.logStringWithObject("error from db:", err.error.message);
      alert("Error:::" + err.error.message);
    }
    this.router.navigate(["devicesView"]);
  }
}
