import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowPasswordsComponent } from './showPasswords/showPasswords.component';
import { SchedulePasswordsComponent } from './schedulePasswords/schedulePasswords.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Password Management',
      status: false
    },
    children: [
      {
        path: 'showPasswords',
        component: ShowPasswordsComponent
      }, 
      {
        path: 'schedulePasswords',
        component: SchedulePasswordsComponent
      }, 
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordManagementRoutingModule { }
