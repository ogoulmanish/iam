import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordService } from 'src/app/modules/services/passwordService.service';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';


@Component({
  selector: 'app-showPasswords',
  templateUrl: './showPasswords.component.html',
  styleUrls: ['./showPasswords.component.css']
})
export class ShowPasswordsComponent implements OnInit {

  tableDataArr: any;
  constructor(private router: Router, private passwordService: PasswordService, private activatedRoute: ActivatedRoute) {
  }

  async ngOnInit() {
    let result = await this.passwordService.getAllPasswordInfo().toPromise();
    CustomLogger.logStringWithObject("result:::::", result);
    this.tableDataArr = result["data"];
  }

}
