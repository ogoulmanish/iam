import { Component, Injector, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';

@Component({
    selector: 'app-listConnector',
    templateUrl: './listConnector.component.html'
})
export class ListConnectorComponent extends BaseComponent implements OnInit {
    field_listConnector_heading_Connector_management = "List of all Current Connectors.";
    field_listConnector_button_add_Connector = "Add Connector";
    field_listConnector_heading_Connector_details = "Current Connector Details";
    field_listConnectorComponent_info = "List of all the connectors that are currently configured inside the system.";
    field_listConnector_connector_id = "Connector ID";
    field_listConnector_connector_name = "Connector Name";
    field_listConnector_details = "Details";
    field_listConnector_button_edit = "Edit";
    field_listConnector_button_delete = "Delete";

    tableDataArr = [];
    constructor(private _router: Router, private _activatedRoute: ActivatedRoute, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.LIST_CONNECTOR, injector);
    }

    async ngOnInit() {
        await this.populateFields();
        await this.init();
    }

    async init() {
        try {
            // let result = await this._dbService.getAllDBConnectors().toPromise();
            // CustomLogger.logStringWithObject("result:", result);
            // this.tableDataArr = result["data"];
            this.tableDataArr = CustomGlobalConstants.ARRAY_ACTUAL_CONNECTORS;
        } catch (error) {
            CustomLogger.logError(error);
        }
    }

    onClickAdd(){
        this._router.navigate(['crudConnector'], { relativeTo: this._activatedRoute.parent, skipLocationChange: true });
    }
}