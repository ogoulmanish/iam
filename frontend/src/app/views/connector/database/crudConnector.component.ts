import { HttpClient } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
import { BaseComponent } from 'src/app/shared/BaseComponent.component';
import { DBConnectorModel } from 'src/app/modules/models/connectors/database/DBConnectorModel.model';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { CustomMisc } from 'src/app/modules/utils/CustomMisc';
import { ConnectorService } from 'src/app/modules/services/connectorService.service';


@Component({
    selector: 'app-crudConnector',
    templateUrl: './crudConnector.component.html'
})
export class CrudConnectorComponent extends BaseComponent implements OnInit {
    field_crudConnector_info = "Database connector details that can be configured in order to link to the Entity.";
    field_Connector_connector_name = "Connector Name";
    field_Connector_connector_type = "Connector Type";
    field_Connector_hostname = "Hostname";
    field_Connector_username = "Username";
    field_Connector_password = "Password";
    field_Connector_port = "Port";
    field_button_save = "Save";
    field_button_update = "Update";
    field_button_reset = "Reset";
    field_button_cancel = "Cancel";

    PICKLIST_DB_CONNECTOR_TYPES: any;
    current_DBconnector: DBConnectorModel;

    constructor(private _connectorService:ConnectorService, injector: Injector) {
        super(CustomGlobalConstants.COMPONENT_NAME.CRUD_CONNECTOR, injector);
        this.current_DBconnector = new DBConnectorModel();
    }

    async ngOnInit() {
        await this.populateFields();
        // this.PICKLIST_DB_CONNECTOR_TYPES = CustomGlobalConstants.ARRAY_CONNECTOR_CATEGORIES[0].DATABASE_CONNECTOR;
        CustomLogger.logStringWithObject("PICKLIST_DB_CONNECTOR_TYPES:::", this.PICKLIST_DB_CONNECTOR_TYPES);
    }

    async onSubmit() {
        CustomLogger.logStringWithObject("Will save dbconnector:", this.current_DBconnector);
        try {
            let result = null;
            // if (this.current_DBconnector.connector_type == CustomGlobalConstants.CONNECTOR_TYPES.DATABASE_CONNECTOR.MONGODB.id) {
            //     result = await this._connectorService.addMongoDBConnector(this.current_DBconnector).toPromise();
            //     CustomLogger.logStringWithObject("addDBConnector:result:", result);
            // } else {
            //     CustomMisc.showAlert("Other Connectors configuration are still underway.", true);
            // }


        } catch (error) {
            CustomLogger.logError(error);
        }
    }
    
    onCancel() {
        // this._miscService.goToURL(CustomGlobalConstants.COMPONENT_NAME.LIST_DBCONNECTOR, this.activatedRoute);
    }
}