import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListConnectorComponent } from './database/listConnector.component';
import { CrudConnectorComponent } from './database/crudConnector.component';


const routes: Routes = [
    {
        path: '',
        data: {
            breadcrumb: 'Connectors',
            status: false
        },
        children: [
            {
                path: 'listConnector',
                component: ListConnectorComponent
            },
            {
                path: 'crudConnector',
                component: CrudConnectorComponent
            },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConnectorRoutingModule { }
