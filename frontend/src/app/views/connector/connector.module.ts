import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular-6-datatable';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConnectorRoutingModule } from './connector.routing';
import { ListConnectorComponent } from './database/listConnector.component';
import { CrudConnectorComponent } from './database/crudConnector.component';

@NgModule({
    imports: [
        SharedModule,
        ConnectorRoutingModule,
        DataTableModule
    ],
    declarations: [
        ListConnectorComponent,
        CrudConnectorComponent
    ]
})
export class ConnectorModule { }