import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationslistsComponent } from './applicationslists.component';

describe('ApplicationslistsComponent', () => {
  let component: ApplicationslistsComponent;
  let fixture: ComponentFixture<ApplicationslistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationslistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationslistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
