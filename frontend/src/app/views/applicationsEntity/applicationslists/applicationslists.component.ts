import { Component, OnInit } from '@angular/core';
import { EntityDetail } from 'src/app/modules/models/sectors/EntityDetail.model';

@Component({
  selector: 'app-applicationslists',
  templateUrl: './applicationslists.component.html',
  styleUrls: ['./applicationslists.component.css']
})
export class ApplicationslistsComponent implements OnInit {


  entitydetailsArr = [];
  entityDetail = new EntityDetail();
  entityDetail2 = new EntityDetail();
  entityDetail3 = new EntityDetail();
  entityDetail4 = new EntityDetail();
  entityDetail5 = new EntityDetail();
  entityDetail6 = new EntityDetail();
  
  constructor() { }
  ngOnInit() {
    this.entityDetail.entity_id = '1';
    this.entityDetail.entity_name ='Office 365';
    this.entityDetail.access_location = '';
    this.entityDetail.icon_location = "assets/images/sectors/applications/office-365.png";

    
    this.entityDetail2.entity_id = '10';
    this.entityDetail2.entity_name ='Skype';
    this.entityDetail2.access_location = '';
    this.entityDetail2.icon_location = "assets/images/sectors/applications/skype.png";
 

    this.entityDetail3.entity_id = '10';
    this.entityDetail3.entity_name ='Salesforce';
    this.entityDetail3.access_location = '';
    this.entityDetail3.icon_location = "assets/images/sectors/applications/salesforce.png";
   
    

    this.entityDetail4.entity_id = '4';
    this.entityDetail4.entity_name ='Google Suite';
    this.entityDetail4.access_location = '';
    this.entityDetail4.icon_location = "assets/images/sectors/applications/g-suite.png";
   

    this.entityDetail5.entity_id = '5';
    this.entityDetail5.entity_name ='Facebook';
    this.entityDetail5.access_location = '';
    this.entityDetail5.icon_location = "assets/images/sectors/applications/fb.png";
    

    this.entityDetail6.entity_id = '6';
    this.entityDetail6.entity_name ='Active Directory';
    this.entityDetail6.access_location = '';
    this.entityDetail6.icon_location = "assets/images/sectors/applications/AD.png";
   

    this.entitydetailsArr.push(this.entityDetail)
    this.entitydetailsArr.push(this.entityDetail2)
    this.entitydetailsArr.push(this.entityDetail3)
    this.entitydetailsArr.push(this.entityDetail4)
    this.entitydetailsArr.push(this.entityDetail5)
    this.entitydetailsArr.push(this.entityDetail6)
  }

}