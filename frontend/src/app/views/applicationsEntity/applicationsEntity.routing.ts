import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ApplicationslistsComponent} from './applicationslists/applicationslists.component'
import { AddapplicationsComponent} from './addapplications/addapplications.component';





const routes: Routes = [
    {
      path: '',
      data: {
        breadcrumb: 'Applications',
        status: false
      },
      children: [
        // {
        //   path: 'listDevices',
        //   component: ListDevicesComponent
        // },
        {
          path: 'applicationslists',
          component: ApplicationslistsComponent
        },  
        {
          path: 'addapplications',
          component: AddapplicationsComponent
        }, 
       
      ]
    }
  ];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ApplicationsEntityRoutingModule { }
  