import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {ApplicationsEntityRoutingModule} from './applicationsEntity.routing'
import{ApplicationslistsComponent} from './applicationslists/applicationslists.component'
import { AddapplicationsComponent} from './addapplications/addapplications.component';

@NgModule({
    imports: [
      CommonModule,

      ApplicationsEntityRoutingModule
    ],
    declarations: [
       ApplicationslistsComponent,
       AddapplicationsComponent,
      ],
    // entryComponents: [ButtonRenderComponent],
  })
  export class ApplicationsEntityModule { }