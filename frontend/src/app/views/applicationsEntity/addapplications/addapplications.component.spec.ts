import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddapplicationsComponent } from './addapplications.component';

describe('AddapplicationsComponent', () => {
  let component: AddapplicationsComponent;
  let fixture: ComponentFixture<AddapplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddapplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddapplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
