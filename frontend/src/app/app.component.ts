import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { DBService } from './modules/services/dbService.service';
import { GlobalService } from './modules/services/global.service';
import { CustomGlobalConstants } from './modules/utils/CustomGlobalConstants';
import { CustomLogger } from './modules/utils/CustomLogger';
import { LoginService } from './modules/services/loginService.service';
import { CustomMisc } from './modules/utils/CustomMisc';
import { UserIdleService } from 'angular-user-idle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private router: Router, private _globalService: GlobalService,
    private _bnIdle: BnNgIdleService, private _loginService: LoginService,
    private _dbService: DBService, private userIdle: UserIdleService) { }

  async ngOnInit() {
    CustomLogger.logString("kkkkkkkkkkkkkkkk");
    //load default setting
    try {

      let result = await this._dbService.getDefaultSetting().toPromise();
      this._globalService.setDefaultSetting(result["data"]);
      CustomLogger.logStringWithObject("this._globalService.getDefaultSetting():", (this._globalService.getDefaultSetting()));

      this.userIdle.setConfigValues({
        idle: this._globalService.getDefaultSetting().session_timeout,
        timeout: this._globalService.getDefaultSetting().session_timeout,
        ping: this._globalService.getDefaultSetting().session_timeout
      });

      //Start watching for user inactivity.
      this.userIdle.startWatching();
      // Start watching when user idle is starting and reset if user action is there.
      this.userIdle.onTimerStart().subscribe(count => {
        var eventList = ["click", "mouseover", "keydown", "DOMMouseScroll", "mousewheel",
          "mousedown", "touchstart", "touchmove", "scroll", "keyup"];
        for (let event of eventList) {
          document.body.addEventListener(event, () => this.userIdle.resetTimer());
        }
      });
      // Start watch when time is up.
      this.userIdle.onTimeout().subscribe(() => {
        CustomLogger.logString("Session Expired. Logging out");
        let alertText = this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.COMMON.session_logout);
        if (alertText != undefined)
          CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.COMMON.session_logout));

        this._loginService.logoutUser();
        // this.userIdle.stopWatching();
        this.userIdle.resetTimer();
        this.router.navigate(["/login"]);
      });


      // let result = await this._dbService.getDefaultSetting().toPromise();
      // this._globalService.setDefaultSetting(result["data"]);
      // CustomLogger.logStringWithObject("this._globalService.getDefaultSetting():", (this._globalService.getDefaultSetting()));

      // // this._bnIdle.startWatching(this._globalService.getDefaultSetting().session_timeout);


      // this._bnIdle.startWatching(this._globalService.getDefaultSetting().session_timeout).subscribe((isTimedOut: boolean) => {
      //   if (isTimedOut) {
      //     CustomLogger.logString("Session Expired. Logging out");
      //     try {
      //       // this._bnIdle.stopTimer();
      //       this._loginService.logoutUser();
      //     } catch (error) {
      //       CustomLogger.logStringWithObject("Error:", error);
      //     }
      //     CustomMisc.showAlert(this._dbService.getAlertValueFromAlertName(CustomGlobalConstants.ALERT_NOFICATION_CONSTANTS.COMMON.session_logout));
      //     this.router.navigate(["/login"]);
      //   }
      // });

    } catch (error) {
      CustomLogger.logStringWithObject("setDefaultSetting:Error:", error);
    }

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  // stop() {
  //   CustomLogger.logString("STOP 000000000000000");
  //   this.userIdle.stopTimer();
  // }

  // stopWatching() {
  //   CustomLogger.logString("STOP WATCHING 1111111111");
  //   this.userIdle.stopWatching();
  // }

  // startWatching() {
  //   CustomLogger.logString("START WATCHING 22222222222");
  //   this.userIdle.startWatching();
  // }

  // restart() {
  //   CustomLogger.logString("RESTART 33333333333333");
  //   this.userIdle.resetTimer();
  // }
}
