import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { DataTableModule } from 'angular-6-datatable';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { AnimatorModule } from 'css-animator';
import { ClickOutsideModule } from 'ng-click-outside';
import { ToastyModule } from 'ng2-toasty';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { BackButtonDirective } from '../views/back-button.directive';
import { AccordionAnchorDirective, AccordionDirective, AccordionLinkDirective } from './accordion';
import { CardRefreshDirective } from './card/card-refresh.directive';
import { CardToggleDirective } from './card/card-toggle.directive';
import { CardComponent } from './card/card.component';
import { DataFilterPipe } from './elements/data-filter.pipe';
import { ParentRemoveDirective } from './elements/parent-remove.directive';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import { MenuItems } from './menu-items/menu-items';
import { ModalAnimationComponent } from './modal-animation/modal-animation.component';
import { ModalBasicComponent } from './modal-basic/modal-basic.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TodoService } from './todo/todo.service';
import { ConfirmationDialogComponent } from '../views/misc/confirmDialog.component';
import { SelectUsersModalComponent } from '../views/configuration/roles/selectUsersModal.component';
import { SelectAccessPolicyComponent } from '../views/configuration/roles/selectAccessPolicyModal.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ToastyModule.forRoot(),
    SimpleNotificationsModule.forRoot(),
    AnimatorModule,
    ScrollToModule.forRoot(),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk' }),
    ClickOutsideModule,
    PerfectScrollbarModule,
    DataTableModule,
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullscreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    ParentRemoveDirective,
    CardComponent,
    SpinnerComponent,
    ModalAnimationComponent,
    ModalBasicComponent,
    DataFilterPipe,
    BackButtonDirective,
    ConfirmationDialogComponent,
    SelectUsersModalComponent,
    SelectAccessPolicyComponent
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullscreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    ParentRemoveDirective,
    CardComponent,
    SpinnerComponent,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    ModalBasicComponent,
    ModalAnimationComponent,
    ToastyModule,
    SimpleNotificationsModule,
    AnimatorModule,
    DataFilterPipe,
    ScrollToModule,
    AgmCoreModule,
    ClickOutsideModule,
    PerfectScrollbarModule,
    BackButtonDirective
  ],
  providers: [
    MenuItems,
    TodoService,
    NotificationsService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    SelectUsersModalComponent,
    SelectAccessPolicyComponent
  ]
})
export class SharedModule {

}
