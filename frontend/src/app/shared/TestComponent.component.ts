import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { DBService } from '../modules/services/dbService.service';
import { Injector, OnInit, Component } from '@angular/core';
import { FieldDetail } from '../modules/models/common/FieldDetail.model';

export class TestComponent {
    isComponentNew: boolean = true;


    component_name: string = "";
    //get field names from database
    fieldDetailMap = new Map<string, string>();

    constructor(component_name, public _dbService:DBService) {
        this.component_name = component_name;
        // this.populateFields();
    }

    async populateFields() {
        CustomLogger.logStringWithObject("Will load map for component:::", this.component_name);
        //populate fields
        CustomLogger.logStringWithObject("_dbService:::", this._dbService);
        let result = await this._dbService.getFieldDetailsForComponent(this.component_name).toPromise();
        CustomLogger.logStringWithObject("RESULT:::", result);
        let fieldDetailArr: FieldDetail[] = result["data"];
        fieldDetailArr.forEach(fieldDetail => {
            this.fieldDetailMap.set(fieldDetail.field_name, fieldDetail.field_value);
        });
        CustomLogger.logStringWithObject("Filled Field Map:", this.fieldDetailMap);
    }

    getFieldValue(field_name) {
        return this.fieldDetailMap.get(field_name);
    }

}