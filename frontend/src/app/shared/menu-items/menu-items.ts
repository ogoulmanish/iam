import { Injectable, Injector, OnInit } from '@angular/core';
import { CustomGlobalConstants } from 'src/app/modules/utils/CustomGlobalConstants';
// import { MenuLabels } from './menuLabels';
import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { BaseComponent } from '../BaseComponent.component';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

@Injectable()
export class MenuItems extends BaseComponent implements OnInit {
  constructor(injector: Injector) {
    super(CustomGlobalConstants.COMPONENT_NAME.DASHBOARD, injector);
    this.init();
  }

  ADMIN_SPECIFIC_MENUITEMS = [];

  async init() {
    await this.populateFields();
    let currentPageMatrix = this._globalService.getPageMatrix();
    this.ADMIN_SPECIFIC_MENUITEMS = [];

    //hr - dashboard
    if (currentPageMatrix.show_field_hr_menu_heading_navigation) {
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_navigation,
        main: [
          {
            state: 'hrDashboard',
            name: this.field_menu_hrDashboard,
            type: 'link',
            icon: 'ti-dashboard'
          }
        ],
      });
    }


    //standard user - dashboard
    if (currentPageMatrix.show_field_standard_menu_heading_navigation) {
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_navigation,
        main: [
          {
            state: 'standardDashboard',
            name: this.field_menu_standardDashboard,
            type: 'link',
            icon: 'ti-dashboard'
          }
        ],
      });
    }

    //standard user - accessEntity
    if (currentPageMatrix.show_field_standard_menu_link_showEntities) {
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_link_accessEntity,
        main: [
          {
            state: 'accessEntity',
            name: this.field_menu_link_accessEntity,
            type: 'sub',
            icon: 'ti-desktop',
            children: [
              {
                state: 'showEntities',
                name: this.field_menu_link_showEntities
              },

            ]
          }
        ]
      });
    }


    //standard user - settings
    if (currentPageMatrix.show_field_standard_menu_link_editUserProfile) {
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_settings,
        main: [{
          state: 'settings',
          name: this.field_menu_heading_settings,
          type: 'sub',
          icon: 'ti-settings',
          children: [
            {
              state: 'editUserProfile',
              name: this.field_menu_link_editUserProfile
            }
          ]
        },
        ]
      });
    }

    ////////////////////
    CustomLogger.logStringWithObject("CURRENT PAGE MATRIX:", currentPageMatrix);

    //dashboard
    if (currentPageMatrix.show_field_menu_heading_navigation) {
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_navigation,
        main: [
          {
            state: 'dashboard',
            name: this.field_menu_dashboard,
            type: 'link',
            icon: 'ti-dashboard'
          }
        ],
      });
    }

    //users
    if (
      currentPageMatrix.show_field_menu_link_listUser
      // || currentPageMatrix.show_field_menu_link_crudUser
      || currentPageMatrix.show_field_menu_link_listAccessEntity
      || currentPageMatrix.show_field_menu_link_listManagerUser
      || currentPageMatrix.show_field_menu_link_listMakerCheckerRequest) {
      let childrenArr = [];

      if (currentPageMatrix.show_field_menu_link_listUser) {
        childrenArr.push({
          state: 'listUser',
          name: this.field_menu_link_listUser,
        });
      }

      // if (currentPageMatrix.show_field_menu_link_crudUser) {
      //   childrenArr.push({
      //     state: 'crudUser',
      //     name: this.field_menu_link_crudUser,
      //   });
      // }

      if (currentPageMatrix.show_field_menu_link_listAccessEntity) {
        childrenArr.push({
          state: 'listAccessEntity',
          name: this.field_menu_link_listAccessEntity,
        });
      }

      if (currentPageMatrix.show_field_menu_link_listManagerUser) {
        childrenArr.push({
          state: 'listManagerUser',
          name: this.field_menu_link_listManagerUser,
        });
      }

      if (currentPageMatrix.show_field_menu_link_listMakerCheckerRequest) {
        childrenArr.push({
          state: 'listMakerCheckerRequest',
          name: this.field_menu_link_listMakerCheckerRequest,
        });
      }



      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_users,
        main: [
          {
            state: 'users',
            name: this.field_menu_heading_users,
            type: 'sub',
            icon: 'ti-user',
            children: childrenArr
          }
        ]
      });
    }


    //connectors
    if (
      currentPageMatrix.show_field_menu_link_listPredefinedConnectors) {
      let childrenArr = [];
      if (currentPageMatrix.show_field_menu_link_listPredefinedConnectors) {
        childrenArr.push({
          state: 'listConnector',
          name: this.field_menu_link_listPredefinedConnectors,
        });
      }

      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_connectors,
        main: [
          {
            state: CustomGlobalConstants.APP_MENU_PATH_NAMES.DB_CONNECTOR,
            name: this.field_menu_heading_connectors,
            type: 'sub',
            icon: 'ti-infinite',
            children: childrenArr
          }
        ]
      });
    }


    //sectors
    if (currentPageMatrix.show_field_menu_link_listEntity || currentPageMatrix.show_field_menu_link_listSector || currentPageMatrix.show_field_menu_link_listSectorElements) {
      let childrenArr = [];
      if (currentPageMatrix.show_field_menu_link_listEntity) {
        childrenArr.push({
          state: 'listEntity',
          name: this.field_menu_link_listEntity,
        });
      }
      if (currentPageMatrix.show_field_menu_link_listSector) {
        childrenArr.push({
          state: 'listSector',
          name: this.field_menu_link_listSector,
        });
      }
      if (currentPageMatrix.show_field_menu_link_listSectorElements) {
        childrenArr.push({
          state: 'listSectorElements',
          name: this.field_menu_link_listSectorElements,
        });
      }
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_sectors,
        main: [
          {
            state: 'sectors',
            name: this.field_menu_heading_sectors,
            type: 'sub',
            icon: 'ti-desktop',
            children: childrenArr
          }
        ]
      });
    }


    //configuration
    if (currentPageMatrix.show_field_menu_link_listRole
      || currentPageMatrix.show_field_menu_link_listPermission
      || currentPageMatrix.show_field_menu_link_listRule
      || currentPageMatrix.show_field_menu_link_listAccessPolicy
    ) {
      let childrenArr = [];
      if (currentPageMatrix.show_field_menu_link_listAccessPolicy) {
        childrenArr.push({
          state: 'listAccessPolicy',
          name: this.field_menu_link_listAccessPolicy,
        });
      }
      if (currentPageMatrix.show_field_menu_link_listRole) {
        childrenArr.push({
          state: 'listRole',
          name: this.field_menu_link_listRole,
        });
      }

      //COMMENTING PERMISSION MENU ITEM
      // if (currentPageMatrix.show_field_menu_link_listPermission) {
      //   childrenArr.push({
      //     state: 'listPermission',
      //     name: this.field_menu_link_listPermission,
      //   });
      // }

      //COMMENTING RULE MENU ITEM
      // if (currentPageMatrix.show_field_menu_link_listRule) {
      //   childrenArr.push({
      //     state: 'listRule',
      //     name: this.field_menu_link_listRule,
      //   });
      // }

      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_configuration,
        main: [
          {
            state: 'configuration',
            name: this.field_menu_heading_configuration,
            type: 'sub',
            icon: 'ti-pencil-alt',
            children: childrenArr
          }
        ]
      });
    }


    //settings
    if (currentPageMatrix.show_field_menu_link_editFields ||
      currentPageMatrix.show_field_menu_link_emailAlerts ||
      currentPageMatrix.show_field_menu_link_editUserProfile ||
      currentPageMatrix.show_field_menu_link_listAlertNotification ||
      currentPageMatrix.show_field_menu_link_defaultSetting ||
      currentPageMatrix.show_field_menu_link_listPageMatrix ||
      currentPageMatrix.show_field_menu_link_listFormElement ||
      currentPageMatrix.show_field_menu_link_listApprovalMatrix
    ) {
      let childrenArr = [];
      if (currentPageMatrix.show_field_menu_link_listApprovalMatrix) {
        childrenArr.push({
          state: 'listApprovalMatrix',
          name: this.field_menu_link_listApprovalMatrix,
        });
      }
      if (currentPageMatrix.show_field_menu_link_listFormElement) {
        childrenArr.push({
          state: 'listFormElement',
          name: this.field_menu_link_listFormElement,
        });
      }
      if (currentPageMatrix.show_field_menu_link_listPageMatrix) {
        childrenArr.push({
          state: 'listPageMatrix',
          name: this.field_menu_link_listPageMatrix
        });
      }
      if (currentPageMatrix.show_field_menu_link_editFields) {
        childrenArr.push({
          state: 'editFields',
          name: this.field_menu_link_editFields
        });
      }
      if (currentPageMatrix.show_field_menu_link_listAlertNotification) {
        childrenArr.push({
          state: 'listMessage',
          name: this.field_menu_link_listAlertNotification,
        });
      }
      if (currentPageMatrix.show_field_menu_link_emailAlerts) {
        childrenArr.push({
          state: 'emailAlerts',
          name: this.field_menu_link_emailAlerts,
        });
      }
      if (currentPageMatrix.show_field_menu_link_defaultSetting) {
        childrenArr.push({
          state: 'listDefaultSetting',
          name: this.field_menu_link_defaultSetting,
        });
      }
      if (currentPageMatrix.show_field_menu_link_editUserProfile) {
        childrenArr.push({
          state: 'editUserProfile',
          name: this.field_menu_link_editUserProfile,
        });
      }
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_settings,
        main: [
          {
            state: 'settings',
            name: this.field_menu_heading_settings,
            type: 'sub',
            icon: 'ti-settings',
            children: childrenArr
          }
        ]
      });
    }


    //reports
    if (currentPageMatrix.show_field_menu_link_report_default || currentPageMatrix.show_field_menu_link_report_entityEvents || currentPageMatrix.show_field_menu_link_report_systemEvents || currentPageMatrix.show_field_menu_link_report_existingUsers || currentPageMatrix.show_field_menu_link_report_userLogging || currentPageMatrix.show_field_menu_link_report_userActivityReports) {
      let childrenArr = [];
      if (currentPageMatrix.show_field_menu_link_report_default) {
        childrenArr.push({
          state: String(CustomGlobalConstants.REPORTING_TYPES.DEFAULT),
          name: this.field_menu_link_report_default
        });
      }
      if (currentPageMatrix.show_field_menu_link_report_entityEvents) {
        childrenArr.push({
          state: 'entityEventReport',
          name: this.field_menu_link_report_entityEvents
        });
      }
      if (currentPageMatrix.show_field_menu_link_report_systemEvents) {
        childrenArr.push({
          state: String(CustomGlobalConstants.REPORTING_TYPES.SYSTEM_EVENTS),
          name: this.field_menu_link_report_systemEvents
        });
      }
      if (currentPageMatrix.show_field_menu_link_report_existingUsers) {
        childrenArr.push({
          state: String(CustomGlobalConstants.REPORTING_TYPES.EXISTING_USERS),
          name: this.field_menu_link_report_existingUsers
        });
      }
      if (currentPageMatrix.show_field_menu_link_report_userLogging) {
        childrenArr.push({
          state: String(CustomGlobalConstants.REPORTING_TYPES.LOGGING_REPORTS),
          name: this.field_menu_link_report_userLogging
        });
      }
      if (currentPageMatrix.show_field_menu_link_report_userActivityReports) {
        childrenArr.push({
          state: String(CustomGlobalConstants.REPORTING_TYPES.USER_ACTIVITY_REPORTS),
          name: this.field_menu_link_report_userActivityReports
        });
      }
      this.ADMIN_SPECIFIC_MENUITEMS.push({
        label: this.field_menu_heading_reports,
        main: [
          {
            state: 'reporting',
            name: this.field_menu_heading_reports,
            type: 'sub',
            icon: 'ti-bar-chart',
            children: childrenArr
          }
        ]
      });
    }
  }

  async ngOnInit() {
  }

  isLocalComplete() {
    return this.flagAreFieldsLoaded;
  }


  getAll(): Menu[] {
    return this.ADMIN_SPECIFIC_MENUITEMS;
  }

}
