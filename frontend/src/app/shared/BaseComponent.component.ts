import { CustomLogger } from 'src/app/modules/utils/CustomLogger';
import { DBService } from '../modules/services/dbService.service';
import { Injector, OnInit, Component } from '@angular/core';
import { FieldDetail } from '../modules/models/common/FieldDetail.model';
import { CustomGlobalConstants } from '../modules/utils/CustomGlobalConstants';
import { GlobalService } from '../modules/services/global.service';
import { MiscService } from '../modules/services/miscService.service';
import { CommonComponent } from './CommonComponent.component';

export class BaseComponent extends CommonComponent {
    //show spinner flag
    showSpinnerFlag: boolean = false;
    isComponentNew: boolean = true;
    isCreatable = false;
    isReadable = false;
    isUpdatable = false;
    isDeletable = false;
    flagAreFieldsLoaded = false;

    /////////////////////////////////////////////////////
    //dashboard - component fields
    field_dashboard_heading_systems: string = "";
    field_dashboard_heading_networks: string = "";
    field_dashboard_heading_files: string = "";
    field_dashboard_heading_applications: string = "";
    field_dashboard_heading_total_system_entities: string = "";
    field_dashboard_heading_total_network_entities: string = "";
    field_dashboard_heading_total_file_entities: string = "";
    field_dashboard_heading_total_application_entities: string = "";
    field_dashboard_heading_recent_activities: string = "";
    field_dashboard_heading_activity_source: string = "";
    field_dashboard_heading_activity_type: string = "";
    field_dashboard_heading_who_initiated: string = "";
    field_dashboard_heading_activity_time: string = "";
    field_dashboard_heading_created: string = "";
    field_dashboard_heading_deleted: string = "";
    field_dashboard_heading_users: string = "";


    //menu items
    field_menu_heading_navigation: string = "NAVIGATION";
    field_menu_dashboard: string = "";
    field_menu_standardDashboard: string = "";
    field_menu_heading_users: string = "";
    field_menu_link_listUser: string = "";
    field_menu_link_crudUser: string = "";
    field_menu_link_listAccessEntity: string = "";
    field_menu_heading_sectors: string = "";
    field_menu_link_listEntity: string = "";
    field_menu_link_listSector: string = "";
    field_menu_link_listSectorElements: string = "";
    field_menu_heading_configuration: string = "";
    field_menu_link_listRole: string = "";
    field_menu_link_listPermission: string = "";
    field_menu_link_listRule: string = "";
    field_menu_heading_settings: string = "";
    field_menu_link_editFields: string = "";
    field_menu_link_emailAlerts: string = "";
    field_menu_link_editUserProfile: string = "";
    field_menu_heading_reports: string = "";
    field_menu_link_report_default: string = "";
    field_menu_link_report_systemEvents: string = "";
    field_menu_link_report_entityEvents: string = "";
    field_menu_link_report_userLogging: string = "";
    field_menu_link_report_existingUsers: string = "";
    field_menu_link_report_userActivityReports: string = "";
    field_menu_heading_entityModule: string = "";
    field_menu_link_accessEntity: string = "";
    field_menu_link_showEntities: string = "";
    field_menu_link_listAlertNotification: string = "";
    field_menu_link_defaultSetting: string = "";
    field_menu_link_listPageMatrix: string = "";
    field_menu_link_listFormElement: string = "";
    field_menu_link_listManagerUser: string = "";
    field_menu_link_listMakerCheckerRequest: string = "";
    field_menu_link_listApprovalMatrix: string = "";
    field_menu_hrDashboard: string = "";
    field_menu_heading_connectors: string = "";
    field_menu_link_listPredefinedConnectors: string = "";
    field_menu_link_listAccessPolicy: string = "";

    //crudUser - component fields
    field_user_type: string = "";
    field_password: string = "";
    field_email: string = "";
    field_salutation: string = "";
    field_first_name: string = "";
    field_middle_name: string = "";
    field_last_name: string = "";
    field_full_name: string = "";
    field_facebook_id: string = "";
    field_google_id: string = "";
    field_phone: string = "";
    field_dob: string = "";
    field_profile_link: string = "";
    field_role_id: string = "";
    field_is_otp_required: string = "";
    field_username: string = "";
    field_user_type_required: boolean = false;
    field_password_required: boolean = false;
    field_email_required: boolean = false;
    field_salutation_required: boolean = false;
    field_first_name_required: boolean = false;
    field_middle_name_required: boolean = false;
    field_last_name_required: boolean = false;
    field_full_name_required: boolean = false;
    field_facebook_id_required: boolean = false;
    field_google_id_required: boolean = false;
    field_phone_required: boolean = false;
    field_dob_required: boolean = false;
    field_profile_link_required: boolean = false;
    field_role_id_required: boolean = false;
    field_is_otp_required_required: boolean = false;
    field_username_required: boolean = false;
    field_crudUser_add_new: string = "";
    field_crudUser_update: string = "";
    field_crudUser_error_user_type: string = "";
    field_crudUser_heading_user: string = "";
    field_crudUser_heading_check_username: string = "";

    field_username_error_message: string = "";
    field_user_type_error_message: string = "";
    field_password_error_message: string = "";
    field_email_error_message: string = "";
    field_salutation_error_message: string = "";
    field_first_name_error_message: string = "";
    field_middle_name_error_message: string = "";
    field_last_name_error_message: string = "";
    field_full_name_error_message: string = "";
    field_facebook_id_error_message: string = "";
    field_google_id_error_message: string = "";
    field_phone_error_message: string = "";
    field_dob_error_message: string = "";
    field_profile_link_error_message: string = "";
    field_role_id_error_message: string = "";
    field_is_otp_error_message_error_message: string = "";

    field_editUserProfile_email_error_message: string = "";
    field_editUserProfile_salutation_error_message: string = "";
    field_editUserProfile_first_name_error_message: string = "";
    field_editUserProfile_middle_name_error_message: string = "";
    field_editUserProfile_last_name_error_message: string = "";
    field_editUserProfile_full_name_error_message: string = "";
    field_editUserProfile_facebook_id_error_message: string = "";
    field_editUserProfile_google_id_error_message: string = "";
    field_editUserProfile_phone_error_message: string = "";
    field_editUserProfile_date_of_birth_error_message: string = "";
    field_editUserProfile_profile_link_error_message: string = "";

    field_editUserProfile_heading_update_user: string = "";

    //listUsercomponent fields
    field_listUser_heading_user_management: string = "";
    field_listUser_heading_add_user: string = "";
    field_listUser_user_name: string = "";
    field_listUser_email: string = "";
    field_listUser_first_name: string = "";
    field_listUser_last_name: string = "";
    field_listUser_phone: string = "";
    field_listUser_button_edit: string = "";
    field_listUser_button_delete: string = "";


    //crudPermission component fields
    field_permission_id: string = "";
    field_permission_name: string = "";
    field_details: string = "";
    field_remarks: string = "";
    field_permission_id_required: boolean = false;
    field_permission_name_required: boolean = false;
    field_details_required: boolean = false;
    field_remarks_required: boolean = false;
    field_crudPermission_permission_name_error_message: string = "";
    field_crudPermission_details_error_message: string = "";

    //crudEntity
    field_entity_id: string = "";
    field_entity_name: string = "";
    field_access_location: string = "";
    field_icon_location: string = "";
    field_is_default: string = "";
    field_entity_id_required: boolean = false;
    field_entity_name_required: boolean = false;
    field_access_location_required: boolean = false;
    field_icon_location_required: boolean = false;
    field_is_default_required: boolean = false;
    field_crudEntity_risk_level: string = "";
    field_crudEntity_risk_level_required: boolean = false;

    field_entity_name_error_message: string = "";
    field_access_location_error_message: string = "";
    field_icon_location_error_message: string = "";
    field_crudEntity_details_error_message: string = "";
    field_crudEntity_risk_level_error_message: string = "";

    //crudRole
    // field_role_id: string = "";
    field_role_name: string = "";
    field_allowed_entity_id_arr: string = "";

    // field_role_id_required: boolean = false;
    field_role_name_required: boolean = false;
    field_allowed_entity_id_arr_required: boolean = false;
    // field_details_required: boolean = false;
    // field_remarks_required: boolean = false;

    field_crudRole_role_name_error_message: string = "";
    field_crudRole_details_error_message: string = "";
    field_crudRole_allowed_entity_id_arr_error_message: string = "";

    //crudRule
    field_rule_id: string = "";
    field_rule_name: string = "";
    field_sector_id_arr: string = "";
    field_role_id_arr: string = "";
    field_entity_id_arr: string = "";
    field_permission_id_arr: string = "";
    field_crudRule_make_the_rule: string = "";
    field_crudRule_role_specific: string = "";
    field_crudRule_user_specific: string = "";
    field_crudRule_can_be_deleted: string = "";


    field_rule_id_required: boolean = false;
    field_rule_name_required: boolean = false;
    field_sector_id_arr_required: boolean = false;
    field_role_id_arr_required: boolean = false;
    field_entity_id_arr_required: boolean = false;
    field_permission_id_arr_required: boolean = false;
    field_crudRule_rule_name_error_message: string = "";
    field_crudRule_details_error_message: string = "";

    //crudSector
    field_sector_id: string = "";
    field_sector_name: string = "";
    field_sector_type: string = "";
    field_sector_icon_location: string = "";

    field_sector_id_required: boolean = false;
    field_sector_name_required: boolean = false;
    field_sector_type_required: boolean = false;
    field_sector_icon_location_required: boolean = false;

    field_crudSector_sector_name_error_message: string = "";
    field_crudSector_entity_id_arr_error_message: string = "";
    field_crudSector_details_error_message: string = "";


    //listAccessEntity
    field_listAccessEntity_heading_user_requests: string = "";
    field_listAccessEntity_requested_username: string = "";
    field_listAccessEntity_requested_entity_id: string = "";
    field_listAccessEntity_requested_time: string = "";
    field_listAccessEntity_current_status: string = "";
    field_listAccessEntity_is_new_request: string = "";
    field_listAccessEntity_button_approve: string = "";
    field_listAccessEntity_button_decline: string = "";

    //listEntity
    field_listEntity_heading_entity_management: string = "";
    field_listEntity_button_add_entity: string = "";
    field_listEntity_heading_entity_details: string = "";
    field_listEntity_entity_name: string = "";
    field_listEntity_details: string = "";
    field_listEntity_access_location: string = "";
    field_listEntity_button_edit: string = "";
    field_listEntity_button_delete: string = "";


    //listSector
    field_listSector_heading_sector_management: string = "";
    field_listSector_button_add_sector: string = "";
    field_listSector_heading_sector_details: string = "";
    field_listSector_sector_id: string = "";
    field_listSector_sector_name: string = "";
    field_listSector_details: string = "";
    field_listSector_button_edit: string = "";
    field_listSector_button_delete: string = "";


    //listRole
    field_listRole_heading_role_management: string = "";
    field_listRole_button_add_role: string = "";
    field_listRole_heading_role_details: string = "";
    field_listRole_role_id: string = "";
    field_listRole_role_name: string = "";
    field_listRole_details: string = "";
    field_listRole_button_edit: string = "";
    field_listRole_button_delete: string = "";

    //listPermission
    field_listPermission_heading_permission_management: string = "";
    field_listPermission_button_add_permission: string = "";
    field_listPermission_heading_permission_details: string = "";
    field_listPermission_permission_id: string = "";
    field_listPermission_permission_name: string = "";
    field_listPermission_details: string = "";
    field_listPermission_button_edit: string = "";
    field_listPermission_button_delete: string = "";

    //listRule
    field_listRule_heading_rule_management: string = "";
    field_listRule_button_add_rule: string = "";
    field_listRule_heading_rule_details: string = "";
    field_listRule_rule_id: string = "";
    field_listRule_rule_name: string = "";
    field_listRule_details: string = "";
    field_listRule_button_edit: string = "";
    field_listRule_button_delete: string = "";


    //editFields
    field_editFields_heading_configure_fields: string = "";
    field_editFields_select_component: string = "";
    field_editFields_select_field_to_edit: string = "";
    field_editFields_current_field_value: string = "";
    field_editFields_make_field: string = "";
    field_editFields_mandatory: string = "";
    field_editFields_optional: string = "";
    field_editFields_error_message: string = "";
    field_editFields_button_update: string = "";
    field_editFields_button_reset: string = "";
    field_editFields_cancel: string = "";

    //emailAlert
    field_emailAlert_heading_configure_fields: string = "";
    field_emailAlert_heading_configure_email_alert: string = "";
    field_emailAlert_should_send_email: string = "";
    field_emailAlert_yes: string = "";
    field_emailAlert_no: string = "";
    field_emailAlert_alert_level: string = "";
    field_emailAlert_fatal: string = "";
    field_emailAlert_critical: string = "";
    field_emailAlert_high: string = "";
    field_emailAlert_medium: string = "";
    field_emailAlert_low: string = "";
    field_emailAlert_button_update: string = "";

    //alertNotification
    field_alertNotification_heading_alert_notification: string = "";
    field_alertNotification_select_component: string = "";
    field_alertNotification_select_alert_name: string = "";
    field_alertNotification_current_alert_text: string = "";
    field_alertNotification_button_update: string = "";
    field_alertNotification_button_reset: string = "";
    field_alertNotification_button_cancel: string = "";


    //crudPageMatrix
    field_crudPageMatrix_dashboard: string;
    field_crudPageMatrix_heading_page_matrix: string;
    field_crudPageMatrix_select_role_id: string;
    field_crudPageMatrix_listUser: string;
    field_crudPageMatrix_crudUser: string;
    field_crudPageMatrix_listAccessEntity: string;
    field_crudPageMatrix_listEntity: string;
    field_crudPageMatrix_listSector: string;
    field_crudPageMatrix_listSectorElements: string;
    field_crudPageMatrix_listRole: string;
    field_crudPageMatrix_listPermission: string;
    field_crudPageMatrix_listRule: string;
    field_crudPageMatrix_editFields: string;
    field_crudPageMatrix_emailAlerts: string;
    field_crudPageMatrix_editUserProfile: string;
    field_crudPageMatrix_listAlertNotification: string;
    field_crudPageMatrix_defaultSetting: string;
    field_crudPageMatrix_report_default: string;
    field_crudPageMatrix_report_entityEvents: string;
    field_crudPageMatrix_report_systemEvents: string;
    field_crudPageMatrix_report_existingUsers: string;
    field_crudPageMatrix_report_userLogging: string;
    field_crudPageMatrix_report_userActivityReports: string;
    field_crudPageMatrix_listPageMatrix: string;
    field_crudPageMatrix_select_username: string;
    field_crudPageMatrix_standardDashboard: string;
    field_crudPageMatrix_standardShowEntities: string;
    field_crudPageMatrix_standardEditUserProfile: string;
    field_crudPageMatrix_allow: string;
    field_crudPageMatrix_deny: string;
    field_crudPageMatrix_details_required: string;
    field_crudPageMatrix_details_error_message: string;
    field_crudPageMatrix_details: string;
    field_crudPageMatrix_button_save: string;
    field_crudPageMatrix_button_update: string;
    field_crudPageMatrix_button_reset: string;
    field_crudPageMatrix_button_cancel: string;
    field_crudPageMatrix_listManagerUser: string;
    field_crudPageMatrix_listMakerCheckerRequest: string;
    field_crudPageMatrix_listApprovalMatrix: string;
    field_crudPageMatrix_hrDashboard: string;

    //listPageMatrix
    field_listPageMatrix_heading_page_matrix_management: string;
    field_listPageMatrix_button_add_page_matrix: string;
    field_listPageMatrix_heading_page_matrix: string;
    field_listPageMatrix_page_matrix_id: string;
    field_listPageMatrix_role: string;
    field_listPageMatrix_associated_user_id: string;
    field_listPageMatrix_details: string;
    field_listPageMatrix_button_edit: string;
    field_listPageMatrix_button_delete: string;

    //listManagerUser
    field_listManagerUser_heading_user_management: string;
    field_listManagerUser_username: string;
    field_listManagerUser_button_edit: string;

    //crudManagerUser
    field_crudManagerUser_heading: string;
    field_crudManagerUser_username: string;



    initializeHtmlFields() {
        switch (this.COMPONENT_NAME) {
            case CustomGlobalConstants.COMPONENT_NAME.CRUD_MANAGER_USER:
                this.field_crudManagerUser_heading = this.getFieldValue("field_crudManagerUser_heading");
                this.field_crudManagerUser_username = this.getFieldValue("field_crudManagerUser_username");
                this.field_button_update = this.getFieldValue("field_button_update");
                this.field_button_reset = this.getFieldValue("field_button_reset");
                this.field_button_cancel = this.getFieldValue("field_button_cancel");
                break;
            case CustomGlobalConstants.COMPONENT_NAME.LIST_MANAGER_USER:
                this.field_listManagerUser_heading_user_management = this.getFieldValue("field_listManagerUser_heading_user_management");
                this.field_listManagerUser_username = this.getFieldValue("field_listManagerUser_username");
                this.field_listManagerUser_button_edit = this.getFieldValue("field_listManagerUser_button_edit");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_PAGE_MATRIX:
                this.field_listPageMatrix_heading_page_matrix_management = this.getFieldValue("field_listPageMatrix_heading_page_matrix_management");
                this.field_listPageMatrix_button_add_page_matrix = this.getFieldValue("field_listPageMatrix_button_add_page_matrix");
                this.field_listPageMatrix_heading_page_matrix = this.getFieldValue("field_listPageMatrix_heading_page_matrix");
                this.field_listPageMatrix_page_matrix_id = this.getFieldValue("field_listPageMatrix_page_matrix_id");
                this.field_listPageMatrix_role = this.getFieldValue("field_listPageMatrix_role");
                this.field_listPageMatrix_associated_user_id = this.getFieldValue("field_listPageMatrix_associated_user_id");
                this.field_listPageMatrix_details = this.getFieldValue("field_listPageMatrix_details");
                this.field_listPageMatrix_button_edit = this.getFieldValue("field_listPageMatrix_button_edit");
                this.field_listPageMatrix_button_delete = this.getFieldValue("field_listPageMatrix_button_delete");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_PAGE_MATRIX:
                this.field_crudPageMatrix_dashboard = this.getFieldValue("field_crudPageMatrix_dashboard");
                this.field_crudPageMatrix_heading_page_matrix = this.getFieldValue("field_crudPageMatrix_heading_page_matrix");
                this.field_crudPageMatrix_select_role_id = this.getFieldValue("field_crudPageMatrix_select_role_id");
                this.field_crudPageMatrix_listUser = this.getFieldValue("field_crudPageMatrix_listUser");
                this.field_crudPageMatrix_crudUser = this.getFieldValue("field_crudPageMatrix_crudUser");
                this.field_crudPageMatrix_listAccessEntity = this.getFieldValue("field_crudPageMatrix_listAccessEntity");
                this.field_crudPageMatrix_listEntity = this.getFieldValue("field_crudPageMatrix_listEntity");
                this.field_crudPageMatrix_listSector = this.getFieldValue("field_crudPageMatrix_listSector");
                this.field_crudPageMatrix_listSectorElements = this.getFieldValue("field_crudPageMatrix_listSectorElements");
                this.field_crudPageMatrix_listRole = this.getFieldValue("field_crudPageMatrix_listRole");
                this.field_crudPageMatrix_listPermission = this.getFieldValue("field_crudPageMatrix_listPermission");
                this.field_crudPageMatrix_listRule = this.getFieldValue("field_crudPageMatrix_listRule");
                this.field_crudPageMatrix_editFields = this.getFieldValue("field_crudPageMatrix_editFields");
                this.field_crudPageMatrix_emailAlerts = this.getFieldValue("field_crudPageMatrix_emailAlerts");
                this.field_crudPageMatrix_editUserProfile = this.getFieldValue("field_crudPageMatrix_editUserProfile");
                this.field_crudPageMatrix_listAlertNotification = this.getFieldValue("field_crudPageMatrix_listAlertNotification");
                this.field_crudPageMatrix_defaultSetting = this.getFieldValue("field_crudPageMatrix_defaultSetting");
                this.field_crudPageMatrix_report_default = this.getFieldValue("field_crudPageMatrix_report_default");
                this.field_crudPageMatrix_report_entityEvents = this.getFieldValue("field_crudPageMatrix_report_entityEvents");
                this.field_crudPageMatrix_report_systemEvents = this.getFieldValue("field_crudPageMatrix_report_systemEvents");
                this.field_crudPageMatrix_report_existingUsers = this.getFieldValue("field_crudPageMatrix_report_existingUsers");
                this.field_crudPageMatrix_report_userLogging = this.getFieldValue("field_crudPageMatrix_report_userLogging");
                this.field_crudPageMatrix_report_userActivityReports = this.getFieldValue("field_crudPageMatrix_report_userActivityReports");
                this.field_crudPageMatrix_listPageMatrix = this.getFieldValue("field_crudPageMatrix_listPageMatrix");
                this.field_crudPageMatrix_select_username = this.getFieldValue("field_crudPageMatrix_select_username");
                this.field_crudPageMatrix_standardDashboard = this.getFieldValue("field_crudPageMatrix_standardDashboard");
                this.field_crudPageMatrix_standardShowEntities = this.getFieldValue("field_crudPageMatrix_standardShowEntities");
                this.field_crudPageMatrix_standardEditUserProfile = this.getFieldValue("field_crudPageMatrix_standardEditUserProfile");
                this.field_crudPageMatrix_allow = this.getFieldValue("field_crudPageMatrix_allow");
                this.field_crudPageMatrix_deny = this.getFieldValue("field_crudPageMatrix_deny");
                this.field_crudPageMatrix_details_required = this.getFieldValue("field_crudPageMatrix_details_required");
                this.field_crudPageMatrix_details_error_message = this.getFieldValue("field_crudPageMatrix_details_error_message");
                this.field_crudPageMatrix_details = this.getFieldValue("field_crudPageMatrix_details");
                this.field_crudPageMatrix_button_save = this.getFieldValue("field_crudPageMatrix_button_save");
                this.field_crudPageMatrix_button_update = this.getFieldValue("field_crudPageMatrix_button_update");
                this.field_crudPageMatrix_button_reset = this.getFieldValue("field_crudPageMatrix_button_reset");
                this.field_crudPageMatrix_button_cancel = this.getFieldValue("field_crudPageMatrix_button_cancel");
                this.field_crudPageMatrix_listManagerUser = this.getFieldValue("field_crudPageMatrix_listManagerUser");
                this.field_crudPageMatrix_listMakerCheckerRequest = this.getFieldValue("field_crudPageMatrix_listMakerCheckerRequest");
                this.field_crudPageMatrix_listApprovalMatrix = this.getFieldValue("field_crudPageMatrix_listApprovalMatrix");
                this.field_crudPageMatrix_hrDashboard = this.getFieldValue("field_crudPageMatrix_hrDashboard");
                break;
            case CustomGlobalConstants.COMPONENT_NAME.ALERT_NOTIFICATION:
                this.field_alertNotification_heading_alert_notification = this.getFieldValue("field_alertNotification_heading_alert_notification");
                this.field_alertNotification_select_component = this.getFieldValue("field_alertNotification_select_component");
                this.field_alertNotification_select_alert_name = this.getFieldValue("field_alertNotification_select_alert_name");
                this.field_alertNotification_current_alert_text = this.getFieldValue("field_alertNotification_current_alert_text");
                this.field_alertNotification_button_update = this.getFieldValue("field_alertNotification_button_update");
                this.field_alertNotification_button_reset = this.getFieldValue("field_alertNotification_button_reset");
                this.field_alertNotification_button_cancel = this.getFieldValue("field_alertNotification_button_cancel");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.EMAIL_ALERT:
                this.field_emailAlert_heading_configure_fields = this.getFieldValue("field_emailAlert_heading_configure_fields");
                this.field_emailAlert_heading_configure_email_alert = this.getFieldValue("field_emailAlert_heading_configure_email_alert");
                this.field_emailAlert_should_send_email = this.getFieldValue("field_emailAlert_should_send_email");
                this.field_emailAlert_yes = this.getFieldValue("field_emailAlert_yes");
                this.field_emailAlert_no = this.getFieldValue("field_emailAlert_no");
                this.field_emailAlert_alert_level = this.getFieldValue("field_emailAlert_alert_level");
                this.field_emailAlert_fatal = this.getFieldValue("field_emailAlert_fatal");
                this.field_emailAlert_critical = this.getFieldValue("field_emailAlert_critical");
                this.field_emailAlert_high = this.getFieldValue("field_emailAlert_high");
                this.field_emailAlert_medium = this.getFieldValue("field_emailAlert_medium");
                this.field_emailAlert_low = this.getFieldValue("field_emailAlert_low");
                this.field_emailAlert_button_update = this.getFieldValue("field_emailAlert_button_update");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.EDIT_FIELDS:
                this.field_editFields_heading_configure_fields = this.getFieldValue("field_editFields_heading_configure_fields");
                this.field_editFields_select_component = this.getFieldValue("field_editFields_select_component");
                this.field_editFields_select_field_to_edit = this.getFieldValue("field_editFields_select_field_to_edit");
                this.field_editFields_current_field_value = this.getFieldValue("field_editFields_current_field_value");
                this.field_editFields_make_field = this.getFieldValue("field_editFields_make_field");
                this.field_editFields_mandatory = this.getFieldValue("field_editFields_mandatory");
                this.field_editFields_optional = this.getFieldValue("field_editFields_optional");
                this.field_editFields_error_message = this.getFieldValue("field_editFields_error_message");
                this.field_editFields_button_update = this.getFieldValue("field_editFields_button_update");
                this.field_editFields_button_reset = this.getFieldValue("field_editFields_button_reset");
                this.field_editFields_cancel = this.getFieldValue("field_editFields_cancel");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_RULE:
                this.field_listRule_heading_rule_management = this.getFieldValue("field_listRule_heading_rule_management")
                this.field_listRule_button_add_rule = this.getFieldValue("field_listRule_button_add_rule")
                this.field_listRule_heading_rule_details = this.getFieldValue("field_listRule_heading_rule_details")
                this.field_listRule_rule_id = this.getFieldValue("field_listRule_rule_id")
                this.field_listRule_rule_name = this.getFieldValue("field_listRule_rule_name")
                this.field_listRule_details = this.getFieldValue("field_listRule_details")
                this.field_listRule_button_edit = this.getFieldValue("field_listRule_button_edit")
                this.field_listRule_button_delete = this.getFieldValue("field_listRule_button_delete")
                break;


            case CustomGlobalConstants.COMPONENT_NAME.LIST_PERMISSION:
                this.field_listPermission_heading_permission_management = this.getFieldValue("field_listPermission_heading_permission_management");
                this.field_listPermission_button_add_permission = this.getFieldValue("field_listPermission_button_add_permission");
                this.field_listPermission_heading_permission_details = this.getFieldValue("field_listPermission_heading_permission_details");
                this.field_listPermission_permission_id = this.getFieldValue("field_listPermission_permission_id");
                this.field_listPermission_permission_name = this.getFieldValue("field_listPermission_permission_name");
                this.field_listPermission_details = this.getFieldValue("field_listPermission_details");
                this.field_listPermission_button_edit = this.getFieldValue("field_listPermission_button_edit");
                this.field_listPermission_button_delete = this.getFieldValue("field_listPermission_button_delete");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_ROLE:
                this.field_listRole_heading_role_management = this.getFieldValue("field_listRole_heading_role_management");
                this.field_listRole_button_add_role = this.getFieldValue("field_listRole_button_add_role");
                this.field_listRole_heading_role_details = this.getFieldValue("field_listRole_heading_role_details");
                this.field_listRole_role_id = this.getFieldValue("field_listRole_role_id");
                this.field_listRole_role_name = this.getFieldValue("field_listRole_role_name");
                this.field_listRole_details = this.getFieldValue("field_listRole_details");
                this.field_listRole_button_edit = this.getFieldValue("field_listRole_button_edit");
                this.field_listRole_button_delete = this.getFieldValue("field_listRole_button_delete");
                break;
            case CustomGlobalConstants.COMPONENT_NAME.LIST_SECTOR:
                this.field_listSector_heading_sector_management = this.getFieldValue("field_listSector_heading_sector_management");
                this.field_listSector_button_add_sector = this.getFieldValue("field_listSector_button_add_sector");
                this.field_listSector_heading_sector_details = this.getFieldValue("field_listSector_heading_sector_details");
                this.field_listSector_sector_id = this.getFieldValue("field_listSector_sector_id");
                this.field_listSector_sector_name = this.getFieldValue("field_listSector_sector_name");
                this.field_listSector_details = this.getFieldValue("field_listSector_details");
                this.field_listSector_button_edit = this.getFieldValue("field_listSector_button_edit");
                this.field_listSector_button_delete = this.getFieldValue("field_listSector_button_delete");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_ENTITY:
                this.field_listEntity_heading_entity_management = this.getFieldValue("field_listEntity_heading_entity_management");
                this.field_listEntity_button_add_entity = this.getFieldValue("field_listEntity_button_add_entity");
                this.field_listEntity_heading_entity_details = this.getFieldValue("field_listEntity_heading_entity_details");
                this.field_listEntity_entity_name = this.getFieldValue("field_listEntity_entity_name");
                this.field_listEntity_details = this.getFieldValue("field_listEntity_details");
                this.field_listEntity_access_location = this.getFieldValue("field_listEntity_access_location");
                this.field_listEntity_button_edit = this.getFieldValue("field_listEntity_button_edit");
                this.field_listEntity_button_delete = this.getFieldValue("field_listEntity_button_delete");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_ACCESS_ENTITY:
                CustomLogger.logString("BaseComponent: Will fill fields for Access Entity Component");
                this.field_listAccessEntity_heading_user_requests = this.getFieldValue("field_listAccessEntity_heading_user_requests");
                this.field_listAccessEntity_requested_username = this.getFieldValue("field_listAccessEntity_requested_username");
                this.field_listAccessEntity_requested_entity_id = this.getFieldValue("field_listAccessEntity_requested_entity_id");
                this.field_listAccessEntity_requested_time = this.getFieldValue("field_listAccessEntity_requested_time");
                this.field_listAccessEntity_current_status = this.getFieldValue("field_listAccessEntity_current_status");
                this.field_listAccessEntity_is_new_request = this.getFieldValue("field_listAccessEntity_is_new_request");
                this.field_listAccessEntity_button_approve = this.getFieldValue("field_listAccessEntity_button_approve");
                this.field_listAccessEntity_button_decline = this.getFieldValue("field_listAccessEntity_button_decline");
                break;

            case CustomGlobalConstants.COMPONENT_NAME.DASHBOARD:
                CustomLogger.logString("BaseComponent: Will fill fields for Dashboard Component");
                this.field_dashboard_heading_systems = this.getFieldValue("field_dashboard_heading_systems");
                this.field_dashboard_heading_networks = this.getFieldValue("field_dashboard_heading_networks");
                this.field_dashboard_heading_files = this.getFieldValue("field_dashboard_heading_files");
                this.field_dashboard_heading_applications = this.getFieldValue("field_dashboard_heading_applications");
                this.field_dashboard_heading_total_system_entities = this.getFieldValue("field_dashboard_heading_total_system_entities");
                this.field_dashboard_heading_total_network_entities = this.getFieldValue("field_dashboard_heading_total_network_entities");
                this.field_dashboard_heading_total_file_entities = this.getFieldValue("field_dashboard_heading_total_file_entities");
                this.field_dashboard_heading_total_application_entities = this.getFieldValue("field_dashboard_heading_total_application_entities");
                this.field_dashboard_heading_recent_activities = this.getFieldValue("field_dashboard_heading_recent_activities");
                this.field_dashboard_heading_activity_source = this.getFieldValue("field_dashboard_heading_activity_source");
                this.field_dashboard_heading_activity_type = this.getFieldValue("field_dashboard_heading_activity_type");
                this.field_dashboard_heading_who_initiated = this.getFieldValue("field_dashboard_heading_who_initiated");
                this.field_dashboard_heading_activity_time = this.getFieldValue("field_dashboard_heading_activity_time");
                this.field_dashboard_heading_created = this.getFieldValue("field_dashboard_heading_created");
                this.field_dashboard_heading_deleted = this.getFieldValue("field_dashboard_heading_deleted");
                this.field_dashboard_heading_users = this.getFieldValue("field_dashboard_heading_users");

                this.field_menu_heading_navigation = this.getFieldValue("field_menu_heading_navigation");
                this.field_menu_dashboard = this.getFieldValue("field_menu_dashboard");
                this.field_menu_standardDashboard = this.getFieldValue("field_menu_standardDashboard");
                this.field_menu_heading_users = this.getFieldValue("field_menu_heading_users");
                this.field_menu_link_listUser = this.getFieldValue("field_menu_link_listUser");
                this.field_menu_link_crudUser = this.getFieldValue("field_menu_link_crudUser");
                this.field_menu_link_listAccessEntity = this.getFieldValue("field_menu_link_listAccessEntity");
                this.field_menu_heading_sectors = this.getFieldValue("field_menu_heading_sectors");
                this.field_menu_link_listEntity = this.getFieldValue("field_menu_link_listEntity");
                this.field_menu_link_listSector = this.getFieldValue("field_menu_link_listSector");
                this.field_menu_link_listSectorElements = this.getFieldValue("field_menu_link_listSectorElements");
                this.field_menu_heading_configuration = this.getFieldValue("field_menu_heading_configuration");
                this.field_menu_link_listRole = this.getFieldValue("field_menu_link_listRole");
                this.field_menu_link_listPermission = this.getFieldValue("field_menu_link_listPermission");
                this.field_menu_link_listRule = this.getFieldValue("field_menu_link_listRule");
                this.field_menu_heading_settings = this.getFieldValue("field_menu_heading_settings");
                this.field_menu_link_editFields = this.getFieldValue("field_menu_link_editFields");
                this.field_menu_link_emailAlerts = this.getFieldValue("field_menu_link_emailAlerts");
                this.field_menu_link_editUserProfile = this.getFieldValue("field_menu_link_editUserProfile");
                this.field_menu_heading_reports = this.getFieldValue("field_menu_heading_reports");
                this.field_menu_link_report_default = this.getFieldValue("field_menu_link_report_default");
                this.field_menu_link_report_systemEvents = this.getFieldValue("field_menu_link_report_systemEvents");
                this.field_menu_link_report_entityEvents = this.getFieldValue("field_menu_link_report_entityEvents");
                this.field_menu_link_report_userLogging = this.getFieldValue("field_menu_link_report_userLogging");
                this.field_menu_link_report_existingUsers = this.getFieldValue("field_menu_link_report_existingUsers");
                this.field_menu_link_report_userActivityReports = this.getFieldValue("field_menu_link_report_userActivityReports");
                this.field_menu_heading_entityModule = this.getFieldValue("field_menu_heading_entityModule");
                this.field_menu_link_accessEntity = this.getFieldValue("field_menu_link_accessEntity");
                this.field_menu_link_showEntities = this.getFieldValue("field_menu_link_showEntities");
                this.field_menu_link_listAlertNotification = this.getFieldValue("field_menu_link_listAlertNotification");
                this.field_menu_link_defaultSetting = this.getFieldValue("field_menu_link_defaultSetting");
                this.field_menu_link_listPageMatrix = this.getFieldValue("field_menu_link_listPageMatrix");
                this.field_menu_link_listFormElement = this.getFieldValue("field_menu_link_listFormElement");
                this.field_menu_link_listManagerUser = this.getFieldValue("field_menu_link_listManagerUser");
                this.field_menu_link_listMakerCheckerRequest = this.getFieldValue("field_menu_link_listMakerCheckerRequest");
                this.field_menu_link_listApprovalMatrix = this.getFieldValue("field_menu_link_listApprovalMatrix");
                this.field_menu_hrDashboard = this.getFieldValue("field_menu_hrDashboard");
                this.field_menu_heading_connectors = this.getFieldValue("field_menu_heading_connectors");
                this.field_menu_link_listPredefinedConnectors = this.getFieldValue("field_menu_link_listPredefinedConnectors");
                this.field_menu_link_listAccessPolicy = this.getFieldValue("field_menu_link_listAccessPolicy");
                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_USER:
            case CustomGlobalConstants.COMPONENT_NAME.EDIT_USER_PROFILE:
                this.field_username = this.getFieldValue("username");
                this.field_user_type = this.getFieldValue("user_type");
                this.field_password = this.getFieldValue("password");
                this.field_email = this.getFieldValue("email");
                this.field_salutation = this.getFieldValue("salutation");
                this.field_first_name = this.getFieldValue("first_name");
                this.field_middle_name = this.getFieldValue("middle_name");
                this.field_last_name = this.getFieldValue("last_name");
                this.field_full_name = this.getFieldValue("full_name");
                this.field_facebook_id = this.getFieldValue("facebook_id");
                this.field_google_id = this.getFieldValue("google_id");
                this.field_phone = this.getFieldValue("phone");
                this.field_dob = this.getFieldValue("dob");
                this.field_profile_link = this.getFieldValue("profile_link");
                this.field_role_id = this.getFieldValue("role_id");
                this.field_is_otp_required = this.getFieldValue("is_otp_required");

                this.field_username_required = this.getFieldRequired("username");
                this.field_user_type_required = this.getFieldRequired("user_type");
                this.field_password_required = this.getFieldRequired("password");
                this.field_email_required = this.getFieldRequired("email");
                this.field_salutation_required = this.getFieldRequired("salutation");
                this.field_first_name_required = this.getFieldRequired("first_name");
                this.field_middle_name_required = this.getFieldRequired("middle_name");
                this.field_last_name_required = this.getFieldRequired("last_name");
                this.field_full_name_required = this.getFieldRequired("full_name");
                this.field_facebook_id_required = this.getFieldRequired("facebook_id");
                this.field_google_id_required = this.getFieldRequired("google_id");
                this.field_phone_required = this.getFieldRequired("phone");
                this.field_dob_required = this.getFieldRequired("dob");
                this.field_profile_link_required = this.getFieldRequired("profile_link");
                this.field_role_id_required = this.getFieldRequired("role_id");
                this.field_is_otp_required_required = this.getFieldRequired("is_otp_required");

                this.field_username_error_message = this.getFieldErrorMessage("username");
                this.field_user_type_error_message = this.getFieldErrorMessage("user_type");
                this.field_password_error_message = this.getFieldErrorMessage("password");
                this.field_email_error_message = this.getFieldErrorMessage("email");
                this.field_salutation_error_message = this.getFieldErrorMessage("salutation");
                this.field_first_name_error_message = this.getFieldErrorMessage("first_name");
                this.field_middle_name_error_message = this.getFieldErrorMessage("middle_name");
                this.field_last_name_error_message = this.getFieldErrorMessage("last_name");
                this.field_full_name_error_message = this.getFieldErrorMessage("full_name");
                this.field_facebook_id_error_message = this.getFieldErrorMessage("facebook_id");
                this.field_google_id_error_message = this.getFieldErrorMessage("google_id");
                this.field_phone_error_message = this.getFieldErrorMessage("phone");
                this.field_dob_error_message = this.getFieldErrorMessage("dob");
                this.field_profile_link_error_message = this.getFieldErrorMessage("profile_link");
                this.field_role_id_error_message = this.getFieldErrorMessage("role_id");
                this.field_is_otp_error_message_error_message = this.getFieldErrorMessage("is_otp_error_message");

                this.field_crudUser_add_new = this.getFieldValue("field_crudUser_add_new");
                this.field_crudUser_update = this.getFieldValue("field_crudUser_update");
                this.field_crudUser_error_user_type = this.getFieldValue("field_crudUser_error_user_type");
                this.field_crudUser_heading_user = this.getFieldValue("field_crudUser_heading_user");
                this.field_crudUser_heading_check_username = this.getFieldValue("field_crudUser_heading_check_username");

                //editUserProfile
                this.field_editUserProfile_email_error_message = this.getFieldErrorMessage("email");
                this.field_editUserProfile_salutation_error_message = this.getFieldErrorMessage("salutation");
                this.field_editUserProfile_first_name_error_message = this.getFieldErrorMessage("first_name");
                this.field_editUserProfile_middle_name_error_message = this.getFieldErrorMessage("middle_name");
                this.field_editUserProfile_last_name_error_message = this.getFieldErrorMessage("last_name");
                this.field_editUserProfile_full_name_error_message = this.getFieldErrorMessage("full_name");
                this.field_editUserProfile_facebook_id_error_message = this.getFieldErrorMessage("facebook_id");
                this.field_editUserProfile_google_id_error_message = this.getFieldErrorMessage("google_id");
                this.field_editUserProfile_phone_error_message = this.getFieldErrorMessage("phone");
                this.field_editUserProfile_date_of_birth_error_message = this.getFieldErrorMessage("dob");
                this.field_editUserProfile_profile_link_error_message = this.getFieldErrorMessage("profile_link");
                this.field_editUserProfile_heading_update_user = this.getFieldValue("field_editUserProfile_heading_update_user");

                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.LIST_USER:
                this.field_listUser_heading_user_management = this.getFieldValue("field_listUser_heading_user_management");
                this.field_listUser_heading_add_user = this.getFieldValue("field_listUser_heading_add_user");
                this.field_listUser_user_name = this.getFieldValue("field_listUser_user_name");
                this.field_listUser_email = this.getFieldValue("field_listUser_email");
                this.field_listUser_first_name = this.getFieldValue("field_listUser_first_name");
                this.field_listUser_last_name = this.getFieldValue("field_listUser_last_name");
                this.field_listUser_phone = this.getFieldValue("field_listUser_phone");
                this.field_listUser_button_edit = this.getFieldValue("field_listUser_button_edit");
                this.field_listUser_button_delete = this.getFieldValue("field_listUser_button_delete");
                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_PERMISSION:
                this.field_permission_id = this.getFieldValue("permission_id");
                this.field_permission_name = this.getFieldValue("permission_name");
                this.field_details = this.getFieldValue("details");
                this.field_remarks = this.getFieldValue("remarks");

                this.field_permission_id_required = this.getFieldRequired("permission_id");
                this.field_permission_name_required = this.getFieldRequired("permission_name");
                this.field_details_required = this.getFieldRequired("details");
                this.field_remarks_required = this.getFieldRequired("remarks");

                this.field_crudPermission_permission_name_error_message = this.getFieldErrorMessage("permission_name");
                this.field_crudPermission_details_error_message = this.getFieldErrorMessage("details");
                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_ENTITY:
                this.field_entity_id = this.getFieldValue("entity_id");
                this.field_entity_name = this.getFieldValue("entity_name");
                this.field_access_location = this.getFieldValue("access_location");
                this.field_icon_location = this.getFieldValue("icon_location");
                this.field_is_default = this.getFieldValue("is_default");
                this.field_details = this.getFieldValue("details");
                this.field_remarks = this.getFieldValue("remarks");
                this.field_crudEntity_risk_level = this.getFieldValue("field_crudEntity_risk_level");

                this.field_entity_id_required = this.getFieldRequired("entity_id");
                this.field_entity_name_required = this.getFieldRequired("entity_name");
                this.field_access_location_required = this.getFieldRequired("access_location");
                this.field_icon_location_required = this.getFieldRequired("icon_location");
                this.field_is_default_required = this.getFieldRequired("is_default");
                this.field_details_required = this.getFieldRequired("details");
                this.field_remarks_required = this.getFieldRequired("remarks");
                this.field_crudEntity_risk_level_required = this.getFieldRequired("risk_level_id");

                this.field_entity_name_error_message = this.getFieldErrorMessage("entity_name");
                this.field_access_location_error_message = this.getFieldErrorMessage("access_location");
                this.field_icon_location_error_message = this.getFieldErrorMessage("icon_location");
                this.field_crudEntity_details_error_message = this.getFieldErrorMessage("details");
                this.field_crudEntity_risk_level_error_message = this.getFieldErrorMessage("risk_level_id");
                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_REPORTING:
                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_ROLE:
                this.field_role_id = this.getFieldValue("role_id");
                this.field_role_name = this.getFieldValue("role_name");
                this.field_allowed_entity_id_arr = this.getFieldValue("allowed_entity_id_arr");
                this.field_details = this.getFieldValue("details");
                this.field_remarks = this.getFieldValue("remarks");

                this.field_role_id_required = this.getFieldRequired("role_id");
                this.field_role_name_required = this.getFieldRequired("role_name");
                this.field_allowed_entity_id_arr_required = this.getFieldRequired("allowed_entity_id_arr");
                this.field_details_required = this.getFieldRequired("details");
                this.field_remarks_required = this.getFieldRequired("remarks");

                this.field_crudRole_role_name_error_message = this.getFieldErrorMessage("role_name");
                this.field_crudRole_details_error_message = this.getFieldErrorMessage("details");
                this.field_crudRole_allowed_entity_id_arr_error_message = this.getFieldErrorMessage("allowed_entity_id_arr");

                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_RULE:
                this.field_rule_id = this.getFieldValue("rule_id");
                this.field_rule_name = this.getFieldValue("rule_name");
                this.field_sector_id_arr = this.getFieldValue("sector_id_arr");
                this.field_role_id_arr = this.getFieldValue("role_id_arr");
                this.field_entity_id_arr = this.getFieldValue("entity_id_arr");
                this.field_permission_id_arr = this.getFieldValue("permission_id_arr");
                this.field_details = this.getFieldValue("details");
                this.field_remarks = this.getFieldValue("remarks");
                this.field_crudRule_make_the_rule = this.getFieldValue("field_crudRule_make_the_rule");
                this.field_crudRule_role_specific = this.getFieldValue("field_crudRule_role_specific");
                this.field_crudRule_user_specific = this.getFieldValue("field_crudRule_user_specific");
                this.field_crudRule_can_be_deleted = this.getFieldValue("field_crudRule_can_be_deleted");

                this.field_rule_id_required = this.getFieldRequired("rule_id");
                this.field_rule_name_required = this.getFieldRequired("rule_name");
                this.field_sector_id_arr_required = this.getFieldRequired("sector_id_arr");
                this.field_role_id_arr_required = this.getFieldRequired("role_id_arr");
                this.field_entity_id_arr_required = this.getFieldRequired("entity_id_arr");
                this.field_permission_id_arr_required = this.getFieldRequired("permission_id_arr");
                this.field_details_required = this.getFieldRequired("details");
                this.field_remarks_required = this.getFieldRequired("remarks");

                this.field_crudRule_rule_name_error_message = this.getFieldErrorMessage("rule_name");
                this.field_crudRule_details_error_message = this.getFieldErrorMessage("details");

                this.flagAreFieldsLoaded = true;
                break;

            case CustomGlobalConstants.COMPONENT_NAME.CRUD_SECTOR:
                this.field_sector_id = this.getFieldValue("sector_id");
                this.field_sector_name = this.getFieldValue("sector_name");
                this.field_sector_type = this.getFieldValue("sector_type");
                this.field_sector_icon_location = this.getFieldValue("sector_icon_location");
                this.field_entity_id_arr = this.getFieldValue("entity_id_arr");
                this.field_details = this.getFieldValue("details");
                this.field_remarks = this.getFieldValue("remarks");

                this.field_sector_id_required = this.getFieldRequired("sector_id");
                this.field_sector_name_required = this.getFieldRequired("sector_name");
                this.field_sector_type_required = this.getFieldRequired("sector_type");
                this.field_sector_icon_location_required = this.getFieldRequired("sector_icon_location");
                this.field_entity_id_arr_required = this.getFieldRequired("entity_id_arr");
                this.field_details_required = this.getFieldRequired("details");
                this.field_remarks_required = this.getFieldRequired("remarks");

                this.field_crudSector_sector_name_error_message = this.getFieldErrorMessage("sector_name");
                this.field_crudSector_entity_id_arr_error_message = this.getFieldErrorMessage("entity_id_arr");
                this.field_crudSector_details_error_message = this.getFieldErrorMessage("details");


                this.flagAreFieldsLoaded = true;
                break;

        }

    }


    //////////////////////////////////
    //common field names
    field_button_save: string = "";
    field_button_reset: string = "";
    field_button_cancel: string = "";
    field_button_update: string = "";
    field_can_be_deleted: string = "";

    fillCommonFieldNames() {
        this.field_button_save = this.getFieldValue("button_save");
        this.field_button_update = this.getFieldValue("button_update");
        this.field_button_reset = this.getFieldValue("button_reset");
        this.field_button_cancel = this.getFieldValue("button_cancel");
        this.field_can_be_deleted = this.getFieldValue("can_be_deleted");
    }

    //////////////////////////////////

    COMPONENT_NAME: string = "";
    protected _dbService: DBService;
    protected _miscService: MiscService;
    protected _globalService: GlobalService;

    //get field names from database
    fieldDetailMap = new Map<string, string>();
    fieldRequiredMap = new Map<string, boolean>();
    fieldErrorMessageMap = new Map<string, string>();

    constructor(componentName, injector: Injector) {
        super();
        this._dbService = injector.get(DBService);
        this._miscService = injector.get(MiscService);
        this._globalService = injector.get(GlobalService);
        this.COMPONENT_NAME = componentName;
    }


    async populateFields() {
        CustomLogger.logStringWithObject("Will load map for component:::", this.COMPONENT_NAME);
        //populate fields
        let result = await this._dbService.getFieldDetailsForComponent(this.COMPONENT_NAME).toPromise();
        CustomLogger.logStringWithObject("getFieldDetailsForComponent:RESULT:::", result);
        let fieldDetailArr: FieldDetail[] = result["data"];
        fieldDetailArr.forEach(fieldDetail => {
            this.fieldDetailMap.set(fieldDetail.field_name, fieldDetail.field_value);
            this.fieldRequiredMap.set(fieldDetail.field_name, fieldDetail.is_required);
            this.fieldErrorMessageMap.set(fieldDetail.field_name, fieldDetail.error_message);
        });
        CustomLogger.logStringWithObject("Filled Field Map:", this.fieldDetailMap);

        this.fillCommonFieldNames();
        this.initializeHtmlFields();
    }

    getFieldValue(field_name): string {
        return this.fieldDetailMap.get(field_name);
    }

    getFieldRequired(field_name): boolean {
        return this.fieldRequiredMap.get(field_name);
    }

    getFieldErrorMessage(field_name): string {
        return this.fieldErrorMessageMap.get(field_name);
    }
}