db = db.getSiblingDB('iamdb');
db.user_detail.drop();
db.user_detail.insertMany(
    [
        {
            "user_id": "1",
            "is_deleted": false,
            "is_otp_required": false,
            "can_be_deleted": false,
            "user_type": "admin",
            "username": "admin",
            "password": "sha1$5e8cd744$1$cd4b39b47f40d326bf5f15481bdd22f32a404a62",
            "email": "admin@ogoul.com",
            "first_name": "admin",
            "middle_name": "A",
            "last_name": "admin",
            "phone": 1234567890,
            "role_id": "1",
            "is_default": true,
            "external_authentication_application": NumberInt(2),
            "is_active": true,
            "parent_user_id": "-1",
            "creator_user_id": "-1",
            "model_name": NumberInt(15),
            "page_matrix_id": "1",
            "is_added_to_main": true
        }
    ]
);
db.entity_detail.drop();
db.entity_detail.insertMany([
    {
        "can_be_deleted": false,
        "entity_name": "Office 365",
        "access_location": "192.8.8.1/office365",
        "icon_location": "some_location",
        "icon_filename": "",
        "details": "Access Office ",
        "entity_id": "1",
        "model_name": NumberInt(22),
        "__v": 0,
        "is_default": true,
        "risk_level_id": "Normal"
    },

]);
db.role_detail.drop();
db.role_detail.insertMany([
    {
        "can_be_deleted": false,
        "is_default": true,
        "allowed_entity_id_arr": [
        ],
        "role_name": "Admin Role",
        "details": "Admin related activities",
        "username": "admin",
        "role_id": "1",
        "model_name": NumberInt(24)
    },

]);
db.sector_detail.drop();
db.sector_detail.insertMany([
    {
        "entity_id_arr": ["1"],
        "can_be_deleted": false,
        "sector_name": "Applications",
        "details": "Default Applications Sector",
        "sector_id": "1",
        "is_default": true,
        "model_name": NumberInt(26)
    },
    {
        "entity_id_arr": [],
        "can_be_deleted": false,
        "sector_name": "Networks",
        "details": "Default Networks Sector",
        "sector_id": "2",
        "is_default": true,
        "model_name": NumberInt(26)
    },
    {
        "entity_id_arr": [],
        "can_be_deleted": false,
        "sector_name": "Systems",
        "details": "Default Systems Sector",
        "sector_id": "3",
        "is_default": true,
        "model_name": NumberInt(26)
    },
    {
        "entity_id_arr": [],
        "can_be_deleted": false,
        "sector_name": "Files",
        "details": "Default Files Sector",
        "sector_id": "4",
        "is_default": true,
        "model_name": NumberInt(26)
    }
]);
db.permission_detail.drop();
db.permission_detail.insert(
    {
        "can_be_deleted": false,
        "permission_name": "NONE",
        "details": "No permission",
        "permission_id": "1",
        "is_default": true,
        "model_name": NumberInt(23)
    }
);

db.page_matrix.drop();
db.page_matrix.insertMany([
    {
        "can_be_deleted": true,
        "is_default": false,
        "associated_user_id": "-1",
        "created_time": 1587644586894.0,
        "last_modified_time": 1587644586894.0,
        "show_field_menu_heading_navigation": true,
        "show_field_menu_link_listUser": true,
        "permission_listUser": {
            "_id": ObjectId("5ea188bc2e14f311c446f5cf"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listAccessEntity": true,
        "permission_listAccessEntity": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d0"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listEntity": true,
        "permission_listEntity": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d1"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listSector": true,
        "permission_listSector": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d2"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listSectorElements": true,
        "permission_listSectorElements": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d3"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listRole": true,
        "permission_listRole": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d4"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listPermission": true,
        "permission_listPermission": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d5"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listRule": true,
        "permission_listRule": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d6"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listPageMatrix": true,
        "permission_listPageMatrix": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d7"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listFormElement": true,
        "permission_listFormElement": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d8"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_editFields": true,
        "permission_editFields": {
            "_id": ObjectId("5ea188bc2e14f311c446f5d9"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_emailAlerts": true,
        "permission_emailAlerts": {
            "_id": ObjectId("5ea188bc2e14f311c446f5da"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_editUserProfile": true,
        "permission_editUserProfile": {
            "_id": ObjectId("5ea188bc2e14f311c446f5db"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listAlertNotification": true,
        "permission_listAlertNotification": {
            "_id": ObjectId("5ea188bc2e14f311c446f5dc"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_defaultSetting": true,
        "permission_defaultSetting": {
            "_id": ObjectId("5ea188bc2e14f311c446f5dd"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_default": true,
        "permission_report_default": {
            "_id": ObjectId("5ea188bc2e14f311c446f5de"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_entityEvents": true,
        "permission_report_entityEvents": {
            "_id": ObjectId("5ea188bc2e14f311c446f5df"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_systemEvents": true,
        "permission_report_systemEvents": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e0"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_existingUsers": true,
        "permission_report_existingUsers": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e1"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_userLogging": true,
        "permission_report_userLogging": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e2"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_userActivityReports": true,
        "permission_report_userActivityReports": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e3"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listManagerUser": true,
        "permission_listManagerUser": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e4"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listMakerCheckerRequest": true,
        "permission_listMakerCheckerRequest": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e5"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listApprovalMatrix": true,
        "permission_listApprovalMatrix": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e6"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listPredefinedConnectors": true,
        "permission_listPredefinedConnectors": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e7"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listAccessPolicy": true,
        "permission_listAccessPolicy": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e8"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_standard_menu_heading_navigation": false,
        "show_field_standard_menu_link_showEntities": false,
        "permission_standard_showEntities": {
            "_id": ObjectId("5ea188bc2e14f311c446f5e9"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_standard_menu_link_editUserProfile": false,
        "permission_standard_editUserProfile": {
            "_id": ObjectId("5ea188bc2e14f311c446f5ea"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_hr_menu_heading_navigation": false,
        "model_name": 13,
        "page_matrix_name": "Admin Matrix",
        "details": "Admin Users",
        "source_username": "admin",
        "page_matrix_id": "1",
        "can_be_deleted": false
    },
    
    {
        "_id": ObjectId("5ea18ad82e14f311c446f7c5"),
        "can_be_deleted": true,
        "is_default": false,
        "associated_user_id": "-1",
        "created_time": 1587645036130.0,
        "last_modified_time": 1587645036130.0,
        "show_field_menu_heading_navigation": true,
        "show_field_menu_link_listUser": true,
        "permission_listUser": {
            "_id": ObjectId("5ea18ad82e14f311c446f7c6"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listAccessEntity": false,
        "permission_listAccessEntity": {
            "_id": ObjectId("5ea18ad82e14f311c446f7c7"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listEntity": true,
        "permission_listEntity": {
            "_id": ObjectId("5ea18ad82e14f311c446f7c8"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listSector": true,
        "permission_listSector": {
            "_id": ObjectId("5ea18ad82e14f311c446f7c9"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listSectorElements": true,
        "permission_listSectorElements": {
            "_id": ObjectId("5ea18ad82e14f311c446f7ca"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_listRole": true,
        "permission_listRole": {
            "_id": ObjectId("5ea18ad82e14f311c446f7cb"),
            "create": true,
            "read": true,
            "update": true,
            "delete": false
        },
        "show_field_menu_link_listPermission": false,
        "permission_listPermission": {
            "_id": ObjectId("5ea18ad82e14f311c446f7cc"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listRule": false,
        "permission_listRule": {
            "_id": ObjectId("5ea18ad82e14f311c446f7cd"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPageMatrix": false,
        "permission_listPageMatrix": {
            "_id": ObjectId("5ea18ad82e14f311c446f7ce"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listFormElement": false,
        "permission_listFormElement": {
            "_id": ObjectId("5ea18ad82e14f311c446f7cf"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editFields": false,
        "permission_editFields": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d0"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_emailAlerts": false,
        "permission_emailAlerts": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d1"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editUserProfile": true,
        "permission_editUserProfile": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d2"),
            "create": true,
            "read": true,
            "update": true,
            "delete": false
        },
        "show_field_menu_link_listAlertNotification": false,
        "permission_listAlertNotification": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d3"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_defaultSetting": false,
        "permission_defaultSetting": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d4"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_default": false,
        "permission_report_default": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d5"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_entityEvents": true,
        "permission_report_entityEvents": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d6"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_systemEvents": true,
        "permission_report_systemEvents": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d7"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_existingUsers": true,
        "permission_report_existingUsers": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d8"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_userLogging": true,
        "permission_report_userLogging": {
            "_id": ObjectId("5ea18ad82e14f311c446f7d9"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_menu_link_report_userActivityReports": false,
        "permission_report_userActivityReports": {
            "_id": ObjectId("5ea18ad82e14f311c446f7da"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listManagerUser": false,
        "permission_listManagerUser": {
            "_id": ObjectId("5ea18ad82e14f311c446f7db"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listMakerCheckerRequest": false,
        "permission_listMakerCheckerRequest": {
            "_id": ObjectId("5ea18ad82e14f311c446f7dc"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listApprovalMatrix": false,
        "permission_listApprovalMatrix": {
            "_id": ObjectId("5ea18ad82e14f311c446f7dd"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPredefinedConnectors": false,
        "permission_listPredefinedConnectors": {
            "_id": ObjectId("5ea18ad82e14f311c446f7de"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAccessPolicy": false,
        "permission_listAccessPolicy": {
            "_id": ObjectId("5ea18ad82e14f311c446f7df"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_standard_menu_heading_navigation": false,
        "show_field_standard_menu_link_showEntities": true,
        "permission_standard_showEntities": {
            "_id": ObjectId("5ea18ad82e14f311c446f7e0"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_standard_menu_link_editUserProfile": false,
        "permission_standard_editUserProfile": {
            "_id": ObjectId("5ea18ad82e14f311c446f7e1"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_hr_menu_heading_navigation": false,
        "model_name": 13,
        "page_matrix_name": "Manager Matrix",
        "details": "Manager Users",
        "source_username": "admin",
        "page_matrix_id": "2",
        "can_be_deleted": false
    },
    {
        "_id": ObjectId("5ea18b0a2e14f311c446f840"),
        "can_be_deleted": true,
        "is_default": false,
        "associated_user_id": "-1",
        "created_time": 1587645147792.0,
        "last_modified_time": 1587645147792.0,
        "show_field_menu_heading_navigation": false,
        "show_field_menu_link_listUser": false,
        "permission_listUser": {
            "_id": ObjectId("5ea18b0a2e14f311c446f841"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAccessEntity": false,
        "permission_listAccessEntity": {
            "_id": ObjectId("5ea18b0a2e14f311c446f842"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listEntity": false,
        "permission_listEntity": {
            "_id": ObjectId("5ea18b0a2e14f311c446f843"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listSector": false,
        "permission_listSector": {
            "_id": ObjectId("5ea18b0a2e14f311c446f844"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listSectorElements": false,
        "permission_listSectorElements": {
            "_id": ObjectId("5ea18b0a2e14f311c446f845"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listRole": false,
        "permission_listRole": {
            "_id": ObjectId("5ea18b0a2e14f311c446f846"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPermission": false,
        "permission_listPermission": {
            "_id": ObjectId("5ea18b0a2e14f311c446f847"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listRule": false,
        "permission_listRule": {
            "_id": ObjectId("5ea18b0a2e14f311c446f848"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPageMatrix": false,
        "permission_listPageMatrix": {
            "_id": ObjectId("5ea18b0a2e14f311c446f849"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listFormElement": false,
        "permission_listFormElement": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84a"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editFields": false,
        "permission_editFields": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84b"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_emailAlerts": false,
        "permission_emailAlerts": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84c"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editUserProfile": false,
        "permission_editUserProfile": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84d"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAlertNotification": false,
        "permission_listAlertNotification": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84e"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_defaultSetting": false,
        "permission_defaultSetting": {
            "_id": ObjectId("5ea18b0a2e14f311c446f84f"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_default": false,
        "permission_report_default": {
            "_id": ObjectId("5ea18b0a2e14f311c446f850"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_entityEvents": false,
        "permission_report_entityEvents": {
            "_id": ObjectId("5ea18b0a2e14f311c446f851"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_systemEvents": false,
        "permission_report_systemEvents": {
            "_id": ObjectId("5ea18b0a2e14f311c446f852"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_existingUsers": false,
        "permission_report_existingUsers": {
            "_id": ObjectId("5ea18b0a2e14f311c446f853"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_userLogging": false,
        "permission_report_userLogging": {
            "_id": ObjectId("5ea18b0a2e14f311c446f854"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_userActivityReports": false,
        "permission_report_userActivityReports": {
            "_id": ObjectId("5ea18b0a2e14f311c446f855"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listManagerUser": false,
        "permission_listManagerUser": {
            "_id": ObjectId("5ea18b0a2e14f311c446f856"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listMakerCheckerRequest": false,
        "permission_listMakerCheckerRequest": {
            "_id": ObjectId("5ea18b0a2e14f311c446f857"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listApprovalMatrix": false,
        "permission_listApprovalMatrix": {
            "_id": ObjectId("5ea18b0a2e14f311c446f858"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPredefinedConnectors": false,
        "permission_listPredefinedConnectors": {
            "_id": ObjectId("5ea18b0a2e14f311c446f859"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAccessPolicy": false,
        "permission_listAccessPolicy": {
            "_id": ObjectId("5ea18b0a2e14f311c446f85a"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_standard_menu_heading_navigation": true,
        "show_field_standard_menu_link_showEntities": true,
        "permission_standard_showEntities": {
            "_id": ObjectId("5ea18b0a2e14f311c446f85b"),
            "create": true,
            "read": true,
            "update": true,
            "delete": true
        },
        "show_field_standard_menu_link_editUserProfile": true,
        "permission_standard_editUserProfile": {
            "_id": ObjectId("5ea18b0a2e14f311c446f85c"),
            "create": false,
            "read": true,
            "update": true,
            "delete": false
        },
        "show_field_hr_menu_heading_navigation": false,
        "model_name": 13,
        "page_matrix_name": "Standard Matrix",
        "details": "Standard (Default)",
        "source_username": "admin",
        "page_matrix_id": "3",
        "can_be_deleted": false
    },
    {
        "can_be_deleted": true,
        "is_default": false,
        "associated_user_id": "-1",
        "created_time": 1587644988271.0,
        "last_modified_time": 1587644988271.0,
        "show_field_menu_heading_navigation": false,
        "show_field_menu_link_listUser": true,
        "permission_listUser": {
            "_id": ObjectId("5ea18a662e14f311c446f767"),
            "create": true,
            "read": true,
            "update": true,
            "delete": false
        },
        "show_field_menu_link_listAccessEntity": false,
        "permission_listAccessEntity": {
            "_id": ObjectId("5ea18a662e14f311c446f768"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listEntity": false,
        "permission_listEntity": {
            "_id": ObjectId("5ea18a662e14f311c446f769"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listSector": false,
        "permission_listSector": {
            "_id": ObjectId("5ea18a662e14f311c446f76a"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listSectorElements": false,
        "permission_listSectorElements": {
            "_id": ObjectId("5ea18a662e14f311c446f76b"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listRole": false,
        "permission_listRole": {
            "_id": ObjectId("5ea18a662e14f311c446f76c"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPermission": false,
        "permission_listPermission": {
            "_id": ObjectId("5ea18a662e14f311c446f76d"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listRule": false,
        "permission_listRule": {
            "_id": ObjectId("5ea18a662e14f311c446f76e"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPageMatrix": false,
        "permission_listPageMatrix": {
            "_id": ObjectId("5ea18a662e14f311c446f76f"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listFormElement": false,
        "permission_listFormElement": {
            "_id": ObjectId("5ea18a662e14f311c446f770"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editFields": false,
        "permission_editFields": {
            "_id": ObjectId("5ea18a662e14f311c446f771"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_emailAlerts": false,
        "permission_emailAlerts": {
            "_id": ObjectId("5ea18a662e14f311c446f772"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_editUserProfile": false,
        "permission_editUserProfile": {
            "_id": ObjectId("5ea18a662e14f311c446f773"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAlertNotification": false,
        "permission_listAlertNotification": {
            "_id": ObjectId("5ea18a662e14f311c446f774"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_defaultSetting": false,
        "permission_defaultSetting": {
            "_id": ObjectId("5ea18a662e14f311c446f775"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_default": false,
        "permission_report_default": {
            "_id": ObjectId("5ea18a662e14f311c446f776"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_entityEvents": false,
        "permission_report_entityEvents": {
            "_id": ObjectId("5ea18a662e14f311c446f777"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_systemEvents": false,
        "permission_report_systemEvents": {
            "_id": ObjectId("5ea18a662e14f311c446f778"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_existingUsers": false,
        "permission_report_existingUsers": {
            "_id": ObjectId("5ea18a662e14f311c446f779"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_userLogging": false,
        "permission_report_userLogging": {
            "_id": ObjectId("5ea18a662e14f311c446f77a"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_report_userActivityReports": false,
        "permission_report_userActivityReports": {
            "_id": ObjectId("5ea18a662e14f311c446f77b"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listManagerUser": false,
        "permission_listManagerUser": {
            "_id": ObjectId("5ea18a662e14f311c446f77c"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listMakerCheckerRequest": false,
        "permission_listMakerCheckerRequest": {
            "_id": ObjectId("5ea18a662e14f311c446f77d"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listApprovalMatrix": false,
        "permission_listApprovalMatrix": {
            "_id": ObjectId("5ea18a662e14f311c446f77e"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listPredefinedConnectors": false,
        "permission_listPredefinedConnectors": {
            "_id": ObjectId("5ea18a662e14f311c446f77f"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_menu_link_listAccessPolicy": false,
        "permission_listAccessPolicy": {
            "_id": ObjectId("5ea18a662e14f311c446f780"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_standard_menu_heading_navigation": false,
        "show_field_standard_menu_link_showEntities": false,
        "permission_standard_showEntities": {
            "_id": ObjectId("5ea18a662e14f311c446f781"),
            "create": false,
            "read": false,
            "update": false,
            "delete": false
        },
        "show_field_standard_menu_link_editUserProfile": true,
        "permission_standard_editUserProfile": {
            "_id": ObjectId("5ea18a662e14f311c446f782"),
            "create": true,
            "read": true,
            "update": true,
            "delete": false
        },
        "show_field_hr_menu_heading_navigation": true,
        "model_name": 13,
        "page_matrix_name": "HR Matrix",
        "details": "HR Users",
        "source_username": "admin",
        "page_matrix_id": "4",
        "can_be_deleted": false
    },

]);
db.form_element.drop();
db.form_element.insertMany(
    [
        {
            "form_element_id": "1",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_salutation",
            "data_name": "Mister",
            "data_value": "Mr.",
            "model_name": "10"
        },
        {
            "form_element_id": "2",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_salutation",
            "data_name": "Miss",
            "data_value": "Ms.",
            "model_name": "10"
        },
        {
            "form_element_id": "3",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_salutation",
            "data_name": "Misses",
            "data_value": "Mrs.",
            "model_name": "10"
        },
        {
            "form_element_id": "4",
            "form_element_type": "Combo Box",
            "form_element_name": "editProfile_salutation",
            "data_name": "Mister",
            "data_value": "Mr.",
            "model_name": "10"
        },
        {
            "form_element_id": "5",
            "form_element_type": "Combo Box",
            "form_element_name": "editProfile_salutation",
            "data_name": "Miss",
            "data_value": "Ms.",
            "model_name": "10"
        },
        {
            "form_element_id": "6",
            "form_element_type": "Combo Box",
            "form_element_name": "editProfile_salutation",
            "data_name": "Misses",
            "data_value": "Mrs.",
            "model_name": "10"
        },
        {
            "form_element_id": "7",
            "form_element_type": "Combo Box",
            "form_element_name": "crudEntity_risk_level",
            "data_name": "Normal",
            "data_value": "Normal",
            "model_name": "10"
        },
        {
            "form_element_id": "8",
            "form_element_type": "Combo Box",
            "form_element_name": "crudEntity_risk_level",
            "data_name": "Critical",
            "data_value": "Critical",
            "model_name": "10"
        },
        {
            "form_element_id": "9",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_department",
            "data_name": "Engineering",
            "data_value": "Engineering",
            "model_name": "10"
        },
        {
            "form_element_id": "10",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_department",
            "data_name": "Marketing",
            "data_value": "Marketing",
            "model_name": "10"
        },
        {
            "form_element_id": "11",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_department",
            "data_name": "HR",
            "data_value": "HR",
            "model_name": "10"
        },
        {
            "form_element_id": "12",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_department",
            "data_name": "Sales",
            "data_value": "Sales",
            "model_name": "10"
        },
        {
            "form_element_id": "13",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_department",
            "data_name": "IT",
            "data_value": "IT",
            "model_name": "10"
        },
        {
            "form_element_id": "14",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "CEO",
            "data_value": "CEO",
            "model_name": "10"
        },
        {
            "form_element_id": "15",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "CTO",
            "data_value": "CTO",
            "model_name": "10"
        },
        {
            "form_element_id": "16",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Technical Head",
            "data_value": "Technical Head",
            "model_name": "10"
        },
        {
            "form_element_id": "17",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Program Manager",
            "data_value": "Program Manager",
            "model_name": "10"
        },
        {
            "form_element_id": "18",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Fresher",
            "data_value": "Fresher",
            "model_name": "10"
        },
        {
            "form_element_id": "19",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Intern",
            "data_value": "Intern",
            "model_name": "10"
        },
        {
            "form_element_id": "20",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Marketing Manager",
            "data_value": "Marketing Manager",
            "model_name": "10"
        },
        {
            "form_element_id": "21",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Project Manager",
            "data_value": "Project Manager",
            "model_name": "10"
        },
        {
            "form_element_id": "22",
            "form_element_type": "Combo Box",
            "form_element_name": "crudUser_job_title",
            "data_name": "Web Developer",
            "data_value": "Web Developer",
            "model_name": "10"
        },
    ]
);
db.email_alert.drop();
db.email_alert.insert(
    {
        "should_send_email": "true",
        "alert_level_all": "true",
        "alert_level_critical": "false",
        "alert_level_high": "false",
        "alert_level_medium": "false",
        "alert_level_low": "false",
        "alert_level": 2,
        "model_name": 7
    }
);
db.default_setting.drop();
db.default_setting.insert(
    {
        "session_timeout": 5000,
        "email_service": "gmail",
        "email_default_from_name": "Ogoul",
        "email_address": "manish@ogoul.com",
        "email_username": "admin",
        "email_password": "admin",
        "model_name": 6
    }
);
db.approval_matrix.drop();
db.approval_matrix.insertMany([
    {
        "approval_matrix_id": "1",
        "user_type": "manager",
        "component_name": "crudUser",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "2",
        "user_type": "manager",
        "component_name": "listUser",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "3",
        "user_type": "manager",
        "component_name": "crudSector",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "4",
        "user_type": "manager",
        "component_name": "crudEntity",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "5",
        "user_type": "manager",
        "component_name": "crudPermission",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "6",
        "user_type": "manager",
        "component_name": "crudRole",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "7",
        "user_type": "manager",
        "component_name": "crudReporting",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "8",
        "user_type": "manager",
        "component_name": "crudRule",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "9",
        "user_type": "manager",
        "component_name": "editUserProfile",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "10",
        "user_type": "manager",
        "component_name": "dashboard",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "11",
        "user_type": "manager",
        "component_name": "listAccessEntity",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "12",
        "user_type": "manager",
        "component_name": "listEntity",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "13",
        "user_type": "manager",
        "component_name": "listSector",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "14",
        "user_type": "manager",
        "component_name": "listRole",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "15",
        "user_type": "manager",
        "component_name": "listPermission",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "16",
        "user_type": "manager",
        "component_name": "listRule",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "17",
        "user_type": "manager",
        "component_name": "editFields",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "18",
        "user_type": "manager",
        "component_name": "emailAlert",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "19",
        "user_type": "manager",
        "component_name": "alertNotification",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "20",
        "user_type": "manager",
        "component_name": "listPageMatrix",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "21",
        "user_type": "manager",
        "component_name": "crudPageMatrix",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "22",
        "user_type": "manager",
        "component_name": "listFormElement",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "23",
        "user_type": "manager",
        "component_name": "crudFormElement",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "24",
        "user_type": "manager",
        "component_name": "listManagerUser",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "25",
        "user_type": "manager",
        "component_name": "crudManagerUser",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    },
    {
        "approval_matrix_id": "26",
        "user_type": "manager",
        "component_name": "common",
        "requires_approval": "true",
        "create_permission": "true",
        "update_permission": "true",
        "delete_permission": "true",
        "model_name": 3,
        "details": " "
    }
]);

db.field_detail.drop();
db.field_detail.insertMany([
    {
        "field_id": "1",
        "field_language": "en",
        "field_name": "username",
        "field_value": "User Name",
        "field_details": "User Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "2",
        "field_language": "en",
        "field_name": "user_type",
        "field_value": "User Type",
        "field_details": "User Type",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "3",
        "field_language": "en",
        "field_name": "password",
        "field_value": "password",
        "field_details": "password",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "4",
        "field_language": "en",
        "field_name": "email",
        "field_value": "email",
        "field_details": "email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "5",
        "field_language": "en",
        "field_name": "salutation",
        "field_value": "salutation",
        "field_details": "salutation",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "6",
        "field_language": "en",
        "field_name": "first_name",
        "field_value": "First Name",
        "field_details": "First Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "7",
        "field_language": "en",
        "field_name": "middle_name",
        "field_value": "Middle Name",
        "field_details": "Middle Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "8",
        "field_language": "en",
        "field_name": "last_name",
        "field_value": "Last Name",
        "field_details": "Last Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "9",
        "field_language": "en",
        "field_name": "full_name",
        "field_value": "Full Name",
        "field_details": "Full Name",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "10",
        "field_language": "en",
        "field_name": "facebook_id",
        "field_value": "Facebook ID",
        "field_details": "Facebook ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "11",
        "field_language": "en",
        "field_name": "google_id",
        "field_value": "Google ID",
        "field_details": "Google ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "12",
        "field_language": "en",
        "field_name": "phone",
        "field_value": "Phone",
        "field_details": "Phone",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "13",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Is Deletable",
        "field_details": "Is the field deletable",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "14",
        "field_language": "en",
        "field_name": "dob",
        "field_value": "DOB",
        "field_details": "Date Of Birth",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "15",
        "field_language": "en",
        "field_name": "profile_link",
        "field_value": "Profile Link",
        "field_details": "Profile Link",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "16",
        "field_language": "en",
        "field_name": "role_id",
        "field_value": "Role ID",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "17",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "18",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "19",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "20",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "21",
        "field_language": "en",
        "field_name": "is_otp_required",
        "field_value": "Is OTP Required",
        "field_details": "Is OTP Required",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "22",
        "field_language": "en",
        "field_name": "entity_id",
        "field_value": "Entity ID",
        "field_details": "Entity ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "23",
        "field_language": "en",
        "field_name": "entity_name",
        "field_value": "Entity Name",
        "field_details": "Entity Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "24",
        "field_language": "en",
        "field_name": "access_location",
        "field_value": "Access Location",
        "field_details": "Access Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "25",
        "field_language": "en",
        "field_name": "icon_location",
        "field_value": "Icon Location",
        "field_details": "Icon Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "26",
        "field_language": "en",
        "field_name": "is_default",
        "field_value": "Is Default",
        "field_details": "Is Default",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "27",
        "field_language": "en",
        "field_name": "details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "28",
        "field_language": "en",
        "field_name": "remarks",
        "field_value": "Remarks",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "29",
        "field_language": "en",
        "field_name": "sector_id",
        "field_value": "sector_id",
        "field_details": "Sector Id",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "30",
        "field_language": "en",
        "field_name": "sector_name",
        "field_value": "Sector Name",
        "field_details": "Sector Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "31",
        "field_language": "en",
        "field_name": "sector_type",
        "field_value": "Sector Type",
        "field_details": "Sector Type",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "32",
        "field_language": "en",
        "field_name": "sector_icon_location",
        "field_value": "Sector Icon Location",
        "field_details": "Sector Icon Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "33",
        "field_language": "en",
        "field_name": "entity_id_arr",
        "field_value": "Entity Ids",
        "field_details": "Entity Ids",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "34",
        "field_language": "en",
        "field_name": "details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "35",
        "field_language": "en",
        "field_name": "remarks",
        "field_value": "Remarks",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "36",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "37",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "38",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "39",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "40",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "41",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "42",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "43",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "44",
        "field_language": "en",
        "field_name": "permission_id",
        "field_value": "Permission ID",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "45",
        "field_language": "en",
        "field_name": "permission_name",
        "field_value": "Permission Name",
        "field_details": "Permission Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "46",
        "field_language": "en",
        "field_name": "details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "47",
        "field_language": "en",
        "field_name": "remarks",
        "field_value": "Remarks",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "48",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "49",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "50",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "51",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "52",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "53",
        "field_language": "en",
        "field_name": "role_id",
        "field_value": "Role ID",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "54",
        "field_language": "en",
        "field_name": "role_name",
        "field_value": "Role Name",
        "field_details": "Role Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "55",
        "field_language": "en",
        "field_name": "allowed_entity_id_arr",
        "field_value": "Allowed Entities",
        "field_details": "Allowed Entities",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "56",
        "field_language": "en",
        "field_name": "details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "57",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "58",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "59",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "60",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "61",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "Error"
    },
    {
        "field_id": "62",
        "field_language": "en",
        "field_name": "rule_id",
        "field_value": "Rule ID",
        "field_details": "Rule ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "63",
        "field_language": "en",
        "field_name": "rule_name",
        "field_value": "Rule Name",
        "field_details": "Rule Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "64",
        "field_language": "en",
        "field_name": "sector_id_arr",
        "field_value": "Sectors",
        "field_details": "Sector ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "65",
        "field_language": "en",
        "field_name": "role_id_arr",
        "field_value": "Roles",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "66",
        "field_language": "en",
        "field_name": "entity_id_arr",
        "field_value": "Entities",
        "field_details": "Entity ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "67",
        "field_language": "en",
        "field_name": "permission_id_arr",
        "field_value": "Permissions",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "68",
        "field_language": "en",
        "field_name": "details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "69",
        "field_language": "en",
        "field_name": "remarks",
        "field_value": "Remarks",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "70",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "71",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "72",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "73",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "74",
        "field_language": "en",
        "field_name": "email",
        "field_value": "email",
        "field_details": "email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "75",
        "field_language": "en",
        "field_name": "salutation",
        "field_value": "salutation",
        "field_details": "salutation",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "76",
        "field_language": "en",
        "field_name": "first_name",
        "field_value": "First Name",
        "field_details": "First Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "77",
        "field_language": "en",
        "field_name": "middle_name",
        "field_value": "Middle Name",
        "field_details": "Middle Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "78",
        "field_language": "en",
        "field_name": "last_name",
        "field_value": "Last Name",
        "field_details": "Last Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "79",
        "field_language": "en",
        "field_name": "full_name",
        "field_value": "Full Name",
        "field_details": "Full Name",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "80",
        "field_language": "en",
        "field_name": "facebook_id",
        "field_value": "Facebook ID",
        "field_details": "Facebook ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "81",
        "field_language": "en",
        "field_name": "google_id",
        "field_value": "Google ID",
        "field_details": "Google ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "82",
        "field_language": "en",
        "field_name": "phone",
        "field_value": "Phone",
        "field_details": "Phone",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "83",
        "field_language": "en",
        "field_name": "dob",
        "field_value": "DOB",
        "field_details": "Date Of Birth",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "84",
        "field_language": "en",
        "field_name": "profile_link",
        "field_value": "Profile Link",
        "field_details": "Profile Link",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "85",
        "field_language": "en",
        "field_name": "button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "86",
        "field_language": "en",
        "field_name": "button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "87",
        "field_language": "en",
        "field_name": "button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "88",
        "field_language": "en",
        "field_name": "button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "89",
        "field_language": "ar",
        "field_name": "username",
        "field_value": "اسم المستخدم",
        "field_details": "User Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "90",
        "field_language": "ar",
        "field_name": "user_type",
        "field_value": "نوع المستخدم",
        "field_details": "User Type",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "91",
        "field_language": "ar",
        "field_name": "password",
        "field_value": "كلمه السر",
        "field_details": "password",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "92",
        "field_language": "ar",
        "field_name": "email",
        "field_value": "البريد الإلكتروني",
        "field_details": "email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "93",
        "field_language": "ar",
        "field_name": "salutation",
        "field_value": "تحية",
        "field_details": "salutation",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "94",
        "field_language": "ar",
        "field_name": "first_name",
        "field_value": "الاسم الاول",
        "field_details": "First Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "95",
        "field_language": "ar",
        "field_name": "middle_name",
        "field_value": "الاسم الوسطى",
        "field_details": "Middle Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "96",
        "field_language": "ar",
        "field_name": "last_name",
        "field_value": "الكنية",
        "field_details": "Last Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "97",
        "field_language": "ar",
        "field_name": "full_name",
        "field_value": "الاسم الكامل",
        "field_details": "Full Name",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "98",
        "field_language": "ar",
        "field_name": "facebook_id",
        "field_value": "معرف الفيسبوك",
        "field_details": "Facebook ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "99",
        "field_language": "ar",
        "field_name": "google_id",
        "field_value": "معرف جوجل",
        "field_details": "Google ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "100",
        "field_language": "ar",
        "field_name": "phone",
        "field_value": "هاتف",
        "field_details": "Phone",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "101",
        "field_language": "ar",
        "field_name": "dob",
        "field_value": "تاريخ الولادة",
        "field_details": "Date Of Birth",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "102",
        "field_language": "ar",
        "field_name": "profile_link",
        "field_value": "رابط الملف الشخصي",
        "field_details": "Profile Link",
        "model_name": "9",
        "is_required": "false",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "103",
        "field_language": "ar",
        "field_name": "role_id",
        "field_value": "معرف الدور",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "104",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "105",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "106",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "107",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "108",
        "field_language": "ar",
        "field_name": "is_otp_required",
        "field_value": "مكتب المدعي العام المطلوبة",
        "field_details": "Is OTP Required",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "109",
        "field_language": "ar",
        "field_name": "can_be_deleted",
        "field_value": "غير قابل للحذف",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "110",
        "field_language": "ar",
        "field_name": "entity_id",
        "field_value": "معرف الكيان",
        "field_details": "Entity ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "111",
        "field_language": "ar",
        "field_name": "entity_name",
        "field_value": "اسم الكيان",
        "field_details": "Entity Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "112",
        "field_language": "ar",
        "field_name": "access_location",
        "field_value": "موقع الوصول",
        "field_details": "Access Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "113",
        "field_language": "ar",
        "field_name": "icon_location",
        "field_value": "أيقونة الموقع",
        "field_details": "Icon Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "114",
        "field_language": "ar",
        "field_name": "is_default",
        "field_value": "هو الافتراضي",
        "field_details": "Is Default",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "115",
        "field_language": "ar",
        "field_name": "details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "116",
        "field_language": "ar",
        "field_name": "remarks",
        "field_value": "ملاحظات",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "117",
        "field_language": "ar",
        "field_name": "can_be_deleted",
        "field_value": "غير قابل للحذف",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "118",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "119",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "120",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "121",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "122",
        "field_language": "ar",
        "field_name": "sector_id",
        "field_value": "معرف القطاع",
        "field_details": "Sector Id",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "123",
        "field_language": "ar",
        "field_name": "sector_name",
        "field_value": "اسم القطاع",
        "field_details": "Sector Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "124",
        "field_language": "ar",
        "field_name": "sector_type",
        "field_value": "نوع القطاع",
        "field_details": "Sector Type",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "125",
        "field_language": "ar",
        "field_name": "sector_icon_location",
        "field_value": "قطاع أيقونة الموقع",
        "field_details": "Sector Icon Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "126",
        "field_language": "ar",
        "field_name": "entity_id_arr",
        "field_value": "معرفات الكيان",
        "field_details": "Entity Ids",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "127",
        "field_language": "ar",
        "field_name": "details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "128",
        "field_language": "ar",
        "field_name": "remarks",
        "field_value": "ملاحظات",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "129",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "130",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "131",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "132",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "133",
        "field_language": "ar",
        "field_name": "can_be_deleted",
        "field_value": "غير قابل للحذف",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "134",
        "field_language": "ar",
        "field_name": "role_id",
        "field_value": "معرف الدور",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "135",
        "field_language": "ar",
        "field_name": "role_name",
        "field_value": "اسم الدور",
        "field_details": "Role Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "136",
        "field_language": "ar",
        "field_name": "allowed_entity_id_arr",
        "field_value": "الكيانات المسموح بها",
        "field_details": "Allowed Entities",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "137",
        "field_language": "ar",
        "field_name": "details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "138",
        "field_language": "ar",
        "field_name": "can_be_deleted",
        "field_value": "غير قابل للحذف",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "139",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "140",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "141",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "142",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "143",
        "field_language": "ar",
        "field_name": "permission_name",
        "field_value": "اسم الإذن",
        "field_details": "Permission Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "144",
        "field_language": "ar",
        "field_name": "details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "145",
        "field_language": "ar",
        "field_name": "remarks",
        "field_value": "ملاحظات",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "146",
        "field_language": "ar",
        "field_name": "can_be_deleted",
        "field_value": "غير قابل للحذف",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "147",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "148",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "149",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "150",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "151",
        "field_language": "ar",
        "field_name": "permission_id",
        "field_value": "معرف إذن",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "152",
        "field_language": "ar",
        "field_name": "rule_id",
        "field_value": "معرف القاعدة",
        "field_details": "Rule ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "153",
        "field_language": "ar",
        "field_name": "rule_name",
        "field_value": "اسم القاعدة",
        "field_details": "Rule Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "154",
        "field_language": "ar",
        "field_name": "sector_id_arr",
        "field_value": "القطاعات",
        "field_details": "Sector ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "155",
        "field_language": "ar",
        "field_name": "role_id_arr",
        "field_value": "الأدوار",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "156",
        "field_language": "ar",
        "field_name": "entity_id_arr",
        "field_value": "جهات",
        "field_details": "Entity ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "157",
        "field_language": "ar",
        "field_name": "permission_id_arr",
        "field_value": "أذونات",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "158",
        "field_language": "ar",
        "field_name": "details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "159",
        "field_language": "ar",
        "field_name": "remarks",
        "field_value": "ملاحظات",
        "field_details": "Remarks",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "160",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "161",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "162",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "163",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "164",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "165",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudSector",
        "error_message": "Error"
    },
    {
        "field_id": "166",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPermission",
        "error_message": "Error"
    },
    {
        "field_id": "167",
        "field_language": "en",
        "field_name": "can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "168",
        "field_language": "en",
        "field_name": "field_crudUser_add_new",
        "field_value": "Add New",
        "field_details": "Add New",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "169",
        "field_language": "en",
        "field_name": "field_crudUser_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "170",
        "field_language": "en",
        "field_name": "field_crudUser_error_user_type",
        "field_value": "User type is required",
        "field_details": "User type is required",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "171",
        "field_language": "en",
        "field_name": "field_crudUser_heading_user",
        "field_value": "User",
        "field_details": "User Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "172",
        "field_language": "ar",
        "field_name": "field_crudUser_heading_user",
        "field_value": "المستعمل",
        "field_details": "User Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "173",
        "field_language": "ar",
        "field_name": "field_crudUser_add_new",
        "field_value": "اضف جديد",
        "field_details": "Add new ",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "174",
        "field_language": "ar",
        "field_name": "field_crudUser_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "175",
        "field_language": "ar",
        "field_name": "field_crudUser_error_user_type",
        "field_value": "نوع المستخدم مطلوب",
        "field_details": "User type is required",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "176",
        "field_language": "en",
        "field_name": "field_crudUser_heading_check_username",
        "field_value": "Check if Username is Available",
        "field_details": "Heading to check username exists",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "Error"
    },
    {
        "field_id": "177",
        "field_language": "ar",
        "field_name": "field_crudUser_heading_check_username",
        "field_value": "تحقق مما إذا كان اسم المستخدم متاحًا",
        "field_details": "Heading to check username exists",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "178",
        "field_language": "en",
        "field_name": "field_listUser_heading_user_management",
        "field_value": "User Management",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "179",
        "field_language": "ar",
        "field_name": "field_listUser_heading_user_management",
        "field_value": "إدارةالمستخدم",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "180",
        "field_language": "en",
        "field_name": "field_listUser_heading_add_user",
        "field_value": "Add User",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "181",
        "field_language": "ar",
        "field_name": "field_listUser_heading_add_user",
        "field_value": "إضافة مستخدم",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "182",
        "field_language": "en",
        "field_name": "field_listUser_user_name",
        "field_value": "Username",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "183",
        "field_language": "ar",
        "field_name": "field_listUser_user_name",
        "field_value": "اسم المستخدم",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "184",
        "field_language": "en",
        "field_name": "field_listUser_email",
        "field_value": "Email",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "185",
        "field_language": "ar",
        "field_name": "field_listUser_email",
        "field_value": "البريد الإلكتروني",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "186",
        "field_language": "en",
        "field_name": "field_listUser_first_name",
        "field_value": "First Name",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "187",
        "field_language": "ar",
        "field_name": "field_listUser_first_name",
        "field_value": "الاسم الاول",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "188",
        "field_language": "en",
        "field_name": "field_listUser_last_name",
        "field_value": "Last Name",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "189",
        "field_language": "ar",
        "field_name": "field_listUser_last_name",
        "field_value": "الكنية",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "190",
        "field_language": "en",
        "field_name": "field_listUser_phone",
        "field_value": "Phone",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "191",
        "field_language": "ar",
        "field_name": "field_listUser_phone",
        "field_value": "هاتف",
        "field_details": "User management heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "192",
        "field_language": "en",
        "field_name": "field_listUser_button_edit",
        "field_value": "Edit",
        "field_details": "Edit Button",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "193",
        "field_language": "ar",
        "field_name": "field_listUser_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit Button",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "194",
        "field_language": "en",
        "field_name": "field_listUser_button_delete",
        "field_value": "Delete",
        "field_details": "Delete Button",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "Error"
    },
    {
        "field_id": "195",
        "field_language": "ar",
        "field_name": "field_listUser_button_delete",
        "field_value": "حذف",
        "field_details": "Delete Button",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "196",
        "field_language": "en",
        "field_name": "field_dashboard_heading_systems",
        "field_value": "Systems",
        "field_details": "System heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "197",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_systems",
        "field_value": "نظم",
        "field_details": "System heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "198",
        "field_language": "en",
        "field_name": "field_dashboard_heading_networks",
        "field_value": "Networks",
        "field_details": "Network heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "199",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_networks",
        "field_value": "الشبكات",
        "field_details": "Network heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "200",
        "field_language": "en",
        "field_name": "field_dashboard_heading_files",
        "field_value": "Files",
        "field_details": "File heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "201",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_files",
        "field_value": "الملفات",
        "field_details": "File heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "202",
        "field_language": "en",
        "field_name": "field_dashboard_heading_applications",
        "field_value": "Applications",
        "field_details": "Applcation heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "203",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_applications",
        "field_value": "التطبيقات",
        "field_details": "Applcation heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "204",
        "field_language": "en",
        "field_name": "field_dashboard_heading_total_system_entities",
        "field_value": "Total System Entities",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "205",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_total_system_entities",
        "field_value": "مجموع كيانات النظام",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "206",
        "field_language": "en",
        "field_name": "field_dashboard_heading_total_network_entities",
        "field_value": "Total Network Entities",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "207",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_total_network_entities",
        "field_value": "مجموع الكيانات الشبكة",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "208",
        "field_language": "en",
        "field_name": "field_dashboard_heading_total_file_entities",
        "field_value": "Total File Entities",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "209",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_total_file_entities",
        "field_value": "مجموع الكيانات الملف",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "210",
        "field_language": "en",
        "field_name": "field_dashboard_heading_total_application_entities",
        "field_value": "Total Application Entities",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "211",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_total_application_entities",
        "field_value": "مجموع الكيانات التطبيقية",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "212",
        "field_language": "en",
        "field_name": "field_dashboard_heading_recent_activities",
        "field_value": "Recent Activities",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "213",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_recent_activities",
        "field_value": "أنشطة حالية",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "214",
        "field_language": "en",
        "field_name": "field_dashboard_heading_activity_source",
        "field_value": "Activity Source",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "215",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_activity_source",
        "field_value": "مصدر النشاط",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "216",
        "field_language": "en",
        "field_name": "field_dashboard_heading_activity_type",
        "field_value": "Activity Type",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "217",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_activity_type",
        "field_value": "نوع النشاط",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "218",
        "field_language": "en",
        "field_name": "field_dashboard_heading_activity_time",
        "field_value": "Activity Time",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "219",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_activity_time",
        "field_value": "وقت النشاط",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "220",
        "field_language": "en",
        "field_name": "field_dashboard_heading_who_initiated",
        "field_value": "Who Initiated",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "221",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_who_initiated",
        "field_value": "الذي بدأ",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "222",
        "field_language": "en",
        "field_name": "field_dashboard_heading_created",
        "field_value": "Created",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "223",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_created",
        "field_value": "خلقت",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "224",
        "field_language": "en",
        "field_name": "field_dashboard_heading_deleted",
        "field_value": "Deleted",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "225",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_deleted",
        "field_value": "تم الحذف",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "226",
        "field_language": "en",
        "field_name": "field_dashboard_heading_users",
        "field_value": "Users",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "227",
        "field_language": "ar",
        "field_name": "field_dashboard_heading_users",
        "field_value": "المستخدمين",
        "field_details": "Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "228",
        "field_language": "en",
        "field_name": "field_menu_heading_navigation",
        "field_value": "Navigation",
        "field_details": "Menu Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "229",
        "field_language": "ar",
        "field_name": "field_menu_heading_navigation",
        "field_value": "التنقل",
        "field_details": "Menu Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "230",
        "field_language": "en",
        "field_name": "field_menu_dashboard",
        "field_value": "Dashboard",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "231",
        "field_language": "ar",
        "field_name": "field_menu_dashboard",
        "field_value": "لوحة القيادة",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "232",
        "field_language": "en",
        "field_name": "field_menu_standardDashboard",
        "field_value": "Dashboard",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "233",
        "field_language": "ar",
        "field_name": "field_menu_standardDashboard",
        "field_value": "لوحة القيادة",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "234",
        "field_language": "en",
        "field_name": "field_menu_heading_users",
        "field_value": "Users",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "235",
        "field_language": "ar",
        "field_name": "field_menu_heading_users",
        "field_value": "المستخدمون",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "236",
        "field_language": "en",
        "field_name": "field_menu_link_listUser",
        "field_value": "Show All Users",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "237",
        "field_language": "ar",
        "field_name": "field_menu_link_listUser",
        "field_value": "إظهار كافة المستخدمين",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "238",
        "field_language": "en",
        "field_name": "field_menu_link_crudUser",
        "field_value": "Add User",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "239",
        "field_language": "ar",
        "field_name": "field_menu_link_crudUser",
        "field_value": "أضف مستخدم",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "240",
        "field_language": "en",
        "field_name": "field_menu_link_listAccessEntity",
        "field_value": "Access Entity Requests",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "241",
        "field_language": "ar",
        "field_name": "field_menu_link_listAccessEntity",
        "field_value": "طلبات الوصول إلى الكيانات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "242",
        "field_language": "en",
        "field_name": "field_menu_heading_sectors",
        "field_value": "Entity Elements",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "243",
        "field_language": "ar",
        "field_name": "field_menu_heading_sectors",
        "field_value": "عناصر الكيان",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "244",
        "field_language": "en",
        "field_name": "field_menu_link_listEntity",
        "field_value": "Entities",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "245",
        "field_language": "ar",
        "field_name": "field_menu_link_listEntity",
        "field_value": "جهات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "246",
        "field_language": "en",
        "field_name": "field_menu_link_listSector",
        "field_value": "Sectors",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "247",
        "field_language": "ar",
        "field_name": "field_menu_link_listSector",
        "field_value": "القطاعات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "248",
        "field_language": "en",
        "field_name": "field_menu_link_listSectorElements",
        "field_value": "Sector Entities",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "249",
        "field_language": "ar",
        "field_name": "field_menu_link_listSectorElements",
        "field_value": "كيانات القطاع",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "250",
        "field_language": "en",
        "field_name": "field_menu_heading_configuration",
        "field_value": "Roles & Policies",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "251",
        "field_language": "ar",
        "field_name": "field_menu_heading_configuration",
        "field_value": "الأدوار والسياسات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "252",
        "field_language": "en",
        "field_name": "field_menu_link_listRole",
        "field_value": "Roles",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "253",
        "field_language": "ar",
        "field_name": "field_menu_link_listRole",
        "field_value": "الأدوار",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "254",
        "field_language": "en",
        "field_name": "field_menu_link_listPermission",
        "field_value": "Permissions",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "255",
        "field_language": "ar",
        "field_name": "field_menu_link_listPermission",
        "field_value": "أذونات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "256",
        "field_language": "en",
        "field_name": "field_menu_link_listRule",
        "field_value": "Rules",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "257",
        "field_language": "ar",
        "field_name": "field_menu_link_listRule",
        "field_value": "قواعد",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "258",
        "field_language": "en",
        "field_name": "field_menu_heading_settings",
        "field_value": "Settings",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "259",
        "field_language": "ar",
        "field_name": "field_menu_heading_settings",
        "field_value": "الإعدادات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "260",
        "field_language": "en",
        "field_name": "field_menu_link_editFields",
        "field_value": "Field Names",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "261",
        "field_language": "ar",
        "field_name": "field_menu_link_editFields",
        "field_value": "أسماء الحقول",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "262",
        "field_language": "en",
        "field_name": "field_menu_link_emailAlerts",
        "field_value": "Email Alerts",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "263",
        "field_language": "ar",
        "field_name": "field_menu_link_emailAlerts",
        "field_value": "تنبيهات البريد الإلكتروني",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "264",
        "field_language": "en",
        "field_name": "field_menu_link_editUserProfile",
        "field_value": "Edit Profile",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "265",
        "field_language": "ar",
        "field_name": "field_menu_link_editUserProfile",
        "field_value": "تعديل الملف الشخصي",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "266",
        "field_language": "en",
        "field_name": "field_menu_heading_reports",
        "field_value": "Reports",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "267",
        "field_language": "ar",
        "field_name": "field_menu_heading_reports",
        "field_value": "التقارير",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "268",
        "field_language": "en",
        "field_name": "field_menu_link_report_default",
        "field_value": "Default",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "269",
        "field_language": "ar",
        "field_name": "field_menu_link_report_default",
        "field_value": "إفتراضي",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "270",
        "field_language": "en",
        "field_name": "field_menu_link_report_systemEvents",
        "field_value": "System Events",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "271",
        "field_language": "ar",
        "field_name": "field_menu_link_report_systemEvents",
        "field_value": "أحداث النظام",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "272",
        "field_language": "en",
        "field_name": "field_menu_link_report_entityEvents",
        "field_value": "Entity Events",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "273",
        "field_language": "ar",
        "field_name": "field_menu_link_report_entityEvents",
        "field_value": "أحداث الكيان",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "274",
        "field_language": "en",
        "field_name": "field_menu_link_report_userLogging",
        "field_value": "User Logging",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "275",
        "field_language": "ar",
        "field_name": "field_menu_link_report_userLogging",
        "field_value": "تسجيل المستخدم",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "276",
        "field_language": "en",
        "field_name": "field_menu_link_report_existingUsers",
        "field_value": "Existing Users",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "277",
        "field_language": "ar",
        "field_name": "field_menu_link_report_existingUsers",
        "field_value": "المستخدمون الحاليون",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "278",
        "field_language": "en",
        "field_name": "field_menu_link_report_userActivityReports",
        "field_value": "User Activity Reports",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "279",
        "field_language": "ar",
        "field_name": "field_menu_link_report_userActivityReports",
        "field_value": "تقارير نشاط المستخدم",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "280",
        "field_language": "en",
        "field_name": "field_menu_heading_entityModule",
        "field_value": "Entity Module",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "281",
        "field_language": "ar",
        "field_name": "field_menu_heading_entityModule",
        "field_value": "وحدة الكيان",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "282",
        "field_language": "en",
        "field_name": "field_menu_link_accessEntity",
        "field_value": "Entities",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "283",
        "field_language": "ar",
        "field_name": "field_menu_link_accessEntity",
        "field_value": "جهات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "284",
        "field_language": "en",
        "field_name": "field_menu_link_showEntities",
        "field_value": "Show Entities",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "285",
        "field_language": "ar",
        "field_name": "field_menu_link_showEntities",
        "field_value": "إظهار الكيانات",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "286",
        "field_language": "en",
        "field_name": "field_listAccessEntity_heading_user_requests",
        "field_value": "User Requests",
        "field_details": "Heading User Requests",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "287",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_heading_user_requests",
        "field_value": "طلبات المستخدم",
        "field_details": "Heading User Requests",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "288",
        "field_language": "en",
        "field_name": "field_listAccessEntity_requested_username",
        "field_value": "Requested Username",
        "field_details": "Requested Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "289",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_requested_username",
        "field_value": "اسم المستخدم المطلوب",
        "field_details": "Requested Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "290",
        "field_language": "en",
        "field_name": "field_listAccessEntity_requested_entity_id",
        "field_value": "Entity",
        "field_details": "Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "291",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_requested_entity_id",
        "field_value": "شخصية",
        "field_details": "Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "292",
        "field_language": "en",
        "field_name": "field_listAccessEntity_requested_time",
        "field_value": "Request Time",
        "field_details": "Request Time",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "293",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_requested_time",
        "field_value": "وقت الطلب",
        "field_details": "Request Time",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "294",
        "field_language": "en",
        "field_name": "field_listAccessEntity_current_status",
        "field_value": "Current Status",
        "field_details": "Current Status",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "295",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_current_status",
        "field_value": "الحالة الحالية",
        "field_details": "Current Status",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "296",
        "field_language": "en",
        "field_name": "field_listAccessEntity_is_new_request",
        "field_value": "Is Request New",
        "field_details": "Is Request New",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "297",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_is_new_request",
        "field_value": "هو طلب جديد",
        "field_details": "Is Request New",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "298",
        "field_language": "en",
        "field_name": "field_listAccessEntity_button_approve",
        "field_value": "Approve",
        "field_details": "Approve",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "299",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_button_approve",
        "field_value": "يوافق",
        "field_details": "Approve",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "300",
        "field_language": "en",
        "field_name": "field_listAccessEntity_button_decline",
        "field_value": "Decline",
        "field_details": "Decline",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "Error"
    },
    {
        "field_id": "301",
        "field_language": "ar",
        "field_name": "field_listAccessEntity_button_decline",
        "field_value": "انخفاض",
        "field_details": "Decline",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listAccessEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "302",
        "field_language": "en",
        "field_name": "field_listEntity_heading_entity_management",
        "field_value": "Entity Management",
        "field_details": "Entity Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "303",
        "field_language": "ar",
        "field_name": "field_listEntity_heading_entity_management",
        "field_value": "إدارة الكيانات",
        "field_details": "Entity Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "304",
        "field_language": "en",
        "field_name": "field_listEntity_button_add_entity",
        "field_value": "Add Entity",
        "field_details": "Add Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "305",
        "field_language": "ar",
        "field_name": "field_listEntity_button_add_entity",
        "field_value": "إضافة كيان",
        "field_details": "Add Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "306",
        "field_language": "en",
        "field_name": "field_listEntity_heading_entity_details",
        "field_value": "Entity Details",
        "field_details": "Entity Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "307",
        "field_language": "ar",
        "field_name": "field_listEntity_heading_entity_details",
        "field_value": "تفاصيل الكيان",
        "field_details": "Entity Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "308",
        "field_language": "en",
        "field_name": "field_listEntity_entity_name",
        "field_value": "Entity Name",
        "field_details": "Entity Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "309",
        "field_language": "ar",
        "field_name": "field_listEntity_entity_name",
        "field_value": "اسم الكيان",
        "field_details": "Entity Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "310",
        "field_language": "en",
        "field_name": "field_listEntity_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "311",
        "field_language": "ar",
        "field_name": "field_listEntity_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "312",
        "field_language": "en",
        "field_name": "field_listEntity_access_location",
        "field_value": "Access Location",
        "field_details": "Access Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "313",
        "field_language": "ar",
        "field_name": "field_listEntity_access_location",
        "field_value": "الوصول إلى الموقع",
        "field_details": "Access Location",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "314",
        "field_language": "en",
        "field_name": "field_listEntity_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "315",
        "field_language": "ar",
        "field_name": "field_listEntity_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "316",
        "field_language": "en",
        "field_name": "field_listEntity_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "Error"
    },
    {
        "field_id": "317",
        "field_language": "ar",
        "field_name": "field_listEntity_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "318",
        "field_language": "en",
        "field_name": "field_listSector_heading_sector_management",
        "field_value": "Sector Management",
        "field_details": "Sector Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "319",
        "field_language": "ar",
        "field_name": "field_listSector_heading_sector_management",
        "field_value": "إدارة القطاع",
        "field_details": "Sector Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "320",
        "field_language": "en",
        "field_name": "field_listSector_button_add_sector",
        "field_value": "Add Sector",
        "field_details": "Add Sector",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "321",
        "field_language": "ar",
        "field_name": "field_listSector_button_add_sector",
        "field_value": "إضافة قطاع",
        "field_details": "Add Sector",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "322",
        "field_language": "en",
        "field_name": "field_listSector_heading_sector_details",
        "field_value": "Sector Details",
        "field_details": "Sector Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "323",
        "field_language": "ar",
        "field_name": "field_listSector_heading_sector_details",
        "field_value": "تفاصيل القطاع",
        "field_details": "Sector Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "324",
        "field_language": "en",
        "field_name": "field_listSector_sector_id",
        "field_value": "Sector ID",
        "field_details": "Sector ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "325",
        "field_language": "ar",
        "field_name": "field_listSector_sector_id",
        "field_value": "معرف القطاع",
        "field_details": "Sector ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "326",
        "field_language": "en",
        "field_name": "field_listSector_sector_name",
        "field_value": "Sector Name",
        "field_details": "Sector Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "327",
        "field_language": "ar",
        "field_name": "field_listSector_sector_name",
        "field_value": "اسم القطاع",
        "field_details": "Sector Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "328",
        "field_language": "en",
        "field_name": "field_listSector_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "329",
        "field_language": "ar",
        "field_name": "field_listSector_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "330",
        "field_language": "en",
        "field_name": "field_listSector_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "331",
        "field_language": "ar",
        "field_name": "field_listSector_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "332",
        "field_language": "en",
        "field_name": "field_listSector_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "Error"
    },
    {
        "field_id": "333",
        "field_language": "ar",
        "field_name": "field_listSector_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listSector",
        "error_message": "خطأ"
    },
    {
        "field_id": "334",
        "field_language": "en",
        "field_name": "field_menu_link_listAlertNotification",
        "field_value": "Alert Notification",
        "field_details": "Configure Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "335",
        "field_language": "ar",
        "field_name": "field_menu_link_listAlertNotification",
        "field_value": "إشعار التنبيه",
        "field_details": "Configure Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "336",
        "field_language": "en",
        "field_name": "field_listRole_heading_role_management",
        "field_value": "Role Management",
        "field_details": "Role Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "337",
        "field_language": "ar",
        "field_name": "field_listRole_heading_role_management",
        "field_value": "إدارة الأدوار",
        "field_details": "Role Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "338",
        "field_language": "en",
        "field_name": "field_listRole_button_add_role",
        "field_value": "Add Role",
        "field_details": "Add Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "339",
        "field_language": "ar",
        "field_name": "field_listRole_button_add_role",
        "field_value": "إضافة دور",
        "field_details": "Add Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "340",
        "field_language": "en",
        "field_name": "field_listRole_heading_role_details",
        "field_value": "Role Details",
        "field_details": "Role Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "341",
        "field_language": "ar",
        "field_name": "field_listRole_heading_role_details",
        "field_value": "تفاصيل الدور",
        "field_details": "Role Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "342",
        "field_language": "en",
        "field_name": "field_listRole_role_id",
        "field_value": "Role ID",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "343",
        "field_language": "ar",
        "field_name": "field_listRole_role_id",
        "field_value": "معرف الدور",
        "field_details": "Role ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "344",
        "field_language": "en",
        "field_name": "field_listRole_role_name",
        "field_value": "Role Name",
        "field_details": "Role Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "345",
        "field_language": "ar",
        "field_name": "field_listRole_role_name",
        "field_value": "اسم الدور",
        "field_details": "Role Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "346",
        "field_language": "en",
        "field_name": "field_listRole_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "347",
        "field_language": "ar",
        "field_name": "field_listRole_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "348",
        "field_language": "en",
        "field_name": "field_listRole_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "349",
        "field_language": "ar",
        "field_name": "field_listRole_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "350",
        "field_language": "en",
        "field_name": "field_listRole_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "Error"
    },
    {
        "field_id": "351",
        "field_language": "ar",
        "field_name": "field_listRole_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRole",
        "error_message": "خطأ"
    },
    {
        "field_id": "352",
        "field_language": "en",
        "field_name": "field_listPermission_heading_permission_management",
        "field_value": "Permission Management",
        "field_details": "Permission Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "353",
        "field_language": "ar",
        "field_name": "field_listPermission_heading_permission_management",
        "field_value": "إدارة الإذن",
        "field_details": "Permission Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "354",
        "field_language": "en",
        "field_name": "field_listPermission_button_add_permission",
        "field_value": "Add Permission",
        "field_details": "Add Permission",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "355",
        "field_language": "ar",
        "field_name": "field_listPermission_button_add_permission",
        "field_value": "إضافة إذن",
        "field_details": "Add Permission",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "356",
        "field_language": "en",
        "field_name": "field_listPermission_permission_id",
        "field_value": "Permission ID",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "357",
        "field_language": "ar",
        "field_name": "field_listPermission_permission_id",
        "field_value": "معرف الإذن",
        "field_details": "Permission ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "358",
        "field_language": "en",
        "field_name": "field_listPermission_permission_name",
        "field_value": "Permission Name",
        "field_details": "Permission Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "359",
        "field_language": "ar",
        "field_name": "field_listPermission_permission_name",
        "field_value": "اسم الإذن",
        "field_details": "Permission Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "360",
        "field_language": "en",
        "field_name": "field_listPermission_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "361",
        "field_language": "ar",
        "field_name": "field_listPermission_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "362",
        "field_language": "en",
        "field_name": "field_listPermission_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "363",
        "field_language": "ar",
        "field_name": "field_listPermission_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "364",
        "field_language": "en",
        "field_name": "field_listPermission_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "Error"
    },
    {
        "field_id": "365",
        "field_language": "ar",
        "field_name": "field_listPermission_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPermission",
        "error_message": "خطأ"
    },
    {
        "field_id": "366",
        "field_language": "en",
        "field_name": "field_listRule_heading_rule_management",
        "field_value": "Rule Management",
        "field_details": "Rule Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "367",
        "field_language": "ar",
        "field_name": "field_listRule_heading_rule_management",
        "field_value": "إدارة القواعد",
        "field_details": "Rule Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "368",
        "field_language": "en",
        "field_name": "field_listRule_button_add_rule",
        "field_value": "Add Rule",
        "field_details": "Add Rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "369",
        "field_language": "ar",
        "field_name": "field_listRule_button_add_rule",
        "field_value": "أضف قاعدة",
        "field_details": "Add Rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "370",
        "field_language": "en",
        "field_name": "field_listRule_heading_rule_details",
        "field_value": "Rule Details",
        "field_details": "Rule Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "371",
        "field_language": "ar",
        "field_name": "field_listRule_heading_rule_details",
        "field_value": "تفاصيل القاعدة",
        "field_details": "Rule Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "372",
        "field_language": "en",
        "field_name": "field_listRule_rule_id",
        "field_value": "Rule ID",
        "field_details": "Rule ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "373",
        "field_language": "ar",
        "field_name": "field_listRule_rule_id",
        "field_value": "معرف القاعدة",
        "field_details": "Rule ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "374",
        "field_language": "en",
        "field_name": "field_listRule_rule_name",
        "field_value": "Rule Name",
        "field_details": "Rule Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "375",
        "field_language": "ar",
        "field_name": "field_listRule_rule_name",
        "field_value": "اسم القاعدة",
        "field_details": "Rule Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "376",
        "field_language": "en",
        "field_name": "field_listRule_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "377",
        "field_language": "ar",
        "field_name": "field_listRule_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "378",
        "field_language": "en",
        "field_name": "field_listRule_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "379",
        "field_language": "ar",
        "field_name": "field_listRule_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "380",
        "field_language": "en",
        "field_name": "field_listRule_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "Error"
    },
    {
        "field_id": "381",
        "field_language": "ar",
        "field_name": "field_listRule_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "382",
        "field_language": "en",
        "field_name": "field_crudRule_make_the_rule",
        "field_value": "Make the rule",
        "field_details": "Make the rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "383",
        "field_language": "ar",
        "field_name": "field_crudRule_make_the_rule",
        "field_value": "اجعل القاعدة",
        "field_details": "Make the rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "384",
        "field_language": "en",
        "field_name": "field_crudRule_role_specific",
        "field_value": "Role Specific",
        "field_details": "Role Specific",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "385",
        "field_language": "ar",
        "field_name": "field_crudRule_role_specific",
        "field_value": "الدور المحدد",
        "field_details": "Role Specific",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "386",
        "field_language": "en",
        "field_name": "field_crudRule_user_specific",
        "field_value": "User Specfic",
        "field_details": "User Specfic",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "387",
        "field_language": "ar",
        "field_name": "field_crudRule_user_specific",
        "field_value": "خاص بالمستخدم",
        "field_details": "User Specfic",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "388",
        "field_language": "en",
        "field_name": "field_crudRule_can_be_deleted",
        "field_value": "Can be deleted",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "Error"
    },
    {
        "field_id": "389",
        "field_language": "ar",
        "field_name": "field_crudRule_can_be_deleted",
        "field_value": "يمكن حذفه",
        "field_details": "Can be deleted",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudRule",
        "error_message": "خطأ"
    },
    {
        "field_id": "390",
        "field_language": "en",
        "field_name": "field_editFields_select_component",
        "field_value": "Select Component",
        "field_details": "Select Component",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "391",
        "field_language": "ar",
        "field_name": "field_editFields_select_component",
        "field_value": "حدد مكون",
        "field_details": "Select Component",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "392",
        "field_language": "en",
        "field_name": "field_editFields_select_field_to_edit",
        "field_value": "Select field to edit",
        "field_details": "Select field to edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "393",
        "field_language": "ar",
        "field_name": "field_editFields_select_field_to_edit",
        "field_value": "حدد الحقل المراد تحريره",
        "field_details": "Select field to edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "394",
        "field_language": "en",
        "field_name": "field_editFields_current_field_value",
        "field_value": "Current field value",
        "field_details": "Current field value",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "395",
        "field_language": "ar",
        "field_name": "field_editFields_current_field_value",
        "field_value": "قيمة الحقل الحالية",
        "field_details": "Current field value",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "396",
        "field_language": "en",
        "field_name": "field_editFields_make_field",
        "field_value": "Make field",
        "field_details": "Make field",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "397",
        "field_language": "ar",
        "field_name": "field_editFields_make_field",
        "field_value": "اجعل المجال",
        "field_details": "Make field",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "398",
        "field_language": "en",
        "field_name": "field_editFields_mandatory",
        "field_value": "Mandatory",
        "field_details": "Mandatory",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "399",
        "field_language": "ar",
        "field_name": "field_editFields_mandatory",
        "field_value": "إلزامي",
        "field_details": "Mandatory",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "400",
        "field_language": "en",
        "field_name": "field_editFields_optional",
        "field_value": "Optional",
        "field_details": "Optional",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "401",
        "field_language": "ar",
        "field_name": "field_editFields_optional",
        "field_value": "اختياري",
        "field_details": "Optional",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "402",
        "field_language": "en",
        "field_name": "field_editFields_error_message",
        "field_value": "Error Message",
        "field_details": "Error Message",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "403",
        "field_language": "ar",
        "field_name": "field_editFields_error_message",
        "field_value": "رسالة خطأ",
        "field_details": "Error Message",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "404",
        "field_language": "en",
        "field_name": "field_editFields_button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "405",
        "field_language": "ar",
        "field_name": "field_editFields_button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "406",
        "field_language": "en",
        "field_name": "field_editFields_button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "407",
        "field_language": "ar",
        "field_name": "field_editFields_button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "408",
        "field_language": "en",
        "field_name": "field_editFields_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "409",
        "field_language": "ar",
        "field_name": "field_editFields_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "410",
        "field_language": "en",
        "field_name": "field_editFields_heading_configure_fields",
        "field_value": "Configure Fields",
        "field_details": "Configure Fields",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "Error"
    },
    {
        "field_id": "411",
        "field_language": "ar",
        "field_name": "field_editFields_heading_configure_fields",
        "field_value": "تكوين الحقول",
        "field_details": "Configure Fields",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editFields",
        "error_message": "خطأ"
    },
    {
        "field_id": "412",
        "field_language": "en",
        "field_name": "field_emailAlert_heading_configure_email_alert",
        "field_value": "Configure Email Alert",
        "field_details": "Configure Email Alert",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "413",
        "field_language": "ar",
        "field_name": "field_emailAlert_heading_configure_email_alert",
        "field_value": "تكوين تنبيه البريد الإلكتروني",
        "field_details": "Configure Email Alert",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "414",
        "field_language": "en",
        "field_name": "field_emailAlert_should_send_email",
        "field_value": "Should send email",
        "field_details": "Should send email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "415",
        "field_language": "ar",
        "field_name": "field_emailAlert_should_send_email",
        "field_value": "يجب إرسال بريد إلكتروني",
        "field_details": "Should send email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "416",
        "field_language": "en",
        "field_name": "field_emailAlert_yes",
        "field_value": "Yes",
        "field_details": "Yes",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "417",
        "field_language": "ar",
        "field_name": "field_emailAlert_yes",
        "field_value": "نعم",
        "field_details": "Yes",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "418",
        "field_language": "en",
        "field_name": "field_emailAlert_no",
        "field_value": "No",
        "field_details": "No",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "419",
        "field_language": "ar",
        "field_name": "field_emailAlert_no",
        "field_value": "لا",
        "field_details": "No",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "420",
        "field_language": "en",
        "field_name": "field_emailAlert_alert_level",
        "field_value": "Alert Level",
        "field_details": "Alert Level",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "421",
        "field_language": "ar",
        "field_name": "field_emailAlert_alert_level",
        "field_value": "مستوى التنبيه",
        "field_details": "Alert Level",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "422",
        "field_language": "en",
        "field_name": "field_emailAlert_fatal",
        "field_value": "Fatal",
        "field_details": "Fatal",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "423",
        "field_language": "ar",
        "field_name": "field_emailAlert_fatal",
        "field_value": "قاتلة - مهلك",
        "field_details": "Fatal",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "424",
        "field_language": "en",
        "field_name": "field_emailAlert_critical",
        "field_value": "Critical",
        "field_details": "Critical",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "425",
        "field_language": "ar",
        "field_name": "field_emailAlert_critical",
        "field_value": "حرج",
        "field_details": "Critical",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "426",
        "field_language": "en",
        "field_name": "field_emailAlert_high",
        "field_value": "High",
        "field_details": "High",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "427",
        "field_language": "ar",
        "field_name": "field_emailAlert_high",
        "field_value": "عالي",
        "field_details": "High",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "428",
        "field_language": "en",
        "field_name": "field_emailAlert_medium",
        "field_value": "Medium",
        "field_details": "Medium",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "429",
        "field_language": "ar",
        "field_name": "field_emailAlert_medium",
        "field_value": "متوسط",
        "field_details": "Medium",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "430",
        "field_language": "en",
        "field_name": "field_emailAlert_low",
        "field_value": "Low",
        "field_details": "Low",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "431",
        "field_language": "ar",
        "field_name": "field_emailAlert_low",
        "field_value": "منخفض",
        "field_details": "Low",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "432",
        "field_language": "en",
        "field_name": "field_emailAlert_button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "Error"
    },
    {
        "field_id": "433",
        "field_language": "ar",
        "field_name": "field_emailAlert_button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "emailAlert",
        "error_message": "خطأ"
    },
    {
        "field_id": "434",
        "field_language": "ar",
        "field_name": "email",
        "field_value": "البريد الإلكتروني",
        "field_details": "email",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "435",
        "field_language": "ar",
        "field_name": "salutation",
        "field_value": "تحية",
        "field_details": "salutation",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "436",
        "field_language": "ar",
        "field_name": "first_name",
        "field_value": "الاسم الاول",
        "field_details": "First Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "437",
        "field_language": "ar",
        "field_name": "middle_name",
        "field_value": "الاسم الوسطى",
        "field_details": "Middle Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "438",
        "field_language": "ar",
        "field_name": "last_name",
        "field_value": "الكنية",
        "field_details": "Last Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "439",
        "field_language": "ar",
        "field_name": "full_name",
        "field_value": "الاسم الكامل",
        "field_details": "Full Name",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "440",
        "field_language": "ar",
        "field_name": "facebook_id",
        "field_value": "معرف الفيسبوك",
        "field_details": "Facebook ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "441",
        "field_language": "ar",
        "field_name": "google_id",
        "field_value": "معرف جوجل",
        "field_details": "Google ID",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "442",
        "field_language": "ar",
        "field_name": "phone",
        "field_value": "هاتف",
        "field_details": "Phone",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "443",
        "field_language": "ar",
        "field_name": "dob",
        "field_value": "تاريخ الولادة",
        "field_details": "Date Of Birth",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "444",
        "field_language": "ar",
        "field_name": "profile_link",
        "field_value": "رابط الملف الشخصي",
        "field_details": "Profile Link",
        "model_name": "9",
        "is_required": "false",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "445",
        "field_language": "ar",
        "field_name": "button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "446",
        "field_language": "ar",
        "field_name": "button_update",
        "field_value": "تحديث",
        "field_details": "Updae",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "447",
        "field_language": "ar",
        "field_name": "button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "448",
        "field_language": "ar",
        "field_name": "button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "449",
        "field_language": "en",
        "field_name": "field_editUserProfile_heading_update_user",
        "field_value": "Update User",
        "field_details": "Update User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "Error"
    },
    {
        "field_id": "450",
        "field_language": "ar",
        "field_name": "field_editUserProfile_heading_update_user",
        "field_value": "تحديث المستخدم",
        "field_details": "Update User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "editUserProfile",
        "error_message": "خطأ"
    },
    {
        "field_id": "451",
        "field_language": "en",
        "field_name": "field_alertNotification_heading_alert_notification",
        "field_value": "Alert Notification",
        "field_details": "Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "452",
        "field_language": "ar",
        "field_name": "field_alertNotification_heading_alert_notification",
        "field_value": "إشعار التنبيه",
        "field_details": "Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "453",
        "field_language": "en",
        "field_name": "field_alertNotification_select_component",
        "field_value": "Select Component",
        "field_details": "Select Component",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "454",
        "field_language": "ar",
        "field_name": "field_alertNotification_select_component",
        "field_value": "حدد مكون",
        "field_details": "Select Component",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "455",
        "field_language": "en",
        "field_name": "field_alertNotification_select_alert_name",
        "field_value": "Alert Name",
        "field_details": "Alert Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "456",
        "field_language": "ar",
        "field_name": "field_alertNotification_select_alert_name",
        "field_value": "اسم التنبيه",
        "field_details": "Alert Name",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "457",
        "field_language": "en",
        "field_name": "field_alertNotification_current_alert_text",
        "field_value": "Alert Text",
        "field_details": "Alert Text",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "458",
        "field_language": "ar",
        "field_name": "field_alertNotification_current_alert_text",
        "field_value": "نص التنبيه",
        "field_details": "Alert Text",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "459",
        "field_language": "en",
        "field_name": "field_alertNotification_button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "460",
        "field_language": "ar",
        "field_name": "field_alertNotification_button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "461",
        "field_language": "en",
        "field_name": "field_alertNotification_button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "462",
        "field_language": "ar",
        "field_name": "field_alertNotification_button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "463",
        "field_language": "en",
        "field_name": "field_alertNotification_button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "Error"
    },
    {
        "field_id": "464",
        "field_language": "ar",
        "field_name": "field_alertNotification_button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "alertNotification",
        "error_message": "خطأ"
    },
    {
        "field_id": "465",
        "field_language": "en",
        "field_name": "field_menu_link_defaultSetting",
        "field_value": "Default Settings",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "466",
        "field_language": "ar",
        "field_name": "field_menu_link_defaultSetting",
        "field_value": "الإعدادات الافتراضية",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "467",
        "field_language": "en",
        "field_name": "field_menu_link_listPageMatrix",
        "field_value": "Page Matrix",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "468",
        "field_language": "ar",
        "field_name": "field_menu_link_listPageMatrix",
        "field_value": "مصفوفة الصفحة",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "469",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_dashboard",
        "field_value": "Dashboard",
        "field_details": "Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "470",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_dashboard",
        "field_value": "لوحة القيادة",
        "field_details": "Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "471",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_heading_page_matrix",
        "field_value": "Page Matrix",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "472",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_heading_page_matrix",
        "field_value": "مصفوفة الصفحة",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "473",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_select_role_id",
        "field_value": "Select Role",
        "field_details": "Select Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "474",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_select_role_id",
        "field_value": "حدد الدور",
        "field_details": "Select Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "475",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listUser",
        "field_value": "List User",
        "field_details": "List User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "476",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listUser",
        "field_value": "سرد المستخدم",
        "field_details": "List User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "477",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_crudUser",
        "field_value": "CRUD User",
        "field_details": "CRUD User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "478",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_crudUser",
        "field_value": "مستخدم CRUD",
        "field_details": "CRUD User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "479",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listAccessEntity",
        "field_value": "List Access Entity",
        "field_details": "List Access Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "480",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listAccessEntity",
        "field_value": "كيان الوصول إلى القائمة",
        "field_details": "List Access Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "481",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listEntity",
        "field_value": "List Entity",
        "field_details": "List Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "482",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listEntity",
        "field_value": "قائمة الكيانات",
        "field_details": "List Entity",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "483",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listSector",
        "field_value": "List Sector",
        "field_details": "List Sector",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "484",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listSector",
        "field_value": "قطاع القائمة",
        "field_details": "List Sector",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "485",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listSectorElements",
        "field_value": "List Sector Elements",
        "field_details": "List Sector Elements",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "486",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listSectorElements",
        "field_value": "قائمة عناصر القطاع",
        "field_details": "List Sector Elements",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "487",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listRole",
        "field_value": "List Role",
        "field_details": "List Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "488",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listRole",
        "field_value": "دور القائمة",
        "field_details": "List Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "489",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listPermission",
        "field_value": "List Permission",
        "field_details": "List Permission",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "490",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listPermission",
        "field_value": "إذن القائمة",
        "field_details": "List Permission",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "491",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listRule",
        "field_value": "List Rule",
        "field_details": "List Rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "492",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listRule",
        "field_value": "قاعدة القائمة",
        "field_details": "List Rule",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "493",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_editFields",
        "field_value": "Edit Fields",
        "field_details": "Edit Fields",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "494",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_editFields",
        "field_value": "تحرير الحقول",
        "field_details": "Edit Fields",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "495",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_emailAlerts",
        "field_value": "Email Alerts",
        "field_details": "Email Alerts",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "496",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_emailAlerts",
        "field_value": "تنبيهات البريد الإلكتروني",
        "field_details": "Email Alerts",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "497",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_editUserProfile",
        "field_value": "Edit User Profile",
        "field_details": "Edit User Profile",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "498",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_editUserProfile",
        "field_value": "تحرير ملف تعريف المستخدم",
        "field_details": "Edit User Profile",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "499",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listAlertNotification",
        "field_value": "List Alert Notification",
        "field_details": "List Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "500",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listAlertNotification",
        "field_value": "تنبيه تنبيه القائمة",
        "field_details": "List Alert Notification",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "501",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_defaultSetting",
        "field_value": "Default Settings",
        "field_details": "Default Settings",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "502",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_defaultSetting",
        "field_value": "الإعدادات الافتراضية",
        "field_details": "Default Settings",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "503",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_default",
        "field_value": "Default Report",
        "field_details": "Default Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "504",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_default",
        "field_value": "تقرير افتراضي",
        "field_details": "Default Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "505",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_entityEvents",
        "field_value": "Entity Events Report",
        "field_details": "Entity Events Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "506",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_entityEvents",
        "field_value": "تقرير أحداث الكيان",
        "field_details": "Entity Events Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "507",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_systemEvents",
        "field_value": "System Events Report",
        "field_details": "System Events Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "508",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_systemEvents",
        "field_value": "تقرير أحداث النظام",
        "field_details": "System Events Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "509",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_existingUsers",
        "field_value": "Existing Users Report",
        "field_details": "Existing Users Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "510",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_existingUsers",
        "field_value": "تقرير المستخدمين الحاليين",
        "field_details": "Existing Users Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "511",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_userLogging",
        "field_value": "User Loggin Report",
        "field_details": "User Loggin Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "512",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_userLogging",
        "field_value": "تقرير تسجيل دخول المستخدم",
        "field_details": "User Loggin Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "513",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_report_userActivityReports",
        "field_value": "User Activitiy Report",
        "field_details": "User Activitiy Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "514",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_report_userActivityReports",
        "field_value": "تقرير Activitiy المستخدم",
        "field_details": "User Activitiy Report",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "515",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listPageMatrix",
        "field_value": "Page Matrix",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "516",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listPageMatrix",
        "field_value": "مصفوفة الصفحة",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "517",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_select_username",
        "field_value": "Select Username",
        "field_details": "Select Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "518",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_select_username",
        "field_value": "حدد اسم المستخدم",
        "field_details": "Select Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "519",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_standardDashboard",
        "field_value": "Standard Dashboard",
        "field_details": "Standard Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "520",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_standardDashboard",
        "field_value": "لوحة القيادة القياسية",
        "field_details": "Standard Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "521",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_standardShowEntities",
        "field_value": "Show User Entities",
        "field_details": "Show User Entities",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "522",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_standardShowEntities",
        "field_value": "إظهار الكيانات",
        "field_details": "Show User Entities",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "523",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_standardEditUserProfile",
        "field_value": "Edit Profile",
        "field_details": "Edit Profile",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "524",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_standardEditUserProfile",
        "field_value": "تعديل الملف الشخصي",
        "field_details": "Edit Profile",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "525",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_allow",
        "field_value": "Allow",
        "field_details": "Allow",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "526",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_allow",
        "field_value": "السماح",
        "field_details": "Allow",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "527",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_deny",
        "field_value": "Deny",
        "field_details": "Deny",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "528",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_deny",
        "field_value": "أنكر",
        "field_details": "Deny",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "529",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "530",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "531",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_button_save",
        "field_value": "Save",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "532",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_button_save",
        "field_value": "حفظ",
        "field_details": "Save",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "533",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "534",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "535",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "536",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "537",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "538",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "539",
        "field_language": "en",
        "field_name": "field_listPageMatrix_heading_page_matrix_management",
        "field_value": "Page  Matrix Management",
        "field_details": "Page  Matrix Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "540",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_heading_page_matrix_management",
        "field_value": "إدارة مصفوفة الصفحة",
        "field_details": "Page  Matrix Management",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "541",
        "field_language": "en",
        "field_name": "field_listPageMatrix_button_add_page_matrix",
        "field_value": "Add Page Matrix",
        "field_details": "Add Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "542",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_button_add_page_matrix",
        "field_value": "إضافة مصفوفة الصفحة",
        "field_details": "Add Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "543",
        "field_language": "en",
        "field_name": "field_listPageMatrix_heading_page_matrix",
        "field_value": "Page Matrix",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "544",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_heading_page_matrix",
        "field_value": "مصفوفة الصفحة",
        "field_details": "Page Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "545",
        "field_language": "en",
        "field_name": "field_listPageMatrix_page_matrix_id",
        "field_value": "Page Matrix ID",
        "field_details": "Page Matrix ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "546",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_page_matrix_id",
        "field_value": "معرف مصفوفة الصفحة",
        "field_details": "Page Matrix ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "547",
        "field_language": "en",
        "field_name": "field_listPageMatrix_role",
        "field_value": "Role",
        "field_details": "Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "548",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_role",
        "field_value": "وظيفة",
        "field_details": "Role",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "549",
        "field_language": "en",
        "field_name": "field_listPageMatrix_associated_user_id",
        "field_value": "Associated User ID",
        "field_details": "Associated User ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "550",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_associated_user_id",
        "field_value": "اسم المستخدم المرتبط",
        "field_details": "Associated User ID",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "551",
        "field_language": "en",
        "field_name": "field_listPageMatrix_details",
        "field_value": "Details",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "552",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_details",
        "field_value": "تفاصيل",
        "field_details": "Details",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "553",
        "field_language": "en",
        "field_name": "field_listPageMatrix_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "554",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "555",
        "field_language": "en",
        "field_name": "field_listPageMatrix_button_delete",
        "field_value": "Delete",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "556",
        "field_language": "ar",
        "field_name": "field_listPageMatrix_button_delete",
        "field_value": "حذف",
        "field_details": "Delete",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "557",
        "field_language": "en",
        "field_name": "field_menu_link_listFormElement",
        "field_value": "Form Elements",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "558",
        "field_language": "ar",
        "field_name": "field_menu_link_listFormElement",
        "field_value": "عناصر النموذج",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "559",
        "field_language": "en",
        "field_name": "field_menu_link_listManagerUser",
        "field_value": "Show Manager User",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "560",
        "field_language": "ar",
        "field_name": "field_menu_link_listManagerUser",
        "field_value": "إظهار مستخدم المدير",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "561",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listManagerUser",
        "field_value": "Show Manager User",
        "field_details": "Show Manager User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "562",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listManagerUser",
        "field_value": "إظهار مستخدم المدير",
        "field_details": "Show Manager User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "563",
        "field_language": "en",
        "field_name": "field_listManagerUser_heading_user_management",
        "field_value": "Manager Users",
        "field_details": "Manager Users",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "564",
        "field_language": "ar",
        "field_name": "field_listManagerUser_heading_user_management",
        "field_value": "مدير المستخدمين",
        "field_details": "Manager Users",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "565",
        "field_language": "en",
        "field_name": "field_listManagerUser_username",
        "field_value": "Username",
        "field_details": "Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "566",
        "field_language": "ar",
        "field_name": "field_listManagerUser_username",
        "field_value": "اسم المستخدم",
        "field_details": "Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "567",
        "field_language": "en",
        "field_name": "field_listManagerUser_button_edit",
        "field_value": "Edit",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "568",
        "field_language": "ar",
        "field_name": "field_listManagerUser_button_edit",
        "field_value": "تعديل",
        "field_details": "Edit",
        "model_name": "9",
        "is_required": "true",
        "component_name": "listManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "569",
        "field_language": "en",
        "field_name": "field_crudManagerUser_heading",
        "field_value": "Alter users under Managed User",
        "field_details": "Alter users under Managed User",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "570",
        "field_language": "ar",
        "field_name": "field_crudManagerUser_heading",
        "field_value": "تعديل المستخدمين ضمن المستخدم المدار",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "571",
        "field_language": "en",
        "field_name": "field_crudManagerUser_username",
        "field_value": "Manager Username",
        "field_details": "Manager Username",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "572",
        "field_language": "ar",
        "field_name": "field_crudManagerUser_username",
        "field_value": "اسم مستخدم المدير",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "573",
        "field_language": "en",
        "field_name": "field_button_update",
        "field_value": "Update",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "574",
        "field_language": "ar",
        "field_name": "field_button_update",
        "field_value": "تحديث",
        "field_details": "Update",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "575",
        "field_language": "en",
        "field_name": "field_button_reset",
        "field_value": "Reset",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "576",
        "field_language": "ar",
        "field_name": "field_button_reset",
        "field_value": "إعادة تعيين",
        "field_details": "Reset",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "577",
        "field_language": "en",
        "field_name": "field_button_cancel",
        "field_value": "Cancel",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "Error"
    },
    {
        "field_id": "578",
        "field_language": "ar",
        "field_name": "field_button_cancel",
        "field_value": "إلغاء",
        "field_details": "Cancel",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudManagerUser",
        "error_message": "خطأ"
    },
    {
        "field_id": "579",
        "field_language": "en",
        "field_name": "field_menu_link_listMakerCheckerRequest",
        "field_value": "Maker Requests",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "580",
        "field_language": "ar",
        "field_name": "field_menu_link_listMakerCheckerRequest",
        "field_value": "طلبات صانع",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "581",
        "field_language": "en",
        "field_name": "field_menu_link_listApprovalMatrix",
        "field_value": "Approval Matrix",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "582",
        "field_language": "ar",
        "field_name": "field_menu_link_listApprovalMatrix",
        "field_value": "مصفوفة الموافقة",
        "field_details": "Menu Item",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "583",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listMakerCheckerRequest",
        "field_value": "Show Requests",
        "field_details": "Show Maker Checker Request",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "584",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listManagerUser",
        "field_value": "إظهار الطلبات",
        "field_details": "Show Maker Checker Request",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "585",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listApprovalMatrix",
        "field_value": "Approval Matrix",
        "field_details": "Show Approval Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "586",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listApprovalMatrix",
        "field_value": "مصفوفة الموافقة",
        "field_details": "Show Approval Matrix",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "587",
        "field_language": "en",
        "field_name": "field_crudEntity_risk_level",
        "field_value": "Risk Level",
        "field_details": "Risk Level",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "Error"
    },
    {
        "field_id": "588",
        "field_language": "ar",
        "field_name": "field_crudEntity_risk_level",
        "field_value": "مستوى الخطر",
        "field_details": "Risk Level",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudEntity",
        "error_message": "خطأ"
    },
    {
        "field_id": "589",
        "field_language": "en",
        "field_name": "field_menu_hrDashboard",
        "field_value": "HR Dashboard",
        "field_details": "HR Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "590",
        "field_language": "ar",
        "field_name": "field_menu_hrDashboard",
        "field_value": "لوحة القيادة",
        "field_details": "HR Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "591",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_hrDashboard",
        "field_value": "HR Dashboard",
        "field_details": "HR Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "592",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_hrDashboard",
        "field_value": "لوحة معلومات الموارد البشرية",
        "field_details": "HR Dashboard",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "593",
        "field_language": "en",
        "field_name": "field_menu_heading_connectors",
        "field_value": "Connectors",
        "field_details": "Connector Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "594",
        "field_language": "ar",
        "field_name": "field_menu_heading_connectors",
        "field_value": "موصلات",
        "field_details": "Connector Heading",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "595",
        "field_language": "en",
        "field_name": "field_menu_link_listPredefinedConnectors",
        "field_value": "Show Connectors",
        "field_details": "Show Connectors",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "596",
        "field_language": "ar",
        "field_name": "field_menu_link_listPredefinedConnectors",
        "field_value": "موصلات قاعدة البيانات",
        "field_details": "Show Connectors",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "597",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listPredefinedConnector",
        "field_value": "Show Connectors",
        "field_details": "Show Connectors",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "598",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listPredefinedConnector",
        "field_value": "موصلات قاعدة البيانات",
        "field_details": "Predefnied Connectors",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    },
    {
        "field_id": "599",
        "field_language": "en",
        "field_name": "field_menu_link_listAccessPolicy",
        "field_value": "Access Policies",
        "field_details": "Access Policies",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "Error"
    },
    {
        "field_id": "600",
        "field_language": "ar",
        "field_name": "field_menu_link_listAccessPolicy",
        "field_value": "سياسات الوصول",
        "field_details": "Access Policies",
        "model_name": "9",
        "is_required": "true",
        "component_name": "dashboard",
        "error_message": "خطأ"
    },
    {
        "field_id": "601",
        "field_language": "en",
        "field_name": "field_crudPageMatrix_listAccessPolicy",
        "field_value": "Access Policies",
        "field_details": "Access Policies",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "Error"
    },
    {
        "field_id": "602",
        "field_language": "ar",
        "field_name": "field_crudPageMatrix_listAccessPolicy",
        "field_value": "سياسات الوصول",
        "field_details": "Access Policies",
        "model_name": "9",
        "is_required": "true",
        "component_name": "crudPageMatrix",
        "error_message": "خطأ"
    }


]);

db.alert_notifications.drop();
db.alert_notifications.insertMany(
    [
        {
            "alert_notification_id": "1",
            "preferred_language": "en",
            "component_name": "listUser",
            "alert_name": "listUser_user_deleted_success",
            "alert_value": "User deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "2",
            "preferred_language": "ar",
            "component_name": "listUser",
            "alert_name": "listUser_user_deleted_success",
            "alert_value": "تم حذف المستخدم بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "3",
            "preferred_language": "en",
            "component_name": "listUser",
            "alert_name": "listUser_user_cannot_be_deleted",
            "alert_value": "User cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "4",
            "preferred_language": "ar",
            "component_name": "listUser",
            "alert_name": "listUser_user_cannot_be_deleted",
            "alert_value": "لا يمكن حذف المستخدم",
            "model_name": "2"
        },
        {
            "alert_notification_id": "5",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_add_success",
            "alert_value": "User added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "6",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_add_success",
            "alert_value": "تمت إضافة المستخدم بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "7",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_update_success",
            "alert_value": "User updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "8",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_update_success",
            "alert_value": "تم تحديث المستخدم بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "9",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_add_error",
            "alert_value": "Error in adding User",
            "model_name": "2"
        },
        {
            "alert_notification_id": "10",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_add_error",
            "alert_value": "خطأ في إضافة المستخدم",
            "model_name": "2"
        },
        {
            "alert_notification_id": "11",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_update_error",
            "alert_value": "Error in updating User",
            "model_name": "2"
        },
        {
            "alert_notification_id": "12",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_update_error",
            "alert_value": "خطأ في تحديث المستخدم",
            "model_name": "2"
        },
        {
            "alert_notification_id": "13",
            "preferred_language": "en",
            "component_name": "listEntity",
            "alert_name": "listEntity_Entity_deleted_success",
            "alert_value": "Entity deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "14",
            "preferred_language": "ar",
            "component_name": "listEntity",
            "alert_name": "listEntity_Entity_deleted_success",
            "alert_value": "تم حذف الكيان بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "15",
            "preferred_language": "en",
            "component_name": "listEntity",
            "alert_name": "listEntity_Entity_cannot_be_deleted",
            "alert_value": "Entity cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "16",
            "preferred_language": "ar",
            "component_name": "listEntity",
            "alert_name": "listEntity_Entity_cannot_be_deleted",
            "alert_value": "لا يمكن حذف الكيان",
            "model_name": "2"
        },
        {
            "alert_notification_id": "17",
            "preferred_language": "en",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_add_success",
            "alert_value": "Entity added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "18",
            "preferred_language": "ar",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_add_success",
            "alert_value": "تمت إضافة الكيان بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "19",
            "preferred_language": "en",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_update_success",
            "alert_value": "Entity updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "20",
            "preferred_language": "ar",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_update_success",
            "alert_value": "تم تحديث الكيان بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "21",
            "preferred_language": "en",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_add_error",
            "alert_value": "Error in adding Entity",
            "model_name": "2"
        },
        {
            "alert_notification_id": "22",
            "preferred_language": "ar",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_add_error",
            "alert_value": "خطأ في إضافة الكيان",
            "model_name": "2"
        },
        {
            "alert_notification_id": "23",
            "preferred_language": "en",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_update_error",
            "alert_value": "Error in updating Entity",
            "model_name": "2"
        },
        {
            "alert_notification_id": "24",
            "preferred_language": "ar",
            "component_name": "crudEntity",
            "alert_name": "crudEntity_entity_update_error",
            "alert_value": "خطأ في تحديث الكيان",
            "model_name": "2"
        },
        {
            "alert_notification_id": "25",
            "preferred_language": "en",
            "component_name": "listSector",
            "alert_name": "listSector_Sector_deleted_success",
            "alert_value": "Sector deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "26",
            "preferred_language": "ar",
            "component_name": "listSector",
            "alert_name": "listSector_Sector_deleted_success",
            "alert_value": "تم حذف القطاع بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "27",
            "preferred_language": "en",
            "component_name": "listSector",
            "alert_name": "listSector_Sector_cannot_be_deleted",
            "alert_value": "Sector cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "28",
            "preferred_language": "ar",
            "component_name": "listSector",
            "alert_name": "listSector_Sector_cannot_be_deleted",
            "alert_value": "لا يمكن حذف القطاع",
            "model_name": "2"
        },
        {
            "alert_notification_id": "29",
            "preferred_language": "en",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_add_success",
            "alert_value": "Sector added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "30",
            "preferred_language": "ar",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_add_success",
            "alert_value": "تمت إضافة القطاع بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "31",
            "preferred_language": "en",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_update_success",
            "alert_value": "Sector updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "32",
            "preferred_language": "ar",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_update_success",
            "alert_value": "تم تحديث القطاع بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "33",
            "preferred_language": "en",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_add_error",
            "alert_value": "Error in adding Sector",
            "model_name": "2"
        },
        {
            "alert_notification_id": "34",
            "preferred_language": "ar",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_add_error",
            "alert_value": "خطأ في إضافة القطاع",
            "model_name": "2"
        },
        {
            "alert_notification_id": "35",
            "preferred_language": "en",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_update_error",
            "alert_value": "Error in updating Sector",
            "model_name": "2"
        },
        {
            "alert_notification_id": "36",
            "preferred_language": "ar",
            "component_name": "crudSector",
            "alert_name": "crudSector_sector_update_error",
            "alert_value": "خطأ في تحديث القطاع",
            "model_name": "2"
        },
        {
            "alert_notification_id": "37",
            "preferred_language": "en",
            "component_name": "listRole",
            "alert_name": "listRole_role_deleted_success",
            "alert_value": "Role deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "38",
            "preferred_language": "ar",
            "component_name": "listRole",
            "alert_name": "listRole_role_deleted_success",
            "alert_value": "تم حذف الدور بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "39",
            "preferred_language": "en",
            "component_name": "listRole",
            "alert_name": "listRole_role_cannot_be_deleted",
            "alert_value": "Role cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "40",
            "preferred_language": "ar",
            "component_name": "listRole",
            "alert_name": "listRole_role_cannot_be_deleted",
            "alert_value": "لا يمكن حذف الدور",
            "model_name": "2"
        },
        {
            "alert_notification_id": "41",
            "preferred_language": "en",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_add_success",
            "alert_value": "Role added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "42",
            "preferred_language": "ar",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_add_success",
            "alert_value": "تمت إضافة الدور بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "43",
            "preferred_language": "en",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_update_success",
            "alert_value": "Role updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "44",
            "preferred_language": "ar",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_update_success",
            "alert_value": "تم تحديث الدور بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "45",
            "preferred_language": "en",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_add_error",
            "alert_value": "Error in adding Role",
            "model_name": "2"
        },
        {
            "alert_notification_id": "46",
            "preferred_language": "ar",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_add_error",
            "alert_value": "خطأ في إضافة الدور",
            "model_name": "2"
        },
        {
            "alert_notification_id": "47",
            "preferred_language": "en",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_update_error",
            "alert_value": "Error in updating Role",
            "model_name": "2"
        },
        {
            "alert_notification_id": "48",
            "preferred_language": "ar",
            "component_name": "crudRole",
            "alert_name": "crudRole_role_update_error",
            "alert_value": "خطأ في تحديث الدور",
            "model_name": "2"
        },
        {
            "alert_notification_id": "49",
            "preferred_language": "en",
            "component_name": "listPermission",
            "alert_name": "listPermission_permission_deleted_success",
            "alert_value": "Permission deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "50",
            "preferred_language": "ar",
            "component_name": "listPermission",
            "alert_name": "listPermission_permission_deleted_success",
            "alert_value": "تم حذف الإذن بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "51",
            "preferred_language": "en",
            "component_name": "listPermission",
            "alert_name": "listPermission_permission_cannot_be_deleted",
            "alert_value": "Permission cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "52",
            "preferred_language": "ar",
            "component_name": "listPermission",
            "alert_name": "listPermission_permission_cannot_be_deleted",
            "alert_value": "لا يمكن حذف الإذن",
            "model_name": "2"
        },
        {
            "alert_notification_id": "53",
            "preferred_language": "en",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_add_success",
            "alert_value": "Permission added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "54",
            "preferred_language": "ar",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_add_success",
            "alert_value": "تمت إضافة الإذن بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "55",
            "preferred_language": "en",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_update_success",
            "alert_value": "Permission updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "56",
            "preferred_language": "ar",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_update_success",
            "alert_value": "تم تحديث الإذن بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "57",
            "preferred_language": "en",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_add_error",
            "alert_value": "Error in adding Permission",
            "model_name": "2"
        },
        {
            "alert_notification_id": "58",
            "preferred_language": "ar",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_add_error",
            "alert_value": "خطأ في إضافة الإذن",
            "model_name": "2"
        },
        {
            "alert_notification_id": "59",
            "preferred_language": "en",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_update_error",
            "alert_value": "Error in updating Permission",
            "model_name": "2"
        },
        {
            "alert_notification_id": "60",
            "preferred_language": "ar",
            "component_name": "crudPermission",
            "alert_name": "crudPermission_permission_update_error",
            "alert_value": "خطأ في تحديث الإذن",
            "model_name": "2"
        },
        {
            "alert_notification_id": "61",
            "preferred_language": "en",
            "component_name": "listRule",
            "alert_name": "listRule_rule_deleted_success",
            "alert_value": "Rule deleted successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "62",
            "preferred_language": "ar",
            "component_name": "listRule",
            "alert_name": "listRule_rule_deleted_success",
            "alert_value": "تم حذف القاعدة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "63",
            "preferred_language": "en",
            "component_name": "listRule",
            "alert_name": "listRule_rule_cannot_be_deleted",
            "alert_value": "Rule cannot be deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "64",
            "preferred_language": "ar",
            "component_name": "listRule",
            "alert_name": "listRule_rule_cannot_be_deleted",
            "alert_value": "لا يمكن حذف القاعدة",
            "model_name": "2"
        },
        {
            "alert_notification_id": "65",
            "preferred_language": "en",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_add_success",
            "alert_value": "Rule added successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "66",
            "preferred_language": "ar",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_add_success",
            "alert_value": "تمت إضافة القاعدة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "67",
            "preferred_language": "en",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_update_success",
            "alert_value": "Rule updated successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "68",
            "preferred_language": "ar",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_update_success",
            "alert_value": "تم تحديث القاعدة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "69",
            "preferred_language": "en",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_add_error",
            "alert_value": "Error in adding Rule",
            "model_name": "2"
        },
        {
            "alert_notification_id": "70",
            "preferred_language": "ar",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_add_error",
            "alert_value": "خطأ في إضافة القاعدة",
            "model_name": "2"
        },
        {
            "alert_notification_id": "71",
            "preferred_language": "en",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_update_error",
            "alert_value": "Error in updating Rule",
            "model_name": "2"
        },
        {
            "alert_notification_id": "72",
            "preferred_language": "ar",
            "component_name": "crudRule",
            "alert_name": "crudRule_rule_update_error",
            "alert_value": "خطأ في تحديث القاعدة",
            "model_name": "2"
        },
        {
            "alert_notification_id": "73",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_alrady_exists",
            "alert_value": "User Already Exists",
            "model_name": "2"
        },
        {
            "alert_notification_id": "74",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_alrady_exists",
            "alert_value": "المستخدم موجود اصلا",
            "model_name": "2"
        },
        {
            "alert_notification_id": "75",
            "preferred_language": "en",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_does_not_exist",
            "alert_value": "User Does not exist",
            "model_name": "2"
        },
        {
            "alert_notification_id": "76",
            "preferred_language": "ar",
            "component_name": "crudUser",
            "alert_name": "crudUser_user_does_not_exist",
            "alert_value": "المستخدم غير موجود",
            "model_name": "2"
        },
        {
            "alert_notification_id": "77",
            "preferred_language": "en",
            "component_name": "editFields",
            "alert_name": "editFields_update_success",
            "alert_value": "Successfully updated the field",
            "model_name": "2"
        },
        {
            "alert_notification_id": "78",
            "preferred_language": "ar",
            "component_name": "editFields",
            "alert_name": "editFields_update_success",
            "alert_value": "تم تحديث الحقل بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "79",
            "preferred_language": "en",
            "component_name": "editFields",
            "alert_name": "editFields_update_error",
            "alert_value": "Error in updating field",
            "model_name": "2"
        },
        {
            "alert_notification_id": "80",
            "preferred_language": "ar",
            "component_name": "editFields",
            "alert_name": "editFields_update_error",
            "alert_value": "خطأ في تحديث المجال",
            "model_name": "2"
        },
        {
            "alert_notification_id": "81",
            "preferred_language": "en",
            "component_name": "emailAlert",
            "alert_name": "emailAlert_update_success",
            "alert_value": "Successfully Updated the Email Configuration",
            "model_name": "2"
        },
        {
            "alert_notification_id": "82",
            "preferred_language": "ar",
            "component_name": "emailAlert",
            "alert_name": "emailAlert_update_success",
            "alert_value": "تم تحديث تهيئة البريد الإلكتروني بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "83",
            "preferred_language": "en",
            "component_name": "emailAlert",
            "alert_name": "emailAlert_update_error",
            "alert_value": "Error in updating",
            "model_name": "2"
        },
        {
            "alert_notification_id": "84",
            "preferred_language": "ar",
            "component_name": "emailAlert",
            "alert_name": "emailAlert_update_error",
            "alert_value": "خطأ في التحديث",
            "model_name": "2"
        },
        {
            "alert_notification_id": "85",
            "preferred_language": "en",
            "component_name": "editUserProfile",
            "alert_name": "editUserProfile_update_success",
            "alert_value": "User Profile successfully updated",
            "model_name": "2"
        },
        {
            "alert_notification_id": "86",
            "preferred_language": "ar",
            "component_name": "editUserProfile",
            "alert_name": "editUserProfile_update_success",
            "alert_value": "تم تحديث ملف تعريف المستخدم بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "87",
            "preferred_language": "en",
            "component_name": "editUserProfile",
            "alert_name": "editUserProfile_update_error",
            "alert_value": "Error in updating user profile",
            "model_name": "2"
        },
        {
            "alert_notification_id": "88",
            "preferred_language": "ar",
            "component_name": "editUserProfile",
            "alert_name": "editUserProfile_update_error",
            "alert_value": "خطأ في تحديث ملف تعريف المستخدم",
            "model_name": "2"
        },
        {
            "alert_notification_id": "89",
            "preferred_language": "en",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_no_records_found",
            "alert_value": "No Records Found",
            "model_name": "2"
        },
        {
            "alert_notification_id": "90",
            "preferred_language": "ar",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_no_records_found",
            "alert_value": "لا توجد سجلات",
            "model_name": "2"
        },
        {
            "alert_notification_id": "91",
            "preferred_language": "en",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_update_success",
            "alert_value": "Successfully updated alert",
            "model_name": "2"
        },
        {
            "alert_notification_id": "92",
            "preferred_language": "ar",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_update_success",
            "alert_value": "تم تحديث التنبيه بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "93",
            "preferred_language": "en",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_update_error",
            "alert_value": "Error in updating alert",
            "model_name": "2"
        },
        {
            "alert_notification_id": "94",
            "preferred_language": "ar",
            "component_name": "alertNotification",
            "alert_name": "alertNotification_update_error",
            "alert_value": "خطأ في تحديث التنبيه",
            "model_name": "2"
        },
        {
            "alert_notification_id": "95",
            "preferred_language": "en",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_add_success",
            "alert_value": "Page Matrix added Successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "96",
            "preferred_language": "ar",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_add_success",
            "alert_value": "تمت إضافة مصفوفة الصفحة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "97",
            "preferred_language": "en",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_update_success",
            "alert_value": "Page Matrix updated Successfully",
            "model_name": "2"
        },
        {
            "alert_notification_id": "98",
            "preferred_language": "ar",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_update_success",
            "alert_value": "تم تحديث مصفوفة الصفحة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "99",
            "preferred_language": "en",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_add_error",
            "alert_value": "Error in adding Page Matrix",
            "model_name": "2"
        },
        {
            "alert_notification_id": "100",
            "preferred_language": "ar",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_add_error",
            "alert_value": "خطأ في إضافة مصفوفة الصفحة",
            "model_name": "2"
        },
        {
            "alert_notification_id": "101",
            "preferred_language": "en",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_update_error",
            "alert_value": "Error in updating Page Matrix",
            "model_name": "2"
        },
        {
            "alert_notification_id": "102",
            "preferred_language": "ar",
            "component_name": "crudPageMatrix",
            "alert_name": "crudPageMatrix_update_error",
            "alert_value": "خطأ في تحديث مصفوفة الصفحة",
            "model_name": "2"
        },
        {
            "alert_notification_id": "103",
            "preferred_language": "en",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_deleted_success",
            "alert_value": "Page Matrix was successfully deleted",
            "model_name": "2"
        },
        {
            "alert_notification_id": "104",
            "preferred_language": "ar",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_deleted_success",
            "alert_value": "تم حذف مصفوفة الصفحة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "105",
            "preferred_language": "en",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_cannot_be_deleted",
            "alert_value": "Successfully deleted Page Matrix",
            "model_name": "2"
        },
        {
            "alert_notification_id": "106",
            "preferred_language": "ar",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_cannot_be_deleted",
            "alert_value": "تم حذف مصفوفة الصفحة بنجاح",
            "model_name": "2"
        },
        {
            "alert_notification_id": "107",
            "preferred_language": "en",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_sure_to_delete",
            "alert_value": "Are you sure you want to delete this",
            "model_name": "2"
        },
        {
            "alert_notification_id": "108",
            "preferred_language": "ar",
            "component_name": "listPageMatrix",
            "alert_name": "listPageMatrix_pageMatrix_sure_to_delete",
            "alert_value": "هل أنت متأكد أنك تريد حذف هذا",
            "model_name": "2"
        }
    ]
);
