const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("./management/config/database");
const Defaults = require("./management/utils/Defaults");

const app = express();
const port = 3000;
app.use(cors());
app.use(bodyParser.json());

//initialize things 
function init() {
    Defaults.initDefaultDatabase();
}

mongoose.connect(config.database, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
mongoose.connection.on("connected", () => {
    console.log("Mongoose connected");
    init();
});
mongoose.connection.on("error", (err) => {
    console.log("Mongoose connection error:" + err);
});


//routes
const userRoutes = require('./management/routes/userRoutes');
const passwordRoutes = require('./management/routes/password-routes');
const ldapRoutes = require('./management/routes/loginRoutes/ldapRoutes');
const configRoutes = require('./management/routes/configRoutes');
const commonRoutes = require('./management/routes/commonRoutes');
const otpLoginRoutes = require('./management/routes/otpLoginRoutes');
const eventRoutes = require('./management/routes/eventRoutes');
const mssqlRoutes = require('./management/routes/loginRoutes/mssqlRoutes');
const connectorRoutes = require('./management/routes/connectorRoutes');
app.use('/api/connector', connectorRoutes);
app.use('/api/mssql', mssqlRoutes);
app.use('/api/event', eventRoutes);
app.use('/api/otpLogin', otpLoginRoutes);
app.use('/api/common', commonRoutes);
app.use('/api/config', configRoutes);
app.use('/api/ldap', ldapRoutes);
app.use('/api/user', userRoutes);
app.use('/api/password', passwordRoutes);

app.use(express.static('images'));

app.get("/", (req, res) => {
    res.send("Invalid Endpoint.");
});


app.listen(port, () => {
    console.log("Server started on port:" + port);
});
