let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let LoggingTracking = require('../models/LoggingTrackingSchema');
let SystemEvent = require('../models/SystemEventSchema');
let EntityEvent = require('../models/EntityEventSchema');
let AccessEntity = require('../models/AccessEntitySchema');
let EmailAlert = require('../models/EmailAlertSchema');
let Utils = require('../utils/Utils');
let Defaults = require('../utils/Defaults');
let constants = require('../config/constants');

//Logging Tracking
/**
 * addLoggingTracking
 */
router.post("/addLoggingTracking", async function (req, res) {
    logger.logString("Enter: addLoggingTracking");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let loggingTracking = new LoggingTracking(req.body);
        let result = await loggingTracking.save();
        logger.logStringWithObj("Success addLoggingTracking", result);
        response.success(req, res, "Success addLoggingTracking", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in addLoggingTracking", error.message);
        response.serverError(req, res, "Error in addLoggingTracking", error);
    }
});


router.post("/updateLoggingTracking", async function (req, res) {
    logger.logString("Enter: updateLoggingTracking");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let result = await LoggingTracking.findOneAndUpdate({ 'logging_tracking_id': req.body.logging_tracking_id }, req.body);
        logger.logStringWithObj("Success updateLoggingTracking", result);
        response.success(req, res, "Success updateLoggingTracking", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in updateLoggingTracking", error.message);
        response.serverError(req, res, "Error in updateLoggingTracking", error);
    }
});


router.get("/getAllLoggingTracking", async function (req, res) {
    logger.logString("Enter: getAllLoggingTracking");
    try {
        logger.logObj(req.query);
        let result = await LoggingTracking.find().sort({ login_time: "desc" });
        logger.logSuccessEvent("Success getAllLoggingTracking", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllLoggingTracking", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllLoggingTracking", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllLoggingTracking", error);
    }
});


router.get("/getLastLoginTrackingOfUsername", async function (req, res) {
    logger.logString("Enter: getLastLoginTrackingOfUsername");
    try {
        logger.logObj(req.query);
        let usernameToFind = req.query.usernameToFind;
        let result = await LoggingTracking.findOne({ "username": usernameToFind, "is_login_successful": true }).sort({ login_time: -1 });
        logger.logSuccessEvent("Success getLastLoginTrackingOfUsername", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getLastLoginTrackingOfUsername", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getLastLoginTrackingOfUsername", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getLastLoginTrackingOfUsername", error);
    }
});


//System Events
router.get("/getAllSystemEvents", async function (req, res) {
    logger.logString("Enter: getAllSystemEvents");
    try {
        logger.logObj(req.query);
        let queryLimit = Number(req.query.limit);
        let result = null;
        if (queryLimit == -1)
            result = await SystemEvent.find().sort({ event_time: "desc" });
        else
            result = await SystemEvent.find().limit(queryLimit).sort({ event_time: "desc" });
        logger.logSuccessEvent("Success getAllSystemEvents", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllSystemEvents", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllSystemEvents", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllSystemEvents", error);
    }
});


router.get("/getSpecificEntityEvents", async function (req, res) {
    logger.logString("Enter: getSpecificEntityEvents");
    try {
        logger.logObj(req.query);
        let queryLimit = Number(req.query.limit);
        let accessed_username = req.query.accessed_username;
        let entity_id = req.query.entity_id;
        let risk_level_id = req.query.risk_level_id;

        let query = EntityEvent.find();
        if (accessed_username != "-1") query.where("accessed_username").equals(accessed_username);
        if (entity_id != "-1") query.where("entity_id").equals(entity_id);
        if (risk_level_id != "-1") query.where("risk_level_id").equals(risk_level_id);
        // let result = await query.exec().limit(queryLimit).sort({ event_time: -1 });
        let result = await query.sort({ "time_of_access": "desc" });
        // let result = await query.exec();

        // let result = null;
        // if (queryLimit == -1)
        //     result = await EntityEvent.find();
        // else
        //     result = await EntityEvent.find().limit(queryLimit).sort({ event_time: -1 });
        logger.logSuccessEvent("Success getSpecificEntityEvents", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificEntityEvents", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificEntityEvents", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificEntityEvents", error);
    }
});


router.get("/getEmailAlert", async function (req, res) {
    logger.logString("Enter: getEmailAlert");
    try {
        logger.logObj(req.query);
        let result = await EmailAlert.findOne();
        logger.logSuccessEvent("Success getEmailAlert", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getEmailAlert", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getEmailAlert", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getEmailAlert", error);
    }
});


router.post("/updateEmailAlert", async function (req, res) {
    logger.logString("Enter: updateEmailAlert");
    try {
        logger.logObj(req.body);
        let result = await EmailAlert.updateOne(req.body);
        logger.logSuccessEvent("Success updateEmailAlert", result, req.query, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateEmailAlert", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in updateEmailAlert", error.message);
        response.serverError(req, res, "Error in updateEmailAlert", error);
    }
});


router.post("/addSystemEvent", async function (req, res) {
    logger.logString("Enter: addSystemEvent");
    try {
        logger.logObj(req.body);
        let systemEvent = new SystemEvent(req.body);
        systemEvent.event_id = Utils.smallUID();
        let result = await systemEvent.save();
        logger.logStringWithObj("Success addSystemEvent", result);
        response.success(req, res, "Success addSystemEvent", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in addSystemEvent", error.message);
        response.serverError(req, res, "Error in addSystemEvent", error);
    }
});


router.post("/addEntityEvent", async function (req, res) {
    logger.logString("Enter: addEntityEvent");
    try {
        logger.logObj(req.body);
        let entityEvent = new EntityEvent(req.body);
        entityEvent.entity_event_id = Utils.smallUID();
        let result = await entityEvent.save();
        logger.logStringWithObj("Success addEntityEvent", result);
        response.success(req, res, "Success addEntityEvent", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in addEntityEvent", error.message);
        response.serverError(req, res, "Error in addEntityEvent", error);
    }
});


router.post("/addAccessEntity", async function (req, res) {
    logger.logString("Enter: addAccessEntity");
    try {
        logger.logObj(req.body);
        let accessEntity = new AccessEntity(req.body);
        accessEntity.access_entity_id = Utils.smallUID();
        let result = await accessEntity.save();
        logger.logStringWithObj("Success addAccessEntity", result);
        response.success(req, res, "Success addAccessEntity", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in addAccessEntity", error.message);
        response.serverError(req, res, "Error in addAccessEntity", error);
    }
});

router.post("/updateAccessEntity", async function (req, res) {
    logger.logString("Enter: updateAccessEntity");
    try {
        logger.logObj(req.body);
        let result = await AccessEntity.updateOne({ 'access_entity_id': req.body.access_entity_id }, req.body);
        logger.logStringWithObj("Success updateAccessEntity", result);
        response.success(req, res, "Success updateAccessEntity", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in updateAccessEntity", error.message);
        response.serverError(req, res, "Error in updateAccessEntity", error);
    }
});

router.get("/getAllAccessEntity", async function (req, res) {
    logger.logString("Enter: getAllAccessEntity");
    try {
        logger.logObj(req.query);
        let queryLimit = Number(req.query.limit);
        let result = null;
        if (queryLimit == -1)
            result = await AccessEntity.find().sort({ request_time: -1 });
        else
            result = await AccessEntity.find().limit(queryLimit).sort({ request_time: -1 });
        logger.logSuccessEvent("Success getAllAccessEntity", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllAccessEntity", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllAccessEntity", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllAccessEntity", error);
    }
});

router.get("/getSpecificAccessEntityByCurrentStatus", async function (req, res) {
    logger.logString("Enter: getSpecificAccessEntityByCurrentStatus");
    try {
        logger.logObj(req.query);
        let current_status = Number(req.query.current_status);
        let result = null;
        result = await AccessEntity.find({ 'current_status': current_status }).sort({ request_time: -1 });
        logger.logSuccessEvent("Success getSpecificAccessEntityByCurrentStatus", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificAccessEntityByCurrentStatus", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificAccessEntityByCurrentStatus", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificAccessEntityByCurrentStatus", error);
    }
});

//////////
module.exports = router;