let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let UserDetail = require('../models/UserDetailSchema');
let MappingUserEntity = require('../models/MappingUserEntitySchema');
let RoleDetail = require('../models/RoleDetailSchema');
let AccessPolicy = require('../models/AccessPolicySchema');
let Utils = require('../utils/Utils');
let constants = require('../config/constants');
let Defaults = require('../utils/Defaults');
let EntityDetail = require('../models/EntityDetailSchema');
let SearchUserDetailDBModel = require('../models/SearchUserDetailDBModel');

let passwordHash = require('password-hash');
let utils = new Utils();

router.post("/authenticateUser", async function (req, res) {
    logger.logString("Enter: authenticateUser");
    try {
        logger.logObj(req.body);
        let username = req.body.username;
        let nonHashedPassword = req.body.password;
        let userDetail = await UserDetail.findOne({ 'username': username });
        if (userDetail == null) {
            throw new Error("Invalid username.");
        }
        let isPasswordValid = passwordHash.verify(nonHashedPassword, userDetail.password);
        if (!isPasswordValid) {
            throw new Error("Username is correct but Password is invalid.");
        }
        return await utils.commonSuccessReturn(req, res, "authenticateUser", userDetail, req.query, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "authenticateUser", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.get("/getSpecificUserDetailFromUsername", async function (req, res) {
    logger.logString("Enter: getSpecificUserDetailFromUsername");
    try {
        logger.logObj(req.query);
        let username = req.query.username;
        let result = await UserDetail.findOne({ 'username': username });
        return await utils.commonSuccessReturn(req, res, "getSpecificUserDetailFromUsername", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getSpecificUserDetailFromUsername", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.get("/getUsersOfManagerUser", async function (req, res) {
    logger.logString("Enter: getUsersOfManagerUser");
    try {
        logger.logObj(req.query);
        let parent_user_id = req.query.parent_user_id;
        let result = await UserDetail.find({ 'parent_user_id': parent_user_id });
        return await utils.commonSuccessReturn(req, res, "getUsersOfManagerUser", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getUsersOfManagerUser", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.get("/getUsersForHRUser", async function (req, res) {
    logger.logString("Enter: getUsersForHRUser");
    try {
        logger.logObj(req.query);
        let result = await UserDetail.find({ 'user_type': constants.USER_TYPES.STANDARD });
        return await utils.commonSuccessReturn(req, res, "getUsersForHRUser", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getUsersForHRUser", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

/**
 * Get unmanaged user except the admin
 */
router.get("/getUnmanagedUsers", async function (req, res) {
    logger.logString("Enter: getUnmanagedUsers");
    try {
        logger.logObj(req.query);
        let calling_user_id = req.query.calling_user_id;
        let result = await UserDetail.find({
            $and: [
                {
                    $or: [
                        { 'parent_user_id': constants.DEFAULT_PARENT_USER_ID.ADMIN },
                        { 'parent_user_id': calling_user_id }
                    ],
                },
                { 'user_id': { $ne: calling_user_id } },
                { 'user_type': { $ne: constants.USER_TYPES.MANAGER } }
            ]
        });
        return await utils.commonSuccessReturn(req, res, "getUnmanagedUsers", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getUnmanagedUsers", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.post("/updateManagersForUsers", async function (req, res) {
    logger.logString("Enter: updateManagersForUsers");
    try {
        logger.logObj(req.body);
        let parent_user_id = req.body.parent_user_id;
        let managerUserIdArr = req.body.managerUserIdArr;
        let defaultParentUserId = constants.DEFAULT_PARENT_USER_ID.ADMIN;
        let result = null;

        //1- get existing managed users and make their parent_user_id to be default
        let oldManagerUserDetailArr = await UserDetail.find({ 'parent_user_id': parent_user_id });
        logger.logStringWithObj("oldManagerUserDetailArr::", oldManagerUserDetailArr);
        for (let k = 0; k < oldManagerUserDetailArr.length; k++) {
            result = await UserDetail.updateOne({ 'user_id': oldManagerUserDetailArr[k].user_id }, { $set: { 'parent_user_id': defaultParentUserId } });
            logger.logStringWithObj("result::", result);
        }

        //2- add new parent_user_id
        for (let k = 0; k < managerUserIdArr.length; k++) {
            result = await UserDetail.updateOne({ 'user_id': managerUserIdArr[k] }, { $set: { 'parent_user_id': parent_user_id } });
            logger.logStringWithObj("result::", result);
        }

        return await utils.commonSuccessReturn(req, res, "updateManagersForUsers", result, req.query, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "updateManagersForUsers", error, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.get("/getSpecificUserDetail", async function (req, res) {
    logger.logString("Enter: getSpecificUserDetail");
    try {
        logger.logObj(req.query);
        logger.logObj(req.body);
        let user_id = req.query.user_id;
        let result = await UserDetail.findOne({ 'user_id': user_id });
        return await utils.commonSuccessReturn(req, res, "getSpecificUserDetail", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getSpecificUserDetail", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});



router.get("/getAllUserDetail", async function (req, res) {
    logger.logString("Enter: getAllUserDetail");
    try {
        logger.logObj(req.body);
        let result = await UserDetail.find();
        return await utils.commonSuccessReturn(req, res, "getAllUserDetail", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getAllUserDetail", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.post("/addUserDetail", async function (req, res) {
    logger.logString("Enter: addUserDetail");
    try {
        logger.logObj(req.body);
        let userDetail = new UserDetail(req.body);
        let userExists = await UserDetail.findOne({ 'username': userDetail.username });
        if (userExists || userExists != null) {
            logger.logString("User Already Exists");
            response.existsAlready(req, res, "User Already Exists", "User Already Exists");
        } else {
            userDetail.user_id = Utils.smallUID();

            //do the hashing when adding to AD
            // userDetail.password = Utils.getHashedPassword(userDetail.password);

            let result = await userDetail.save();

            //initialize user 
            Defaults.initUserDetailSettings(userDetail.user_id);
            return await utils.commonSuccessReturn(req, res, "addUserDetail", result, req.query, constants.EVENT_TYPES.CREATE);
        }
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "addUserDetail", error, constants.EVENT_TYPES.CREATE, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.post("/updateUserDetail", async function (req, res) {
    logger.logString("Enter: updateUserDetail");
    try {
        logger.logObj(req.body);
        let updatedUserDetail = new UserDetail(req.body);
        logger.logStringWithObj("updatedUserDetail::", updatedUserDetail);
        //currently don't update the password
        let tmpUserDetail = await UserDetail.findOne({ 'user_id': updatedUserDetail.user_id });

        updatedUserDetail.password = tmpUserDetail.password;
        let result = await UserDetail.updateOne({ 'user_id': updatedUserDetail.user_id }, updatedUserDetail);
        logger.logStringWithObj("updatedUserDetail:result:", result);
        return await utils.commonSuccessReturn(req, res, "updateUserDetail", result, req.query, constants.EVENT_TYPES.UPDATE);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "updateUserDetail", error, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.post("/deleteUserDetail", async function (req, res) {
    logger.logString("Enter: deleteUserDetail");
    try {
        logger.logObj(req.body);
        // let result = await UserDetail.updateOne({ 'user_id': req.body.user_id }, { 'is_deleted': true });
        let result = await UserDetail.deleteOne({ 'user_id': req.body.user_id }, req.body);
        return await utils.commonSuccessReturn(req, res, "deleteUserDetail", result, req.query, constants.EVENT_TYPES.DELETE);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "deleteUserDetail", error, constants.EVENT_TYPES.DELETE, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.post("/updateMappingUserEntity", async function (req, res) {
    logger.logString("Enter: updateMappingUserEntity");
    try {
        logger.logObj(req.body);
        let result = await MappingUserEntity.updateOne({ 'mapping_user_entity_id': req.body.mapping_user_entity_id }, req.body);
        return await utils.commonSuccessReturn(req, res, "updateMappingUserEntity", result, req.query, constants.EVENT_TYPES.UPDATE);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "updateMappingUserEntity", error, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.get("/getSpecificMappingUserEntity", async function (req, res) {
    logger.logString("Enter: getSpecificMappingUserEntity");
    try {
        logger.logObj(req.query);
        let mapping_user_entity_id = req.query.mapping_user_entity_id;
        let result = await MappingUserEntity.findOne({ 'mapping_user_entity_id': mapping_user_entity_id });
        return await utils.commonSuccessReturn(req, res, "getSpecificMappingUserEntity", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getSpecificMappingUserEntity", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.get("/getAllEntityDetailsOfMappingUser", async function (req, res) {
    logger.logString("Enter: getAllEntityDetailsOfMappingUser");
    try {
        logger.logObj(req.query);
        let user_id = req.query.user_id;
        let resultEntityIdArr = await MappingUserEntity.find({ 'user_id': user_id }, { _id: 0, entity_id: 1 });
        logger.logStringWithObj("Entity IDS received:", resultEntityIdArr);
        let entityIdArr = [];
        for (let k = 0; k < resultEntityIdArr.length; k++) {
            entityIdArr.push(resultEntityIdArr[k].entity_id);
        }
        logger.logStringWithObj("Entities to search for:", entityIdArr);
        let result = await EntityDetail.find({ 'entity_id': { $in: entityIdArr } });
        logger.logStringWithObj("RRRRRRRRR:", result);
        return await utils.commonSuccessReturn(req, res, "getAllEntityDetailsOfMappingUser", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getAllEntityDetailsOfMappingUser", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.get("/getMappingUserEntityOfUserId", async function (req, res) {
    logger.logString("Enter: getMappingUserEntityOfUserId");
    try {
        logger.logObj(req.query);
        let user_id = req.query.user_id;
        let result = await MappingUserEntity.find({ 'user_id': user_id });
        return await utils.commonSuccessReturn(req, res, "getMappingUserEntityOfUserId", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getMappingUserEntityOfUserId", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});


router.get("/getRoleDetailsOfSpecificUserId", async function (req, res) {
    logger.logString("Enter: getRoleDetailsOfSpecificUserId");
    try {
        logger.logObj(req.query);
        let user_id = req.query.user_id;
        let result = await MappingUserEntity.find({ 'user_id': user_id }, { _id: 0, role_id: 1 });
        let matchingRoleIdArr = [];
        for (let k = 0; k < result.length; k++) {
            matchingRoleIdArr.push(result[k].role_id);
        }
        matchingRoleIdArr = Utils.removeDuplicatesFromArray(matchingRoleIdArr);
        result = await RoleDetail.find({ 'role_id': { $in: matchingRoleIdArr } });
        return await utils.commonSuccessReturn(req, res, "getRoleDetailsOfSpecificUserId", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getRoleDetailsOfSpecificUserId", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});
// router.get("/refreshMappingUserEntity_orig", async function (req, res) {
//     logger.logString("Enter:refreshMappingUserEntity");
//     try {
//         logger.logObj(req.query);
//         let result = null;
//         let user_id = req.query.user_id;
//         //1 - deprovision the user from the entities it is currently connected to
//         //2 - delete all entries of this user from the map
//         //3 - add new entry with all access as pending

//         //1 - BIG TASK

//         //2
//         let r1 = await MappingUserEntity.deleteMany({ 'user_id': user_id })
//         logger.logStringWithObj("configureMap:deleted user ", r1);

//         //3
//         let userDetail = await UserDetail.findOne({ 'user_id': user_id });
//         logger.logStringWithObj("configureMap:userDetail", userDetail);
//         let role_id = userDetail.role_id;
//         let roleDetail = await RoleDetail.findOne({ 'role_id': role_id });
//         logger.logStringWithObj("configureMap:roleDetail", roleDetail);
//         let allowed_entity_id_arr = roleDetail.allowed_entity_id_arr;
//         logger.logStringWithObj("configureMap:allowed_entity_id_arr", allowed_entity_id_arr);
//         if (allowed_entity_id_arr.length > 0) {
//             for (let k = 0; k < allowed_entity_id_arr.length; k++) {
//                 let entity_id = allowed_entity_id_arr[k];
//                 let mappingUserEntity = new MappingUserEntity();
//                 mappingUserEntity.mapping_user_entity_id = Utils.smallUID();
//                 mappingUserEntity.user_id = user_id;
//                 mappingUserEntity.entity_id = entity_id;
//                 mappingUserEntity.role_id = role_id;
//                 mappingUserEntity.current_status = constants.ACCESS_ENTITY_STATUS.PENDING;
//                 result = await mappingUserEntity.save();
//             };
//         }
//         logger.logSuccessEvent("Success refreshMappingUserEntity", result, req.body, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
//         response.success(req, res, "Success refreshMappingUserEntity", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in refreshMappingUserEntity:" + error.message, error, req, constants.EVENT_TYPES.UPDATE);
//         response.serverError(req, res, "Error in refreshMappingUserEntity", error);
//     }
// });


// router.get("/refreshMappingUserEntity", async function (req, res) {
//     logger.logString("Enter:refreshMappingUserEntity");
//     try {
//         logger.logObj(req.query);
//         let result = null;
//         let user_id = req.query.user_id;
//         //1 - deprovision the user from the entities it is currently connected to
//         //2 - delete all entries of this user from the map
//         //3 - add new entry with all access as pending

//         //1 - BIG TASK

//         //2
//         let r1 = await MappingUserEntity.deleteMany({ 'user_id': user_id })
//         logger.logStringWithObj("configureMap:deleted user ", r1);

//         //3
//         let userDetail = await UserDetail.findOne({ 'user_id': user_id });
//         logger.logStringWithObj("configureMap:userDetail", userDetail);
//         let username = userDetail.username;
//         let role_id = userDetail.role_id;

//         let roleDetail = await RoleDetail.findOne({ 'role_id': role_id });
//         logger.logStringWithObj("configureMap:roleDetail", roleDetail);

//         if (roleDetail == null) {
//             logger.logString("Role details is null for this user.");
//         } else {
//             //from the access given to the user (as per access policy) find the respective entities and then add it
//             let allowed_access_policy_id_arr = roleDetail.allowed_access_policy_id_arr;
//             logger.logStringWithObj("configureMap:allowed_entity_id_arr", allowed_access_policy_id_arr);
//             if (allowed_access_policy_id_arr.length > 0) {
//                 for (let k = 0; k < allowed_access_policy_id_arr.length; k++) {
//                     let accessPolicy = await AccessPolicy.findOne({ 'access_policy_id': allowed_access_policy_id_arr[k] });
//                     if (accessPolicy.entity_id_arr && accessPolicy.entity_id_arr.length > 0) {
//                         for (let z = 0; z < accessPolicy.entity_id_arr.length; z++) {
//                             logger.logString("Putting entity_id into MappingUserEntity");
//                             let entity_id = accessPolicy.entity_id_arr[z];
//                             let mappingUserEntity = new MappingUserEntity();
//                             mappingUserEntity.mapping_user_entity_id = Utils.smallUID();
//                             mappingUserEntity.user_id = user_id;
//                             mappingUserEntity.username = username;
//                             mappingUserEntity.entity_id = entity_id;
//                             mappingUserEntity.role_id = role_id;
//                             mappingUserEntity.current_status = constants.ACCESS_ENTITY_STATUS.PENDING;
//                             result = await mappingUserEntity.save();
//                             logger.logStringWithObj("Just saved Mapping User Entity:", mappingUserEntity);
//                         }
//                     }
//                 };
//             }
//         }

//         logger.logSuccessEvent("Success refreshMappingUserEntity", result, req.body, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
//         response.success(req, res, "Success refreshMappingUserEntity", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in refreshMappingUserEntity:" + error.message, error, req, constants.EVENT_TYPES.UPDATE);
//         response.serverError(req, res, "Error in refreshMappingUserEntity", error);
//     }
// });



router.get("/getAdvancedUsersSearchValues", async function (req, res) {
    logger.logString("Enter: getAdvancedUsersSearchValues");
    try {
        logger.logStringWithObj("query", req.query);
        let search_by = req.query.search_by;
        let result = null;

        if (search_by == constants.ADVANCED_USERS_SEARCH_BY.ROLE_NAME) {
            result = await RoleDetail.aggregate([
                {
                    "$project": {
                        "_id": 0,
                        "id": "$role_id",
                        "value": "$role_name"
                    }
                }
            ]);
        } else if (search_by == constants.ADVANCED_USERS_SEARCH_BY.ACCESS_POLICY_NAME) {
            // result = await AccessPolicy.find({}, { _id: 0, access_policy_id: 1, access_policy_name: 1 });
            result = await AccessPolicy.aggregate([
                {
                    "$project": {
                        "_id": 0,
                        "id": "$access_policy_id",
                        "value": "$access_policy_name"
                    }
                }
            ]);
        } else if (search_by == constants.ADVANCED_USERS_SEARCH_BY.ENTITY_NAME) {
            // result = await EntityDetail.find({}, { _id: 0, entity_id: 1, entity_name: 1 });
            result = await EntityDetail.aggregate([
                {
                    "$project": {
                        "_id": 0,
                        "id": "$entity_id",
                        "value": "$entity_name"
                    }
                }
            ]);
        }
        return await utils.commonSuccessReturn(req, res, "getAdvancedUsersSearchValues", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getAdvancedUsersSearchValues", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});



router.post("/searchUsers", async function (req, res) {
    logger.logString("Enter: searchUsers");
    try {
        logger.logObj(req.body);
        let searchUserDetail = new SearchUserDetailDBModel(req.body);
        logger.logStringWithObj("searchUserDetail::", searchUserDetail);

        let query = UserDetail.find();
        if (searchUserDetail.username != "") query.where('username').equals({ $regex: '.*' + searchUserDetail.username + '.*', $options: 'i' });
        if (searchUserDetail.is_added_to_main != "") {
            if (searchUserDetail.is_added_to_main == constants.SEARCH_USER_BOOLEAN_OPTION.TRUE) query.where('is_added_to_main').equals("true");
            else if (searchUserDetail.is_added_to_main == constants.SEARCH_USER_BOOLEAN_OPTION.FALSE) query.where('is_added_to_main').equals("false");
        }
        if (searchUserDetail.user_id != "") query.where('user_id').equals({ $regex: '.*' + searchUserDetail.user_id + '.*', $options: 'i' });
        if (searchUserDetail.email != "") query.where('email').equals({ $regex: '.*' + searchUserDetail.email + '.*', $options: 'i' });
        if (searchUserDetail.first_name != "") query.where('first_name').equals({ $regex: '.*' + searchUserDetail.first_name + '.*', $options: 'i' });
        if (searchUserDetail.last_name != "") query.where('last_name').equals({ $regex: '.*' + searchUserDetail.last_name + '.*', $options: 'i' });
        if (searchUserDetail.active != "") {
            if (searchUserDetail.active == constants.SEARCH_USER_BOOLEAN_OPTION.TRUE) query.where('is_active').equals("true");
            else if (searchUserDetail.active == constants.SEARCH_USER_BOOLEAN_OPTION.FALSE) query.where('is_active').equals("false");
        }
        if (searchUserDetail.locked != "") {
            if (searchUserDetail.locked == constants.SEARCH_USER_BOOLEAN_OPTION.TRUE) query.where('is_locked').equals("true");
            else if (searchUserDetail.locked == constants.SEARCH_USER_BOOLEAN_OPTION.FALSE) query.where('is_locked').equals("false");
        }
        if (searchUserDetail.department != "") query.where('department').equals(searchUserDetail.department);
        if (searchUserDetail.job_title != "") query.where('job_title').equals(searchUserDetail.job_title);

        //based on user_type send appropriate users
        if (searchUserDetail.source_user_type == constants.USER_TYPES.HR) {
            query.where('user_type').equals(constants.USER_TYPES.STANDARD);
        }

        let result = await query.exec();
        return await utils.commonSuccessReturn(req, res, "searchUsers", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "searchUsers", error, constants.EVENT_TYPES.READ, constants.ALERT_LEVEL.MEDIUM);
    }
});

router.get("/getAllUsersOfType", async function (req, res) {
    logger.logString("Enter: getAllUsersOfType");
    try {
        logger.logObj(req.query);
        let user_type = req.query.user_type;
        let result = await UserDetail.find({ 'user_type': user_type });
        return await utils.commonSuccessReturn(req, res, "getAllUsersOfType", result, req.query, constants.EVENT_TYPES.READ);
    } catch (error) {
        return await utils.commonErrorReturn(req, res, "getAllUsersOfType", error, constants.EVENT_TYPES.READ);
    }
});

//////////
module.exports = router;
