let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let FieldDetail = require('../models/FieldDetailSchema');
let Utils = require('../utils/Utils');
let constants = require('../config/constants');
let Defaults = require('../utils/Defaults');
let UserDetail = require('../models/UserDetailSchema');
let EntityDetail = require('../models/EntityDetailSchema');
let SectorDetail = require('../models/SectorDetailSchema');
let RoleDetail = require('../models/RoleDetailSchema');
let RuleDetail = require('../models/RuleDetailSchema');
let PermissionDetail = require('../models/PermissionDetailSchema');
let AccessPolicy = require('../models/AccessPolicySchema');
let MappingUserEntity = require('../models/MappingUserEntitySchema');


router.get("/getFieldDetailsForComponent", async function (req, res) {
    logger.logString("Enter: getFieldDetailsForComponent");
    try {
        logger.logStringWithObj("query", req.query);
        let component_name = req.query.component_name;
        let field_languare = req.query.field_language;
        let result = await FieldDetail.find({ "component_name": component_name, "field_language": field_languare });
        logger.logSuccessEvent("Success getFieldDetailsForComponent", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getFieldDetailsForComponent", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getFieldDetailsForComponent", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getFieldDetailsForComponent", error);
    }
});

router.post("/updateFieldDetail", async function (req, res) {
    logger.logString("Enter: updateFieldDetail");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let result = await FieldDetail.updateOne({ 'field_id': req.body.field_id }, req.body);
        await logger.logSuccessEvent("Success updateFieldDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateFieldDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateFieldDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateFieldDetail", error);
    }
});



router.get("/getModuleIdNameArray", async function (req, res) {
    logger.logString("Enter: getModuleIdNameArray");
    try {
        logger.logStringWithObj("query", req.query);
        let module_name_id = req.query.module_name_id;
        let result = null;
        let module_name_number = Number(module_name_id);
        switch (module_name_number) {
            case constants.FRONTEND_MODULE_NAME_ID.USER:
                result = await UserDetail.find({}, { _id: 0, user_id: 1, username: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.ENTITY:
                result = await EntityDetail.find({}, { _id: 0, entity_id: 1, entity_name: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.SECTOR:
                result = await SectorDetail.find({}, { _id: 0, sector_id: 1, sector_name: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.ROLE:
                result = await RoleDetail.find({}, { _id: 0, role_id: 1, role_name: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.PERMISSION:
                result = await PermissionDetail.find({}, { _id: 0, permission_id: 1, permission_name: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.RULE:
                result = await RuleDetail.find({}, { _id: 0, rule_id: 1, rule_name: 1 });
                break;
            case constants.FRONTEND_MODULE_NAME_ID.ACCESS_POLICY:
                result = await AccessPolicy.find({}, { _id: 0, access_policy_id: 1, access_policy_name: 1 });
                break;
            // case constants.FRONTEND_MODULE_NAME_ID.MAPPING_USER_ENTITY:
            //     result = await MappingUserEntity.find({}, { _id: 0, mapping_user_entity_id: 1, details: 1 });
            //     break;
            default:
                logger.logString("getModuleIdNameArray:Invalid Module Name");
                break;
        }
        // logger.logStringWithObj("getModuleIdNameArray:result:", result);
        logger.logSuccessEvent("Success getModuleIdNameArray", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getModuleIdNameArray", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getModuleIdNameArray", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getModuleIdNameArray", error);
    }
});


//send mail
router.post("/sendEmail", async function (req, res) {
    try {
        logger.logString("Enter:sendEmail");
        logger.logObj(req.body);

        let to_email = req.body.to_email;
        let subject = req.body.subject;
        let text = req.body.text;
        await Utils.sendEmail(to_email, subject, text, true);
        logger.logSuccessEvent("Success sendEmail", constants.RESPONSE_MESSAGE.SUCCESS, req.query, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success sendEmail", constants.RESPONSE_MESSAGE.SUCCESS);
    } catch (error) {
        await logger.logErrorEvent("Error in sendEmail :" + error.message, error, req, constants.EVENT_TYPES.UPDATE_ERROR);
        response.serverError(req, res, "Error in sendEmail", error);
    }
});


//fileuploading
var multer = require('multer');
var fullPathOfFileStorage = constants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS;
const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, fullPathOfFileStorage)
    },
    filename: (req, file, callBack) => {
        // customFileName = Utils.getCurrentUTCTimestamp() + "_" + file.originalname;
        customFileName = file.originalname;
        console.log("customfilename:::: " + customFileName)
        callBack(null, `${customFileName}`);
    }
});

const upload = multer({ storage: storage })
router.post('/uploadFile', upload.single('newFileToSave'), (req, res, next) => {
    logger.logErrorStringWithObj("uploadFile: params:", req.params);
    const file = req.file;
    console.log(file.filename);
    if (!file) {
        const error = new Error('No File')
        error.httpStatusCode = 400
        return next(error)
    }
    logger.logSuccessEvent("Success uploadFile", file, req.body, constants.EVENT_TYPES.CREATE);
    response.success(req, res, "Success uploadFile", file);
});


// router.post("/testCommon", async function (req, res) {
//     logger.logString("testCommon: Enter");
//     try {
//         logger.logObj(req.params);
//         logger.logObj(req.body);
//         // throw new Error("TESTING.....");
//         // Defaults.addSuccessSystemEvent(req, constants.EVENT_TYPES.READ);
//         // logger.logStringWithObj("Success testCommon", constants.DEFAULT_MESSAGES.SUCCESS);
//         logger.logSuccessEvent("Success testCommon", constants.DEFAULT_MESSAGES.SUCCESS, req, constants.EVENT_TYPES.READ);
//         response.success(req, res, "Success testCommon", constants.DEFAULT_MESSAGES.SUCCESS);
//     } catch (error) {
//         // Defaults.addErrorSystemEvent(req, constants.EVENT_TYPES.READ, error);
//         await logger.logErrorEvent("Error testCommon", error.message, req, constants.EVENT_TYPES.READ);
//         response.serverError(req, res, "Error testCommon", error);
//     }
// });


// router.get("/testGetCommon", async function (req, res) {
//     logger.logString("testGetCommon: Enter");
//     try {
//         logger.logStringWithObj("params", req.params);
//         logger.logStringWithObj("body", req.body);
//         // throw new Error("TESTING.....");
//         Defaults.addSuccessSystemEvent(req.body, constants.EVENT_TYPES.READ);
//         // logger.logStringWithObj("Success testGetCommon", constants.DEFAULT_MESSAGES.SUCCESS);
//         logger.logSuccessEvent("Success testGetCommon", constants.DEFAULT_MESSAGES.SUCCESS, req, constants.EVENT_TYPES.READ);
//         response.success(req, res, "Success testGetCommon", constants.DEFAULT_MESSAGES.SUCCESS);
//     } catch (error) {
//         // Defaults.addErrorSystemEvent(req, constants.EVENT_TYPES.READ, error);
//         await logger.logErrorEvent("Error testGetCommon", error.message, req, constants.EVENT_TYPES.READ);
//         response.serverError(req, res, "Error testGetCommon", error);
//     }
// });



//////////
module.exports = router;
