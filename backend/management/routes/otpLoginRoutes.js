let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let UserDetail = require('../models/UserDetailSchema');
let Utils = require('../utils/Utils');
let Defaults = require('../utils/Defaults');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const jwt = require("jsonwebtoken");
let constants = require("../config/constants");

router.get('/otpSetup/:user_id', async function (req, res) {
    logger.logString("otpSetup:Entered");
    logger.logObj(req.params);
    let userDetail = await UserDetail.findOne({ 'user_id': req.params.user_id });
    let username = userDetail.username;
    // let customIssuer = "Ogoul IAM (" + Utils.getCurrentUTCTimestamp() + ")";
    let customIssuer = "Ogoul IAM";
    const secret = speakeasy.generateSecret({
        length: 10,
        name: username,
        issuer: customIssuer
    });
    var url = speakeasy.otpauthURL({
        secret: secret.base32,
        label: username,
        issuer: customIssuer,
        encoding: 'base32'
    });


    QRCode.toDataURL(url, (err, dataURL) => {
        tfa = {
            secret: '',
            tempSecret: secret.base32,
            dataURL,
            tfaURL: url
        };
        userDetail.tempSecret = secret.base32;
        userDetail.dataURL = dataURL;
        userDetail.tfaURL = secret.otpauth_url;
        return response.newOTPSetup(req, res, "User doesn't have OTP", userDetail);
    });
});


router.post('/verify', async function (req, res) {
    logger.logString("Entered: verify");
    logger.logObj(req.body);
    let user_id = req.body.user_id;
    let token = req.body.token;
    let tempSecret = req.body.tempSecret;
    let isVerified = speakeasy.totp.verify({
        secret: tempSecret,
        encoding: 'base32',
        token: token
    });

    if (isVerified) {
        logger.logString("TFA is verified to be enabled");
        let token = getJsonToken(user_id);

        // commons.userObject.tfa.secret = commons.userObject.tfa.tempSecret;
        logger.logStringWithObj("OTP Verification Successful", token);
        return response.success(req, res, "OTP Verification Successful", token);

    }

    logger.logStringWithObj("OTP Verification Failed", null);
    return response.failedOTPVerification(req, res, "OTP Verification Failed. Kindly enter correct OTP.", null);

});

router.post('/getAuthTokenForLoggedInUser', async function (req, res){
    logger.logString("Enter: getAuthTokenForLoggedInUser");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let user_id = req.body.user_id;
        let token = getJsonToken(user_id);
        logger.logStringWithObj("Success getAuthTokenForLoggedInUser", token);
        response.success(req, res, "Success getAuthTokenForLoggedInUser", token);
    } catch (error) {
        logger.logErrorStringWithObj("Error in getAuthTokenForLoggedInUser", error.message);
        response.serverError(req, res, "Error in getAuthTokenForLoggedInUser", error);
    }
})

function getJsonToken(user_id) {
    //now generate JWT token
    let payload = { subject: user_id };
    return jwt.sign(payload, constants.SECRET_KEYS.KEY_1);
}

router.post('/login', async function (req, res) {
    logger.logString("Entered: login");
    logger.logObj(req.params);
    logger.logObj(req.body);

    try {
        let username = req.body.uname;
        let password = req.body.upass;

        let existingUserDetail = await UserDetail.findOne({ 'username': username, 'password': password });
        if (!existingUserDetail || existingUserDetail == null) {
            logger.logString("Please check your username and password..... ");
            return response.success(req, res, "Please check your username and password", null);
        }

        logger.logStringWithObj("User Found", existingUserDetail);
        response.success(req, res, "User FOUND", existingUserDetail);

        // //check if otp is required
        // if (!existingUserDetail.is_otp_required) {
        //     logger.logString("User doesn't require OTP");
        //     return response.success(req, res, "Success Login", existingUserDetail);
        // }

        // //check if secret key is present
        // if (!existingUserDetail.secret_key || existingUserDetail.secret_key == "") {
        //     logger.logString("User doesn't have OTP. So configure it.");

        //     const secret = speakeasy.generateSecret({
        //         length: 10,
        //         name: existingUserDetail.username,
        //         issuer: 'Ogoul IAM'
        //     });
        //     var url = speakeasy.otpauthURL({
        //         secret: secret.base32,
        //         label: existingUserDetail.username,
        //         issuer: 'Ogoul IAM',
        //         encoding: 'base32'
        //     });
        //     QRCode.toDataURL(url, (err, dataURL) => {
        //         tfa = {
        //             secret: '',
        //             tempSecret: secret.base32,
        //             dataURL,
        //             tfaURL: url
        //         };
        //         // otpDetails = {
        //         //     message: 'TFA Auth needs to be verified',
        //         //     tempSecret: secret.base32,
        //         //     dataURL,
        //         //     tfaURL: secret.otpauth_url
        //         // };
        //         existingUserDetail.tempSecret = secret.base32;
        //         existingUserDetail.dataURL = dataURL;
        //         existingUserDetail.tfaURL = secret.otpauth_url;
        //         return response.newOTPSetup(req, res, "User doesn't have OTP", existingUserDetail);
        //     });
        // }


    } catch (error) {
        logger.logErrorStringWithObj("Error in login", error.message);
        response.serverError(req, res, "Error in login", error);
    }
});

module.exports = router;