let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');

let SectorDetail = require('../models/SectorDetailSchema');
let RoleDetail = require('../models/RoleDetailSchema');
let RuleDetail = require('../models/RuleDetailSchema');
let PermissionDetail = require('../models/PermissionDetailSchema');
let Utils = require('../utils/Utils');
let UserDetail = require('../models/UserDetailSchema');
let AlertNotification = require('../models/AlertNotificationSchema');
let DefaultSetting = require('../models/DefaultSettingSchema');
let PageMatrix = require('../models/PageMatrixSchema');
let EntityDetail = require('../models/EntityDetailSchema');
let MapGroupEntity = require('../models/MapSectorEntitySchema');
let MapUserRole = require('../models/MapUserRoleSchema');
let MapRoleEntity = require('../models/MapRoleEntitySchema');
let constants = require('../config/constants');
let FormElement = require('../models/FormElementSchema');
let MakerCheckerRequest = require('../models/MakerCheckerRequestSchema');
let ApprovalMatrix = require('../models/ApprovalMatrixSchema');
let MappingUserEntity = require('../models/MappingUserEntitySchema');
let AccessPolicy = require('../models/AccessPolicySchema');
let SpecificRule = require('../models/SpecificRuleSchema');

let LoggingTracking = require('../models/LoggingTrackingSchema');
let SystemEvent = require('../models/SystemEventSchema');
let EntityEvent = require('../models/EntityEventSchema');
let AccessEntity = require('../models/AccessEntitySchema');
let EmailAlert = require('../models/EmailAlertSchema');


///// common - will be done for most of other models
router.post("/commonCreate", async function (req, res) {
    logger.logString("Enter: commonCreate");
    let componentMessage = "";
    let obj = null;
    let result = null;

    try {
        logger.logObj(req.body);
        let model_name = req.body.model_name;
        //based on component name, call the respective Schema
        switch (model_name) {
            case constants.MODEL_NAME.FORM_ELEMENT:
                obj = new FormElement(req.body);
                obj.form_element_id = Utils.smallUID();
                componentMessage = "Form Element";
                break;

            case constants.MODEL_NAME.SECTOR_DETAIL:
                obj = new SectorDetail(req.body);
                obj.sector_id = Utils.smallUID();
                componentMessage = "Sector";
                break;

            case constants.MODEL_NAME.ENTITY_DETAIL:
                obj = new EntityDetail(req.body);
                obj.entity_id = Utils.smallUID();
                componentMessage = "Entity";
                break;

            case constants.MODEL_NAME.PERMISSION_DETAIL:
                obj = new PermissionDetail(req.body);
                obj.permission_id = Utils.smallUID();
                componentMessage = "Permission";
                break;

            case constants.MODEL_NAME.ACCESS_POLICY:
                obj = new AccessPolicy(req.body);
                obj.access_policy_id = Utils.smallUID();
                componentMessage = "Access Policy";
                break;

            case constants.MODEL_NAME.SPECIFIC_RULE:
                obj = new SpecificRule(req.body);
                obj.specific_rule_id = Utils.smallUID();
                componentMessage = "Specific Rule";
                break;

            default:
                throw new Error("No valid model found for creating");
        }

        result = await obj.save();
        logger.logSuccessEvent("Success in adding " + componentMessage, result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success in adding " + componentMessage, result);
    } catch (error) {
        await logger.logErrorEvent("Error in adding " + componentMessage + " : " + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in adding " + componentMessage, error);
    }
});

router.post("/commonUpdate", async function (req, res) {
    logger.logString("Enter: commonUpdate");
    let componentMessage = "";
    let result = null;

    try {
        logger.logObj(req.body);
        let model_name = req.body.model_name;
        //based on component name, call the respective Schema
        switch (model_name) {
            case constants.MODEL_NAME.SECTOR_DETAIL:
                let sector_id = req.body.sector_id;
                result = await SectorDetail.updateOne({ 'sector_id': sector_id }, req.body);
                componentMessage = "Sector";
                break;
            case constants.MODEL_NAME.ENTITY_DETAIL:
                let entity_id = req.body.entity_id;
                result = await EntityDetail.updateOne({ 'entity_id': entity_id }, req.body);
                componentMessage = "Entity";
                break;
            case constants.MODEL_NAME.PERMISSION_DETAIL:
                let permission_id = req.body.permission_id;
                result = await PermissionDetail.updateOne({ 'permission_id': permission_id }, req.body);
                componentMessage = "Permission";
                break;
            case constants.MODEL_NAME.ACCESS_POLICY:
                let access_policy_id = req.body.access_policy_id;
                result = await AccessPolicy.updateOne({ 'access_policy_id': access_policy_id }, req.body);
                componentMessage = "Access Policy";
                await updateUserMatchingAccessPolicy(access_policy_id);
                break;
            case constants.MODEL_NAME.SPECIFIC_RULE:
                let specific_rule_id = req.body.specific_rule_id;
                //first check if this rule exists
                result = await SpecificRule.findOne({ 'specific_rule_id': specific_rule_id });
                if (result == null) {
                    let newSpecificRule = new SpecificRule(req.body);
                    newSpecificRule.specific_rule_id = Utils.smallUID();
                    result = await newSpecificRule.save();
                } else {
                    result = await SpecificRule.updateOne({ 'specific_rule_id': specific_rule_id }, req.body);
                }

                componentMessage = "Specific Rule";
                break;
            default:
                throw new Error("No valid model found for updating");
        }

        logger.logSuccessEvent("Success in updating " + componentMessage, result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success in updating " + componentMessage, result);
    } catch (error) {
        await logger.logErrorEvent("Error in updating " + componentMessage + ": " + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in updating " + componentMessage, error);
    }
});

async function updateUserMatchingAccessPolicy(access_policy_id) {
    try {
        logger.logString("Will update users matching access policy:" + access_policy_id);
    } catch (error) {
        console.log(error);
    }
}

router.post("/commonDelete", async function (req, res) {
    logger.logString("Enter: commonDelete");
    let componentMessage = "";
    let result = null;
    try {
        logger.logObj(req.body);
        let model_name = req.body.model_name;
        //based on component name, call the respective Schema
        switch (model_name) {
            case constants.MODEL_NAME.SECTOR_DETAIL:
                let sector_id = req.body.sector_id;
                result = await SectorDetail.deleteOne({ 'sector_id': sector_id }, req.body);
                componentMessage = "Sector";
                break;
            case constants.MODEL_NAME.PERMISSION_DETAIL:
                let permission_id = req.body.permission_id;
                result = await PermissionDetail.deleteOne({ 'permission_id': permission_id }, req.body);
                componentMessage = "Permission";
                break;
            case constants.MODEL_NAME.SPECIFIC_RULE:
                let specific_rule_id = req.body.specific_rule_id;
                result = await SpecificRule.deleteOne({ 'specific_rule_id': specific_rule_id }, req.body);
                componentMessage = "Specific Rule";
                break;
            // case constants.MODEL_NAME.ENTITY_DETAIL:
            //     componentMessage = "Entity";
            //     result = deleteEntityDetail(req.body);
            //     if (result != constants.DEFAULT_MESSAGES.success) {
            //         throw new Error(result);
            //     }
            //     logger.logSuccessEvent("Success deleteEntityDetail", result, req.body, constants.EVENT_TYPES.DELETE);
            //     response.success(req, res, "Success deleteEntityDetail", result);
            //     break;

            default:
                throw new Error("No valid model found for deleting");
        }

        logger.logSuccessEvent("Success in deleting " + componentMessage, result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success in deleting " + componentMessage, result);
    } catch (error) {
        await logger.logErrorEvent("Error in deleting " + componentMessage + ": " + error.message, error, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleting " + componentMessage, error);
    }
});


router.get("/commonGetAll", async function (req, res) {
    let componentMessage = "";
    let result = null;
    try {
        logger.logString("Enter: commonGetAll");
        logger.logObj(req.query);
        // let model_name = req.query.model_name;
        let model_name = parseInt(req.query.model_name);
        //based on component name, call the respective Schema
        switch (model_name) {
            case constants.MODEL_NAME.ACCESS_POLICY:
                result = await AccessPolicy.find();
                componentMessage = "Access Policy";
                break;

            case constants.MODEL_NAME.SPECIFIC_RULE:
                result = await SpecificRule.find();
                componentMessage = "Specific Rule";
                break;

            default:
                throw new Error("No valid model found for getting results.");
        }
        logger.logSuccessEvent("Success in getting " + componentMessage, result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success in getting " + componentMessage, result);
    } catch (error) {
        await logger.logErrorEvent("Error in getting " + componentMessage + " : " + error.message, error, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getting " + componentMessage, error);
    }
});



router.get("/commonGetSpecific", async function (req, res) {
    let componentMessage = "";
    let result = null;
    try {
        logger.logString("Enter: commonGetSpecific");
        logger.logObj(req.query);
        // let model_name = req.query.model_name;
        let model_name = parseInt(req.query.model_name);
        let access_policy_id = req.query.id;
        //based on component name, call the respective Schema
        switch (model_name) {
            case constants.MODEL_NAME.ACCESS_POLICY:
                result = await AccessPolicy.findOne({ 'access_policy_id': access_policy_id });
                componentMessage = "Access Policy";
                break;
            case constants.MODEL_NAME.SPECIFIC_RULE:
                result = await SpecificRule.findOne({ 'specific_rule_id': specific_rule_id });
                componentMessage = "Access Policy";
                break;

            default:
                throw new Error("No valid model found for getting results.");
        }
        logger.logSuccessEvent("Success in getting " + componentMessage, result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success in getting " + componentMessage, result);
    } catch (error) {
        await logger.logErrorEvent("Error in getting " + componentMessage + " : " + error.message, error, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getting " + componentMessage, error);
    }
});


////////////////////////////////////
/**
 * Get all sectors based on sectorType (constants SECTOR_TYPES)
 */
router.get("/getAllSectors", async function (req, res) {
    logger.logString("Enter: getAllSectors");
    try {
        logger.logObj(req.query);
        logger.logObj(req.body);
        let sectorType = req.query.sectorType;
        let result = null;
        switch (sectorType) {
            case constants.CONFIG_TYPES.SECTOR:
                result = await SectorDetail.find();
                break;
            case constants.CONFIG_TYPES.ENTITY:
                result = await EntityDetail.find();
                break;
            case constants.CONFIG_TYPES.ROLE:
                result = await RoleDetail.find();
                break;
            case constants.CONFIG_TYPES.RULE:
                result = await RuleDetail.find();
                break;
            case constants.CONFIG_TYPES.PERMISSION:
                result = await PermissionDetail.find();
                break;
            default:
        }
        logger.logSuccessEvent("Success getAllSectors", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllSectors", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllSectors", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllSectors", error);
    }
});

router.post("/addSectorDetail", async function (req, res) {
    logger.logString("Enter: addSectorDetail");
    try {
        logger.logObj(req.body);
        let sectorDetail = new SectorDetail(req.body);
        sectorDetail.sector_id = Utils.smallUID();
        let result = await sectorDetail.save();

        logger.logSuccessEvent("Success addSectorDetail", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addSectorDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addSectorDetail", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addSectorDetail", error);
    }
});


router.post("/updateSectorDetail", async function (req, res) {
    logger.logString("Enter: updateSectorDetail");
    try {
        logger.logObj(req.body);

        let sector_id = req.body.sector_id;
        let result = await SectorDetail.updateOne({ 'sector_id': sector_id }, req.body);
        logger.logSuccessEvent("Success updateSectorDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateSectorDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateSectorDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateSectorDetail", error);
    }
});

router.post("/deleteSectorDetail", async function (req, res) {
    logger.logString("Enter: deleteSectorDetail");
    try {
        logger.logObj(req.body);
        let sector_id = req.body.sector_id;
        let result = await SectorDetail.deleteOne({ 'sector_id': sector_id }, req.body);
        logger.logSuccessEvent("Success deleteSectorDetail", result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success deleteSectorDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deleteSectorDetail", error.message, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleteSectorDetail", error);
    }
});

router.get("/getAllSectorDetail", async function (req, res) {
    logger.logString("Enter: getAllSectorDetail");
    try {
        logger.logObj(req.query);
        let result = await SectorDetail.find();
        logger.logSuccessEvent("Success getAllSectorDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllSectorDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllSectorDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllSectorDetail", error);
    }
});

router.get("/getSpecificSectorDetail", async function (req, res) {
    logger.logString("Enter: getSpecificSectorDetail");
    try {
        let sector_id = req.query.sector_id;
        let result = await SectorDetail.findOne({ "sector_id": sector_id });
        logger.logSuccessEvent("Success getSpecificSectorDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificSectorDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificSectorDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificSectorDetail", error);
    }
});

////////////Roles
router.post("/addRoleDetail", async function (req, res) {
    logger.logString("Enter: addRoleDetail");
    try {
        logger.logObj(req.body);
        let obj = new RoleDetail(req.body);
        obj.role_id = Utils.smallUID();
        let result = await obj.save();
        logger.logStringWithObj("addRoleDetail:result:", result);

        // //create page matrix - LOGIC CHANGE
        // let pageMatrix = new PageMatrix();
        // pageMatrix.page_matrix_id = Utils.smallUID();
        // pageMatrix.role_id = result.role_id;
        // pageMatrix.can_be_deleted = false;
        // let result1 = await pageMatrix.save();

        logger.logSuccessEvent("Success addRoleDetail", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addRoleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addRoleDetail", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addRoleDetail", error);
    }
});

router.post("/updateRoleDetail", async function (req, res) {
    logger.logString("Enter: updateRoleDetail");
    try {
        logger.logObj(req.body);
        let role_id = req.body.role_id;
        let result = await RoleDetail.updateOne({ 'role_id': role_id }, req.body);

        // #TO_BE_DONE# 
        // //Update users that are belonging to this role..... IMPORTANT
        // //1 - get all users that has the entity id of this role from MappingUserEntity
        // //2 - check the users' entity ids with the role entity id
        // //3 - if entity is there in updatedRole but not in Mapping then make a new entry in Mapping
        // //4 - if entity is there in updatedRole and also in Mapping then do nothing
        // //5 - if entity is not there in updatedRole but exists in Mapping then delete it - Also we have to deprovision this user from the actual entity

        // mappingUserEntityArr = await MappingUserEntity.find({ 'role_id': role_id });
        // for(let k = 0; k<mappingUserEntityArr.length; k++){

        // }

        logger.logSuccessEvent("Success updateRoleDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateRoleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateRoleDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateRoleDetail", error);
    }
});


router.post("/deleteRoleDetail", async function (req, res) {
    logger.logString("Enter: deleteRoleDetail");
    try {
        logger.logObj(req.body);
        let role_id = req.body.role_id;
        let result = await RoleDetail.deleteOne({ 'role_id': role_id }, req.body);

        // //delete associated pageMatrix - LOGIC CHANGE
        // let result1 = await PageMatrix.deleteMany({ 'role_id': role_id });

        logger.logSuccessEvent("Success deleteRoleDetail", result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success deleteRoleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deleteRoleDetail", error.message, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleteRoleDetail", error);
    }
});

router.get("/getAllRoleDetail", async function (req, res) {
    logger.logString("Enter: getAllRoleDetail");
    try {
        let result = await RoleDetail.find();
        logger.logSuccessEvent("Success getAllRoleDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllRoleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllRoleDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllRoleDetail", error);
    }
});


router.get("/getRoleDetailOfUserType", async function (req, res) {
    logger.logString("Enter: getRoleDetailOfUserType");
    try {
        let ignorableRoleDetailIdArr = [];
        let user_type = req.query.user_type;
        if (user_type == constants.USER_TYPES.HR) {
            ignorableRoleDetailIdArr.push(constants.DEFAULT_ROLES.ADMIN_ROLE);
            ignorableRoleDetailIdArr.push(constants.DEFAULT_ROLES.MANAGER_ROLE);
            ignorableRoleDetailIdArr.push(constants.DEFAULT_ROLES.HR_ROLE);
        }
        let result = await RoleDetail.find({ "role_id": { $nin: ignorableRoleDetailIdArr } });
        logger.logSuccessEvent("Success getRoleDetailOfUserType", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getRoleDetailOfUserType", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getRoleDetailOfUserType", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getRoleDetailOfUserType", error);
    }
});



router.get("/getSpecificRoleDetail", async function (req, res) {
    logger.logString("Enter: getSpecificRoleDetail");
    try {
        let role_id = req.query.role_id;
        let result = await RoleDetail.findOne({ "role_id": role_id });
        logger.logSuccessEvent("Success getSpecificRoleDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificRoleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificRoleDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificRoleDetail", error);
    }
});

///////////////Entities
router.post("/addEntityDetail", async function (req, res) {
    logger.logString("Enter: addEntityDetail");
    try {
        logger.logObj(req.body);
        let entityDetail = new EntityDetail(req.body);
        entityDetail.entity_id = Utils.smallUID();
        let result = await entityDetail.save();

        logger.logSuccessEvent("Success addEntityDetail", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addEntityDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addEntityDetail", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addEntityDetail", error);
    }
});

router.get("/getAllEntityDetail", async function (req, res) {
    logger.logString("Enter: getAllEntityDetail");
    try {
        let result = await EntityDetail.find();
        logger.logSuccessEvent("Success getAllEntityDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllEntityDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllEntityDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllEntityDetail", error);
    }
});


router.post("/updateEntityDetail", async function (req, res) {
    logger.logString("Enter: updateEntityDetail");
    try {
        logger.logObj(req.body);
        let entity_id = req.body.entity_id;
        let result = await EntityDetail.updateOne({ 'entity_id': entity_id }, req.body);
        logger.logSuccessEvent("Success updateEntityDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateEntityDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateEntityDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateEntityDetail", error);
    }
});


async function specificDeleteEntityDetail(body) {
    try {
        logger.logString("Enter: specificDeleteEntityDetail");
        logger.logObj(body);
        let entity_id = body.entity_id;
        let result = await EntityDetail.deleteOne({ 'entity_id': entity_id }, body);

        //delete the entities from entity_id_arr of the access policy for this entity
        let matchingAccessPolicyArr = await AccessPolicy.find({ "entity_id_arr": { $in: [entity_id] } });
        for (let z = 0; z < matchingAccessPolicyArr.length; z++) {
            let accessPolicy = matchingAccessPolicyArr[z];
            logger.logString("Removing entityId: '" + entity_id + "' from access policy: '" + accessPolicy.access_policy_id + "'");
            let entityIdArr = accessPolicy.entity_id_arr;
            accessPolicy.entity_id_arr = Utils.removeItemFromStringArray(entityIdArr, entity_id);
            let r2 = await AccessPolicy.updateOne({ 'access_policy_id': accessPolicy.access_policy_id }, accessPolicy);
        }

        //delete the entities present in the sector detail
        let entityIdArr = [];
        entityIdArr.push(entity_id);
        logger.logStringWithObj("entityIdArr:", entityIdArr);
        let sectorDetailArr = await SectorDetail.find({ "entity_id_arr": { $in: entityIdArr } });
        logger.logStringWithObj("specificDeleteEntityDetail:sectorDetailArr:", sectorDetailArr);
        if (sectorDetailArr != null && sectorDetailArr.length > 0) {
            for (let k = 0; k < sectorDetailArr.length; k++) {
                let sectorDetail = sectorDetailArr[k];
                logger.logStringWithObj("Updating Sector:::", sectorDetail);
                let newEntityIdArr = Utils.removeItemFromStringArray(sectorDetail.entity_id_arr, entity_id);
                if (newEntityIdArr != null && newEntityIdArr.length == 1 && newEntityIdArr[0] == null)
                    newEntityIdArr = [];
                sectorDetail.entity_id_arr = newEntityIdArr;
                await SectorDetail.updateOne({ 'sector_id': sectorDetail.sector_id }, sectorDetail);
            }
        }
        return constants.DEFAULT_MESSAGES.SUCCESS;
    } catch (error) {
        logger.logErrorStringWithObj("Error in specificDeleteEntityDetail: " + error.message, error);
        return error.message;
    }
}


router.post("/deleteEntityDetail", async function (req, res) {
    logger.logString("Enter: deleteEntityDetail");
    try {
        let resultFromFunction = await specificDeleteEntityDetail(req.body);
        logger.logStringWithObj("resultFromFunction:", resultFromFunction);
        if (resultFromFunction == constants.DEFAULT_MESSAGES.SUCCESS) {
            logger.logSuccessEvent("Success deleteEntityDetail", resultFromFunction, req.body, constants.EVENT_TYPES.DELETE);
            response.success(req, res, "Success deleteEntityDetail", resultFromFunction);
        } else {
            throw new Error(resultFromFunction); 
        }
    } catch (error) {
        await logger.logErrorEvent("Error in deleteEntityDetail: " + error.message, error, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleteEntityDetail", error);
    }
});


// router.post("/deleteEntityDetail", async function (req, res) {
//     logger.logString("Enter: deleteEntityDetail");
//     try {
//         let resultFromFunction = deleteEntityDetail(req.body);
//         logger.logObj(req.body);
//         let result = null;
//         let entity_id = req.body.entity_id;
//         result = await EntityDetail.deleteOne({ 'entity_id': entity_id }, req.body);

//         //delete the entities present in the sector detail
//         let entityIdArr = [];
//         entityIdArr.push(entity_id);
//         logger.logStringWithObj("entityIdArr:", entityIdArr);
//         let sectorDetailArr = await SectorDetail.find({ "entity_id_arr": { $in: entityIdArr } });
//         logger.logStringWithObj("deleteEntityDetail:sectorDetailArr:", sectorDetailArr);
//         if (sectorDetailArr != null && sectorDetailArr.length > 0) {
//             for (let k = 0; k < sectorDetailArr.length; k++) {
//                 let sectorDetail = sectorDetailArr[k];
//                 logger.logStringWithObj("Updating Sector:::", sectorDetail);
//                 let newEntityIdArr = Utils.removeItemFromStringArray(sectorDetail.entity_id_arr, entity_id);
//                 if (newEntityIdArr != null && newEntityIdArr.length == 1 && newEntityIdArr[0] == null)
//                     newEntityIdArr = [];
//                 sectorDetail.entity_id_arr = newEntityIdArr;
//                 await SectorDetail.updateOne({ 'sector_id': sectorDetail.sector_id }, sectorDetail);
//             }
//         }

//         logger.logSuccessEvent("Success deleteEntityDetail", result, req.body, constants.EVENT_TYPES.DELETE);
//         response.success(req, res, "Success deleteEntityDetail", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in deleteEntityDetail", error.message, req, constants.EVENT_TYPES.DELETE);
//         response.serverError(req, res, "Error in deleteEntityDetail", error);
//     }
// });

router.get("/getAllEntitiesForUser", async function (req, res) {
    logger.logString("Enter: getAllEntitiesForUser");
    try {
        logger.logObj(req.query);
        logger.logObj(req.body);
        let user_id = req.query.user_id;
        let userRolesArr = await MapUserRole.find({ 'user_id': user_id });

        let entityIdArr = [];
        for (var userRole of userRolesArr) {
            let roleEntityArr = await MapRoleEntity.find({ "role_id": userRole.user_id });
            for (var roleEntity of roleEntityArr) {
                entityIdArr.push(roleEntity.entity_id);
            }
        }

        result = await EntityDetail.find({ "entity_id": { $in: entityIdArr } });
        logger.logSuccessEvent("Success getAllEntitiesForUser", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllEntitiesForUser", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllEntitiesForUser", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllEntitiesForUser", error);
    }
});


router.get("/getAllEntitiesForGroup", async function (req, res) {
    logger.logString("Enter: getAllEntitiesForGroup");
    try {
        logger.logObj(req.query);
        let group_id = req.query.group_id;
        let result = await MapGroupEntity.find({ 'group_id': group_id });
        logger.logSuccessEvent("Success getAllEntitiesForGroup", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllEntitiesForGroup", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllEntitiesForGroup", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllEntitiesForGroup", error);
    }
});



router.post("/getMultipleEntityDetail", async function (req, res) {
    logger.logString("Enter: getMultipleEntityDetail");
    try {
        logger.logObj(req.body);
        let multiple_entity_id_arr = req.body.multiple_entity_id_arr;
        let result = await EntityDetail.find({ "entity_id": { $in: multiple_entity_id_arr } });
        logger.logSuccessEvent("Success getMultipleEntityDetail", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getMultipleEntityDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getMultipleEntityDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getMultipleEntityDetail", error);
    }
});


router.get("/getSpecificEntityDetail", async function (req, res) {
    logger.logString("Enter: getSpecificEntityDetail");
    try {
        let entity_id = req.query.entity_id;
        let result = await EntityDetail.findOne({ "entity_id": entity_id });
        logger.logSuccessEvent("Success getSpecificEntityDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificEntityDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificEntityDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificEntityDetail", error);
    }
});


router.get("/getAllEntityDetailsOfSpecificRole", async function (req, res) {
    logger.logString("Enter: getAllEntityDetailsOfSpecificRole");
    try {
        logger.logObj(req.query);
        let role_id = req.query.role_id;
        let roleDetail = await RoleDetail.findOne({ "role_id": role_id });
        logger.logStringWithObj("roleDetailLLLL:", roleDetail);
        let multiple_entity_id_arr = roleDetail.allowed_entity_id_arr;
        let result = await EntityDetail.find({ "entity_id": { $in: multiple_entity_id_arr } });
        logger.logSuccessEvent("Success getAllEntityDetailsOfSpecificRole", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllEntityDetailsOfSpecificRole", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllEntityDetailsOfSpecificRole", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllEntityDetailsOfSpecificRole", error);
    }
});


router.get("/OLD_getAllUserDetailsOfSpecificRole", async function (req, res) {
    logger.logString("Enter: getAllUserDetailsOfSpecificRole");
    try {
        logger.logObj(req.query);
        let role_id = req.query.role_id;
        let result = await UserDetail.find({ "role_id": role_id });
        logger.logSuccessEvent("Success getAllUserDetailsOfSpecificRole", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllUserDetailsOfSpecificRole", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllUserDetailsOfSpecificRole", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllUserDetailsOfSpecificRole", error);
    }
});


router.get("/getAllUserDetailsOfSpecificRole", async function (req, res) {
    logger.logString("Enter: getAllUserDetailsOfSpecificRole");
    try {
        logger.logObj(req.query);
        let role_id = req.query.role_id;
        //first get the matching user ids of this role
        let result = await MappingUserEntity.find({ "role_id": role_id }, { _id: 0, user_id: 1 });
        logger.logStringWithObj("result:", result);
        let matchingUserIdArr = [];
        for (let k = 0; k < result.length; k++) {
            matchingUserIdArr.push(result[k].user_id);
        }
        logger.logStringWithObj("matchingUserIdArr:", matchingUserIdArr);
        matchingUserIdArr = Utils.removeDuplicatesFromArray(matchingUserIdArr);

        //second get the userdetails of these users
        result = await UserDetail.find({ 'user_id': { $in: matchingUserIdArr } });
        logger.logSuccessEvent("Success getAllUserDetailsOfSpecificRole", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllUserDetailsOfSpecificRole", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllUserDetailsOfSpecificRole", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllUserDetailsOfSpecificRole", error);
    }
});



router.get("/getAllUserDetailsOfSpecificEntities", async function (req, res) {
    logger.logString("Enter: getAllUserDetailsOfSpecificEntities");
    try {
        logger.logObj(req.query);
        let entity_id = req.query.entity_id;

        //first get the matching user ids of this role
        let result = await MappingUserEntity.find({ "entity_id": entity_id }, { _id: 0, user_id: 1 });
        logger.logStringWithObj("result:", result);
        let matchingUserIdArr = [];
        for (let k = 0; k < result.length; k++) {
            matchingUserIdArr.push(result[k].user_id);
        }
        logger.logStringWithObj("matchingUserIdArr:", matchingUserIdArr);
        matchingUserIdArr = Utils.removeDuplicatesFromArray(matchingUserIdArr);

        //second get the userdetails of these users
        result = await UserDetail.find({ 'user_id': { $in: matchingUserIdArr } });
        logger.logSuccessEvent("Success getAllUserDetailsOfSpecificEntities", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllUserDetailsOfSpecificEntities", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllUserDetailsOfSpecificEntities", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllUserDetailsOfSpecificEntities", error);
    }
});


router.get("/getAllUserDetailsOfSpecificAccessPolicies", async function (req, res) {
    logger.logString("Enter: getAllUserDetailsOfSpecificAccessPolicies");
    try {
        logger.logObj(req.query);
        let access_policy_id = req.query.access_policy_id;
        let accessPolicyIdArr = [access_policy_id];

        //1 - get the matching role ids of the access_policy
        let result = await RoleDetail.find({ 'allowed_access_policy_id_arr': { $in: accessPolicyIdArr } }, { _id: 0, role_id: 1 });
        logger.logStringWithObj("result:", result);
        let matchingRoleIdArr = [];
        for (let k = 0; k < result.length; k++) {
            matchingRoleIdArr.push(result[k].role_id);
        }
        logger.logStringWithObj("matchingRoleIdArr:", matchingRoleIdArr);
        matchingRoleIdArr = Utils.removeDuplicatesFromArray(matchingRoleIdArr);

        //2- from role get all the matching users
        result = await MappingUserEntity.find({ "role_id": { $in: matchingRoleIdArr } }, { _id: 0, user_id: 1 });
        logger.logStringWithObj("result:", result);
        let matchingUserIdArr = [];
        for (let k = 0; k < result.length; k++) {
            matchingUserIdArr.push(result[k].user_id);
        }
        logger.logStringWithObj("matchingUserIdArr:", matchingUserIdArr);
        matchingUserIdArr = Utils.removeDuplicatesFromArray(matchingUserIdArr);

        //3- get the userdetails of these users
        result = await UserDetail.find({ 'user_id': { $in: matchingUserIdArr } });

        logger.logSuccessEvent("Success getAllUserDetailsOfSpecificAccessPolicies", result, req.body, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllUserDetailsOfSpecificAccessPolicies", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllUserDetailsOfSpecificAccessPolicies", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllUserDetailsOfSpecificAccessPolicies", error);
    }
});

////////////Rules
router.post("/addRuleDetail", async function (req, res) {
    logger.logString("Enter: addRuleDetail");
    try {
        logger.logObj(req.body);
        let ruleDetail = new RuleDetail(req.body);
        ruleDetail.rule_id = Utils.smallUID();
        let result = await ruleDetail.save();

        logger.logSuccessEvent("Success addRuleDetail", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addRuleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addRuleDetail", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addRuleDetail", error);
    }
});

router.post("/updateRuleDetail", async function (req, res) {
    logger.logString("Enter: updateRuleDetail");
    try {
        logger.logObj(req.body);
        let rule_id = req.body.rule_id;
        let result = await RuleDetail.updateOne({ 'rule_id': rule_id }, req.body);
        logger.logSuccessEvent("Success updateRuleDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateRuleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateRuleDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateRuleDetail", error);
    }
});


router.post("/deleteRuleDetail", async function (req, res) {
    logger.logString("Enter: deleteRuleDetail");
    try {
        logger.logObj(req.body);
        let rule_id = req.body.rule_id;
        let result = await RuleDetail.deleteOne({ 'rule_id': rule_id }, req.body);
        logger.logSuccessEvent("Success deleteRuleDetail", result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success deleteRuleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deleteRuleDetail", error.message, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleteRuleDetail", error);
    }
});

router.get("/getAllRuleDetail", async function (req, res) {
    logger.logString("Enter: getAllRuleDetail");
    try {
        let result = await RuleDetail.find();
        logger.logSuccessEvent("Success getAllRuleDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllRuleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllRuleDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllRuleDetail", error);
    }
});

router.get("/getParticularRuleDetail", async function (req, res) {
    logger.logString("Enter: getParticularRuleDetail");
    try {
        let rule_id = req.query.rule_id;
        let result = await RuleDetail.findOne({ "rule_id": rule_id });
        logger.logSuccessEvent("Success getParticularRuleDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getParticularRuleDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getParticularRuleDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getParticularRuleDetail", error);
    }
});


router.get("/getParticularRuleDetailFromRoleId", async function (req, res) {
    logger.logString("Enter: getParticularRuleDetailFromRoleId");
    try {
        let role_id = req.query.role_id;
        let result = await RuleDetail.findOne({ "role_id": role_id });
        logger.logSuccessEvent("Success getParticularRuleDetailFromRoleId", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getParticularRuleDetailFromRoleId", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getParticularRuleDetailFromRoleId", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getParticularRuleDetailFromRoleId", error);
    }
});


router.get("/getParticularRuleDetailFromUsername", async function (req, res) {
    logger.logString("Enter: getParticularRuleDetailFromUsername");
    try {
        let associated_username = req.query.associated_username;
        let result = await RuleDetail.findOne({ "associated_username": associated_username });
        logger.logSuccessEvent("Success getParticularRuleDetailFromUsername", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getParticularRuleDetailFromUsername", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getParticularRuleDetailFromUsername", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getParticularRuleDetailFromUsername", error);
    }
});


router.get("/getPermissionDetailsForRule", async function (req, res) {
    logger.logString("Enter: getPermissionDetailsForRule");
    try {
        logger.logObj(req.query);
        let role_id_arr = [];
        role_id_arr.push(req.query.role_id);
        let entity_id_arr = [req.query.entity_id];
        let ruleDetail = await RuleDetail.findOne({ "role_id": { $in: role_id_arr }, "entity_id_arr": { $in: entity_id_arr } });
        // let ruleDetail = await RuleDetail.findOne({ "role_id_arr": { $in: role_id_arr }, "entity_id_arr": { $in: entity_id_arr } });
        let result = null;
        if (ruleDetail != null) {
            let permission_id_arr = ruleDetail.permission_id_arr;
            result = await PermissionDetail.find({ "permission_id": { $in: permission_id_arr } });
        }
        logger.logSuccessEvent("Success getPermissionDetailsForRule", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getPermissionDetailsForRule", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getPermissionDetailsForRule", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getPermissionDetailsForRule: " + error.message, error);
    }
});

////////////Permissions
// router.post("/addPermissionDetail", async function (req, res) {
//     logger.logString("Enter: addPermissionDetail");
//     try {
//         logger.logObj(req.body);
//         let permissionDetail = new PermissionDetail(req.body);
//         permissionDetail.permission_id = Utils.smallUID();
//         let result = await permissionDetail.save();
//         logger.logSuccessEvent("Success addPermissionDetail", result, req.body, constants.EVENT_TYPES.CREATE);
//         response.success(req, res, "Success addPermissionDetail", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in addPermissionDetail", error.message, req, constants.EVENT_TYPES.CREATE);
//         response.serverError(req, res, "Error in addPermissionDetail", error);
//     }
// });

// router.post("/updatePermissionDetail", async function (req, res) {
//     logger.logString("Enter: updatePermissionDetail");
//     try {
//         logger.logObj(req.body);
//         let permission_id = req.body.permission_id;
//         let result = await PermissionDetail.updateOne({ 'permission_id': permission_id }, req.body);
//         logger.logSuccessEvent("Success updatePermissionDetail", result, req.body, constants.EVENT_TYPES.UPDATE);
//         response.success(req, res, "Success updatePermissionDetail", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in updatePermissionDetail", error.message, req, constants.EVENT_TYPES.UPDATE);
//         response.serverError(req, res, "Error in updatePermissionDetail", error);
//     }
// });


// router.post("/deletePermissionDetail", async function (req, res) {
//     logger.logString("Enter: deletePermissionDetail");
//     try {
//         logger.logObj(req.body);
//         let permission_id = req.body.permission_id;
//         let result = await PermissionDetail.deleteOne({ 'permission_id': permission_id }, req.body);
//         logger.logSuccessEvent("Success deletePermissionDetail", result, req.body, constants.EVENT_TYPES.DELETE);
//         response.success(req, res, "Success deletePermissionDetail", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in deletePermissionDetail", error.message, req, constants.EVENT_TYPES.DELETE);
//         response.serverError(req, res, "Error in deletePermissionDetail", error);
//     }
// });

router.get("/getAllPermissionDetail", async function (req, res) {
    logger.logString("Enter: getAllPermissionDetail");
    try {
        let result = await PermissionDetail.find();
        logger.logSuccessEvent("Success getAllPermissionDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllPermissionDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllPermissionDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllPermissionDetail", error);
    }
});


router.get("/getSpecificPermissionDetail", async function (req, res) {
    logger.logString("Enter: getSpecificPermissionDetail");
    try {
        let permission_id = req.query.permission_id;
        let result = await PermissionDetail.findOne({ "permission_id": permission_id });
        logger.logSuccessEvent("Success getSpecificPermissionDetail", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificPermissionDetail", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificPermissionDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificPermissionDetail", error);
    }
});
/////////// Mappings
router.post("/addUpdateMapGroupEntity", async function (req, res) {
    logger.logString("Enter: addUpdateMapGroupEntity");
    try {
        logger.logObj(req.body);
        let mapGroupEntity = new MapGroupEntity(req.body);
        let result = await MapGroupEntity.findOne({ 'group_id': mapGroupEntity.group_id, 'entity_id': mapGroupEntity.entity_id });
        if (result == null) {
            result = await mapGroupEntity.save();
            logger.logSuccessEvent("Success addUpdateMapGroupEntity", result, req.body, constants.EVENT_TYPES.CREATE);
        } else {
            result = await MapGroupEntity.updateOne({ 'group_id': mapGroupEntity.group_id, 'entity_id': mapGroupEntity.entity_id }, req.body);
            logger.logSuccessEvent("Success addUpdateMapGroupEntity", result, req.body, constants.EVENT_TYPES.UPDATE);
        }
        response.success(req, res, "Success addUpdateMapGroupEntity", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addUpdateMapGroupEntity", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addUpdateMapGroupEntity", error);
    }
});



router.post("/addUpdateMapUserRole", async function (req, res) {
    logger.logString("Enter: addUpdateMapUserRole");
    try {
        logger.logObj(req.body);
        let mapUserRole = new MapUserRole(req.body);
        let result = await MapUserRole.findOne({ 'user_id': mapUserRole.user_id, 'entity_id': mapUserRole.role_id });
        if (result == null) {
            result = await mapUserRole.save();
            logger.logSuccessEvent("Success addUpdateMapUserRole", result, req.body, constants.EVENT_TYPES.CREATE);
        } else {
            result = await MapUserRole.updateOne({ 'user_id': mapUserRole.user_id, 'role_id': mapUserRole.role_id }, req.body);
            logger.logSuccessEvent("Success addUpdateMapUserRole", result, req.body, constants.EVENT_TYPES.UPDATE);
        }
        response.success(req, res, "Success addUpdateMapUserRole", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addUpdateMapUserRole", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addUpdateMapUserRole", error);
    }
});

router.post("/addUpdateMapRoleEntity", async function (req, res) {
    logger.logString("Enter: addUpdateMapRoleEntity");
    try {
        logger.logObj(req.body);
        let mapRoleEntity = new MapRoleEntity(req.body);
        let result = await MapRoleEntity.findOne({ 'role_id': mapRoleEntity.role_id, 'entity_id': mapRoleEntity.entity_id });
        if (result == null) {
            result = await mapRoleEntity.save();
            logger.logSuccessEvent("Success addUpdateMapRoleEntity", result, req.body, constants.EVENT_TYPES.CREATE);
        } else {
            result = await MapRoleEntity.updateOne({ 'role_id': mapRoleEntity.role_id, 'entity_id': mapRoleEntity.entity_id }, req.body);
            logger.logSuccessEvent("Success addUpdateMapRoleEntity", result, req.body, constants.EVENT_TYPES.UPDATE);
        }
        response.success(req, res, "Success addUpdateMapRoleEntity", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addUpdateMapRoleEntity", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addUpdateMapRoleEntity", error);
    }
});

//alertNotification
router.get("/getAllAlertNotification", async function (req, res) {
    logger.logString("Enter: getAllAlertNotification");
    try {
        logger.logObj(req.query);
        let preferred_language = req.query.preferred_language;
        let result = await AlertNotification.find({ "preferred_language": preferred_language });
        logger.logSuccessEvent("Success getAllAlertNotification", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllAlertNotification", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllAlertNotification", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllAlertNotification", error);
    }
});

router.get("/getAlertNotificationOfComponent", async function (req, res) {
    logger.logString("Enter: getAlertNotificationOfComponent");
    try {
        let component_name = req.query.component_name;
        let preferred_language = req.query.preferred_language;
        let result = await AlertNotification.find({ "component_name": component_name, "preferred_language": preferred_language });
        logger.logSuccessEvent("Success getAlertNotificationOfComponent", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAlertNotificationOfComponent", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAlertNotificationOfComponent", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAlertNotificationOfComponent", error);
    }
});

router.post("/updateAlertNotification", async function (req, res) {
    logger.logString("Enter: updateAlertNotification");
    try {
        logger.logObj(req.body);
        let alert_notification_id = req.body.alert_notification_id;
        let result = await AlertNotification.updateOne({ 'alert_notification_id': alert_notification_id }, req.body);
        logger.logSuccessEvent("Success updateAlertNotification", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateAlertNotification", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateAlertNotification", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateAlertNotification", error);
    }
});


//defaultSetting
router.get("/getDefaultSetting", async function (req, res) {
    logger.logString("Enter: getDefaultSetting");
    try {
        let result = await DefaultSetting.findOne();
        logger.logSuccessEvent("Success getDefaultSetting", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getDefaultSetting", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getDefaultSetting", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getDefaultSetting", error);
    }
});

router.post("/updateDefaultSetting", async function (req, res) {
    logger.logString("Enter: updateDefaultSetting");
    try {
        logger.logObj(req.body);
        let defaultSetting = new DefaultSetting(req.body);
        let result = await DefaultSetting.updateOne(req.body);

        //update constants also
        constants.EMAIL_CREDENTIALS.SERVICE = defaultSetting.email_service;
        // constants.EMAIL_CREDENTIALS.DEFAULT_FROM = defaultSetting.email_default_from_name;
        constants.EMAIL_CREDENTIALS.USERNAME = defaultSetting.email_username;
        constants.EMAIL_CREDENTIALS.PASSWORD = defaultSetting.email_password;

        logger.logSuccessEvent("Success updateDefaultSetting", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateDefaultSetting", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateDefaultSetting", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateDefaultSetting", error);
    }
});


//pageMatrix
router.get("/getAllPageMatrix", async function (req, res) {
    logger.logString("Enter: getAllPageMatrix");
    try {
        let result = await PageMatrix.find();
        logger.logSuccessEvent("Success getAllPageMatrix", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllPageMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllPageMatrix", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllPageMatrix", error);
    }
});

router.get("/getSpecificPageMatrix", async function (req, res) {
    logger.logString("Enter: getSpecificPageMatrix");
    try {
        let page_matrix_id = req.query.page_matrix_id;
        let result = await PageMatrix.findOne({ "page_matrix_id": page_matrix_id });
        logger.logSuccessEvent("Success getSpecificPageMatrix", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificPageMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificPageMatrix", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificPageMatrix", error);
    }
});


router.get("/getPageMatrixForRoleId", async function (req, res) {
    logger.logString("Enter: getPageMatrixForRoleId");
    try {
        let role_id = req.query.role_id;
        let result = await PageMatrix.findOne({ "role_id": role_id });
        logger.logSuccessEvent("Success getPageMatrixForRoleId", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getPageMatrixForRoleId", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getPageMatrixForRoleId", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getPageMatrixForRoleId", error);
    }
});


// router.get("/loadPageMatrixForUserId", async function (req, res) {
//     logger.logString("Enter: loadPageMatrixForUserId");
//     try {
//         logger.logObj(req.query);
//         let user_id = req.query.user_id;
//         let result = await PageMatrix.findOne({ "user_id": user_id });
//         logger.logStringWithObj("loadPageMatrixForUserId:result:", result);

//         logger.logSuccessEvent("Success loadPageMatrixForUserId", result, req.query, constants.EVENT_TYPES.READ);
//         response.success(req, res, "Success loadPageMatrixForUserId", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in loadPageMatrixForUserId: " + error.message, error, req, constants.EVENT_TYPES.READ);
//         response.serverError(req, res, "Error in loadPageMatrixForUserId: " + error.message, error);
//     }
// });

// router.get("/getRespectivePageMatrixForUsername", async function (req, res) {
//     logger.logString("Enter: getRespectivePageMatrixForUsername");
//     try {
//         logger.logObj(req.query);
//         //first check if username specific page matrix is available and if not then fetch the associated role page matrix
//         let associated_username = req.query.associated_username;
//         let result = await PageMatrix.findOne({ "associated_username": associated_username });
//         logger.logStringWithObj("getRespectivePageMatrixForUsername:result:", result);
//         if (result == null) {
//             //now fetch the role specific
//             let userDetailResult = await UserDetail.findOne({ "username": associated_username });
//             logger.logStringWithObj("getRespectivePageMatrixForUsername:userDetailResult:", userDetailResult);
//             if (userDetailResult == null) {
//                 throw new Error("No username page matrix found for this user.");
//             }
//             result = await PageMatrix.findOne({ "role_id": userDetailResult.role_id });
//             logger.logStringWithObj("getRespectivePageMatrixForUsername:result2:", result);
//         }

//         logger.logSuccessEvent("Success getRespectivePageMatrixForUsername", result, req.query, constants.EVENT_TYPES.READ);
//         response.success(req, res, "Success getRespectivePageMatrixForUsername", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in getRespectivePageMatrixForUsername: " + error.message, error, req, constants.EVENT_TYPES.READ);
//         response.serverError(req, res, "Error in getRespectivePageMatrixForUsername: " + error.message, error);
//     }
// });


// router.get("/loadPageMatrixForUserId", async function (req, res) {
//     logger.logString("Enter: loadPageMatrixForUserId");
//     try {
//         logger.logObj(req.query);
//         let user_id = req.query.user_id;
//         let result = await PageMatrix.findOne({ "user_id": user_id });
//         logger.logStringWithObj("loadPageMatrixForUserId:result:", result);

//         logger.logSuccessEvent("Success loadPageMatrixForUserId", result, req.query, constants.EVENT_TYPES.READ);
//         response.success(req, res, "Success loadPageMatrixForUserId", result);
//     } catch (error) {
//         await logger.logErrorEvent("Error in loadPageMatrixForUserId: " + error.message, error, req, constants.EVENT_TYPES.READ);
//         response.serverError(req, res, "Error in loadPageMatrixForUserId: " + error.message, error);
//     }
// });


router.get("/getUnassociatedUsernames", async function (req, res) {
    logger.logString("Enter: getUnassociatedUsernames");
    try {
        let pageMatrixResult = await PageMatrix.find({ "associated_username": { $ne: "-1" } }).select({ "associated_username": 1, "_id": 0 });
        logger.logStringWithObj("pageMatrixResult:::", pageMatrixResult);
        let unassociatedUsernameArr = [];
        for (let k = 0; k < pageMatrixResult.length; k++) {
            unassociatedUsernameArr.push(pageMatrixResult[k].associated_username);
        }
        logger.logStringWithObj("unassociatedUsernameArr:::", unassociatedUsernameArr);
        let usernameResult = await UserDetail.find({ "username": { $nin: unassociatedUsernameArr } }).select({ "username": 1, "_id": 0 });
        logger.logStringWithObj("usernameResult:::", usernameResult);
        // logger.logSuccessEvent("Success getUnassociatedUsernames", pageMatrixResult, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getUnassociatedUsernames", usernameResult);
    } catch (error) {
        await logger.logErrorEvent("Error in getUnassociatedUsernames", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getUnassociatedUsernames", error);
    }
});

router.post("/updatePageMatrix", async function (req, res) {
    logger.logString("Enter: updatePageMatrix");
    try {
        logger.logObj(req.body);
        let page_matrix_id = req.body.page_matrix_id;
        let result = await PageMatrix.findOneAndUpdate({ 'page_matrix_id': page_matrix_id }, req.body);
        logger.logSuccessEvent("Success updatePageMatrix", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updatePageMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updatePageMatrix", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updatePageMatrix", error);
    }
});

router.post("/addPageMatrix", async function (req, res) {
    logger.logString("Enter: addPageMatrix");
    try {
        logger.logObj(req.body);
        let pageMatrix = new PageMatrix(req.body);
        pageMatrix.page_matrix_id = Utils.smallUID();
        let result = await pageMatrix.save();
        logger.logSuccessEvent("Success addPageMatrix", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addPageMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addPageMatrix", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addPageMatrix", error);
    }
});


router.post("/deletePageMatrix", async function (req, res) {
    logger.logString("Enter: deletePageMatrix");
    try {
        logger.logObj(req.body);
        let page_matrix_id = req.body.page_matrix_id;
        let result = await PageMatrix.deleteOne({ 'page_matrix_id': page_matrix_id }, req.body);
        logger.logSuccessEvent("Success deletePageMatrix", result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success deletePageMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deletePageMatrix", error.message, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deletePageMatrix", error);
    }
});


//formElement
router.get("/getUniqueFormElement", async function (req, res) {
    logger.logString("Enter: getUniqueFormElement");
    try {
        let result = await FormElement.aggregate([
            {
                "$group": {
                    "_id": "$form_element_name",
                    "doc": { "$first": "$$ROOT" }
                }
            },
            {
                "$replaceRoot": {
                    "newRoot": "$doc"
                }
            }
        ]);
        logger.logStringWithObj("getUniqueFormElement::result:", result);
        logger.logSuccessEvent("Success getUniqueFormElement", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getUniqueFormElement", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getUniqueFormElement", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getUniqueFormElement", error);
    }
});

router.get("/getDataOfFormElement", async function (req, res) {
    logger.logString("Enter: getDataOfFormElement");
    try {
        logger.logObj(req.query);
        let form_element_name = req.query.form_element_name;
        let result = await FormElement.find({ "form_element_name": form_element_name });
        logger.logSuccessEvent("Success getDataOfFormElement", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getDataOfFormElement", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getDataOfFormElement", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getDataOfFormElement", error);
    }
});


router.post("/addFormElement", async function (req, res) {
    logger.logString("Enter: addFormElement");
    try {
        logger.logObj(req.body);
        let formElement = new FormElement(req.body);
        formElement.form_element_id = Utils.smallUID();
        let result = await formElement.save();

        logger.logSuccessEvent("Success addFormElement", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addFormElement", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addFormElement", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addFormElement", error);
    }
});


router.post("/updateFormElement", async function (req, res) {
    logger.logString("Enter: updateFormElement");
    try {
        logger.logObj(req.body);
        let form_element_id = req.body.form_element_id;
        let result = await FormElement.updateOne({ 'form_element_id': form_element_id }, req.body);
        logger.logSuccessEvent("Success updateFormElement", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateFormElement", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateFormElement", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateFormElement", error);
    }
});

router.post("/deleteFormElement", async function (req, res) {
    logger.logString("Enter: deleteFormElement");
    try {
        logger.logObj(req.body);
        let form_element_id = req.body.form_element_id;
        let result = await FormElement.deleteOne({ 'form_element_id': form_element_id }, req.body);
        logger.logSuccessEvent("Success deleteFormElement", result, req.body, constants.EVENT_TYPES.DELETE);
        response.success(req, res, "Success deleteFormElement", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deleteFormElement", error.message, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error in deleteFormElement", error);
    }
});

/////////////////////////////////////////////////////
//maker-checker
router.post("/addMakerCheckerRequest", async function (req, res) {
    logger.logString("Enter: addMakerCheckerRequest");
    try {
        logger.logObj(req.body);
        let makerCheckerRequest = new MakerCheckerRequest(req.body);
        makerCheckerRequest.maker_checker_request_id = Utils.smallUID();
        let result = await makerCheckerRequest.save();

        logger.logSuccessEvent("Success addMakerCheckerRequest", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addMakerCheckerRequest", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addMakerCheckerRequest", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addMakerCheckerRequest", error);
    }
});


router.get("/getMakerCheckerRequestFromStatus", async function (req, res) {
    logger.logString("Enter: getMakerCheckerRequestFromStatus");
    try {
        logger.logObj(req.query);
        let current_status = req.query.current_status;
        let result = await MakerCheckerRequest.find({ "current_status": current_status });
        logger.logSuccessEvent("Success getMakerCheckerRequestFromStatus", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getMakerCheckerRequestFromStatus", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getMakerCheckerRequestFromStatus", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getMakerCheckerRequestFromStatus", error);
    }
});



router.post("/approveMakerCheckerRequest", async function (req, res) {
    logger.logString("Enter: approveMakerCheckerRequest");
    try {
        logger.logObj(req.body);
        let result = null;
        let makerCheckerRequest = new MakerCheckerRequest(req.body);
        let flagCustomSave = false;
        //perform the operation
        //check if it is create/update/delete
        if (makerCheckerRequest.maker_CRUD_operation == constants.CRUD_TYPE.CREATE) {
            logger.logString("It is an APPOVAL of create request for component: " + makerCheckerRequest.maker_component_name);
            if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                obj = new FormElement(makerCheckerRequest.maker_object);
                obj.form_element_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_SECTOR) {
                obj = new SectorDetail(makerCheckerRequest.maker_object);
                obj.sector_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ENTITY) {
                obj = new EntityDetail(makerCheckerRequest.maker_object);
                obj.entity_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PERMISSION) {
                obj = new PermissionDetail(makerCheckerRequest.maker_object);
                obj.permission_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ROLE) {
                //add role and page matrix associated with it
                flagCustomSave = true;
                obj = new RoleDetail(makerCheckerRequest.maker_object);
                obj.role_id = Utils.smallUID();
                result = await obj.save();
                logger.logStringWithObj("makerchecker:createRole:result:", result);
                //create page matrix
                let pageMatrix = new PageMatrix();
                pageMatrix.page_matrix_id = Utils.smallUID();
                pageMatrix.role_id = result.role_id;
                pageMatrix.can_be_deleted = false;
                result = await pageMatrix.save();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_RULE) {
                obj = new RuleDetail(makerCheckerRequest.maker_object);
                obj.rule_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.EDIT_USER_PROFILE) {
                obj = new UserDetail(makerCheckerRequest.maker_object);
                obj.user_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PAGE_MATRIX) {
                obj = new PageMatrix(makerCheckerRequest.maker_object);
                obj.page_matrix_id = Utils.smallUID();
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                obj = new FormElement(makerCheckerRequest.maker_object);
                obj.form_element_id = Utils.smallUID();
            }
            if (!flagCustomSave)
                result = await obj.save();
            logger.logStringWithObj("Created " + makerCheckerRequest.maker_component_name, result);
        }
        else if (makerCheckerRequest.maker_CRUD_operation == constants.CRUD_TYPE.UPDATE) {
            logger.logString("It is an update request");
            if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                result = await FormElement.updateOne({ 'form_element_id': makerCheckerRequest.maker_object.form_element_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_SECTOR) {
                result = await SectorDetail.updateOne({ 'sector_id': makerCheckerRequest.maker_object.sector_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ENTITY) {
                result = await EntityDetail.updateOne({ 'entity_id': makerCheckerRequest.maker_object.entity_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PERMISSION) {
                result = await PermissionDetail.updateOne({ 'permission_id': makerCheckerRequest.maker_object.permission_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ROLE) {
                result = await RoleDetail.updateOne({ 'role_id': makerCheckerRequest.maker_object.role_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_RULE) {
                result = await RuleDetail.updateOne({ 'rule_id': makerCheckerRequest.maker_object.rule_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.EDIT_USER_PROFILE) {
                result = await UserDetail.updateOne({ 'user_id': makerCheckerRequest.maker_object.user_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PAGE_MATRIX) {
                result = await PageMatrix.updateOne({ 'page_matrix_id': makerCheckerRequest.maker_object.page_matrix_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                result = await FormElement.updateOne({ 'form_element_id': makerCheckerRequest.maker_object.form_element_id }, makerCheckerRequest.maker_object);
            }
            logger.logStringWithObj("Updated " + makerCheckerRequest.maker_component_name, result);
        }
        else if (makerCheckerRequest.maker_CRUD_operation == constants.CRUD_TYPE.DELETE) {
            logger.logString("It is a delete request");
            if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                result = await FormElement.deleteOne({ 'form_element_id': makerCheckerRequest.maker_object.form_element_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_SECTOR || makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.LIST_SECTOR) {
                result = await SectorDetail.deleteOne({ 'sector_id': makerCheckerRequest.maker_object.sector_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ENTITY || makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.LIST_ENTITY) {
                result = await specificDeleteEntityDetail(makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PERMISSION || makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.LIST_PERMISSION) {
                result = await PermissionDetail.deleteOne({ 'permission_id': makerCheckerRequest.maker_object.permission_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_ROLE) {
                result = await RoleDetail.deleteOne({ 'role_id': makerCheckerRequest.maker_object.role_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_RULE) {
                result = await RuleDetail.deleteOne({ 'rule_id': makerCheckerRequest.maker_object.rule_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.EDIT_USER_PROFILE) {
                result = await UserDetail.deleteOne({ 'user_id': makerCheckerRequest.maker_object.user_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_PAGE_MATRIX) {
                result = await PageMatrix.deleteOne({ 'page_matrix_id': makerCheckerRequest.maker_object.page_matrix_id }, makerCheckerRequest.maker_object);
            } else if (makerCheckerRequest.maker_component_name == constants.COMPONENT_NAME.CRUD_FORM_ELEMENT) {
                result = await FormElement.deleteOne({ 'form_element_id': makerCheckerRequest.maker_object.form_element_id }, makerCheckerRequest.maker_object);
            }
            logger.logStringWithObj("Deleted " + makerCheckerRequest.maker_component_name, result);
        }

        logger.logString("Now Updating MakerCheckerRequest entry in DB");
        makerCheckerRequest.current_status = constants.MAKER_CHECKER_REQUEST_STATUS.APPROVE;
        result = await MakerCheckerRequest.updateOne({ 'maker_checker_request_id': makerCheckerRequest.maker_checker_request_id }, makerCheckerRequest);
        logger.logSuccessEvent("Success approveMakerCheckerRequest", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success approveMakerCheckerRequest", result);
    } catch (error) {
        await logger.logErrorEvent("Error in approveMakerCheckerRequest", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in approveMakerCheckerRequest", error);
    }
});


router.post("/declineMakerCheckerRequest", async function (req, res) {
    logger.logString("Enter: declineMakerCheckerRequest");
    try {
        logger.logObj(req.body);
        let result = null;
        let makerCheckerRequest = new MakerCheckerRequest(req.body);
        makerCheckerRequest.current_status = constants.MAKER_CHECKER_REQUEST_STATUS.DECLINE;
        result = await MakerCheckerRequest.updateOne({ 'maker_checker_request_id': makerCheckerRequest.maker_checker_request_id }, makerCheckerRequest);
        logger.logSuccessEvent("Success declineMakerCheckerRequest", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success declineMakerCheckerRequest", result);
    } catch (error) {
        await logger.logErrorEvent("Error in declineMakerCheckerRequest", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in declineMakerCheckerRequest", error);
    }
});


router.post("/cancelMakerCheckerRequest", async function (req, res) {
    logger.logString("Enter: cancelMakerCheckerRequest");
    try {
        logger.logObj(req.body);
        let result = null;
        let makerCheckerRequest = new MakerCheckerRequest(req.body);
        makerCheckerRequest.current_status = constants.MAKER_CHECKER_REQUEST_STATUS.CANCEL;
        result = await MakerCheckerRequest.updateOne({ 'maker_checker_request_id': makerCheckerRequest.maker_checker_request_id }, makerCheckerRequest);
        logger.logSuccessEvent("Success cancelMakerCheckerRequest", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success cancelMakerCheckerRequest", result);
    } catch (error) {
        await logger.logErrorEvent("Error in cancelMakerCheckerRequest", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in cancelMakerCheckerRequest", error);
    }
});




//approval matrix
router.get("/getApprovalMatrixFromUserType", async function (req, res) {
    logger.logString("Enter: getApprovalMatrixFromUserType");
    try {
        logger.logObj(req.query);
        let user_type = req.query.user_type;
        let result = await ApprovalMatrix.find({ "user_type": user_type });
        logger.logSuccessEvent("Success getApprovalMatrixFromUserType", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getApprovalMatrixFromUserType", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getApprovalMatrixFromUserType", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getApprovalMatrixFromUserType", error);
    }
});

router.post("/updateSpecificApprovalMatrix", async function (req, res) {
    logger.logString("Enter: updateSpecificApprovalMatrix");
    try {
        logger.logObj(req.body);
        let approval_matrix_id = req.body.approval_matrix_id;
        let result = await ApprovalMatrix.updateOne({ 'approval_matrix_id': approval_matrix_id }, req.body);
        logger.logSuccessEvent("Success updateSpecificApprovalMatrix", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success updateSpecificApprovalMatrix", result);
    } catch (error) {
        await logger.logErrorEvent("Error in updateSpecificApprovalMatrix", error.message, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in updateSpecificApprovalMatrix", error);
    }
});

//specific rule

router.get("/getSpecificRuleForRole", async function (req, res) {
    logger.logString("Enter: getSpecificRuleForRole");
    try {
        logger.logObj(req.query);
        let role_id = req.query.role_id;
        let result = await SpecificRule.findOne({ "role_id": role_id });
        logger.logSuccessEvent("Success getSpecificRuleForRole", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getSpecificRuleForRole", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getSpecificRuleForRole", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getSpecificRuleForRole", error);
    }
});


//restoration
router.post("/runRestoration", async function (req, res) {
    logger.logString("Enter: runRestoration");
    try {

        //1- check the roles that were created/modified since last restoration

        let resultRoleDetail = await RoleDetail.find();
        let matchingUserIdArr = [];
        // logger.logStringWithObj("resultRoleDetail::", resultRoleDetail);


        //2- get the users that match the new access policies of the roles


        //3- get the users that match the new rules of these roles Roles->Rule->User
        for (let k = 0; k < resultRoleDetail.length; k++) {
            let roleDetail = resultRoleDetail[k];
            logger.logString("\nCHECKING FOR ROLE : '" + roleDetail.role_name + "'");

            //first check if the role assigned any direct users
            matchingUserIdArr.push(...roleDetail.allowed_user_id_arr);

            //now check for matching rules
            if (roleDetail.rule_associated) {
                //get the specific rule of this roleDetail        
                let specificRule = await SpecificRule.findOne({ 'role_id': roleDetail.role_id });
                // logger.logStringWithObj("specificRule::", specificRule);
                logger.logString("SpecificRule attribute name ::" + specificRule.attribute_name + " value:" + specificRule.attribute_value);
                //check the rule matching for each of the attributes
                // for (let z = 0; z < constants.ARRAY_SPECIFIC_RULE_ATTRIBUTE_NAMES.length; z++) {
                if (specificRule.attribute_name == constants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_department) {
                    //check for users that has this department
                    let resultUserDetail = await UserDetail.find({ 'department': specificRule.attribute_value });
                    logger.logString("Total users matching Department rule is:" + resultUserDetail.length);
                    //for all these users add this role
                    for (let p = 0; p < resultUserDetail.length; p++) {
                        let userDetail = resultUserDetail[p];
                        logger.logString("The user '" + userDetail.username + "' will be updated for 'DEPARTMENT' Rule as its value is " + specificRule.attribute_value);
                        matchingUserIdArr.push(userDetail.user_id);
                    }
                } else if (specificRule.attribute_name == constants.SPECIFIC_RULE_ATTRIBUTE_NAMES.crudUser_job_title) {
                    //check for users that has this job title
                    let resultUserDetail = await UserDetail.find({ 'job_title': specificRule.attribute_value });
                    logger.logString("Total users matching Job Title rule is:" + resultUserDetail.length);
                    //for all these users add this role
                    for (let p = 0; p < resultUserDetail.length; p++) {
                        let userDetail = resultUserDetail[p];
                        logger.logString("The user '" + userDetail.username + "' will be updated for 'JOB TITLE' Rule as its value is " + specificRule.attribute_value);
                        matchingUserIdArr.push(userDetail.user_id);
                    }
                }
            }

            //remove duplicates from matchingUserIdArr
            logger.logStringWithObj("Current matchingUserIdArr ", matchingUserIdArr);
            matchingUserIdArr = Utils.removeDuplicatesFromArray(matchingUserIdArr);
            logger.logStringWithObj("After removing duplicates, matchingUserIdArr", matchingUserIdArr);

            if (matchingUserIdArr.length == 0) {
                //no matching users found, so go to next role
                logger.logString("No matching Users found for Role ID: '" + roleDetail.role_id + "' and Role Name: '" + roleDetail.role_name + "'");
                continue;
            }

            //IGNORE PERMISSIONS AT THIS POINT OF TIME - THIS FEATURE WILL NEED TO BE ADDED DOWN THE LINE. CURRENTLY USE THE PERMISSIONS SPECIFIED IN THE ROLE
            //4- create entry in the mappinguserentity table - 
            //for each of the roles, get the access policy and from it get the matching entities
            let matchingEntityIdArr = [];
            let resultAccessPolicy = await AccessPolicy.find({ 'access_policy_id': { $in: roleDetail.allowed_access_policy_id_arr } });
            for (let a1 = 0; a1 < resultAccessPolicy.length; a1++) {
                let accessPolicy = resultAccessPolicy[a1];
                matchingEntityIdArr.push(...accessPolicy.entity_id_arr);
                // //check if the user already has this entity and if not then add one
                // let resultMappingUserEntity = await MappingUserEntity.find({ 'entity_id': { $nin: accessPolicy.entity_id_arr } });
                // logger.logStringWithObj("These are the users to which new entity entries will need to be created....", resultMappingUserEntity);
                // let resultEntityDetailPolicy = await EntityDetail.find({ 'entity_id': { $in: accessPolicy.entity_id_arr } });
            }

            //remove duplicates from matchingEntityIdArr
            logger.logStringWithObj("matchingEntityIdArr", matchingEntityIdArr);
            matchingEntityIdArr = Utils.removeDuplicatesFromArray(matchingEntityIdArr);

            //traverse each entity and find corresponding entry in MappingUserDetail table and if not found then insert it
            for (let e1 = 0; e1 < matchingEntityIdArr.length; e1++) {
                let entityIdToFind = matchingEntityIdArr[e1];
                logger.logString("Looking for entity: " + entityIdToFind);

                //fill the array of users whose entries are to be made inside MappingUserEntity table
                let mappingUserEntityUserIdArr = [];
                mappingUserEntityUserIdArr.push(...matchingUserIdArr);

                //find all the users from the mappingUserEntityTable that has this entity id and remove those users from the mappingUserEntityUserIdArr array
                let userIdsInMappingUserEntityArr = await MappingUserEntity.find({ 'entity_id': entityIdToFind }, { _id: 0, user_id: 1 });
                logger.logStringWithObj("userIdsInMappingUserEntityArr:::", userIdsInMappingUserEntityArr);
                for (let f1 = 0; f1 < userIdsInMappingUserEntityArr.length; f1++) {
                    let userIdToFind = userIdsInMappingUserEntityArr[f1].user_id;
                    if (mappingUserEntityUserIdArr.includes(userIdToFind)) {
                        logger.logStringWithObj("Removing userId :'" + userIdToFind + "' from the mappingUserEntityUserIdArr");
                        mappingUserEntityUserIdArr = Utils.removeItemFromStringArray(mappingUserEntityUserIdArr, userIdToFind);
                    }
                    // logger.logStringWithObj("matchingUserIdArr 22", matchingUserIdArr);
                    // logger.logString("Looking for user: " + userIdToFind + " and its index found is: " + matchingUserIdArr.indexOf(userIdToFind));
                    // if (matchingUserIdArr.indexOf(userIdToFind) < 0) {
                    //     logger.logString("uuser with id: '" + userIdToFind + "' will be added entity: '" + entityIdToFind + "'")
                    // }
                }

                logger.logStringWithObj("The Users that will be assinged entityId: '" + entityIdToFind + "' are: ", mappingUserEntityUserIdArr);
                for (let m1 = 0; m1 < mappingUserEntityUserIdArr.length; m1++) {
                    let mappingUserEntity = new MappingUserEntity();
                    mappingUserEntity.mapping_user_entity_id = Utils.smallUID();
                    mappingUserEntity.role_id = roleDetail.role_id;
                    mappingUserEntity.user_id = mappingUserEntityUserIdArr[m1];
                    logger.logString("Map")
                    let u1 = await UserDetail.findOne({ 'user_id': mappingUserEntity.user_id }, { _id: 0, username: 1 });
                    mappingUserEntity.username = u1.username;
                    mappingUserEntity.entity_id = entityIdToFind;
                    mappingUserEntity.current_status = constants.ACCESS_ENTITY_STATUS.PENDING;
                    mappingUserEntity.permissions_arr = roleDetail.allowed_permission_id_arr;
                    let newRes = await mappingUserEntity.save();
                }

            }

        }//end of restoration

        let result = constants.DEFAULT_MESSAGES.SUCCESS;
        logger.logSuccessEvent("Success runRestoration", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success runRestoration", result);
    } catch (error) {
        await logger.logErrorEvent("Error in runRestoration:" + error.message, error, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in runRestoration", error);
    }
});


//////////
module.exports = router;