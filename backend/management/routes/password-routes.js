let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let PasswordInfo = require('../models/PasswordInfoSchema');
let PasswordScheduler = require('../models/PasswordSchedulerSchema');

let DEFAULT_DEVICE_UID = "0";

router.get("/getAllPasswordInfo", async function (req, res) {
    logger.logString("Enter: getAllPasswordInfo");
    logger.logObj(req.params);
    try {
        let result = await PasswordInfo.find();
        logger.logStringWithObj("Success getAllPasswordInfo", result);
        response.success(req, res, "Success getAllPasswordInfo", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in getAllPasswordInfo", error.message);
        response.serverError(req, res, "Error in getAllPasswordInfo", error);
    }
});


router.post("/addPasswordInfo", async function (req, res) {
    logger.logString("Enter: addPasswordInfo");
    try {
        logger.logObj(req.body);
        let passwordInfo = new PasswordInfo(req.body);
        result = await passwordInfo.save();
        logger.logStringWithObj("Success addPasswordInfo", result);
        response.success(req, res, "Success addPasswordInfo", result);

    } catch (error) {
        logger.logErrorStringWithObj("Error in addPasswordInfo", error.message);
        response.serverError(req, res, "Error in addPasswordInfo", error.message);
    }
});



router.get("/getCurrentPasswordScheduler", async function (req, res) {
    logger.logString("Enter: getCurrentPasswordScheduler");
    logger.logObj(req.params);
    try {
        let result = await PasswordScheduler.findOne();
        logger.logStringWithObj("Success getCurrentPasswordScheduler", result);
        response.success(req, res, "Success getCurrentPasswordScheduler", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in getCurrentPasswordScheduler", error.message);
        response.serverError(req, res, "Error in getCurrentPasswordScheduler", error);
    }
});


router.post("/updatePasswordScheduler", async function (req, res) {
    logger.logString("Enter: updatePasswordScheduler");
    logger.logObj(req.body);
    try {
        let result = await PasswordScheduler.updateOne(req.body);
        logger.logStringWithObj("Success updatePasswordScheduler", result);
        response.success(req, res, "Success updatePasswordScheduler", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error in updatePasswordScheduler", error.message);
        response.serverError(req, res, "Error in updatePasswordScheduler", error);
    }
});


////////////////
module.exports = router;

