let express = require('express');
let router = express.Router();
let logger = require('../libs/Logger');
let response = require('../libs/Response');
let Utils = require('../utils/Utils');
let constants = require('../config/constants');
let EntityDetail = require('../models/EntityDetailSchema');
let UserDetail = require('../models/UserDetailSchema');
let MappingUserEntity = require('../models/MappingUserEntitySchema');
let RoleDetail = require('../models/RoleDetailSchema');
let Defaults = require('../utils/Defaults');

let ActiveDirectoryConnector = require('../models/connectorModels/database/ActiveDirectoryConnectorSchema');
let LinuxConnector = require('../models/connectorModels/database/LinuxConnectorSchema');
let MongoDBConnector = require('../models/connectorModels/database/MongoDBConnectorSchema');
let ConnectMongoDB = require('../connectors/ConnectMongoDB');
let ConnectActiveDirectory = require('../connectors/ConnectActiveDirectory');
let ConnectLinux = require('../connectors/ConnectLinux');

///////////////////////////// MONGODB

router.post("/MongoDBCreateUser", async function (req, res) {
    logger.logString("Enter Connector: MongoDBCreateUser");
    try {
        logger.logObj(req.body);
        let mongoDBConnector = new MongoDBConnector(req.body);
        logger.logStringWithObj("mongoDBConnector:::", mongoDBConnector);

        const connectMongoDB = new ConnectMongoDB();
        let returnMessage = await connectMongoDB.createDBUser(mongoDBConnector);
        if (returnMessage == constants.DEFAULT_MESSAGES.SUCCESS) {
            logger.logString("User Credentials are successfully created. Now Sending Email.");
            try {
                sendCreatedCredentialsEmail(mongoDBConnector.connector_type, mongoDBConnector);
            } catch (err) {
                logger.logErrorStringWithObj("Error in sending email...", err);
            }

        } else {
            throw new Error(returnMessage);
        }
        // let result = null;
        // await logger.logSuccessEvent("Success MongoDBCreateUser", result, req.body, constants.EVENT_TYPES.UPDATE);
        response.success(req, res, "Success MongoDBCreateUser", null);
    } catch (error) {
        // await logger.logErrorEvent("Error in MongoDBCreateUser", error.message, req, constants.EVENT_TYPES.UPDATE);
        logger.logStringWithObj("Error:::", error);
        response.serverError(req, res, "Error in MongoDBCreateUser", error);
    }
});


router.post("/addMongoDBConnector", async function (req, res) {
    logger.logString("Enter: addMongoDBConnector");
    try {
        logger.logObj(req.body);
        let mongodbConnector = new MongoDBConnector(req.body);
        mongodbConnector.connector_id = Utils.smallUID();
        let result = await mongodbConnector.save();

        logger.logSuccessEvent("Success addMongoDBConnector", result, req.body, constants.EVENT_TYPES.CREATE);
        response.success(req, res, "Success addMongoDBConnector", result);
    } catch (error) {
        await logger.logErrorEvent("Error in addMongoDBConnector:" + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in addMongoDBConnector", error);
    }
});

router.get("/getAllDBConnectors", async function (req, res) {
    logger.logString("Enter: getAllDBConnectors");
    try {
        logger.logObj(req.query);
        // get all db connectors
        let result = await MongoDBConnector.find();
        logger.logSuccessEvent("Success getAllDBConnectors", result, req.query, constants.EVENT_TYPES.READ);
        response.success(req, res, "Success getAllDBConnectors", result);
    } catch (error) {
        await logger.logErrorEvent("Error in getAllDBConnectors:" + error.message, error, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getAllDBConnectors", error);
    }
});


////////////////////// ACTIVE DIRECTORY
router.post("/ActiveDirectoryCreateUser", async function (req, res) {
    logger.logString("ActiveDirectoryCreateUser: Enter");
    logger.logObj(req.body);

    try {
        let activeDirectoryConnector = new ActiveDirectoryConnector(req.body);
        let userDetail = await UserDetail.findOne({ 'user_id': activeDirectoryConnector.source_user_id });

        const connectActiveDirectory = new ConnectActiveDirectory();
        let returnMessage = await connectActiveDirectory.createActiveDirectoryUser(activeDirectoryConnector, userDetail);
        if (returnMessage == constants.DEFAULT_MESSAGES.SUCCESS) {
            logger.logString("AD User Credentials are successfully created. Now Sending Email.");
            sendCreatedCredentialsEmail(activeDirectoryConnector.connector_type, activeDirectoryConnector);
        }
        let result = null;
        response.success(req, res, "Success ActiveDirectoryCreateUser", result);

    } catch (error) {
        logger.logErrorEvent("Error in ActiveDirectoryCreateUser: " + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in ActiveDirectoryCreateUser", error);
    }
});


///////////////////////// LINUX
router.post("/LinuxCreateUser", async function (req, res) {
    logger.logString("LinuxCreateUser: Enter");
    logger.logObj(req.body);

    try {
        let linuxConnector = new LinuxConnector(req.body);
        let userDetail = await UserDetail.findOne({ 'user_id': linuxConnector.source_user_id });

        const connectLinux = new ConnectLinux();
        let returnMessage = await connectLinux.createUser(linuxConnector, userDetail);
        if (returnMessage == constants.DEFAULT_MESSAGES.SUCCESS) {
            logger.logString("Linux User Credentials are successfully created. Now Sending Email.");
            sendCreatedCredentialsEmail(linuxConnector.connector_type, linuxConnector);
        }
        let result = null;
        response.success(req, res, "Success LinuxCreateUser", result);

    } catch (error) {
        logger.logErrorEvent("Error in LinuxCreateUser: " + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in LinuxCreateUser", error);
    }
});


/**
 * Deleted LDAP User
 */
router.post("/ActiveDirectoryDeleteUser", async function (req, res) {
    logger.logString("ActiveDirectoryDeleteUser: Enter");
    logger.logObj(req.body);

    //1 - read the MappingUserEntity

    try {
        let activeDirectoryConnector = new ActiveDirectoryConnector(req.body);
        let userDetail = await UserDetail.findOne({ 'user_id': activeDirectoryConnector.source_user_id });

        const connectActiveDirectory = new ConnectActiveDirectory();
        let returnMessage = await connectActiveDirectory.createActiveDirectoryUser(activeDirectoryConnector, userDetail);
        if (returnMessage == constants.DEFAULT_MESSAGES.SUCCESS) {
            logger.logString("AD User Credentials are successfully created. Now Sending Email.");
            sendCreatedCredentialsEmail(activeDirectoryConnector.connector_type, activeDirectoryConnector);
        }
        let result = null;
        response.success(req, res, "Success ActiveDirectoryDeleteUser", result);

    } catch (error) {
        logger.logErrorEvent("Error in ActiveDirectoryDeleteUser: " + error.message, error, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, "Error in ActiveDirectoryDeleteUser", error);
    }
});



//////////////////////////// COMMON

router.post("/provisionUser", async function (req, res) {
    // logger.logString("Enter: provisionUser");
    try {
        logger.logObj(req.body);
        //1 - get all mappingUserEntity
        //2 - based on each of the entity it is connected, delete its credentials
        //3 - delete the MappingUserEntity documents of this user
        let result = null;
        let user_id = req.body.user_id;
        logger.logString("START PROVISION oF USER: " + user_id);

        logger.logString("END PROVISION oF USER: " + user_id);
        logger.logSuccessEvent("Success provisionUser", result, req.body, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
        response.success(req, res, "Success provisionUser", result);
    } catch (error) {
        await logger.logErrorEvent("Error in provisionUser:" + error.message, error, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in provisionUser", error);
    }
});


router.post("/deprovisionUser", async function (req, res) {
    // logger.logString("Enter: deprovisionUser");
    try {
        logger.logObj(req.body);
        //1 - get all mappingUserEntity
        //2 - based on each of the entity it is connected, delete its credentials
        //3 - delete the MappingUserEntity documents of this user

        let result = null;
        let user_id = req.body.user_id;
        logger.logString("START DEPROVISION oF USER: " + user_id);
        //1
        logger.logString("STEP 1");
        let mappingUserEntityArr = await MappingUserEntity.find({ 'user_id': user_id });
        logger.logStringWithObj("mappingUserEntityArr:", mappingUserEntityArr);
        for (let k = 0; k < mappingUserEntityArr.length; k++) {
            logger.logString("STEP 2");
            logger.logStringWithObj("mappingUserEntity[k]:", mappingUserEntityArr[k]);
            let entity_id = mappingUserEntityArr[k].entity_id;
            let entityDetail = await EntityDetail.findOne({ 'entity_id': entity_id });
            logger.logStringWithObj("entityDetail:", entityDetail);
            if (entityDetail == null) {
                logger.logString("Entity Detail is null. Surprising. Someone messed up with database configuration.");
                continue;
            }
            let connector_type = entityDetail.connector_type;
            if (connector_type == constants.CONNECTOR_TYPE.MONGODB) {
                const connectMongoDB = new ConnectMongoDB();
                //2
                logger.logString("Deleting the MongoDB Credentials");
                await connectMongoDB.deleteDBUser(entityDetail.connector_object.host_name, entityDetail.connector_object.host_port, mappingUserEntityArr[k].login_name);
            } else if (connector_type == constants.CONNECTOR_TYPE.ACTIVE_DIRECTORY) {
                const connectActiveDirectory = new ConnectActiveDirectory();
                logger.logString("Deleting the Active Directory Credentials");
                await connectActiveDirectory.deleteActiveDirectoryUser(entityDetail.connector_object, mappingUserEntityArr[k]);
            } else if (connector_type == constants.CONNECTOR_TYPE.LINUX) {
                const connectLinux = new ConnectLinux();
                logger.logString("Deleting the Linux Credentials");
                await connectLinux.deleteUser(entityDetail.connector_object, mappingUserEntityArr[k]);
            } else {
                logger.logString("Connector type: " + connector_type + " is not yet implemented.");
            }

            //3
            logger.logString("STEP 3");
            logger.logString("Deleting the MappingUserEntity document whoise mapping_user_entity_id is " + mappingUserEntityArr[k].mapping_user_entity_id);
            await MappingUserEntity.deleteOne({ 'mapping_user_entity_id': mappingUserEntityArr[k].mapping_user_entity_id });

            //4
            //delete the users from associated_users_id_arr of the roles for this user
            let matchingRoleDetailArr = await RoleDetail.find({ "allowed_user_id_arr": { $in: [mappingUserEntityArr[k].user_id] } });
            for (let z = 0; z < matchingRoleDetailArr.length; z++) {
                let roleDetail = matchingRoleDetailArr[z];
                logger.logString("Removing userId: '" + mappingUserEntityArr[k].user_id + "' from role: '" + roleDetail.role_id + "'");
                let allowedUserIdArr = roleDetail.allowed_user_id_arr;
                roleDetail.allowed_user_id_arr = Utils.removeItemFromStringArray(allowedUserIdArr, mappingUserEntityArr[k].user_id);
                let r2 = await RoleDetail.updateOne({ 'role_id': roleDetail.role_id }, roleDetail);

            }

        }
        logger.logString("END DEPROVISION oF USER: " + user_id);
        logger.logSuccessEvent("Success deprovisionUser", result, req.body, constants.EVENT_TYPES.UPDATE, constants.ALERT_LEVEL.MEDIUM);
        response.success(req, res, "Success deprovisionUser", result);
    } catch (error) {
        await logger.logErrorEvent("Error in deprovisionUser:" + error.message, error, req, constants.EVENT_TYPES.UPDATE);
        response.serverError(req, res, "Error in deprovisionUser", error);
    }
});



//EMAILS
function sendCreatedCredentialsEmail(connectorType, connectorObj) {
    logger.logString("sendCreatedCredentialsEmail:About to send email.");
    logger.logStringWithObj("connectorType:", connectorType);
    logger.logStringWithObj("connectorObj:", connectorObj);
    try {
        if (connectorType == constants.CONNECTOR_TYPE.MONGODB) {
            let mongoDBConnector = new MongoDBConnector(connectorObj);
            logger.logStringWithObj("sendEmail:mongoDBConnector:", mongoDBConnector);
            //send email to the user
            toEmail = constants.EMAIL_CREDENTIALS.DEFAULT_TO;
            subject = "User Credentials for " + mongoDBConnector.connector_name;
            text = "Here are the credentials for your <b>'" + mongoDBConnector.connector_name + "'</b> Entity :"
                + "<br><b> Hostname: </b> 'mongodb://" + mongoDBConnector.host_name + ":" + mongoDBConnector.host_port + "'"
                + "<br><b> Username: </b> " + mongoDBConnector.new_login_name
                + "<br><b> Password: </b> " + mongoDBConnector.new_password;
            Utils.sendEmail(toEmail, subject, text, true);
        } else if (connectorType == constants.CONNECTOR_TYPE.ACTIVE_DIRECTORY) {
            let activeDirectoryConnector = new ActiveDirectoryConnector(connectorObj);
            //send email to the user
            toEmail = constants.EMAIL_CREDENTIALS.DEFAULT_TO;
            subject = "User Credentials for " + activeDirectoryConnector.connector_name;
            text = "Here are the credentials for your " + activeDirectoryConnector.connector_name + " Entity :"
                + "<br><b> Hostname: </b> 'ldap://" + activeDirectoryConnector.host_name + ":" + activeDirectoryConnector.host_port + "/" + activeDirectoryConnector.container_dn + "'"
                + "<br><b> Username: </b> " + activeDirectoryConnector.new_login_name
                + "<br><b> Password: </b> " + activeDirectoryConnector.new_password;
            Utils.sendEmail(toEmail, subject, text, true);
        } else {
            logger.logString("No valid connectorType found");
        }
    } catch (error) {
        logger.logErrorStringWithObj("Error in sendCreatedCredentialsEmail:", error);
    }




}
//////////
module.exports = router;
