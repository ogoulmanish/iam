let express = require('express');
let router = express.Router();
let logger = require('../../libs/Logger');
let response = require('../../libs/Response');
let UserDetail = require('../../models/UserDetailSchema');
let constants = require('../../config/constants');
let Utils = require('../../utils/Utils');
let Defaults = require('../../utils/Defaults');



/**
 * Authenticate MSSQL User
 */
router.post("/authenticateUser", async function (req, res) {
    logger.logString("authenticateUser: Enter");
    try {
        logger.logObj(req.body);
        let result = null;
        logger.logStringWithObj("Success authenticateUser", result);
        response.success(req, res, "Success authenticateUser", result);

    } catch (error) {
        logger.logErrorStringWithObj("Error authenticateUser", error.message);
        response.serverError(req, res, "Error authenticateUser", error);
    }
});


/**
 * Add MSSQL User
 */
router.post("/crudUser/:crudType", async function (req, res) {
    logger.logString("crudUser: Enter");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let crudType = req.params.crudType;
        let result = null;
        switch (crudType) {
            case constants.CRUD_TYPE.CREATE:
                logger.logString("create...");
                break;
            case constants.CRUD_TYPE.READ:
                logger.logString("Read...");
                break;
            case constants.CRUD_TYPE.UPDATE:
                logger.logString("update...");
                break;
            case constants.CRUD_TYPE.DELETE:
                logger.logString("Delete...");
                break;
            default:
                throw new Error("Invalid CRUD Operation");
        }
        logger.logStringWithObj("Success crudUser", result);
        response.success(req, res, "Success crudUser", result);

    } catch (error) {
        logger.logErrorStringWithObj("Error crudUser", error.message);
        response.serverError(req, res, "Error crudUser", error);
    }
});


// /**
//  * Update MSSQL User
//  */
// router.post("/updateUser", async function (req, res) {
//     logger.logString("updateUser: Enter");
//     try {
//         logger.logObj(req.body);
//         let result = null;
//         logger.logStringWithObj("Success updateUser", result);
//         response.success(req, res, "Success updateUser", result);

//     } catch (error) {
//         logger.logErrorStringWithObj("Error updateUser", error.message);
//         response.serverError(req, res, "Error updateUser", error);
//     }
// });



// /**
//  * Delete MSSQL User
//  */
// router.post("/deleteUser", async function (req, res) {
//     logger.logString("deleteUser: Enter");
//     try {
//         logger.logObj(req.body);
//         let result = null;
//         logger.logStringWithObj("Success deleteUser", result);
//         response.success(req, res, "Success deleteUser", result);

//     } catch (error) {
//         logger.logErrorStringWithObj("Error deleteUser", error.message);
//         response.serverError(req, res, "Error deleteUser", error);
//     }
// });


router.get("/testConnection", async function (req, res) {
    logger.logString("testConnection: Enter");
    try {
        await sql.connect('mssql://username:password@localhost/database');
        const result = await sql.query`select * from mytable where id = ${value}`;
        console.dir(result);
        logger.logStringWithObj("Success testConnection", result);
        response.success(req, res, "Success testConnection", result);
    } catch (error) {
        logger.logErrorStringWithObj("Error testConnection", error.message);
        response.serverError(req, res, "Error testConnection", error);
    }
});

//////////
module.exports = router;