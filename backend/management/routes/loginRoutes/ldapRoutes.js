let express = require('express');
let router = express.Router();
let logger = require('../../libs/Logger');
let response = require('../../libs/Response');
let UserDetail = require('../../models/UserDetailSchema');
let constants = require('../../config/constants');
let Utils = require('../../utils/Utils');
let Defaults = require('../../utils/Defaults');

var ldapjs = require('ldapjs');
const ldapOptions = {
    // url: 'ldap://localhost:389',
    url: "ldap://" + constants.ACTIVE_DIRECTORY_SERVER.LDAP_HOST_NAME + ":" + constants.ACTIVE_DIRECTORY_SERVER.LDAP_PORT,
    connectTimeout: 3000000,
    reconnect: true
};
var baseDomain = constants.ACTIVE_DIRECTORY_SERVER.BASE_DOMAIN;
var ldapLoginDN = constants.ACTIVE_DIRECTORY_SERVER.LDAP_LOGIN;
var ldapLoginPassword = constants.ACTIVE_DIRECTORY_SERVER.LDAP_PASSWORD;


router.post("/createLDAPUsersFromHR", async function (req, res) {
    logger.logString("createLDAPUsersFromHR: Enter");
    try {
        const ldapClient = ldapjs.createClient(ldapOptions);
        ldapClient.bind(
            ldapLoginDN,
            ldapLoginPassword,
            async function (err) {
                if (err) {
                    logger.logErrorEvent("Error in createLDAPUsersFromHR", err.message, req, constants.EVENT_TYPES.CREATE);
                    response.serverError(req, res, err.message, err);
                    return;
                }
                try {
                    //get all users from user_detail whose is_added_to_main is false and create LDAP user for them
                    let userDetailArr = await UserDetail.find({ "is_added_to_main": false });
                    for (let k = 0; k < userDetailArr.length; k++) {
                        let userDetail = userDetailArr[k];
                        logger.logStringWithObj("LDAP User to be added:", userDetail);
                        let newUser = {
                            uid: userDetail.username,
                            userPassword: userDetail.password,
                            mail: userDetail.email,
                            givenname: userDetail.first_name,
                            initials: userDetail.middle_name,
                            surname: userDetail.last_name,
                            telephoneNumber: userDetail.phone,
                            objectClass: ["person", "organizationalPerson", "inetOrgPerson"]
                        };
                        let newDn = 'cn=' + userDetail.username + ',' + baseDomain;
                        logger.logString("About to add new user DN:" + newDn);
                        ldapClient.add(
                            newDn,
                            newUser,
                            async (err, res1) => {
                                if (err) {
                                    logger.logErrorEvent("Error in createLDAPUsersFromHR", err.message, req, constants.EVENT_TYPES.CREATE);
                                    // logger.logErrorStringWithObj("Error createLDAPUsersFromHR:", err.message);
                                    response.serverError(req, res, err.message, err);
                                    return;
                                }

                                //update the user
                                userDetail.is_added_to_main = true;

                                //has the password and then update it in database
                                userDetail.password = Utils.getHashedPassword(userDetail.password);

                                let result = await UserDetail.updateOne({ 'user_id': userDetail.user_id }, userDetail);
                                logger.logSuccessEvent("Success updateLdapUser", result, req.body, constants.EVENT_TYPES.UPDATE);

                                // Utils.sendPlainEmail(userDetail.email, "New User", "New user with id '" + userDetail.username + "' is created.");
                                // response.success(req, res, "Success createLDAPUsersFromHR", result);
                            }
                        );
                    }
                    response.success(req, res, "Success createLDAPUsersFromHR", constants.DEFAULT_MESSAGES.SUCCESS);
                } catch (error) {
                    logger.logErrorEvent("Error in createLDAPUsersFromHR", error.message, req, constants.EVENT_TYPES.CREATE);
                    response.serverError(req, res, error.message, error);
                }
            }
        );

    } catch (error) {
        logger.logErrorEvent("Error in createLDAPUsersFromHR", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, error.message, error);
    }
});


/**
 * Add LDAP User
 */
router.post("/addLdapUser", async function (req, res) {
    logger.logString("addLdapUser: Enter");
    try {
        // logger.logObj(req.body);
        let userDetail = new UserDetail(req.body);
        logger.logStringWithObj("LDAP User to be added:", userDetail);
        const ldapClient = ldapjs.createClient(ldapOptions);
        ldapClient.bind(
            ldapLoginDN,
            ldapLoginPassword,
            (err) => {
                if (err) {
                    logger.logErrorEvent("Error in addLdapUser", err.message, req, constants.EVENT_TYPES.CREATE);
                    // logger.logErrorStringWithObj("Error addLdapUser", err.message);
                    response.serverError(req, res, err.message, err);
                    return;
                }
                try {
                    let newUser = {
                        uid: userDetail.username,
                        // username: userDetail.username,
                        userPassword: userDetail.password,
                        mail: userDetail.email,
                        givenname: userDetail.first_name,
                        initials: userDetail.middle_name,
                        surname: userDetail.last_name,
                        telephoneNumber: userDetail.phone,
                        // name: userDetail.full_name,
                        objectClass: ["person", "organizationalPerson", "inetOrgPerson"]
                    };
                    let newDn = 'cn=' + userDetail.username + ',' + baseDomain;
                    logger.logString("About to add DN:" + newDn);
                    ldapClient.add(
                        newDn,
                        newUser,
                        async (err, res1) => {
                            if (err) {
                                logger.logErrorEvent("Error in addLdapUser", err.message, req, constants.EVENT_TYPES.CREATE);
                                // logger.logErrorStringWithObj("Error addLdapUser:", err.message);
                                response.serverError(req, res, err.message, err);
                                return;
                            }
                            logger.logString("Adding LDAP User to Database");
                            //now add the user to the database
                            userDetail.user_id = Utils.smallUID();
                            userDetail.password = Utils.getHashedPassword(userDetail.password);
                            logger.logStringWithObj("Adding LDAP user details to our database", userDetail);
                            let result = await userDetail.save();

                            //CHANGED
                            //initialize user 
                            // Defaults.initUserDetailSettings(userDetail.user_id);
                            // logger.logSuccessEvent("Success addLdapUser", result, req.body, constants.EVENT_TYPES.CREATE, constants.ALERT_LEVEL.HIGH);


                            //logger.logStringWithObj("Success addLdapUser", result);
                            // Utils.sendPlainEmail(userDetail.email, "New User", "New user with id '" + userDetail.username + "' is created.");
                            response.success(req, res, "Success addLdapUser", result);
                        }
                    );
                } catch (error) {
                    logger.logErrorEvent("Error in addLdapUser", error.message, req, constants.EVENT_TYPES.CREATE);
                    response.serverError(req, res, error.message, error);
                }
            }
        );
    } catch (error) {
        logger.logErrorEvent("Error in addLdapUser", error.message, req, constants.EVENT_TYPES.CREATE);
        response.serverError(req, res, error.message, error);
    }
});

/**
 * Update LDAP User
 */
router.post("/updateLdapUser", async function (req, res) {
    logger.logString("updateLdapUser: Enter");

    try {
        logger.logObj(req.body);
        let userDetail = new UserDetail(req.body);
        const ldapClient = ldapjs.createClient(ldapOptions);

        ldapClient.bind(
            ldapLoginDN,
            ldapLoginPassword,
            (err) => {
                if (err) {
                    logger.logErrorEvent("Error in updateLdapUser", err.message, req, constants.EVENT_TYPES.UPDATE);
                    response.serverError(req, res, err.message, err);
                    return;
                }
                try {

                    let dnToModify = 'cn=' + userDetail.username + ',' + baseDomain;
                    logger.logString("About to modify DN:" + dnToModify);

                    var change1 = new ldapjs.Change({
                        operation: 'replace',
                        modification: {
                            mail: userDetail.email
                        }
                    });
                    var change2 = new ldapjs.Change({
                        operation: 'replace',
                        modification: {
                            givenname: userDetail.first_name
                        }
                    });
                    var change3 = new ldapjs.Change({
                        operation: 'replace',
                        modification: {
                            initials: userDetail.middle_name
                        }
                    });
                    var change4 = new ldapjs.Change({
                        operation: 'replace',
                        modification: {
                            surname: userDetail.last_name
                        }
                    });
                    var change5 = new ldapjs.Change({
                        operation: 'replace',
                        modification: {
                            telephoneNumber: userDetail.phone
                        }
                    });

                    var changeArr = [change1, change2, change3, change4, change5]
                    ldapClient.modify(
                        dnToModify,
                        changeArr,
                        async (err, res1) => {
                            if (err) {
                                logger.logErrorEvent("Error in updateLdapUser", err.message, req, constants.EVENT_TYPES.UPDATE);
                                // logger.logErrorStringWithObj("Error updateLdapUser:", err.message);
                                response.serverError(req, res, "Error updateLdapUser:", err.message);
                                return;
                            }
                            logger.logString("Updating LDAP User to Database");
                            //now update the user in the database
                            let result = await UserDetail.updateOne({ 'user_id': userDetail.user_id }, userDetail);
                            logger.logSuccessEvent("Success updateLdapUser", result, req.body, constants.EVENT_TYPES.UPDATE);
                            //logger.logStringWithObj("Success updateLdapUser", result);
                            response.success(req, res, "Success updateLdapUser", result);
                        }
                    );
                } catch (error) {
                    logger.logErrorEvent("Error in updateLdapUser", error.message, req, constants.EVENT_TYPES.UPDATE);
                    // logger.logErrorStringWithObj("Error updateLdapUser = ", error.message);
                    response.serverError(req, res, "Error updateLdapUser = ", error.message);
                }
            }
        );
    } catch (error) {
        logger.logErrorEvent("Error in updateLdapUser", error.message, req, constants.EVENT_TYPES.UPDATE);
        // logger.logErrorStringWithObj("Error updateLdapUser", error.message);
        response.serverError(req, res, "Error updateLdapUser", error);
    }
});


/**
 * Deleted LDAP User
 */

router.post("/deleteLdapUser", async function (req, res) {
    logger.logString("deleteLdapUser: Enter");
    try {
        logger.logObj(req.body);
        let userDetail = new UserDetail(req.body);
        const ldapClient = ldapjs.createClient(ldapOptions);
        ldapClient.bind(
            ldapLoginDN,
            ldapLoginPassword,
            (err) => {
                if (err) {
                    logger.logErrorEvent("Error in deleteLdapUser", err.message, req, constants.EVENT_TYPES.DELETE);
                    // logger.logErrorStringWithObj("Error deleteLdapUser", err.message);
                    response.serverError(req, res, "Error deleteLdapUser", err);
                    return;
                }
                try {
                    let dnToDelete = userDetail.DN;
                    // let dnToDelete = 'cn=' + userDetail.username + ',' + baseDomain;
                    // if (userDetail.username == "admin") {
                    //     //this is unbelievable
                    //     throw new Error("CANNOT DELETE ADMIN.....");
                    // }
                    logger.logString("About to delete DN:" + dnToDelete);
                    ldapClient.del(
                        dnToDelete,
                        async (err, res1) => {
                            if (err) {
                                logger.logErrorEvent("Error in deleteLdapUser", err.message, req, constants.EVENT_TYPES.DELETE);
                                // logger.logErrorStringWithObj("Error deleteLdapUser:", err.message);
                                response.serverError(req, res, "Error deleteLdapUser:", err.message);
                                return;
                            }
                            logger.logString("Delete LDAP User from Database");
                            // let result = await UserDetail.updateOne({ 'user_id': userDetail.user_id }, { 'is_deleted': true });
                            let result = await UserDetail.deleteOne({ 'user_id': userDetail.user_id });

                            await logger.logSuccessEvent("Success deleteLdapUser", result, req.body, constants.EVENT_TYPES.DELETE, constants.ALERT_LEVEL.CRITICAL);
                            response.success(req, res, "Success deleteLdapUser", result);
                        }
                    );
                } catch (error) {
                    logger.logErrorEvent("Error in deleteLdapUser1:" + error.message, error, req, constants.EVENT_TYPES.DELETE);
                    response.serverError(req, res, "Error deleteLdapUser1 = ", error.message);
                }
            }
        );
    } catch (error) {
        logger.logErrorEvent("Error in deleteLdapUser2: " + error.message, error, req, constants.EVENT_TYPES.DELETE);
        response.serverError(req, res, "Error deleteLdapUser2", error);
    }
});



/**
 * Authenticate LDAP User
 */
router.post("/authenticateLDAPUser", async function (req, res) {
    logger.logString("authenticateLDAPUser: Enter");
    try {
        logger.logObj(req.body);
        let username = req.body.username;
        let password = req.body.password;

        const ldapClient = await ldapjs.createClient(ldapOptions);
        let usernameBind = req.body.DN;
        logger.logString("About to search for user:" + usernameBind);
        await ldapClient.bind(
            usernameBind,
            password,
            async (err, res1) => {
                if (err) {
                    logger.logErrorEvent("Error in authenticateLDAPUser", err.message, req, constants.EVENT_TYPES.READ);
                    response.serverError(req, res, err.message, err);
                    return;
                }
                logger.logStringWithObj("Result from LDAP:", res1);
                //1 - Update the user details from ldap to mongodb

                //2 - return the mongodb user back
                let existingUserDetail = await UserDetail.findOne({ 'username': username });
                if (!existingUserDetail || existingUserDetail == null) {

                    //so create it
                    let newUserDetail = new UserDetail();
                    newUserDetail.user_id = Utils.smallUID();
                    newUserDetail.username = username;

                    // newUserDetail.password = password;
                    newUserDetail.password = Utils.getHashedPassword(password);

                    // newUserDetail.role_id = constants.DEFAULT_ROLES.DEFAULT;
                    newUserDetail.is_otp_required = true;
                    newUserDetail.external_authentication_application = constants.EXTERNAL_AUTH_TYPE.ACTIVE_DIRECTORY;
                    newUserDetail.user_type = constants.USER_TYPES.STANDARD;
                    newUserDetail.is_active = true;
                    newUserDetail.parent_user_id = "-1";
                    newUserDetail.model_name = constants.MODEL_NAME.USER_DETAIL;

                    existingUserDetail = await newUserDetail.save();
                    logger.logStringWithObj("New User Created", existingUserDetail);
                }

                logger.logSuccessEvent("Success authenticateLDAPUser", existingUserDetail, req.body, constants.EVENT_TYPES.READ);
                response.success(req, res, "Success authenticateLDAPUser", existingUserDetail);
                return;
            }
        );
        await ldapClient.unbind();
    } catch (error) {
        logger.logErrorEvent("Error in authenticateLDAPUser", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, error.message, error);
    }
});


router.get("/getSpecificLdapUserDetail/:username", async function (req, res) {
    logger.logString("Enter: getLdapUserDetail");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let username = req.params.username;
        const ldapClient = ldapjs.createClient(ldapOptions);

        // 2
        let options = {
            attributes: [
                // "cn",
                // "createTimestamp",
                // "modifyTimestamp",
                // "pwdPolicySubentry"
            ],
            scope: "sub",
            filter: "(objectClass=person)"
        };

        let dn = "cn=" + username + "," + baseDomain;
        console.log("Searching for dn:", dn);
        let r1 = await ldapClient.search(dn, options, async function (err, result) {
            console.log("e2:", err);
            console.log("result:", result);
            var userObj = null;
            result.on('searchEntry', function (entry) {
                userObj = entry.object;
                console.log("userObj:::", userObj);
            });
            result.on('error', function (err) {
                logger.logErrorEvent("Error in getLdapUserDetail", err.message, req, constants.EVENT_TYPES.READ);
                response.serverError(req, res, err.message, err);
                return;
            });
            result.on('end', function (p) {
                logger.logSuccessEvent("Success getLdapUserDetail", userObj, req.body, constants.EVENT_TYPES.CREATE);
                response.success(req, res, "Success getLdapUserDetail", userObj);
                return;
            });
        });

    } catch (error) {
        logger.logErrorEvent("Error in getLdapUserDetail", error.message, req, constants.EVENT_TYPES.READ);
        response.serverError(req, res, "Error in getLdapUserDetail", error);
    }
});


router.get("/getAllLdapUserDetails", async function (req, res) {
    logger.logString("Enter: getLdapUserDetail");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        const ldapClient = ldapjs.createClient(ldapOptions);

        let options = {
            attributes: [
                // "cn",
                // "createTimestamp",
                // "modifyTimestamp",
                // "pwdPolicySubentry"
            ],
            scope: "sub",
            filter: "(objectClass=person)"
        };

        let dn = baseDomain;
        console.log("Searching for dn:", dn);
        let r1 = await ldapClient.search(dn, options, async function (err, result) {
            console.log("e2:", err);
            console.log("result:", result);
            let entries = [];
            result.on('searchEntry', function (entry) {
                entries.push(entry.object);
                // logger.logStringWithObj("entry:::", entry);
            });
            result.on('error', function (err) {
                logger.logErrorEvent("Error in getLdapUserDetail", err, req, constants.EVENT_TYPES.READ);
                // logger.logErrorStringWithObj("Error in getLdapUserDetail::", err);
                response.serverError(req, res, "Error in getLdapUserDetail::", err);
                // return;
            });
            result.on('end', function (p) {
                logger.logSuccessEvent("Success getLdapUserDetail", entries, req.body, constants.EVENT_TYPES.CREATE);
                //logger.logStringWithObj("Success getLdapUserDetail", entries);
                response.success(req, res, "Success getLdapUserDetail", entries);
                return;
            });
        });
    } catch (error) {
        logger.logErrorEvent("Error in getLdapUserDetail", error, req, constants.EVENT_TYPES.READ);
        // logger.logErrorStringWithObj("Error in getLdapUserDetail", error);
        response.serverError(req, res, "Error in getLdapUserDetail", error);
    }
});

//////////
module.exports = router;