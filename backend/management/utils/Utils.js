let constants = require('../config/constants');
var nodemailer = require('nodemailer');
const uniqid = require('uniqid');
let logger = require('../libs/Logger');
let passwordHash = require('password-hash');
let response = require('../libs/Response');

class Utils {
    constructor() { }

    static getCurrentUTCTimestamp() {
        var tmLoc = new Date();
        return tmLoc.getTime();
    }

    static isValidReq(req) {
        return req.isAuthenticated();
    }

    static smallUID() {
        return uniqid();
    }


    static sendPlainEmail(toEmail, subject, text, alert_level = constants.ALERT_LEVEL.LOW) {
        Utils.sendEmail(toEmail, subject, text, false);
    }

    static sendEventEmail(toEmail, event_subject, event_text, alert_level_str = "") {
        var subject = "An event of type '" + event_subject + "' has happened";
        var text = '<p><font face="verdana" color="green"><strong>New Event Occured:</strong><br>This is to inform you that an event <strong color="red">"' + event_text + '"</strong> of alert level <b>"' + alert_level_str + '"</b> has occured.</font></p>';
        Utils.sendEmail(toEmail, subject, text, true);
    }

    static sendEmail(toEmail, subject, text, formattedFlag = false) {
        //currently send all email to the default emailid 
        toEmail = constants.EMAIL_CREDENTIALS.DEFAULT_TO;

        logger.logString("SEND EMAIL: Will send Email to " + toEmail + " of Subject:" + subject);
        try {
            var transporter = nodemailer.createTransport({
                service: constants.EMAIL_CREDENTIALS.SERVICE,
                auth: {
                    user: constants.EMAIL_CREDENTIALS.USERNAME,
                    pass: constants.EMAIL_CREDENTIALS.PASSWORD
                }
            });
            var mailOptions = null;
            if (formattedFlag) {
                mailOptions = {
                    from: constants.EMAIL_CREDENTIALS.DEFAULT_FROM,
                    to: toEmail,
                    subject: subject,
                    html: text
                };
            } else {
                mailOptions = {
                    from: constants.EMAIL_CREDENTIALS.DEFAULT_FROM,
                    to: toEmail,
                    subject: subject,
                    text: text
                };
            }

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    logger.logErrorStringWithObj("sendMail:ERROR:::::", error);
                } else {
                    logger.logStringWithObj('Successfully sent email to:' + toEmail + ". Response received: " + info.response);
                }
            });
        } catch (error) {
            logger.logErrorStringWithObj("Error in sending mail:", error);
        }
    }

    static removeItemFromStringArray(stringArray, itemToRemove) {
        logger.logStringWithObj("Before Splicing:", stringArray);
        let ind = stringArray.indexOf(itemToRemove);
        stringArray.splice(ind, 1);
        logger.logStringWithObj("After Splicing:", stringArray);
        return stringArray;
    }


    static getHashedPassword(password) {
        return passwordHash.generate(password);
    }

    static removeDuplicatesFromArray(arr) {
        let newArr = [];
        if (arr != null && arr.length > 0) {
            newArr = [];
            for (let k = 0; k < arr.length; k++) {

                if (newArr.indexOf(arr[k]) < 0)
                    newArr.push(arr[k]);
            }
        }
        return newArr;
    }

    async commonSuccessReturn(req, res, functionName, result, reqBody, event_type, alert_level = constants.ALERT_LEVEL.LOW) {
        await logger.logSuccessEvent("Success in : " + functionName + ".", result, reqBody, event_type, alert_level);
        response.newSuccess(res, "Success in : " + functionName + ".", result);
    }

    async commonErrorReturn(req, res, functionName, error, event_type, alert_level = constants.ALERT_LEVEL.LOW) {
        await logger.logErrorEvent("Error in : " + functionName + ". Error is " + error.message + ".", error, req, event_type, alert_level);
        response.newServerError(res, "Error in : " + functionName + ". Error is " + error.message + ".", error);
    }
}





module.exports = Utils;