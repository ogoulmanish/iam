let logger = require('../libs/Logger');
let UserDetail = require('../models/UserDetailSchema');
let RoleDetail = require('../models/RoleDetailSchema');
let SectorDetail = require('../models/SectorDetailSchema');
let SystemEvent = require('../models/SystemEventSchema');
let EntityDetail = require('../models/EntityDetailSchema');
let MapGroupEntity = require('../models/MapSectorEntitySchema');
let MapRoleEntity = require('../models/MapRoleEntitySchema');
let MapUserRole = require('../models/MapUserRoleSchema');
let constants = require('../config/constants')
let Utils = require("./Utils");
let DefaultSetting = require('../models/DefaultSettingSchema');

const express = require('express');
// const speakeasy = require('speakeasy');
// const QRCode = require('qrcode');
// const commons = require('./commons');
// const router = express.Router();


class Defaults {
    static async initUserDetailSettings(user_id) {
        try {
            logger.logString("Will Initialize Default User Details for user:" + user_id);
            let defaultUserRolesIdArr = Defaults.getDefaultUserRoles();
            let result = null;
            for (var roleId of defaultUserRolesIdArr) {
                let mapUserRole = new MapUserRole();
                mapUserRole.user_id = user_id;
                mapUserRole.role_id = roleId;
                result = await mapUserRole.save();
            }

            return constants.RESPONSE_MESSAGE.SUCCESS;
        } catch (error) {
            logger.logErrorStringWithObj("Error in initializing User Details", error);
            return constants.RESPONSE_MESSAGE.FAILURE;
        }
    }

    static getDefaultUserEntities() {
        let entityIdArr = ["1", "2", "3"];
        return entityIdArr;
    }

    static getDefaultUserRoles() {
        let arr = ["1"];
        return arr;
    }


    /**
     * Put default data into database
     */
    static async initDefaultDatabase() {
        try {
            let defaultSetting = await DefaultSetting.findOne();
            constants.EMAIL_CREDENTIALS.DEFAULT_TO = "manish@ogoul.com";
            constants.EMAIL_CREDENTIALS.SERVICE = defaultSetting.email_service;
            constants.EMAIL_CREDENTIALS.DEFAULT_FROM = defaultSetting.email_default_from_name;
            constants.EMAIL_CREDENTIALS.USERNAME = defaultSetting.email_username;
            constants.EMAIL_CREDENTIALS.PASSWORD = defaultSetting.email_password;

        } catch (error) {
            logger.logErrorStringWithObj(error, error);
        }

    }


    static setupOTP(userDetail) {
        logger.logString("Setting up OTP for user");

        //     const secret = speakeasy.generateSecret({
        //         length: 10,
        //         name: userDetail.username,
        //         issuer: 'Ogoul IAM'
        //     });
        //     var url = speakeasy.otpauthURL({
        //         secret: secret.base32,
        //         label: userDetail.username,
        //         issuer: 'Ogoul IAM',
        //         encoding: 'base32'
        //     });
        //     QRCode.toDataURL(url, (err, dataURL) => {
        //         commons.userObject.tfa = {
        //             secret: '',
        //             tempSecret: secret.base32,
        //             dataURL,
        //             tfaURL: url
        //         };
        //         return res.json({
        //             message: 'TFA Auth needs to be verified',
        //             tempSecret: secret.base32,
        //             dataURL,
        //             tfaURL: secret.otpauth_url
        //         });
        //     });
    }


    static async addSuccessSystemEvent(body, event_type, message_alert_level) {
        try {
            // logger.logString("Will store success event:::" + event_type);
            let systemEvent = new SystemEvent(body);
            systemEvent._id = null;
            // logger.logStringWithObj("System event to be stored:", systemEvent);
            systemEvent.event_id = Utils.smallUID();
            systemEvent.event_type = event_type;
            systemEvent.event_time = Utils.getCurrentUTCTimestamp();
            systemEvent.alert_type = message_alert_level;
            // logger.logStringWithObj("System event to be stored:", systemEvent);
            // systemEvent.username = body.username;
            // systemEvent.remarks = body.remarks;
            // systemEvent.details = body.details;
            await systemEvent.save();
        } catch (error) {
            await logger.logErrorStringWithObj("Error in addSuccessSystemEvent", error);
        }
    }


    static async addErrorSystemEvent(obj, event_type, error) {
        try {
            // let obj = req.body;
            logger.logString("Will store failure event:::");
            let systemEvent = new SystemEvent();
            systemEvent.event_id = Utils.smallUID();
            systemEvent.event_type = event_type;
            systemEvent.event_time = Utils.getCurrentUTCTimestamp();
            systemEvent.username = obj.username;
            systemEvent.error_code = "ERROR";
            if (error && error.message)
                systemEvent.error_details = error.message;
            else
                systemEvent.error_details = error;
            systemEvent.event_remarks = obj.remarks;
            let result = await systemEvent.save();
            return result;
        } catch (error) {
            logger.logErrorStringWithObj("Error in addFailureSystemEvent", error);
            return null;
        }
    }



}

module.exports = Defaults;