const mongoose = require('mongoose');

class CommonSchema extends mongoose.Schema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            source_user_id: { type: String },
            source_username: { type: String },
            details: { type: String },
            remarks: { type: String },
            model_name: { type: Number },
            created_time: { type: Number },
            last_modified_time: { type: Number }
        });
    }
}

module.exports = CommonSchema;