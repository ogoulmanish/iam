const CommonSchema = require("./CommonSchemaClass");

class BaseSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            can_be_deleted: { type: Boolean, default: true },
            is_default: { type: Boolean, default: false },
        });
    }
}

module.exports = BaseSchema;