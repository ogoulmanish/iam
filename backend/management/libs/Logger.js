// let Defaults = require('../utils/Defaults');
const log4js = require('log4js');
let constants = require('../config/constants');

log4js.configure({
    appenders: { idamlog: { type: 'file', filename: 'idamlog.log' }, console: { type: 'console' } },
    categories: { default: { appenders: ['idamlog', 'console'], level: 'debug' } }
});

let logger = log4js.getLogger('idamlog');
//logger.setLevel('DEBUG');

class Logger {
    constructor() { }

    static success(req, res, params) {
        logger.info('Success: ' + req.method + ' ' + req.originalUrl + '\n for props: ' + JSON.stringify(params));
    }

    static log(req, res, message) {
        logger.info(message + " from " + req.originalUrl + ':' + req.method)
    }

    static error(req, res, params, error) {
        logger.error('Error: ' + req.method + ' ' + req.originalUrl + '\n for props: ' + JSON.stringify(params) + '\n Error Details: ' + error + '\n' + error.stack);
    }

    /////////////////////////// MHS
    static logString(message) {
        logger.debug("\n##### " + message + " #####");
    }
    static logObj(obj) {
        // logger.debug("######################");
        logger.debug(obj);
    }
    static logStringWithObj(message, obj) {
        logger.debug("\n##### " + message + " #####");
        logger.debug(obj);
    }
    static logErrorStringWithObj(message, obj) {
        logger.debug("\n##### ERROR: " + message + " #####");
        logger.debug(obj);
    }

    static async logSuccessEvent(message, result, body, event_type, message_alert_level = constants.ALERT_LEVEL.LOW) {
        let Defaults = require('../utils/Defaults');
        let EmailAlert = require('../models/EmailAlertSchema');
        let Utils = require('../utils/Utils');

        let flagLogMessagesOnScreen = false;
        body.details = message;
        await Defaults.addSuccessSystemEvent(body, event_type, message_alert_level);

        //send email
        //check if email needs to be sent or not
        let emailAlert = await EmailAlert.findOne();

        if (emailAlert.should_send_email) {
            //check for alert level
            logger.debug("emailAlert:" + emailAlert.alert_level + " messageAlert:" + message_alert_level);
            if (emailAlert.alert_level >= message_alert_level) {
                var messageAlertLevelStr = "Fatal";
                if (message_alert_level == constants.ALERT_LEVEL.CRITICAL) messageAlertLevelStr = "Critical";
                else if (message_alert_level == constants.ALERT_LEVEL.HIGH) messageAlertLevelStr = "High";
                else if (message_alert_level == constants.ALERT_LEVEL.MEDIUM) messageAlertLevelStr = "Medium";
                else if (message_alert_level == constants.ALERT_LEVEL.LOW) messageAlertLevelStr = "Low";
                await Utils.sendEventEmail(constants.EMAIL_CREDENTIALS.DEFAULT_TO, event_type, message, messageAlertLevelStr);
            }
        }

        if (flagLogMessagesOnScreen) {
            logger.debug("##### " + message + " #####");
            logger.debug(result);
        }
    }

    static async logErrorEvent(message, obj, body, event_type, alert_level = constants.ALERT_LEVEL.LOW) {
        let Defaults = require('../utils/Defaults');
        let Utils = require('../utils/Utils');
        let EmailAlert = require('../models/EmailAlertSchema');

        body.details = message;
        Defaults.addErrorSystemEvent(body, event_type);

        //send email
        //check if email needs to be sent or not
        let emailAlert = await EmailAlert.findOne();
        if (emailAlert.should_send_email)
            Utils.sendEventEmail(constants.EMAIL_CREDENTIALS.DEFAULT_TO, event_type, message, alert_level);
        logger.debug("##### " + message + " #####");
        logger.debug(obj);
    }

    ////////////////////
}

module.exports = Logger;