const CommonSchema = require("./CommonSchemaClass");

class BaseConnectorSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            connector_id: { type: String },
            connector_name: { type: String },
            connector_type: { type: String },
            connector_category: { type: String },
        });
    }
}
module.exports = BaseConnectorSchema;