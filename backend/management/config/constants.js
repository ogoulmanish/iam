module.exports = {

    DEFAULT_PARENT_USER_ID: {
        ADMIN: "1"
    },

    MAKER_CHECKER_REQUEST_STATUS: {
        PENDING: 1,
        APPROVE: 2,
        DECLINE: 3,
        CANCEL: 4
    },

    FRONTEND_MODULE_NAME_ID: {
        USER: 1,
        ENTITY: 2,
        SECTOR: 3,
        ROLE: 4,
        PERMISSION: 5,
        RULE: 6,
        MAPPING_USER_ENTITY: 7,
        ACCESS_POLICY: 8
    },

    PREFERRED_LANGUAGE: {
        ENGLISH: "en",
        ARABIC: "ar"
    },

    USER_TYPES: {
        ADMIN: "admin",
        MANAGER: "manager",
        STANDARD: "standard",
        HR: "hr"
    },

    EXTERNAL_AUTH_TYPE: {
        DEFAULT: 1,
        ACTIVE_DIRECTORY: 2,
        MSSQL: 3,
        ORACLE: 4
    },

    DEFAULT_ROLES: {
        ADMIN_ROLE: "1",
        MANAGER_ROLE: "2",
        STANDARD_ROLE: "3",
        HR_ROLE: "4"
    },

    ALERT_LEVEL: {
        FATAL: 0,
        CRITICAL: 1,
        HIGH: 2,
        MEDIUM: 3,
        LOW: 4
    },

    SECRET_KEYS: {
        KEY_1: "OGOULIDAM"
    },

    MS_SQL_SERVER: {
        HOST: "",
        USERNAME: "admin",
        PASSWORD: "admin",
        DATABASE: "db_name",
    },

    DEFAULT_MESSAGES: {
        SUCCESS: "Success",
        FAILURE: "Failure"
    },

    EVENT_TYPES: {
        CREATE: "Create",
        READ: "Read",
        UPDATE: "Update",
        DELETE: "Delete",
        ADDING_USER: "Adding User",
        ADDING_FIELD: "Adding Field",
        UPDATE_ERROR: "Error in Update"

    },

    ACTIVE_DIRECTORY_SERVER: {
        LDAP_HOST_NAME: "localhost",
        LDAP_PORT: "10389",
        BASE_DOMAIN: "ou=developer,dc=ogoul1,dc=com",
        LDAP_LOGIN: "",
        LDAP_PASSWORD: ""
        // BASE_DOMAIN: "ou=developer,dc=my-domain,dc=com",
        // LDAP_LOGIN: "cn=Manager,dc=my-domain,dc=com",
        // LDAP_PASSWORD: "secret"
    },

    CRUD_TYPE: {
        CREATE: "c",
        READ: "r",
        UPDATE: "u",
        DELETE: "d"

    },

    DEFAULTS_FILE_LOCATIONS: {
        // ENTITY_ICONS: "../frontend/src/assets/images/sectors"
        // ENTITY_ICONS: "c://fakepath"
        ENTITY_ICONS: "images"
    },

    MODEL_NAME: {
        ACCESS_ENTITY: 1,
        ALERT_NOTIFICATION: 2,
        APPROVAL_MATRIX: 3,
        DASHBOARD: 4,
        DEFAULT_OPTIONS: 5,
        DEFAULT_SETTING: 6,
        EMAIL_ALERT: 7,
        ENTITY_EVENT: 8,
        FIELD_DETAIL: 9,
        FORM_ELEMENT: 10,
        MAKER_CHECKER_REQUEST: 11,
        MANAGER_USER_DETAIL: 12,
        PAGE_MATRIX: 13,
        SYSTEM_EVENT: 14,
        USER_DETAIL: 15,
        USER_SESSION_TRACKING: 16,
        LOGGING_TRACKING: 17,
        MAP_GROUP_ENTITY: 18,
        MAP_ROLE_ENTITY: 19,
        MAP_USER_ROLE: 20,
        BASE_SECTOR_DETAIL: 21,
        ENTITY_DETAIL: 22,
        PERMISSION_DETAIL: 23,
        ROLE_DETAIL: 24,
        RULE_DETAIL: 25,
        SECTOR_DETAIL: 26,
        BASE_MODEL: 27,
        COMMON_MODEL: 28,
        COMMON_EMAIL: 29,
        DB_CONNECTOR: 30,
        ACCESS_POLICY: 31,
        SPECIFIC_RULE: 32,
        ACTIVE_DIRECTORY_CONNECTOR: 33
    },

    COMPONENT_NAME: {
        CRUD_USER: "crudUser",
        LIST_USER: "listUser",
        CRUD_SECTOR: "crudSector",
        CRUD_ENTITY: "crudEntity",
        CRUD_PERMISSION: "crudPermission",
        CRUD_ROLE: "crudRole",
        CRUD_REPORTING: "crudReporting",
        CRUD_RULE: "crudRule",
        EDIT_USER_PROFILE: "editUserProfile",
        DASHBOARD: "dashboard",
        LIST_ACCESS_ENTITY: "listAccessEntity",
        LIST_ENTITY: "listEntity",
        LIST_SECTOR: "listSector",
        LIST_ROLE: "listRole",
        LIST_PERMISSION: "listPermission",
        LIST_RULE: "listRule",
        EDIT_FIELDS: "editFields",
        EMAIL_ALERT: "emailAlert",
        ALERT_NOTIFICATION: "alertNotification",
        LIST_PAGE_MATRIX: "listPageMatrix",
        CRUD_PAGE_MATRIX: "crudPageMatrix",
        LIST_FORM_ELEMENT: "listFormElement",
        CRUD_FORM_ELEMENT: "crudFormElement",
        LIST_MANAGER_USER: "listManagerUser",
        CRUD_MANAGER_USER: "crudManagerUser",
        LIST_MAKER_CHECKER: "listMakerChecker",
        COMMON: "common"
    },


    DEFAULT_ENTITY_IDS: {
        ENGINEER: 1,
        DATABASE: 2
    },

    ACCESS_ENTITY_STATUS: {
        DECLINE: 0, //NONE PERMISSION
        APPROVE: 1, //READ PERMISSION
        PENDING: -1
    },

    RESPONSE_MESSAGE: {
        SUCCESS: "Success",
        FAILURE: "Failure",
        ERROR: "Error"
    },

    DEFAULT_SECTOR_IDS: {
        APPLICATIONS: 1,
        NETWORKS: 2,
        SYSTEMS: 3,
        FILES: 4,
    },

    CONFIG_TYPES: {
        SECTOR: 1,
        ROLE: 2,
        ENTITY: 3,
        RULE: 4,
        PERMISSION: 5
    },

    EMAIL_CREDENTIALS: {
        SERVICE: "gmail",
        DEFAULT_FROM: "MANISH SHAH",
        DEFAULT_TO: "manish@ogoul.com",
        USERNAME: 'manish@ogoul.com',
        PASSWORD: 'xxxxxxxxxx'
    },

    DEFAULT_SELECTED_STRING_VALUE: "-1",

    //connectors
    // CONNECTOR_CATEGORIES: {
    //     DATABASE_CONNECTOR: "1",
    //     DIRECTORY_CONNECTOR: "2"
    // },

    CONNECTOR_TYPE: {
        MONGODB: "1",
        ACTIVE_DIRECTORY: "2",
        LINUX: "3",
        MSSQL: "4"
    },

    SPECIFIC_RULE_ATTRIBUTE_NAMES: {
        crudUser_department: "crudUser_department",
        crudUser_job_title: "crudUser_job_title"
    },

    ARRAY_SPECIFIC_RULE_ATTRIBUTE_NAMES: [
        "crudUser_department",
        "crudUser_job_title"
    ],

    ADVANCED_USERS_SEARCH_BY: {
        ROLE_NAME: "Role Name",
        ACCESS_POLICY_NAME: "Access Policy",
        ENTITY_NAME: "Entity Name"
    },

    SEARCH_USER_BOOLEAN_OPTION: {
        ANY: "Any",
        TRUE: "True",
        FALSE: "False"
    }

    // ARRAY_ACTUAL_CONNECTORS: [
    //     {
    //         actual_connector_id: "A1",
    //         connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.DATABASE_CONNECTOR,
    //         connector_type: CustomGlobalConstants.CONNECTOR_TYPE.MONGODB,
    //         name: "MongoDB",
    //         details: "Mongodb Database Connector"
    //     },
    //     {
    //         actual_connector_id: "B1",
    //         connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.DIRECTORY_CONNECTOR,
    //         connector_type: CustomGlobalConstants.CONNECTOR_TYPE.ACTIVE_DIRECTORY,
    //         name: "Active Directory",
    //         details: "LDAP - Active Directory Connector"
    //     },
    //     {
    //         actual_connector_id: "C1",
    //         connector_category: CustomGlobalConstants.CONNECTOR_CATEGORIES.DATABASE_CONNECTOR,
    //         connector_type: CustomGlobalConstants.CONNECTOR_TYPE.MSSQL,
    //         name: "MSSQL",
    //         details: "MSSQL Database Connector"
    //     },
    // ],

    // CONNECTOR_TYPES: {
    //     DATABASE_CONNECTOR: {
    //         MONGODB: {
    //             id: 1,
    //             name: "MongoDB",
    //             details: "Mongodb Database Connector"
    //         },
    //         MYSQL: {
    //             id: 2,
    //             name: "MSSQL",
    //             details: "MSSQL Database Connector"
    //         },
    //     }
    // }
}