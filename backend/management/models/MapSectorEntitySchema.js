const mongoose = require("mongoose");

const MapSectorEntitySchema = mongoose.Schema({
    sector_id: { type: String, required: true },
    entity_id: { type: String, required: true },
});
module.exports = mongoose.model('MapSectorEntity', MapSectorEntitySchema, 'map_sector_entity');
