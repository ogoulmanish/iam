const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class FormElementSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            form_element_id: String,
            form_element_type: String,
            form_element_name: String,
            data_name: String,
            data_value: String,
        });
    }
}

const schema = new FormElementSchema();
module.exports = mongoose.model('FormElement', schema, 'form_element');