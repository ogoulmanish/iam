const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class SectorDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            sector_id: String,
            sector_name: { type: String },
            sector_type: { type: Number },
            sector_icon_location: { type: String },
            entity_id_arr: [],
        });
    }
}

const schema = new SectorDetailSchema();
module.exports = mongoose.model('SectorDetail', schema, 'sector_detail');



// const mongoose = require("mongoose");

// const SectorDetailSchema = mongoose.Schema({
//     sector_id: String,
//     sector_name: { type: String },
//     sector_type: { type: Number },
//     sector_icon_location: { type: String },
//     entity_id_arr: [],
//     details: { type: String },
//     remarks: { type: String },
//     can_be_deleted: { type: Boolean, default: true }
// });
// module.exports = mongoose.model('SectorDetail', SectorDetailSchema, 'sector_detail');
