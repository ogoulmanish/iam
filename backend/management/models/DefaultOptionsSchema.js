const mongoose = require("mongoose");

const DefaultOptionsSchema = mongoose.Schema({
    sector_id: { type: String, required: true },
    entity_id: { type: String, required: true },
});
module.exports = mongoose.model('DefaultOptions', DefaultOptionsSchema, 'default_options');
