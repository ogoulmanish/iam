const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class RoleDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            role_id: { type: String },
            role_name: { type: String },
            // allowed_entity_id_arr: [],
            allowed_access_policy_id_arr: [],
            allowed_permission_id_arr: [],
            allowed_user_id_arr: [],
            rule_associated: { type: Boolean }
        });
    }
}

const schema = new RoleDetailSchema();
module.exports = mongoose.model('RoleDetail', schema, 'role_detail');
