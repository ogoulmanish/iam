const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class UserDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            user_id: { type: String, required: true },
            username: { type: String, required: true, unique: true },
            external_authentication_application: { type: Number },
            user_type: { type: String },
            password: { type: String },
            email: { type: String },
            salutation: { type: String },
            first_name: { type: String },
            middle_name: { type: String },
            last_name: { type: String },
            full_name: { type: String },
            facebook_id: { type: String },
            google_id: { type: String },
            phone: Number,
            date_of_birth: { type: String },
            profile_link: { type: String },
            created_on: Number,
            last_modified: Number,
            last_login_time: Number,
            // role_id: { type: String },
            DN: { type: String },
            is_otp_required: { type: Boolean, default: true },
            secret_key: { type: String },
            tempSecret: { type: String },
            dataURL: { type: String },
            tfaURL: { type: String },
            parent_user_id: { type: String, default: "1" }, //default to admin - specifically for manager user
            creator_user_id: { type: String, default: "1" }, //default to admin - who created this user
            is_deleted: { type: Boolean, default: false },
            is_active: { type: Boolean, default: true },
            is_added_to_main: { type: Boolean, default: false },
            is_locked: { type: Boolean, default: false },

            department: { type: String },
            job_title: { type: String },
            page_matrix_id: { type: String },
        });
    }
}

const schema = new UserDetailSchema();
module.exports = mongoose.model('UserDetail', schema, 'user_detail');

// const mongoose = require("mongoose");

// const UserDetailSchema = mongoose.Schema({
//     user_id: { type: String },
//     external_authentication_application: { type: Number },
//     user_type: { type: String },
//     username: { type: { type: String }, required: true },
//     password: { type: String },
//     email: { type: String },
//     salutation: { type: String },
//     first_name: { type: String },
//     middle_name: { type: String },
//     last_name: { type: String },
//     full_name: { type: String },
//     facebook_id: { type: String },
//     google_id: { type: String },
//     phone: Number,
//     date_of_birth: { type: String },
//     profile_link: { type: String },
//     created_on: Number,
//     last_modified: Number,
//     last_login_time: Number,
//     role_id: { type: String },
//     // is_admin: { type: Boolean, default: false },
//     is_deleted: { type: Boolean, default: false },
//     is_otp_required: { type: Boolean, default: true },
//     secret_key: { type: String },
//     tempSecret: { type: String },
//     dataURL: { type: String },
//     tfaURL: { type: String },
//     can_be_deleted: { type: Boolean, default: true },
// });
// module.exports = mongoose.model('UserDetail', UserDetailSchema, 'user_detail');
