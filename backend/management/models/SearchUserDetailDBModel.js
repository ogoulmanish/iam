const mongoose = require('mongoose');

const SearchUserDetailDBModel = mongoose.model("SearchUserDetail", new mongoose.Schema(
    {
        source_user_type:  { type: String },
        username: { type: String },
        is_added_to_main: { type: String },
        user_id: { type: String },
        email: { type: String },
        first_name: { type: String },
        last_name: { type: String },
        active: { type: String },
        locked: { type: String },
        department: { type: String },
        job_title: { type: String },
    }
));

module.exports = SearchUserDetailDBModel;
