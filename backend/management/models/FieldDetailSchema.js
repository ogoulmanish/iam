const mongoose = require('mongoose');
const CommonSchema = require("../libs/CommonSchemaClass");

class FieldDetailSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            field_uid: String,
            field_language: String,
            field_name: String,
            field_value: String,
            field_details: String,
            is_required: Boolean,
            error_message: String,
        });
    }
}

const schema = new FieldDetailSchema();
module.exports = mongoose.model('FieldDetail', schema, 'field_detail');
