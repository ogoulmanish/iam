
const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class EntityDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            entity_id: String,
            entity_name: { type: String },
            access_location: { type: String },
            icon_location: { type: String },
            is_default: { type: Boolean, default: false },
            risk_level_id: { type: String },
            icon_filename: { type: String },
            connector_type: { type: String },
            connector_object: { type: Object },
        });
    }
}

const schema = new EntityDetailSchema();
module.exports = mongoose.model('EntityDetail', schema, 'entity_detail');
