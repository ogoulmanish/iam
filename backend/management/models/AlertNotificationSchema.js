const mongoose = require('mongoose');
const CommonSchema = require("../libs/CommonSchemaClass");

class AlertNotificationSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            alert_notification_id: { type: String },
            component_name: { type: String },
            alert_name: { type: String },
            alert_value: { type: String },
            preferred_language: { type: String },
        });
    }
}

const schema = new AlertNotificationSchema();
module.exports = mongoose.model('AlertNotification', schema, 'alert_notifications');