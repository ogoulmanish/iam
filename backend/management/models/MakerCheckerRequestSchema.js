const mongoose = require("mongoose");
const BaseSchema = require("../libs/BaseSchemaClass");

class MakerCheckerRequestDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            maker_checker_request_id: { type: String },
            maker_user_id: { type: String },
            maker_request_time: { type: Number },
            maker_component_name: { type: String },
            maker_CRUD_operation: { type: String },
            maker_object: { type: Object },
            current_status: { type: Number },
            maker_query: { type: String },
            checker_user_id: { type: String },
            checker_status_time: { type: String },
        });
    }
}

const schema = new MakerCheckerRequestDetailSchema();
module.exports = mongoose.model('MakerCheckerRequestDetail', schema, 'maker_checker_request_detail');
