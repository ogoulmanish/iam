
const mongoose = require('mongoose');
const CommonSchema = require("../libs/CommonSchemaClass");

class AccessEntitySchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            access_entity_id: { type: String },
            mapping_user_entity_id: { type: String },
            requested_user_id: { type: String },
            requested_username: { type: String },
            requested_entity_id: { type: String },
            is_request_new: { type: Boolean },
            requested_time: { type: Number },
            current_status: { type: Number },
            last_update_time: { type: Number },
        });
    }
}

const schema = new AccessEntitySchema();
module.exports = mongoose.model('AccessEntity', schema, 'access_entity');
