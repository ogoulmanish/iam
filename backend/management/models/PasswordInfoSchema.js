const mongoose = require("mongoose");

const PasswordInfoSchema = mongoose.Schema({
    user_uid: { type: String, required: true, unique: true },
    device_uid: { type: String },
    original_password: { type: String },
    last_password: { type: String },
    new_password: { type: String },
    platform_id: { type: String },
    new_password_update_time: { type: String },
    remarks: { type: String }
});
module.exports = mongoose.model('PasswordInfo', PasswordInfoSchema, 'password_info');
