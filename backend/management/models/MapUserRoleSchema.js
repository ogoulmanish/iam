const mongoose = require("mongoose");

const MapUserRoleSchema = mongoose.Schema({
    user_id: { type: String, required: true },
    role_id: { type: String, required: true },
});
module.exports = mongoose.model('MapUserRole', MapUserRoleSchema, 'map_user_role');
