const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class RuleDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            rule_id: { type: String },
            rule_name: { type: String },
            role_id: { type: String },
            // sector_id_arr: [],
            // entity_id_arr: [],
            // permission_id_arr: [],
            // mapEntityToPermission: {
            //     type: Map,
            //     of: String
            // },
            // tmpMap: {
            //     type: Map,
            //     of: String,
            //     default: {}
            // }
            tmpStr: String,
            associated_username: { type: String },
            rule_type: { type: Number },
        });
    }
}

const schema = new RuleDetailSchema();
module.exports = mongoose.model('RuleDetail', schema, 'rule_detail');
