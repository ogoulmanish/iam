const mongoose = require("mongoose");

const PasswordSchedulerSchema = mongoose.Schema({
    device_uid: String,
    password_change_days: String,
    password_verification_days: String
});
module.exports = mongoose.model('PasswordScheduler', PasswordSchedulerSchema, 'password_scheduler');
