const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class EntityEventSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            entity_event_id: { type: String },
            accessed_username: { type: String },
            entity_id: { type: String },
            entity_name: { type: String },
            time_of_access: { type: Number },
            risk_level_id: { type: String },
        });
    }
}

const schema = new EntityEventSchema();
module.exports = mongoose.model('EntityEvent', schema, 'entity_events');
