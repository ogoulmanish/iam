const mongoose = require("mongoose");
const BaseSchema = require("../libs/BaseSchemaClass");

class AccessPolicyDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            access_policy_id: { type: String },
            access_policy_name: { type: String },
            entity_id_arr: []
        });
    }
}

const schema = new AccessPolicyDetailSchema();
module.exports = mongoose.model('AccessPolicy', schema, 'access_policy');