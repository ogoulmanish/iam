const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class SpecificRuleSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            specific_rule_id: { type: String },
            role_id: { type: String },
            attribute_name: { type: String },
            attribute_value: { type: String },
            arithmetic_operation: { type: String },
        });
    }
}

const schema = new SpecificRuleSchema();
module.exports = mongoose.model('SpecificRule', schema, 'specific_rules');
