const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class MappingUserEntitySchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            mapping_user_entity_id: String,
            user_id: String,
            username: String,
            entity_id: String,
            role_id: String,
            current_status: Number,
            login_name: String,
            password: String,
            permissions_arr: []
        });
    }
}

const schema = new MappingUserEntitySchema();
module.exports = mongoose.model('MappingUserEntity', schema, 'mapping_user_entity');
