const mongoose = require('mongoose');
const CommonSchema = require("../libs/CommonSchemaClass");

class DefaultSettingSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            session_timeout: { type: Number },
            email_service: { type: String },
            email_default_from_name: { type: String },
            email_address: { type: String },
            email_username: { type: String },
            email_password: { type: String },
            account_expiry_days: { type: Number, default: 1000 },
        });
    }
}

const schema = new DefaultSettingSchema();
module.exports = mongoose.model('DefaultSetting', schema, 'default_setting');