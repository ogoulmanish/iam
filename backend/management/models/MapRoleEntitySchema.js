const mongoose = require("mongoose");

const MapRoleEntitySchema = mongoose.Schema({
    role_id: { type: String, required: true },
    entity_id: { type: String, required: true },
    access_status: { type: Number },
    permission_status: { type: Number }
});
module.exports = mongoose.model('MapRoleEntity', MapRoleEntitySchema, 'map_role_entity');
