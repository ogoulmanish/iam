const mongoose = require('mongoose');
const DBConnectorSchema = require("./DBConnectorSchema");

class MongoDBConnectorSchema extends DBConnectorSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            new_login_name: { type: String },
            new_password: { type: String },
        });
    }
}

const schema = new MongoDBConnectorSchema();
module.exports = mongoose.model('MongoDBCOnnector', schema, 'db_connector_detail');
