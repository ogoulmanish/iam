const BaseConnectorSchema = require("../../../libs/BaseConnectorSchema");

class DBConnectorSchema extends BaseConnectorSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            host_name: { type: String },
            host_username: { type: String },
            host_password: { type: String },
            host_port: { type: String },
            database_name: { type: String },
        });
    }
}
module.exports = DBConnectorSchema;
