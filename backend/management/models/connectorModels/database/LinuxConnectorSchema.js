const mongoose = require('mongoose');
const BaseConnectorSchema = require("../../../libs/BaseConnectorSchema");

class LinuxConnectorSchema extends BaseConnectorSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            host_name: { type: String },
            host_username: { type: String },
            host_password: { type: String },
            host_port: { type: String },
            source_user_id: { type: String },
            new_login_name: { type: String },
            new_password: { type: String },
        });
    }
}

const schema = new LinuxConnectorSchema();
module.exports = mongoose.model('LinuxConnector', schema, 'linux_connector_detail');
