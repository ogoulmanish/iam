const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");
class LoggingTrackingSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            logging_tracking_id: String,
            username: String,
            password: String,
            access_browser: String,
            login_time: Number,
            logout_time: Number,
            source_ip_address: String,
            clean_logged_out_flag: Boolean,
            is_login_successful: Boolean,            
        });
    }
}

const schema = new LoggingTrackingSchema();
module.exports = mongoose.model('LoggingTracking', schema, 'logging_tracking');


// const LoggingTrackingSchema = mongoose.Schema({
//     logging_tracking_id: String,
//     username: String,
//     password: String,
//     access_browser: String,
//     login_time: Number,
//     logout_time: Number,
//     source_ip_address: String,
//     clean_logged_out_flag: Boolean,
//     // status_code: String,
//     is_login_successful: Boolean,
//     remarks: String
// });
// module.exports = mongoose.model('LoggingTracking', LoggingTrackingSchema, 'logging_tracking');