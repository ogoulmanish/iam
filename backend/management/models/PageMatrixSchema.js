const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");
let constants = require('../config/constants');
let SimplePermissions = require("./SimplePermissionsSchema")
let Schema = mongoose.Schema;

class PageMatrixSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            page_matrix_id: { type: String },
            role_id: { type: String },
            page_matrix_name: { type: String },
            associated_user_id: { type: String, default: constants.DEFAULT_SELECTED_STRING_VALUE },

            show_field_menu_heading_navigation: { type: Boolean },
            show_field_menu_link_listUser: { type: Boolean },
            permission_listUser: { type: SimplePermissions },
            // show_field_menu_link_crudUser: { type: Boolean },
            show_field_menu_link_listAccessEntity: { type: Boolean },
            permission_listAccessEntity: { type: SimplePermissions },
            show_field_menu_link_listEntity: { type: Boolean },
            permission_listEntity: { type: SimplePermissions },
            show_field_menu_link_listSector: { type: Boolean },
            permission_listSector: { type: SimplePermissions },
            show_field_menu_link_listSectorElements: { type: Boolean },
            permission_listSectorElements: { type: SimplePermissions },
            show_field_menu_link_listRole: { type: Boolean },
            permission_listRole: { type: SimplePermissions },
            show_field_menu_link_listPermission: { type: Boolean },
            permission_listPermission: { type: SimplePermissions },
            show_field_menu_link_listRule: { type: Boolean },
            permission_listRule: { type: SimplePermissions },
            show_field_menu_link_listPageMatrix: { type: Boolean },
            permission_listPageMatrix: { type: SimplePermissions },
            show_field_menu_link_listFormElement: { type: Boolean },
            permission_listFormElement: { type: SimplePermissions },
            show_field_menu_link_editFields: { type: Boolean },
            permission_editFields: { type: SimplePermissions },
            show_field_menu_link_emailAlerts: { type: Boolean },
            permission_emailAlerts: { type: SimplePermissions },
            show_field_menu_link_editUserProfile: { type: Boolean },
            permission_editUserProfile: { type: SimplePermissions },
            show_field_menu_link_listAlertNotification: { type: Boolean },
            permission_listAlertNotification: { type: SimplePermissions },
            show_field_menu_link_defaultSetting: { type: Boolean },
            permission_defaultSetting: { type: SimplePermissions },
            show_field_menu_link_report_default: { type: Boolean },
            permission_report_default: { type: SimplePermissions },
            show_field_menu_link_report_entityEvents: { type: Boolean },
            permission_report_entityEvents: { type: SimplePermissions },
            show_field_menu_link_report_systemEvents: { type: Boolean },
            permission_report_systemEvents: { type: SimplePermissions },
            show_field_menu_link_report_existingUsers: { type: Boolean },
            permission_report_existingUsers: { type: SimplePermissions },
            show_field_menu_link_report_userLogging: { type: Boolean },
            permission_report_userLogging: { type: SimplePermissions },
            show_field_menu_link_report_userActivityReports: { type: Boolean },
            permission_report_userActivityReports: { type: SimplePermissions },
            show_field_menu_link_listManagerUser: { type: Boolean },
            permission_listManagerUser: { type: SimplePermissions },
            show_field_menu_link_listMakerCheckerRequest: { type: Boolean },
            permission_listMakerCheckerRequest: { type: SimplePermissions },
            show_field_menu_link_listApprovalMatrix: { type: Boolean },
            permission_listApprovalMatrix: { type: SimplePermissions },
            show_field_menu_link_listPredefinedConnectors: { type: Boolean },
            permission_listPredefinedConnectors: { type: SimplePermissions },
            show_field_menu_link_listAccessPolicy: { type: Boolean },
            permission_listAccessPolicy: { type: SimplePermissions },

            //standard
            show_field_standard_menu_heading_navigation: { type: Boolean },
            show_field_standard_menu_link_showEntities: { type: Boolean },
            permission_standard_showEntities: { type: SimplePermissions },
            show_field_standard_menu_link_editUserProfile: { type: Boolean },
            permission_standard_editUserProfile: { type: SimplePermissions },

            //hr
            show_field_hr_menu_heading_navigation: { type: Boolean },
        });
    }
}

const schema = new PageMatrixSchema();
module.exports = mongoose.model('PageMatrix', schema, 'page_matrix');