const mongoose = require("mongoose");
const CommonSchema = require("../libs/CommonSchemaClass");

class ApprovalMatrixSchema extends CommonSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            approval_matrix_id: { type: String },
            user_type: { type: String },
            component_name: { type: String },
            requires_approval: { type: Boolean },
            create_permission: { type: Boolean },
            update_permission: { type: Boolean },
            delete_permission: { type: Boolean },
        });
    }
}

const schema = new ApprovalMatrixSchema();
module.exports = mongoose.model('ApprovalMatrix', schema, 'approval_matrix');
