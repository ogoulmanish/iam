const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class EmailAlertSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            should_send_email: { type: Boolean },
            alert_level_all: { type: Boolean },
            alert_level_critical: { type: Boolean },
            alert_level_high: { type: Boolean },
            alert_level_medium: { type: Boolean },
            alert_level_low: { type: Boolean },
            alert_level: { type: Number }
        });
    }
}

const schema = new EmailAlertSchema();
module.exports = mongoose.model('EmailAlert', schema, 'email_alert');
