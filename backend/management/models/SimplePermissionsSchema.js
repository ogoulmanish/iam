const mongoose = require('mongoose');

const SimplePermissionsSchema = mongoose.Schema(
    {
        create: { type: Boolean },
        read: { type: Boolean },
        update: { type: Boolean },
        delete: { type: Boolean }
    }
);

module.exports = SimplePermissionsSchema;
