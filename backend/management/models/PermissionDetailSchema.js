const mongoose = require("mongoose");
const BaseSchema = require("../libs/BaseSchemaClass");

class PermissionDetailSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            permission_id: { type: String },
            permission_name: { type: String }
        });
    }
}

const schema = new PermissionDetailSchema();
module.exports = mongoose.model('PermissionDetail', schema, 'permission_detail');
