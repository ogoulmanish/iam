const mongoose = require('mongoose');
const BaseSchema = require("../libs/BaseSchemaClass");

class SystemEventSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            event_id: { type: String },
            event_type: { type: String },
            event_time: { type: Number },
            event_source: { type: String, default: "Browser" },
            error_code: { type: String },
            error_details: { type: String },
            alert_type: { type: Number, default: 4 }
        });
    }
}

const schema = new SystemEventSchema();
module.exports = mongoose.model('SystemEvent', schema, 'system_events');