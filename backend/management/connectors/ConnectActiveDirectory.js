let ActiveDirectoryConnector = require('../models/connectorModels/database/ActiveDirectoryConnectorSchema');
let constants = require('../config/constants');
let logger = require('../libs/Logger');
var ldapjs = require('ldapjs');

class ConnectActiveDirectory {
    constructor() { }

    async createActiveDirectoryUser(activeDirectoryConnector, userDetail) {
        let returnMessage = constants.DEFAULT_MESSAGES.SUCCESS;
        try {
            logger.logString("Enter:createActiveDirectoryUser");
            const ldapOptions = {
                connectTimeout: 3000000,
                reconnect: true
            };

            ldapOptions["url"] = "ldap://" + activeDirectoryConnector.host_name + ":" + activeDirectoryConnector.host_port;
            logger.logStringWithObj("ldapOptions::", ldapOptions);

            var ldapLoginDN = activeDirectoryConnector.host_username;
            var ldapLoginPassword = activeDirectoryConnector.host_password;

            ldapLoginDN = "";
            ldapLoginPassword = "";

            const ldapClient = ldapjs.createClient(ldapOptions);
            ldapClient.bind(
                ldapLoginDN,
                ldapLoginPassword,
                async function (err) {
                    if (err) {
                        logger.logErrorStringWithObj("Bind Error in createActiveDirectoryUser: ", err);
                        // response.serverError(req, res, err.message, err);
                        throw new Error("Bind Error in createActiveDirectoryUser: " + err.message);
                    }
                    try {
                        // let userDetail = await UserDetail.findOne({ 'user_id': activeDirectoryConnector.source_user_id });
                        let newUser = {
                            uid: userDetail.username,
                            userPassword: userDetail.password,
                            mail: userDetail.email,
                            givenname: userDetail.first_name,
                            initials: userDetail.middle_name,
                            surname: userDetail.last_name,
                            telephoneNumber: userDetail.phone,
                            objectClass: ["person", "organizationalPerson", "inetOrgPerson"]
                        };
                        //add the newDN
                        ldapClient.add(
                            activeDirectoryConnector.new_login_name,
                            newUser,
                            async (err, res1) => {
                                if (err) {
                                    logger.logErrorStringWithObj("Add Error in createActiveDirectoryUser: ", err);
                                    // response.serverError(req, res, err.message, err);
                                    // return;
                                    throw new Error("Add Error in createActiveDirectoryUser: " + err.message);
                                }

                                // Utils.sendPlainEmail(userDetail.email, "New Active Directory User", "New Active Directory user with id '" + userDetail.username + "' is created.");
                                // response.success(req, res, "Success createActiveDirectoryUser", null);
                            }
                        );
                    } catch (error) {
                        logger.logErrorStringWithObj("Error in createActiveDirectoryUser: ", err);
                        throw new Error("Error in createActiveDirectoryUser: " + error.message);
                    }
                }
            );

        } catch (error) {
            logger.logErrorStringWithObj("Error in createActiveDirectoryUser 2: ", err);
            return Message = error.message;
        }
        return returnMessage;
    }


    async deleteActiveDirectoryUser(activeDirectoryConnector, mappingUserEntity) {
        let returnMessage = constants.DEFAULT_MESSAGES.SUCCESS;
        try {
            logger.logString("Enter:deleteActiveDirectoryUser");
            logger.logStringWithObj("activeDirectoryConnector::", activeDirectoryConnector);
            logger.logStringWithObj("mappingUserEntity::", mappingUserEntity);
            const ldapOptions = {
                connectTimeout: 3000000,
                reconnect: true
            };

            ldapOptions["url"] = "ldap://" + activeDirectoryConnector.host_name + ":" + activeDirectoryConnector.host_port;
            logger.logStringWithObj("ldapOptions::", ldapOptions);
            var ldapLoginDN = activeDirectoryConnector.host_username;
            var ldapLoginPassword = activeDirectoryConnector.host_password;
            ldapLoginDN = "";
            ldapLoginPassword = "";

            const ldapClient = ldapjs.createClient(ldapOptions);
            ldapClient.bind(
                ldapLoginDN,
                ldapLoginPassword,
                (err) => {
                    if (err) {
                        logger.logErrorStringWithObj("Bind Error in deleteActiveDirectoryUser: ", err);
                        throw new Error("Bind Error in deleteActiveDirectoryUser: " + err.message);
                    }
                    try {
                        let dnToDelete = mappingUserEntity.login_name;
                        logger.logString("About to DELETE DN:" + dnToDelete);
                        ldapClient.del(
                            dnToDelete,
                            async (err, res1) => {
                                if (err) {
                                    logger.logErrorStringWithObj("Add Error in deleteActiveDirectoryUser: ", err);
                                    throw new Error("Add Error in deleteActiveDirectoryUser: " + err.message);
                                }
                            }
                        );
                    } catch (error) {
                        logger.logErrorStringWithObj("Error in ActiveDirectoryDeleteUser 1: ", error);                        
                    }
                }
            );
        } catch (error) {
            logger.logErrorStringWithObj("Error in deleteActiveDirectoryUser 2: ", err);
            return Message = error.message;
        }
        return returnMessage;
    }

}




///////////////////////////////////////
module.exports = ConnectActiveDirectory;