let MongoDBConnector = require('../models/connectorModels/database/MongoDBConnectorSchema');
let logger = require('../libs/Logger');
let constants = require('../config/constants');

class ConnectMongoDB {

    constructor() { }

    async createDBUser(mongodbConnector) {
        logger.logStringWithObj("ConnectMongoDB:createDBUser:", mongodbConnector);
        let returnMessage = constants.DEFAULT_MESSAGES.SUCCESS;
        const { MongoClient } = require('mongodb');
        const uri = "mongodb://" + mongodbConnector.host_name + ":" + mongodbConnector.host_port;
        const new_login_name = mongodbConnector.new_login_name;
        const new_password = mongodbConnector.new_password;
        const client = new MongoClient(uri);
        try {
            await client.connect();
            await client.db().admin().addUser(
                new_login_name,
                new_password,
                [
                    { role: "read", db: "testdb" },
                ],
                { w: "majority", wtimeout: 5000 }
            );
        } catch (e) {
            console.error(e);
            returnMessage = constants.DEFAULT_MESSAGES.FAILURE;
        } finally {
            await client.close();
        }
        return returnMessage;
    }

    async deleteDBUser(host_name, host_port, login_name) {
        logger.logString("ConnectMongoDB:deleteDBUser:Will delete user: " + login_name + " against host: " + host_name);
        if (login_name == undefined || login_name == null) {
            logger.logErrorStringWithObj("ConnectMongoDB:deleteDBUser:login_name not valid...", login_name);
            return;
        }
        if (host_name == undefined || host_name == null) {
            logger.logErrorStringWithObj("ConnectMongoDB:deleteDBUser:hostname not valid...", host_name);
            return;
        }
        const { MongoClient } = require('mongodb');
        const uri = "mongodb://" + host_name + ":" + host_port;
        const client = new MongoClient(uri);
        try {
            await client.connect();
            await client.db().admin().removeUser(
                login_name,
                {
                    w: "majority", wtimeout: 5000
                }
            );
        } catch (e) {
            console.error(e);
        } finally {
            await client.close();
        }
    }
}

///////////////////////////////////////
module.exports = ConnectMongoDB;