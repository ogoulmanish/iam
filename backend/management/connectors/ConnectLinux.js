let LinuxConnector = require('../models/connectorModels/database/LinuxConnectorSchema');
let constants = require('../config/constants');
let logger = require('../libs/Logger');
let node_ssh = require('node-ssh');
let ssh = new node_ssh();

var default_host_name = "test.rebex.net";
var default_host_username = "demo";
var default_host_password = "password";

class ConnectLinux {
    constructor() { }

    async createUser(linuxConnector, userDetail) {
        logger.logStringWithObj("ConnectLinux:createUser:", linuxConnector);
        let returnMessage = constants.DEFAULT_MESSAGES.SUCCESS;
        try {
            logger.logString("Will create Linux User id and password for username:" + userDetail.username);
            ssh.connect({
                host: linuxConnector.host_name,
                username: linuxConnector.host_username,
                password: linuxConnector.host_password
            }).then(function () {
                // CREATE USER COMMAND
                ssh.execCommand('ls').then(function (result) {
                    console.log('STDOUT: ' + result.stdout);
                    console.log('STDERR: ' + result.stderr);
                })
            });
        } catch (error) {
            logger.logErrorStringWithObj("Error in Linux CreateUser", error);
            returnMessage = constants.DEFAULT_MESSAGES.FAILURE;
        }
        return returnMessage;
    }

    
    async deleteUser(linuxConnector, mappingUserEntity) {
        logger.logStringWithObj("ConnectLinux:deleteUser:", linuxConnector);
        let returnMessage = constants.DEFAULT_MESSAGES.SUCCESS;
        try {
            logger.logString("Will delete Linux User id and password for username:" + mappingUserEntity.username);
            ssh.connect({
                host: linuxConnector.host_name,
                username: linuxConnector.host_username,
                password: linuxConnector.host_password
            }).then(function () {
                // CREATE USER COMMAND
                ssh.execCommand('ls').then(function (result) {
                    console.log('STDOUT: ' + result.stdout);
                    console.log('STDERR: ' + result.stderr);
                })
            });
        } catch (error) {
            logger.logErrorStringWithObj("Error in Linux deleteUser", error);
            returnMessage = constants.DEFAULT_MESSAGES.FAILURE;
        }
        return returnMessage;
    }
}




///////////////////////////////////////
module.exports = ConnectLinux;