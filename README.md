Software Dependencies:-
------------------------------------------
(A) Angular (Currently version 8)  
(B) Mongodb  
(C) Nodejs (currently version )  
(D) Apache Directory Studio  
(E) Robo 3T - MongoDB Database Client (https://robomongo.org/download)  

Configure the project in the steps mentioned below:
-------------------------------------------------------------

(A) Angular
--------------------------------------------------
Go to frontend folder and do the following tasks:
1. If Angular is not installed then type the following in command prompt:  
npm install -g @angular/cli

2. Type the following command:  
npm install

3. Run the frontend server by typing:  
npm start

(B) MongoDB
--------------------------------------------------
1. Install MongoDB from the following site:  
https://docs.mongodb.com/manual/installation/
2. Create a "db" directory e.g. C:\data\db
3. Run the MongoDB server by typing the following command (present in the mongodb bin folder)  
"mongod --dbpath C:\data\db"
4. Execute the initialization script initMongodb.js (under docs folder) by typing the following command (present in the mongodb bin folder)  
mongo initMongodb.js
5. Install Robo 3T (an open source MongoDB Client) from   
https://robomongo.org/download


(C) NodeJS
--------------------------------------------------
1. Install nodejs from : https://nodejs.org/en/download/  
2. Now go to "backend" folder and type the following command  
npm install  
3. Create a new directory "images" under backend folder  
3. Then run the backend server by typing  
npm start


(D) Apache Directory Studio
--------------------------------------------------
1. Install this from the following website  
http://directory.apache.org/studio/downloads.html  
If you are totally new to this installation then please look into the apache_directory_screenshots.doc document present under the docs directory else you can follow the steps given below:  

2. Open Apache Directory Studio (LDAP - Active Directory) and start the LDAP Server
3. Create a new partition "dc=ogoul1,dc=com" by clicking on Open Configuration in LDAP Server that is running
4. Restart Server
5. Create a new LDAP connection with credentials:  
Connection Name : <any readable name>  
hostname: localhost  
port: 10389  
username:uid=admin,ou=system  
password:secret  

6. In the LDAP Browser, Under dc=ogoul1,dc=com, Create new Entry of type "OrganizationUnit" (ou) as "ou=developer,dc=ogoul1,dc=com" 
7. In the LDAP Browser, Under ou=developer,dc=ogoul1,dc=com, Create new Entry of type "inetOrgPerson" as cn=admin, Click Next and then use following attributes in the new entry  
sn = Admin  
givenname = Admin  
initials = A  
mail = admin@ogoul.com  
telephoneNumber = 1234567890  
userPassword = admin  
8. Click on "Finish"  


Start the product
--------------------------------------------------------
In order to start the product, please type the below URL in any browser  
http://localhost:4200 
